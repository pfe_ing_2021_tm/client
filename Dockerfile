# test server
FROM node:13.12.0-alpine

LABEL maintainer="tarek.mdemegh@chifco.com"

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
RUN npm install -f
RUN npm i babel-eslint@10.0.3
COPY . ./

RUN npm run build-dev

# Main command
CMD npm start
