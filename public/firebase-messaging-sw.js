importScripts("https://www.gstatic.com/firebasejs/7.5.2/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.5.2/firebase-messaging.js");
// importScripts('/__/firebase/7.5.0/firebase-app.js');
// importScripts('/__/firebase/7.5.0/firebase-messaging.js');
// // importScripts('/__/firebase/init.js');
// var firebase = require("firebase/app");
// import firebase from 'firebase/app';
// require("firebase/messaging");

var firebaseConfig = {
  apiKey: "AIzaSyBytRP4GbsKQ12F-Jgj7FOJmCdyhRDhKhc",
  authDomain: "discovered-stg.firebaseapp.com",
  databaseURL: "https://discovered-stg.firebaseio.com",
  projectId: "discovered-stg",
  storageBucket: "discovered-stg.appspot.com",
  messagingSenderId: "634973216209",
  appId: "1:634973216209:web:32e653402f9acce17a94f0",
  measurementId: "G-0D5S2L9HPM"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// const messaging = firebase.messaging();

// messaging.setBackgroundMessageHandler(function (payload) {
//     console.log('[firebase-messaging-sw.js] Received background message ', payload);
//     // Customize notification here
//     const notificationTitle = 'Background Message Title';
//     const notificationOptions = {
//         body: 'Background Message body.'
//     };

//     return self.registration.showNotification(notificationTitle,
//         notificationOptions);
// });

// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
// importScripts('https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js');
// importScripts('https://www.gstatic.com/firebasejs/7.5.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
// firebase.initializeApp({
//     'messagingSenderId': '634973216209'
// });

// // Retrieve an instance of Firebase Messaging so that it can handle background
// // messages.
// const messaging = firebase.messaging();

// importScripts("https://www.gstatic.com/firebasejs/7.3.0/firebase-app.js");
// importScripts("https://www.gstatic.com/firebasejs/7.3.0/firebase-messaging.js");

// let messaging = null;
// if (firebase.messaging.isSupported()) {
//     firebase.initializeApp({
//         // Project Settings => Add Firebase to your web app
//         messagingSenderId: "1062407524656",
//     });
//     console.log("Service Worker => Messaging is Supported");
//     // const messaging = firebase.messaging();
// }

firebase.messaging().setBackgroundMessageHandler(function (payload) {
  const promiseChain = clients
    .matchAll({
      type: "window",
      includeUncontrolled: true
    })
    .then(windowClients => {
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        windowClient.postMessage(payload);
      }
    })
    .then(() => {
      return registration.showNotification(playload.data.title);
    });
  return promiseChain;
});

self.addEventListener("push", function (event) {

  var jsonObj = event.data.json();
  // console.log('jsonObj', jsonObj);
  var title = jsonObj.data.title;
  var bodyn = jsonObj.data.body;
  var icons = jsonObj.data.icon;

  var notificationOptions = {
    body: bodyn,
    icon: icons,
    tag: title
  };
  if (self.registration.showNotification) {
    self.registration.showNotification(title, {
      body: bodyn,
      icon: icons
    });
    return;
  } else {
    new Notification(title, notificationOptions);
  }
});

self.addEventListener("notificationclick", event => {
  const clickedNotification = event.notification;
  clickedNotification.close();
  const promiseChain = clients
    .matchAll({
      type: "window",
      includeUncontrolled: true
    })
    .then(windowClients => {
      let matchingClient = null;
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        if (windowClient.url === feClickAction) {
          matchingClient = windowClient;
          break;
        }
      }
      if (matchingClient) {
        return matchingClient.focus();
      } else {
        return clients.openWindow(windowClient.url);
      }
    });
  event.waitUntil(promiseChain);
});
