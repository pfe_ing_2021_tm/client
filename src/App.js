/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-22T16:38:46+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-01T09:15:59+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React, { Component, Suspense, lazy, useEffect, useState } from "react";
import { Router, Route, Switch } from "react-router-dom";
import "./App.css";
import { history } from "./history";
import Loading from "./components/loading";
import Footer from "./components/footer";
import TagManager from "react-gtm-module";
import Challenge from "./components/popupChallenge";
import { createMuiTheme } from "@material-ui/core/styles"
import socketIOClient from "socket.io-client"

// import { theme } from "./theme"
import { MuiThemeProvider } from "@material-ui/core/styles"
import { useDispatch, useSelector } from "react-redux";
import { dataSettingsFromWSocket, getSettings } from "./store/actions";
 
const tagManagerArgs = {
  gtmId: process.env.REACT_APP_IDGTM,
};
const SendGifts = lazy(() => import("./pages/SendGifts"))
const Home = lazy(() => import("./pages/home"));
const Shopmap = lazy(() => import("./pages/shopMap"));
const Recipe = lazy(() => import("./pages/recipes"));
const Login = lazy(() => import("./pages/login"));
const Wheel = lazy(() => import("./pages/wheel"));
const VideoPlayer = lazy(() => import("./pages/videoPlayer"));
const EntryCode = lazy(() => import("./pages/entryCode"));
const QuizShot = lazy(() => import("./pages/quizShot"));
const QuizInfo = lazy(() => import("./pages/quizInfo"));
const ExtraVideo = lazy(() => import("./pages/extravideo"));
const Profile = lazy(() => import("./pages/profile"));
 
const Events = lazy(() => import("./pages/events"));
const EventDetails = lazy(() => import("./pages/eventDetails"));
const Contact = lazy(() => import("./pages/contact"));
const Survey = lazy(() => import("./pages/survey"));
const Signup = lazy(() => import("./pages/signup"))
const FooterComponent = lazy(() => import("./components/FooterComponent"));
const wheelTest = lazy(() => import("./pages/puzzleTarekTest/index"));
const PuzzelGame = lazy(() => import("./pages/puzzle"));
const NewsLetter = lazy(() => import("./pages/newsletter"));
const Consent_dsv  = lazy(() => import("./pages/consent-dsv"));
     var socket = socketIOClient(`${process.env.REACT_APP_DOMAIN}`)
 
 const App = () =>{
   const dispatch = useDispatch() ;
   const [color, setColor] = useState("#FFE600") ;
   
   const settings_ws = useSelector((state) => state.settings_ws.settings_ws)
   const settings = useSelector((state) => state.settings.settings)

   
   useEffect(()=>{

       socket.on("backgrounLoginSend", (data) => {
        
        dispatch(dataSettingsFromWSocket(data))
        //  this.setState({
        //    settings: data,
        //    bg: process.env.REACT_APP_DOMAIN + data.backgroundLogin,
        //  })

       })

   },[])
   useEffect(() => {
     dispatch(dataSettingsFromWSocket(settings))
   }, [settings])

   useEffect(()=>{
  dispatch( getSettings())
   },[])
  
     return (
       <MuiThemeProvider
         theme={createMuiTheme({
           palette: {
             primary: {
               main: settings_ws?.primaryColor
                 ? settings_ws?.primaryColor
                 : color,
             },
             secondary: {
               main: settings_ws?.secondaryColor
                 ? settings_ws?.secondaryColor
                 : "#fff",
             },
             black: { main: "#000" },
           },
           typography: {
             useNextVariants: true,
           },
         })}
       >
         <Suspense fallback={<Loading />}>
           {/* {challengeClick && <Challenge />} */}
           <Router history={history}>
             <Switch>
               <Route exact path="/" component={Login} />
               {/* <Route exact path="/" component={login} /> */}
               <Route exact path="/signup" component={Signup} />
               <Route exact path="/signup/:id" component={Signup} />
               <Route exact path="/home" component={Home} />
               {/* <Route exact path="/newsletter" component={NewsLetter} /> */}
               <Route exact path="/consent-dsv" component={Consent_dsv} />
               <Route path="/wheel" component={Wheel} />
               <Route path="/profile" component={Profile} />
               <Route exact path="/videoplayer/:id" component={VideoPlayer} />
               <Route
                 exact
                 path="/recipe/:idrecipe/videoplayer/:id"
                 component={VideoPlayer}
               />
               <Route path="/quizshot/:quizid" component={QuizShot} />
               <Route path="/quizinfo/:quizid" component={QuizInfo} />
               <Route path="/extrat/:extraid" component={ExtraVideo} />
               <Route exact path="/entrycode/:videoid" component={EntryCode} />
               <Route path="/recipe/:idrecipe" component={Recipe} />
               <Route
                 path="/shopmap"
                 component={
                   process.env.REACT_APP_SHOPMAP === "true" ? Shopmap : Home
                 }
               />

               <Route exact path="/sendGifts" component={SendGifts} />

               <Route
                 exact
                 path="/events"
                 component={
                   process.env.REACT_APP_EVENTS === "true" ? Events : Home
                 }
               />
               <Route
                 path="/events/:eventid"
                 component={
                   process.env.REACT_APP_EVENTS === "true" ? EventDetails : Home
                 }
               />
               <Route path="/contact" component={Contact} />
               <Route path="/survey/:surveyid" component={Survey} />
               <Route path="/wheelTest" component={wheelTest} />
               <Route path="/puzzleGame/:puzzleid" component={PuzzelGame} />
               <Route path="*" component={Home} />
             </Switch>
             {/* <FooterComponent /> */}
             {/* {installButton && (
               <Footer
                 condition={this.state.installButton}
                 onClick={this.installApp}
               />
             )} */}
           </Router>
         </Suspense>
       </MuiThemeProvider>
     )
 }
TagManager.initialize(tagManagerArgs);

export default App;
