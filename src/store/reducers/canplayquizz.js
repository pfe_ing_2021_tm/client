/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-12 11:20:27
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-24 11:04:56
 */
import { CAN_PLAY_QUIZZ, CAN_PLAY_QUIZZ_RESET } from '../actions/actionTypes';

const initialState = {
  canplayquizz: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CAN_PLAY_QUIZZ:
      return {
        ...state,
        canplayquizz: action.canplayquizz
      };
    case CAN_PLAY_QUIZZ_RESET:
      return {
        ...state,
        canplayquizz: action.canplayquizz
      };
    default:
      return state;
  }
};
export default reducer;
