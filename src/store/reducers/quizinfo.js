import {
    SET_INFOQUIZ_DATA,
    SET_ANSWERQUIZINFO_DATA
} from "../actions/actionTypes";

const initialState = {
    quizInfoResult: [],
    quizInfoAnswerResult: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_INFOQUIZ_DATA:
            return {
                ...state,
                quizInfoResult: action.quizInfoResult
            };
        case SET_ANSWERQUIZINFO_DATA:
            return {
                ...state,
                quizInfoAnswerResult: action.quizInfoAnswerResult
            };
        default:
            return state;
    }
};
export default reducer;