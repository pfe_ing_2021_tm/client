/*
 * @Author: Nessrine khedhiri
 * @Date: 2019-11-28 15:53:52
 * @Last Modified by: Nessrine khedhiri
 * @Last Modified time: 2019-11-28 15:54:26
 */
import { SET_USER_SURVEY } from "../actions/actionTypes";

const initialState = {
    userSurvey: []

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_SURVEY:
            return {
                ...state,
                userSurvey: action.userSurvey
            }

        default:
            return state;
    }
};
export default reducer;
