/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-13 12:32:34
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-13 13:49:31
 */
import { SET_POSITION } from '../actions/actionTypes';

const initialState = {
  position: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_POSITION:
      return {
        ...state,
        position: action.position
      };

    default:
      return state;
  }
};
export default reducer;
