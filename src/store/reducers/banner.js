/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-28 15:53:58
 * @Last Modified by:   Aymen ZAOUALI
 * @Last Modified time: 2019-10-28 15:53:58
 */
import { SET_BANNER_DATA } from "../actions/actionTypes";

const initialState = {
  bannerData: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_BANNER_DATA:
      return {
        ...state,
        bannerData: action.bannerData
      };

    default:
      return state;
  }
};
export default reducer;
