/*
 * Author: <> with ❤ tarek.mdemegh 
 * -----
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 * -----
 * Last Modified: Friday, 16th July 2021 11:43:03 am
 * Modified By: tarek.mdemegh
 * -----
 * Copyright , Chifco - 2021 
 */

import { LIST_SETTINGS, DATA_FROM_WD_SETTINGS } from "../actions/actionTypes"

const initialState = {
  settings: [],
  settings_ws:[], 
}

const reducer = (state = initialState, action) => {

  switch (action.type) {
    case LIST_SETTINGS:
      return {
        ...state,
        settings: action.settings,
      }
    case DATA_FROM_WD_SETTINGS:
      return {
        ...state,
        settings_ws: action.settings_ws,
        settings: action.settings,
      }

    default:
      return state
  }
};
export default reducer;
