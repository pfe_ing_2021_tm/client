/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-23 10:10:39
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-25 09:06:49
 */
import {
  SET_SHOTQUIZ_DATA,
  SET_ANSWERQUIZ_DATA,
  RESULT_CODE,
  SET_LIST_GAMES
} from "../actions/actionTypes";

const initialState = {
  quizShotResult: null,
  resultcode: null,
  listGames: [],
  quizAnswerResult: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SHOTQUIZ_DATA:
      return {
        ...state,
        quizShotResult: action.quizShotResult
      };
    case SET_ANSWERQUIZ_DATA:
      return {
        ...state,
        quizAnswerResult: action.quizAnswerResult
      };
    case RESULT_CODE:
      return {
        ...state,
        resultcode: action.resultcode
      };
    case SET_LIST_GAMES:
      return {
        ...state,
        listGames: action.listGames
      };
    default:
      return state;
  }
};
export default reducer;
