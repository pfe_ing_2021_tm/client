/*
 * Created Date: Wednesday October 30th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, October 31st 2019, 2:52:17 pm
 * Modified By: Amir Dorgham
 * -----
 */
import { SET_EVENTS, SET_EVENT_ID } from "../actions/actionTypes";

const initialState = {
  listEvents: [],
  eventInfo: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_EVENTS:
      return {
        ...state,
        listEvents: action.listEvents
      };
    case SET_EVENT_ID:
      return {
        ...state,
        eventInfo: action.eventInfo
      };
    default:
      return state;
  }
};
export default reducer;
