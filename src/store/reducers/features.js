/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-22 18:13:54
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-22 18:14:31
 */
import { SET_LIST_FEATURES } from "../actions/actionTypes";

const initialState = {
  features: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_FEATURES:
      return {
        ...state,
        features: action.features
      };

    default:
      return state;
  }
};
export default reducer;
