/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-13 12:32:34
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-13 13:49:31
 */
import { USER_REGISTER } from "../actions/actionTypes"

const initialState = {
  registerReducer: {},
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_REGISTER:
      return {
        ...state,
        registerReducer: action.registerReducer,
      }

    default:
      return state
  }
}
export default reducer
