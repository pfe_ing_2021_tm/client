import { PUZZLE_LIST, SET_ID_PUZZLE, GET_PIECES, GET_PUZZLE_RESULT, SET_PUZZLE_ID } from "../actions/actionTypes";
/*
 * @Author:NOUHA MBAREK
 * @Date: 2020/07/07 
 * @Last Modified by: WATHEK KAHLA
 * @Last Modified time: 2020/07/07 15:00:00
 */


const initialState = {
    userPuzzle: [],
    resultPuzzel: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case PUZZLE_LIST:
            return {
                ...state,
                PuzzleList: action.PuzzleList
            };
        case GET_PIECES:
            return {
                ...state,
                puzzlePieces: action.puzzlePieces
            };

        case SET_ID_PUZZLE:
            return {
                ...state,
                Puzzleid: action.Puzzleid
            };
        case GET_PUZZLE_RESULT:
            return {
                ...state,
                resultPuzzel: action.resultPuzzel
            };
        case SET_PUZZLE_ID:
            return {
                ...state,
                PuzzleId: action.PuzzleId,
                puzzleTable: action.puzzleTable

            };
        default:
            return state;
    }

};
export default reducer;
