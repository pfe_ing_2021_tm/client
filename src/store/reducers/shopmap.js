import {
  SET_SHOP_MAP,
  SHOPMAP_DATA,
  CHECKINLIMITE,
} from "../actions/actionTypes";

const initialState = {
  shopmapList: [],
  shopmapData: [],
  checkinLimite: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SHOP_MAP:
      return {
        ...state,
        shopmapList: action.shopmapList,
      };
    case CHECKINLIMITE:
      return {
        ...state,
        checkinLimite: action.checkinLimite,
      };
    case SHOPMAP_DATA:
      return {
        ...state,
        shopmapData: action.shopmapData,
      };
    default:
      return state;
  }
};
export default reducer;
