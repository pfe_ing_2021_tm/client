import { CAN_ADD_FCM, SET_FCM_TOKEN } from '../actions/actionTypes';

const initialState = {
    canaddfcm: [],
    setfcmtoken: null

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CAN_ADD_FCM:
            return {
                ...state,
                canaddfcm: action.canaddfcm
            };
        case SET_FCM_TOKEN:
            return {
                ...state,
                setfcmtoken: action.setfcmtoken
            };
        default:
            return state;
    }
};
export default reducer;

