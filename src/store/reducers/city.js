/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-08-19 14:52:42
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-08-22 11:23:04
 */

import { CITY_DATA, CITY_LIST } from '../actions/actionTypes';

const initialState = {
  cityData: [],
  cityList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CITY_DATA:
      return {
        ...state,
        cityData: action.cityData
      };
    case CITY_LIST:
      return {
        ...state,
        cityList: action.cityList
      };
    default:
      return state;
  }
};
export default reducer;
