import { GET_CHALLENGE, GET_CLASSEMENT_TOP10 } from "../actions/actionTypes";

const initialState = {
    challenge: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CHALLENGE:
            return {
                ...state,
                challenge: action.challenge
            };
        case GET_CLASSEMENT_TOP10:
            return {
                ...state,
                classement: action.classement
            };
        default:
            return state;
    }
};
export default reducer;
