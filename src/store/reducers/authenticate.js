/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-08-01 11:37:35
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-26 15:11:09
 */

import { USER_DATA } from '../actions/actionTypes';

const initialState = {
  status: null,
  userInfo: null,
  token: null,
  nickname: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        status: action.status,
        userInfo: action.userInfo,
        token: action.token,
        nickname: action.nickname
      };
    default:
      return state;
  }
};
export default reducer;
