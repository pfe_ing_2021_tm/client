import { SET_EXTRA_BYID } from "../actions/actionTypes";
const initialState = {
    videoExtra: ''
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_EXTRA_BYID:
            return {
                ...state,
                videoExtra: action.videoExtra
            };

        default:
            return state;
    }
};
export default reducer;