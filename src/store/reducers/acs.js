/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-06-26 10:26:43
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-26 12:34:49
 */

import { ACS_FAIL, USER_ACS, DETECT_ACS, DETECT_ACS_FAIL, DETECT_ACS_RESET } from "../actions/actionTypes";

const initialState = {
  registrationFailACS: [],
  registerToSf: [],
  detectionACS: [],
  detectionFailACS: [],
  acsStatus : ""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case DETECT_ACS_RESET:
      return {
        ...state,
        detectionFailACS: action.detectionFailACS,
        detectionACS: action.detectionACS,
      };
    case DETECT_ACS_FAIL:
      return {
        ...state,
        detectionFailACS: action.detectionFailACS,
      };
    case ACS_FAIL:
      return {
        ...state,
        registrationFailACS: action.registrationFailACS,
        acsStatus : action.status
      };
    case USER_ACS:
      return {
        ...state,
        registerToSf: action.registerToSf,
        acsStatus : action.status
      };
    case DETECT_ACS:
      return {
        ...state,
        detectionACS: action.detectionACS,
      };
    default:
      return state;
  }
};
export default reducer;
