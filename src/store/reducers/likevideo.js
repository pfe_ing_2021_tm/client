/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-24 11:19:15
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-24 11:20:38
 */
import { CAN_LIKE_VIDEO, DID_LIKE_VIDEO } from "../actions/actionTypes";

const initialState = {
  canlikevideo: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CAN_LIKE_VIDEO:
      return {
        ...state,
        canlikevideo: action.canlikevideo
      };
    case DID_LIKE_VIDEO:
      return {
        ...state,
        didlikevideo: action.didlikevideo
      };

    default:
      return state;
  }
};
export default reducer;
