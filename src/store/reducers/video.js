import { SET_VIDEO_BYID } from "../actions/actionTypes";
const initialState = {
  videoItem: null
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_VIDEO_BYID:
      return {
        ...state,
        videoItem: action.videoItem
      };

    default:
      return state;
  }
};
export default reducer;
