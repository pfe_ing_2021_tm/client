/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-23 18:57:07
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-24 13:54:59
 */
import {
  SET_LIST_RECIPES,
  SET_LIST_RECIPES_ID,
  SET_WATCH_VIDEO,
  RECIPES_PDF_BY_ID
} from "../actions/actionTypes";

const initialState = {
  listRecipes: [],
  infoRecipesid: [],
  watchVideo: [],
  pdfrecipeid:""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_RECIPES:
      return {
        ...state,
        listRecipes: action.listRecipes
      };
    case SET_LIST_RECIPES_ID:
      return {
        ...state,
        infoRecipesid: action.infoRecipesid
      };
    case SET_WATCH_VIDEO:
      return {
        ...state,
        watchVideo: action.watchVideo
      };
      case RECIPES_PDF_BY_ID:{
    return {
      ...state,
      pdfrecipeid:action.pdfrecipeid
    }
      };

    default:
      return state;
  }
 
};
export default reducer;
