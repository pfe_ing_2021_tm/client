import { BRAND_DETAIL , BRAND_LIST } from '../actions/actionTypes';

const initialState = {
  brandList: [],
  brandData: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case BRAND_LIST:
      return {
        ...state,
        brandList: action.brandList
      };
    case BRAND_DETAIL:
      return {
        ...state,
        brandData: action.brandData
      };  
    default:
      return state;
  }
};
export default reducer;