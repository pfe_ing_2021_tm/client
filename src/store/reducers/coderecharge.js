import { SET_CODE_RECHARGE, GET_VOUCHERS } from "../actions/actionTypes";
const initialState = {
  coderecharge: {}
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CODE_RECHARGE:
      return {
        ...state,
        coderecharge: action.coderecharge
      };
    case GET_VOUCHERS:
      return {
        ...state,
        vouchers: action.vouchers
      };

    default:
      return state;
  }
};
export default reducer;
