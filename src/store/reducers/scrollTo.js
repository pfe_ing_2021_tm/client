/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-17 12:42:11
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-17 12:42:46
 */
import { SCROLL_TO } from '../actions/actionTypes';

const initialState = {
  scrolltosection: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SCROLL_TO:
      return {
        ...state,
        scrolltosection: action.scrolltosection
      };

    default:
      return state;
  }
};
export default reducer;
