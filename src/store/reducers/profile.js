/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-09 11:18:49
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-28 09:39:05
 */

import {
  SET_USER_INFO,
  SET_USER_CHECKIN,
  SET_USER_AWARDS,
  SET_RESPONSE_UPDATE_USER,

} from "../actions/actionTypes";

const initialState = {
  userInfoData: null,
  userCheckIn: null,
  userAwards: null,
  responseUpdateUser: null,

};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_INFO:
      return {
        ...state,
        userInfoData: action.userInfo
      };
    case SET_USER_CHECKIN:
      return {
        ...state,
        userCheckIn: action.userCheckIn
      };
    case SET_USER_AWARDS:
      return {
        ...state,
        userAwards: action.userAwards
      };
    case SET_RESPONSE_UPDATE_USER:
      return {
        ...state,
        responseUpdateUser: action.responseUpdateUser
      };

    default:
      return state;
  }
};

export default reducer;
