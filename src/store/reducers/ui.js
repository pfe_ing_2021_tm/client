/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 11:47:26
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-24 11:47:24
 */

import { UI_START_LOADING, UI_STOP_LOADING } from "../actions/actionTypes";
const initialState = {
  isLoading: false
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UI_START_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case UI_STOP_LOADING:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};
export default reducer;
