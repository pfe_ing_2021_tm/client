/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-28 15:53:52
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-28 15:54:26
 */
import { SET_USER_BADGES, BADGE_LIST } from "../actions/actionTypes";

const initialState = {
  userBadge: [],
  userRedsChallenge: [],
  Bbadges: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_BADGES:
      return {
        ...state,
        userBadge: action.userBadge,
        userRedsChallenge: action.userRedsChallenge,
      }
    case BADGE_LIST:
      return {
        ...state,
        Bbadges: action.Bbadges
      };

    default:
      return state;
  }
};
export default reducer;
