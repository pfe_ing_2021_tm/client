
import { POST_CONTACT, SEND_GIFTS } from "../actions/actionTypes"

const initialState = {
  contact: [],
  sendGifts:null,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
      case POST_CONTACT:
        return {
          ...state,
          contact: action.contact,
        }

      case SEND_GIFTS:
        return {
          ...state,
          sendGifts: action.sendGifts,
        }
      default:
        return state
    }
};
export default reducer;
