import { SET_GIFT_VALUE, SET_GIFT_WIN, SET_USER_INFO_REDS } from '../actions/actionTypes';

const initialState = {
  wheelgift: [],
  giftWin: null,
  userInfoDataReds: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_GIFT_VALUE:
      return {
        ...state,
        wheelgift: action.wheelgift
      };
    case SET_GIFT_WIN:
      return {
        ...state,
        giftWin: action.giftWin
      };
    case SET_USER_INFO_REDS:
      return {
        ...state,
        userInfoReds: action.userInfo
      };
    default:
      return state;
  }
};
export default reducer;
