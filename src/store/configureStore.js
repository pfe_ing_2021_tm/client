/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 10:24:49
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-26 12:35:03
 */

import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { refreshTokenMiddleware } from "./middleware/refreshToken";
import isLoading from "./reducers/ui";
import userInfo from "./reducers/authenticate";
import token from "./reducers/authenticate";
import nickname from "./reducers/authenticate";
import status from "./reducers/authenticate";
import cityData from "./reducers/city";
import cityList from "./reducers/city";
import resultcode from "./reducers/games";
import canplayquizz from "./reducers/canplayquizz";
import position from "./reducers/position";
import scrolltosection from "./reducers/scrollTo";
import canlikevideo from "./reducers/likevideo";
import listRecipes from "./reducers/recipes";
import events from "./reducers/events";
import infoRecipesid from "./reducers/recipes";
import watchVideo from "./reducers/recipes";
import userInfoReds from "./reducers/wheel";
import wheelgift from "./reducers/wheel";
import giftWin from "./reducers/wheel";
import shopmapList from "./reducers/shopmap";
import shopmapData from "./reducers/shopmap";
import checkinLimite from "./reducers/shopmap";
import quizShotResult from "./reducers/games";
import userInfoData from "./reducers/profile";
import userCheckIn from "./reducers/profile";
import bannerData from "./reducers/banner";
import userAwards from "./reducers/profile";
import quizAnswerResult from "./reducers/games";
import features from "./reducers/features";
import listGames from "./reducers/games";
import videoItem from "./reducers/video";
import videoExtra from "./reducers/extra";
import responseUpdateUser from "./reducers/profile";
import userBadge from "./reducers/badges";
import Bbadges from "./reducers/badges";
import canaddfcm from "./reducers/fcm";
import setfcmtoken from "./reducers/fcm";
import userSurvey from "./reducers/survey";
import coderecharge from "./reducers/coderecharge";
import vouchers from "./reducers/coderecharge";
import contact from "./reducers/contact";
import sendGifts from "./reducers/contact"
import challenge from "./reducers/challenge";
import classement from "./reducers/challenge";
import quizInfoResult from "./reducers/quizinfo";
import quizInfoAnswerResult from "./reducers/quizinfo";
import registrationFailACS from "./reducers/acs";
import acsStatus from "./reducers/acs";

import registerToSf from "./reducers/acs";
import detectionACS from "./reducers/acs";
import detectionFailACS from "./reducers/acs";
import brandList from "./reducers/brand";
import brandData from "./reducers/brand";

import PuzzleList from "./reducers/puzzles";
import puzzlePieces from "./reducers/puzzles";
import resultPuzzel from "./reducers/puzzles";
import registerReducer from "./reducers/register"
import settings_ws from "./reducers/settings"
import settings from "./reducers/settings"

const rootReducer = combineReducers({
  brandList,
  brandData,

  PuzzleList,
  puzzlePieces,
  resultPuzzel,

  ui: isLoading,
  User: userInfo,
  token,
  nickname,
  status,
  cityData,
  cityList,
  resultcode,
  canplayquizz,
  position,
  scrolltosection,
  canlikevideo,
  listRecipes,
  infoRecipesid,
  events,
  Wheel: wheelgift,
  Gift: giftWin,
  shopmapList,
  shopmapData,
  QuizShot: quizShotResult,
  userInfoData,
  userCheckIn,
  bannerData,
  userAwards,
  quizAnswerResult,
  features,
  listGames,
  videoItem,
  responseUpdateUser,
  userBadge,
  Bbadges,
  userInfoReds,
  videoExtra,
  canaddfcm,
  setfcmtoken,
  userSurvey,
  watchVideo,
  coderecharge,
  vouchers,
  contact,
  checkinLimite,
  challenge,
  classement,
  quizInfoResult,
  quizInfoAnswerResult,
  registrationFailACS,
  acsStatus,
  registerToSf,
  detectionACS,
  detectionFailACS,

  registerReducer,

  settings_ws,
  settings,
  sendGifts,
})

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["ui"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers = (process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

export const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(refreshTokenMiddleware, thunk))
);

export const persistor = persistStore(store);
