/** @format */

import jwt from "jsonwebtoken";
import moment from "moment";
import { history } from "../../history";
import axios from "axios";
//array for last actions
const arraction = [];

export const refreshTokenMiddleware = (store) => (next) => (action) => {
  //TODO check action type LOGIN
  if (localStorage.getItem("refreshtoken") === null) {
    next(action);
  } else {
    const token = localStorage.getItem("token");
    //push last action before err
    arraction.push(action);
    jwt.verify(token, "nodeRestApi", function (err, decoded) {
      if (!err && moment().unix() < decoded.exp) {
        next(action);
      } else {
        //if token expired fetch to refresh token
        refreshtoken(action, next);
      }
    });
  }

  //fetch to refresh token api
  async function refreshtoken(action, next) {
    axios.defaults.headers.common = {
      "Content-Type": "*/*",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
      Accept: "application/json",
      "x-access-token": localStorage.getItem("token"),
    };
    let body = {
      login: localStorage.getItem("login"),
      refreshtoken: localStorage.getItem("refreshtoken"),
    };

    axios
      .post(
        `${process.env.REACT_APP_DOMAIN}/api/v1/customer/refreshtoken`,
        body,
      )
      .then(function (response) {
        console.log(
          "%cMyProject%cline:47%cresponse",
          "color:#fff;background:#ee6f57;padding:3px;border-radius:2px",
          "color:#fff;background:#1f3c88;padding:3px;border-radius:2px",
          "color:#fff;background:rgb(56, 13, 49);padding:3px;border-radius:2px",
          response,
        );
      })
      .catch(async function (error) {
        if (error.response.status == 401) {
          await localStorage.removeItem("login");
          await localStorage.removeItem("refreshtoken");
          await localStorage.removeItem("fcmtoken");
          await localStorage.removeItem("_id");
          await localStorage.removeItem("uid");
          await localStorage.removeItem("token");
          console.log("LOGIN");
          window.location.reload();
          history.push("/");
        }
      });
    //////////////////////////// REFRESH TOKEN NOT WORKING //////////////////////////////

    // const settings = {
    //   method: "POST",

    //   body: JSON.stringify({
    //     login: localStorage.getItem("login"),
    //     refreshtoken: localStorage.getItem("refreshtoken"),
    //   }),
    // };
    // let response = await fetch(
    //   `${process.env.REACT_APP_DOMAIN}/api/v1/customer/refreshtoken`,
    //   settings,
    // );
    // let data = await response.json();

    // if (data.message === "verify refresh token") {
    //   //if refresh token fail
    //   //await localStorage.clear();
    //   await localStorage.removeItem("login");
    //   await localStorage.removeItem("refreshtoken");
    //   await localStorage.removeItem("fcmtoken");
    //   await localStorage.removeItem("_id");
    //   await localStorage.removeItem("uid");
    //   await localStorage.removeItem("token");
    //   await history.push("/");
    // } else if (data.status === "error") {
    //   //await localStorage.clear();
    //   await localStorage.removeItem("login");
    //   await localStorage.removeItem("refreshtoken");
    //   await localStorage.removeItem("fcmtoken");
    //   await localStorage.removeItem("_id");
    //   await localStorage.removeItem("uid");
    //   await localStorage.removeItem("token");
    //   if (data.data === "token_error") {
    //     history.push("/");
    //     window.location.reload();
    //   } else {
    //     await history.push("/home");
    //   }
    // } else {
    //   await setItemInLocalStorage(data.data.token, action, next);
    // }
  }

  //Update token in localstorage
  async function setItemInLocalStorage(value, action, next) {
    try {
      //set token in localstorage
      await localStorage.setItem("token", value);
      //dispatch last action after refresh token
      await next(action);
    } catch (err) {}
  }
};
