import { POST_CONTACT, SEND_GIFTS } from "./actionTypes"
import { uiStartLoading, uiStopLoading } from "./index";
import { getUserInfo } from "./profile";

export const sendContact = data => {
    return dispatch => {
        dispatch(uiStartLoading());
        fetch(`${process.env.REACT_APP_DOMAIN}/v1/contact/add`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("token")
            },
            body: JSON.stringify({
                data
            })
        })
            .catch(err => {
                dispatch(uiStopLoading());
            })
            .then(parsedRes => {
                if (parsedRes.status === 200) {
                    dispatch(uiStopLoading());
                    parsedRes.json().then(data => dispatch(contactData(data)));
                }
            });
    };
};

export const contactData = value => {
    return {
        type: POST_CONTACT,
        contact: value
    };
};

export const sendGifts = (data) => {
  return (dispatch) => {
    dispatch(uiStartLoading())
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/sendGift`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token"),
      },
      body: JSON.stringify({
        data,
      }),
    })
      .catch((err) => {
        dispatch(uiStopLoading())
      })
      .then((parsedRes) => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading())
          parsedRes.json().then((data) =>{ dispatch(setsendGifts(data) ) ; dispatch(getUserInfo() ) ;  dispatch(uiStopLoading())})
        }
      })
  }
}

export const setsendGifts = (value) => {
  return {
    type: SEND_GIFTS,
    sendGifts: value,
  }
}

