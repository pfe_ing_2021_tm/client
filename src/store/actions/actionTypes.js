/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 10:28:52
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-26 10:25:50
 */

export const UI_START_LOADING = "UI_START_LOADING";
export const UI_STOP_LOADING = "UI_STOP_LOADING";
export const USER_DATA = "USER_DATA";
export const CITY_DATA = "CITY_DATA";
export const SET_USER_BADGES = "SET_USER_BADGES";
export const CITY_LIST = "CITY_LIST";
export const RESULT_CODE = "RESULT_CODE";
export const CAN_PLAY_QUIZZ = "CAN_PLAY_QUIZZ";
export const CAN_PLAY_QUIZZ_RESET = "CAN_PLAY_QUIZZ_RESET";
export const SET_POSITION = "SET_POSITION";
export const SCROLL_TO = "SCROLL_TO";
export const SET_LIST_RECIPES = "SET_LIST_RECIPES";
export const SET_LIST_RECIPES_ID = "SET_LIST_RECIPES_ID";
export const SET_WATCH_VIDEO = "SET_WATCH_VIDEO";
export const CAN_LIKE_VIDEO = "CAN_LIKE_VIDEO";
export const DID_LIKE_VIDEO = "DID_LIKE_VIDEO";
export const SET_GIFT_VALUE = "SET_GIFT_VALUE";
export const SET_GIFT_WIN = "SET_GIFT_WIN";
export const SET_SHOP_MAP = "SET_SHOP_MAP";
export const SHOPMAP_DATA = "SHOPMAP_DATA";
export const SET_SHOTQUIZ_DATA = "SET_SHOTQUIZ_DATA";
export const SET_INFOQUIZ_DATA = "SET_INFOQUIZ_DATA";
export const SET_ANSWERQUIZINFO_DATA = "SET_ANSWERQUIZINFO_DATA";
export const SET_USER_INFO = "SET_USER_INFO";
export const SET_USER_CHECKIN = "SET_USER_CHECKIN";
export const SET_BANNER_DATA = "SET_BANNER_DATA";
export const SET_USER_AWARDS = "SET_USER_AWARDS";
export const SET_ANSWERQUIZ_DATA = "SET_ANSWERQUIZ_DATA";
export const SET_LIST_FEATURES = "SET_LIST_FEATURES";
export const SET_LIST_GAMES = "SET_LIST_GAMES";
export const SET_VIDEO_BYID = "SET_VIDEO_BYID";
export const SET_RESPONSE_UPDATE_USER = "SET_RESPONSE_UPDATE_USER";
export const SET_EVENTS = "SET_EVENTS";
export const SET_EVENT_ID = "SET_EVENT_ID";
export const BADGE_LIST = "BADGE_LIST";
export const CAN_ADD_FCM = "CAN_ADD_FCM";
export const SET_FCM_TOKEN = "SET_FCM_TOKEN";
export const SET_USER_INFO_REDS = "SET_USER_INFO_REDS";
export const SET_EXTRA_BYID = "SET_EXTRA_BYID";
export const SET_USER_SURVEY = "SET_USER_SURVEY";
export const RECIPES_PDF_BY_ID = "RECIPES_PDF_BY_ID";
export const SET_CODE_RECHARGE = "SET_CODE_RECHARGE";
export const GET_VOUCHERS = "GET_VOUCHERS";
export const POST_CONTACT = "POST_CONTACT";
export const CHECKINLIMITE = "CHECKINLIMITE";
export const GET_CHALLENGE = "GET_CHALLENGE";
export const GET_CLASSEMENT_TOP10 = "GET_CLASSEMENT_TOP10";
export const USER_ACS = "USER_ACS";
export const ACS_FAIL = "ACS_FAIL";
export const DETECT_ACS = "DETECT_ACS";
export const DETECT_ACS_FAIL = "DETECT_ACS_FAIL";
export const DETECT_ACS_RESET = "DETECT_ACS_RESET";
//action puzzle
export const PUZZLE_LIST = "PUZZLE_LIST";
export const SET_ID_PUZZLE = "SET_ID_PUZZLE";
export const GET_PIECES = "GET_PIECES";
export const SET_PUZZLE_ID = "SET_PUZZLE_ID";
export const GET_PUZZLE_RESULT = "GET_PUZZLE_RESULT";


export const BRAND_LIST = "BRAND_LIST";
export const BRAND_DETAIL = "BRAND_DETAIL";

export const USER_REGISTER = "USER_REGISTER"

export const DATA_FROM_WD_SETTINGS = "DATA_FROM_WD_SETTINGS"
export const LIST_SETTINGS = "LIST_SETTINGS"

export const SEND_GIFTS = "SEND_GIFTS"



