/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-16 16:47:02
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-17 16:35:43
 */

import { animateScroll as scroll } from 'react-scroll';
import { SCROLL_TO } from './actionTypes';

export const scrollTo = value => {
  //alert(value);
  return async dispatch => {
    // await dispatch(scrollToAndOpenSection(null));
    scroll.scrollTo(750);
    dispatch(scrollToAndOpenSection(value.toString()));
  };
};

export const scrollToAndOpenSection = value => {
  return {
    type: SCROLL_TO,
    scrolltosection: value
  };
};
