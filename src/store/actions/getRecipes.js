/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-23 18:49:14
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-03-13 11:32:55
 */
import {
  SET_LIST_RECIPES,
  SET_LIST_RECIPES_ID,
  SET_WATCH_VIDEO,
  RECIPES_PDF_BY_ID
} from "./actionTypes";
import { uiStopLoading, uiStartLoading } from "./index";
import { decode } from "../cryptage/index";
export const getRecipes = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/recipes`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);
            dispatch(listRecipes(value));
          });
        }
      });
  };
};

export const getRecipesbyid = recipeid => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(
      `${process.env.REACT_APP_DOMAIN}/api/v1/recipe/info?recipeid=${recipeid}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": localStorage.getItem("token")
        }
      }
    )
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(infoRecipesid(data.data)));
        }
      });
  };
};
export const getRecipesbyidPdf = recipeid => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/v1/recipe/downloadpdf/${recipeid}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(pdfrecipeid(data)));
        }
      });
  };
};

export const Recipeslikedislike = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/recipe/like/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        recipeid: data.recipeid,
        status: data.status
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
        }
      });
  };
};
export const RecipesPdf = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/recipe/download/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        recipeid: data.recipeid,
        status: data.status
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
        }
      });
  };
};

export const RecipeWatchVideo = data => {
  if (localStorage.getItem("city")) data.idcity = localStorage.getItem("city"); else data.idcity = null
  if (localStorage.getItem("brand")) data.brand = localStorage.getItem("brand"); else data.brand = null
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/recipe/watch`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify(data)
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(data => {
        dispatch(watchVideo(data));
      });
  };
};

export const watchVideo = data => {
  return {
    type: SET_WATCH_VIDEO,
    watchVideo: data
  };
};

const listRecipes = value => {
  return {
    type: SET_LIST_RECIPES,
    listRecipes: value
  };
};
const infoRecipesid = recipeid => {
  return {
    type: SET_LIST_RECIPES_ID,
    infoRecipesid: recipeid
  };
};
export const pdfrecipeid = value => {
  return {
    type: RECIPES_PDF_BY_ID,
    pdfrecipeid: value
  };
};
