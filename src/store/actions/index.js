/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 10:28:42
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-26 12:45:30
 */
export { uiStartLoading, uiStopLoading } from "./ui";

export { tryLogin, userData } from "./authenticate";
export { tryRegisterNickname, register, resetRegistredino } from "./register"
export { getActiveCity, getCityList } from "./city";
export {
  setCodeResult,
  setResultCode,
  getQuizShotById,
  setAnswerQuizShot,
  getListGames,
  quizShotResult,
  quizAnswerResult,
} from "./games";

export { dataSettingsFromWSocket, getSettings } from "./settings"
export {
  getCanPlayQuizz,
  canplayquizzreset,
  getListOfQuiz,
} from "./canplayquizz";
export { setPosition, getCurrentLocation } from "./position";
export { scrollTo, scrollToAndOpenSection } from "./scrollTo";
export { getCanLikeVideo, setDidLikeVideo } from "./likevideo";
export {
  getRecipes,
  getRecipesbyid,
  Recipeslikedislike,
  RecipeWatchVideo,
  RecipesPdf,
  getRecipesbyidPdf,
} from "./getRecipes";
export { getEvents, getEventbyid, EventView } from "./getEvents";
export { getExtrabyid } from "./getExtrat";
export { addFCM, findFCM, setfcmtoken } from "./fcm";

export { getGifts, getGiftsWin } from "./wheel";
export {
  getShop,
  getShopChecks,
  setCheckInProduct,
  setCheckinLimite,
} from "./shopmap";
export {
  getUserBadges,
  setUserBadgesStatus,
  setUserChallengesStatus,
  getbadgelist,
  setUserBadges,
} from "./badges";
export {
  getUserInfo,
  updateAvatar,
  updatePseudo,
  getUserCheckIn,
  setResponseUpdate,
  getUserAwards,
} from "./profile";
export { getBanner } from "./banner";
export { getFeatures } from "./features";
export { setvideohistory } from "./history";
export { getVideoById, setVideoItem } from "./video";
export { setUserSurvey, getUserSurvey, setUserSurveyAnswer } from "./survey";

export { getCodeRecharge } from "./coderecharge";
export { getVouchers } from "./coderecharge";
export { sendContact, sendGifts } from "./contact"
export { getChallenge, getClassement } from "./challenge";
export { getQuizInfoById, setAnswerQuizInfo } from "./quizinfo";

export { registerToSF, registerToSFReset, registerToSFResetSucc, acsDetect, acsDetectReset } from "./acs";
export { getBrandList, getActiveBrand } from "./brand";
export {
  getPuzzlelist,
  setPuzzleById,
  setPuzzleResultById,
  GetPuzzleResult,
} from "./puzzle";
