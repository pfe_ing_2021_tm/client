/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-28 15:49:18
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-29 10:47:58
 */

import { SET_USER_BADGES, BADGE_LIST } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";

export const getUserBadges = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/customer/getuserbadges`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        // console.log(parsedRes);
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes
            .json()
            .then(data => dispatch(setUserBadges(data.data)));
        }
      });
  };
};

export const setUserBadgesStatus = badgeid => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(
      `${process.env.REACT_APP_DOMAIN}/api/v1/customer/setuserbadgesstatus`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": localStorage.getItem("token")
        },
        body: JSON.stringify({
          badgeid,
          login: localStorage.getItem("login")
        })
      }
    )
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        // console.log(parsedRes);
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => { });
        }
      });
  };
};


export const setUserChallengesStatus = challenge => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(
      `${process.env.REACT_APP_DOMAIN}/api/v1/customer/setuserchallengestatus`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": localStorage.getItem("token")
        },
        body: JSON.stringify({
          challengeid: challenge.id,
          reds: challenge.reds,
          login: localStorage.getItem("login")
        })
      }
    )
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        // console.log(parsedRes);
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => { });
        }
      });
  };
};

export const getbadgelist = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(
      `${process.env.REACT_APP_DOMAIN}/api/v1/customer/getuserbadgesmiles`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "x-access-token": localStorage.getItem("token")
        }
      }
    )
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(Bbadges(data.data)));
        }
      });
  };
};

export const Bbadges = value => {
  return {
    type: BADGE_LIST,
    Bbadges: value
  };
};
export const setUserBadges = value => {
  return {
    type: SET_USER_BADGES,
    userBadge: value.badgesToShow,
    userRedsChallenge: value.RedsToShow
  };
};
