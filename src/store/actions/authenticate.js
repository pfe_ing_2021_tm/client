/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 10:28:49
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-11 10:43:15
 */
import { USER_DATA } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
import { history } from "../../history";

export const tryLogin = (data) => {
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/customer/login`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        login: data.email,
        password: data.password,
      }),
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((res) => res.json())
      .then((parsedRes) => {
        dispatch(uiStopLoading());
        if (parsedRes.status === "success") {
          localStorage.setItem("token", parsedRes.data.token);
          localStorage.setItem("uid", parsedRes.data.uid);
          localStorage.setItem("refreshtoken", parsedRes.data.refreshtoken);
          localStorage.setItem("login", parsedRes.data.user.mobilephone);
          localStorage.setItem("_id", parsedRes.data.user._id);
          // history.push("/home");
          if (parsedRes.data.user.pseudo === undefined) {
            //to do dispatche nickname and change input to add nickname

            dispatch(
              userData(
                parsedRes.status,
                null,
                null,
                parsedRes.data.suggested_pseudo
              )
            );
          } else {
            dispatch(
              userData(
                parsedRes.status,
                parsedRes.data.user,
                parsedRes.data.token,
                parsedRes.data.suggested_pseudo
              )
            );
            history.push("/home");
          }
        } else if (
          parsedRes.status === "error" ||
          parsedRes.status === "errorLogin" ||
          parsedRes.status === ""
        ) {
          dispatch(userData(parsedRes.status, null, null, null));
        }
      });
  };
};
export const userData = (status, value, token, name) => {
  return {
    type: USER_DATA,
    status: status,
    userInfo: value,
    token: token,
    nickname: name,
  };
};
