import { uiStartLoading, uiStopLoading } from "./index";

export const setvideohistory = video => {
  let data = {
    login: localStorage.getItem("login"),
    idvideo: video.id,
    spenttime: video.timespent,
    type: video.type
  };
  if (localStorage.getItem("city")) data.idcity = localStorage.getItem("city"); else data.idcity = null
  if (localStorage.getItem("brand")) data.brand = localStorage.getItem("brand"); else data.brand = null
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/videohistory`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify(data)
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
        }
      });
  };
};
export const setshopmapViewistory = item => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(
      `${process.env.REACT_APP_DOMAIN}/api/v1/shopmap/viewshopmap/history`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": localStorage.getItem("token")
        },
        body: JSON.stringify({
          login: localStorage.getItem("login"),
          shopmapId: item._id,
          shopmapCode: item.code,
          checkinHistory: item.checkinHistory
        })
      }
    )
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
        }
      });
  };
};
