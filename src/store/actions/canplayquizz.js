/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-11 16:39:54
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-02-13 11:55:08
 */
import { CAN_PLAY_QUIZZ, CAN_PLAY_QUIZZ_RESET } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";

export const getCanPlayQuizz = idvideo => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/canplayquizz`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        idvideo: idvideo,
        idcustomer: localStorage.getItem("_id")
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(canplayquizz(data.message)));
        }
      });
  };
};

export const getListOfQuiz = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/getListOfQuiz`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        //idvideo: data,
        idcustomer: localStorage.getItem("_id")
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          //dispatch(getlistofquiz(data)
          //parsedRes.json().then(data => console.log("data"));
        }
      });
  };
};

export const canplayquizz = data => {
  return {
    type: CAN_PLAY_QUIZZ,
    canplayquizz: data
  };
};

export const canplayquizzreset = () => {
  return {
    type: CAN_PLAY_QUIZZ_RESET,
    canplayquizz: []
  };
};
