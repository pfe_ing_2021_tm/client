import { uiStartLoading, uiStopLoading } from "./index";
import { CAN_ADD_FCM, SET_FCM_TOKEN } from "./actionTypes";

export const addFCM = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/fcm/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        fcmtoken: localStorage.getItem("fcmtoken"),
        customerid: localStorage.getItem("_id"),
        uid: localStorage.getItem("uid")
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          //parsedRes.json().then(data => console.log("fcm add message"));
        }
      });
  };
};

export const findFCM = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/fcm/findbytoken`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        fcmtoken: localStorage.getItem("fcmtoken")
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());

          parsedRes.json().then(data =>
            //console.log('data', data.data),

            dispatch(canaddfcm(data.data))
          );
        }
      });
  };
};

export const canaddfcm = data => {
  return {
    type: CAN_ADD_FCM,
    canaddfcm: data
  };
};

export const setfcmtoken = data => {
  return {
    type: SET_FCM_TOKEN,
    setfcmtoken: data
  };
};
