/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 13:07:31
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-16 10:59:42
 */

import { uiStartLoading, uiStopLoading } from "./index";
import { history } from "../../history";
import { userData } from "./index";
import { store } from "../configureStore";
import { USER_REGISTER } from "./actionTypes"
export const tryRegisterNickname = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/customer/setpseudo`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json", 
        "x-access-token": localStorage.getItem("token") 
      },
      body: JSON.stringify({ 
        login: data.login, 
        pseudo: data.pseudo
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(res => res.json())
      .then(parsedRes => {
        dispatch(uiStopLoading());
        if (parsedRes.status === "success") {
          dispatch(
            userData(
              parsedRes.status,
              store.getState().User.userInfo,
              store.getState().token.token,
              store.getState().nickname.nickname
            )
          );
          history.push("./home");
        } else if (parsedRes.errors !== "duplicate") {
          //to do dispatche nikename
          dispatch(
            userData(
              parsedRes.message,
              store.getState().User.userInfo,
              store.getState().token.token,
              store.getState().nickname.nickname
            )
          );
        } else if (parsedRes.errors === "duplicate") {
          dispatch(
            userData(
              parsedRes.message,
              store.getState().User.userInfo,
              store.getState().token.token,
              store.getState().nickname.nickname
            )
          );
        }
      });
  };
};
export const register = (data) => {
  return (dispatch) => {
    dispatch(uiStartLoading())
    fetch(`${process.env.REACT_APP_DOMAIN}/v1/cusotmer/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        //"x-access-token": localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .catch((err) => {
        dispatch(uiStopLoading())
      })
      .then((parsedRes) => {
        dispatch(uiStopLoading())
        if (parsedRes.status === 200) {
          parsedRes.json().then(async (data) => {
            dispatch(userRegister(data))
          })
        }
      })
  }
}
export const userRegister = (value, status) => {
  return {
    type: USER_REGISTER,
    registerReducer: value,
    status: status,
  }
}

export const resetRegistredino = () => {
  return {
    type: USER_REGISTER,
    registerReducer: null,
  }
}