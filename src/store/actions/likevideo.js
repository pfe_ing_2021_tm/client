/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-19 17:29:24
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-16 10:59:06
 */
import { CAN_LIKE_VIDEO } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";

export const getCanLikeVideo = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/canlikevideo`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        idcustomer: data.idcustomer,
        idvideo: data.idvideo
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(canlikevideo(data)));
        }
      });
  };
};

export const setDidLikeVideo = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/didlikevideo`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        idcustomer: data.idcustomer,
        idvideo: data.idvideo,
        status: data.status
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then();
        }
      });
  };
};

const canlikevideo = value => {
  return {
    type: CAN_LIKE_VIDEO,
    canlikevideo: value
  };
};
