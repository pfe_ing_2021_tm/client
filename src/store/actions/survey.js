/*
 * @Author: Nessrine KHEDHIRI
 * @Date: 2019-10-28 15:49:18
 * @Last Modified by: Nessrine KHEDHIRI
 * @Last Modified time: 2019-10-29 10:47:58
 */

import { SET_USER_SURVEY } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";

export const getUserSurvey = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/survey/customer`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(setUserSurvey(data.data)));
        }
      });
  };
};
export const setUserSurveyAnswer = (surveyid, data) => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/survey/answers/${surveyid}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        // question: data.question,
        // questionAnswers: data.questionAnswers //array of object
        data
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => { });
        }
      });
  };
};

export const setUserSurvey = value => {
  return {
    type: SET_USER_SURVEY,
    userSurvey: value
  };
};
