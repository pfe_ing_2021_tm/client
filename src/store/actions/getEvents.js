/*
 * Created Date: Wednesday October 30th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Tuesday, November 5th 2019, 3:41:10 pm
 * Modified By: Amir Dorgham
 * -----
 */
import { SET_EVENTS, SET_EVENT_ID } from "./actionTypes";
import { uiStopLoading, uiStartLoading } from "./index";

export const getEvents = filterObject => {
  return dispatch => {
    dispatch(uiStartLoading());
    let filter = {};
    filter.page = filterObject.page;
    if (filterObject.city !== "") {
      filter.city = filterObject.city;
    }
    if (filterObject.from !== "") {
      filter.from = filterObject.from;
    }
    if (filterObject.categorie !== "") {
      filter.categorie = filterObject.categorie;
    }

    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/events`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify(filter)
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(listEvents(data.data)));
        }
      });
  };
};

export const getEventbyid = eventid => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(
      `${process.env.REACT_APP_DOMAIN}/api/v1/events/info?eventid=${eventid}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": localStorage.getItem("token")
        }
      }
    )
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(infoEvent(data.data)));
        }
      });
  };
};
export const EventView = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/event/view/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        eventid: data.eventid,
        status: data.status
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
        }
      });
  };
};
const listEvents = value => {
  return {
    type: SET_EVENTS,
    listEvents: value
  };
};
const infoEvent = info => {
  return {
    type: SET_EVENT_ID,
    eventInfo: info
  };
};
