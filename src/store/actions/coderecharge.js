/*
 * @Author: Tarek.Mdimagh
 * @Date: 2020-01-31 12:02:43
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-02-05 14:25:56
 */
import { SET_CODE_RECHARGE, GET_VOUCHERS } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";

export const getCodeRecharge = data => {
  return dispatch => {
    dispatch(uiStartLoading());

    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/getcoderecharge`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        login: localStorage.getItem("login"),
        idGiftWin: data.idGiftWin,
        operateur: data.operateur,
        typevoucher: data.typevoucher,
        points: data.points
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        //if (parsedRes.status === 200) {
        dispatch(uiStopLoading());
        parsedRes.json().then(data => dispatch(setCodeRecharge(data.data)));
        //}
      });
  };
};
export const setCodeRecharge = data => {
  return {
    type: SET_CODE_RECHARGE,
    coderecharge: data
  };
};

export const getVouchers = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    //console.log("data : ", data)
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/vouchers`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        points: data
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        //if (parsedRes.status === 200) {
        dispatch(uiStopLoading());
        parsedRes.json().then(data => dispatch(setVouchers(data.data)));
        //}
      });
  };
};
export const setVouchers = data => {
  return {
    type: GET_VOUCHERS,
    vouchers: data
  };
};
