/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-09 11:22:05
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-02-24 09:53:51
 */
import {
  SET_USER_INFO,
  SET_USER_CHECKIN,
  SET_USER_AWARDS,
  SET_RESPONSE_UPDATE_USER
  // SET_GIFT_WIN_REDS
} from "./actionTypes";

export const getUserInfo = () => {
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/cusotmer/profile`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {})
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          parsedRes.json().then(data => dispatch(setUserInfo(data.data)));
        }
      });
  };
};

export const updateAvatar = avatar => {
  return dispatch => {
    dispatch(setUserInfo(null));
    const formData = new FormData();
    formData.append("image", avatar);
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/customer/image/update`, {
      method: "POST",
      headers: {
        "x-access-token": localStorage.getItem("token")
      },
      body: formData
    })
      .catch(err => {})
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          parsedRes.json().then(async data => {
            dispatch(setUserInfo(null));
            dispatch(getUserInfo());
          });
        }
      });
  };
};

export const updatePseudo = pseudo => {
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/customer/pseudo/update`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({ pseudo })
    })
      .catch(err => {})
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          parsedRes.json().then(async data => {
            dispatch(setUserInfo(null));
            dispatch(getUserInfo());
            dispatch(setResponseUpdate(data.data));
          });
        }
      });
  };
};

export const getUserCheckIn = numPage => {
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/cusotmer/checkins`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        page: numPage
      })
    })
      .catch(err => {})
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          parsedRes.json().then(data => dispatch(setUserCheckIn(data.data)));
        }
      });
  };
};

export const getUserAwards = numPage => {
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/cusotmer/awards`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        page: numPage
      })
    })
      .catch(err => {})
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          parsedRes.json().then(data => dispatch(setUserAwards(data.data)));
        }
      });
  };
};
export const setUserInfo = value => {
  return {
    type: SET_USER_INFO,
    userInfo: value
  };
};

export const setUserCheckIn = value => {
  return {
    type: SET_USER_CHECKIN,
    userCheckIn: value
  };
};

export const setUserAwards = value => {
  return {
    type: SET_USER_AWARDS,
    userAwards: value
  };
};

export const setResponseUpdate = response => {
  return {
    type: SET_RESPONSE_UPDATE_USER,
    responseUpdateUser: response
  };
};
