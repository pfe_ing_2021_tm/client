
import { PUZZLE_LIST, SET_ID_PUZZLE, GET_PIECES, SET_PUZZLE_ID, GET_PUZZLE_RESULT } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
/*
 * @Author:NOUHA MBAREK
 * @Date: 2020/07/07 
 * @Last Modified by: WATHEK KAHLA
 * @Last Modified time: 2020/07/07 15:00:00
 */

export const setPuzzleById = Puzzleid => {
    return dispatch => {
        dispatch(setPuzzleId(Puzzleid));
        fetch(
            `${process.env.REACT_APP_DOMAIN}/api/v1/findPuzzelById/${Puzzleid}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": localStorage.getItem("token")
                }
            }
        ).catch(err => {
            dispatch(uiStopLoading());
        })
            .then(parsedRes => {
                if (parsedRes.status === 200) {
                    dispatch(uiStopLoading());
                    parsedRes.json().then(data => dispatch(GetPuzzlePieces(data.data)));

                }
            });
    };
};

export const setPuzzleResultById = (puzzelid, answerCustomer,videoid) => {
    let sendData = {
        puzzelid:puzzelid,
        answerCustomer:answerCustomer,
        videoid:videoid
    }
    if(localStorage.getItem("city")) sendData.idcity = localStorage.getItem("city");
    if(localStorage.getItem("brand")) sendData.brand = localStorage.getItem("brand");
    return dispatch => {
        //dispatch(setPuzzleResultId(puzzelid, answerCustomer));
        fetch(
            `${process.env.REACT_APP_DOMAIN}/api/v1/puzzel/checkforAnswer`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": localStorage.getItem("token")
                },
                body: JSON.stringify(sendData)
            }
        ).catch(err => {
            console.log('Error : ', err);
            dispatch(uiStopLoading());
        })
            .then(parsedRes => {

                if (parsedRes.status === 200) {
                    dispatch(uiStopLoading());
                    parsedRes.json().then(data => dispatch(GetPuzzleResult(data)));

                }
            });
    };
};

export const setPuzzleResultId = (puzzelid, puzzleTable) => {
    return {
        type: SET_PUZZLE_ID,
        PuzzleId: puzzelid,
        puzzleTable: puzzleTable
    }

}

export const GetPuzzleResult = data => {
    return {
        type: GET_PUZZLE_RESULT,
        resultPuzzel: data
    }

}
export const setPuzzleId = value => {
    return {
        type: SET_ID_PUZZLE,
        Puzzleid: value
    };
};

//get details of badge by id
export const getPuzzlelist = () => {
    return dispatch => {
        dispatch(uiStartLoading());
        fetch(
            `${process.env.REACT_APP_DOMAIN}/api/v1/availablePuzzels`,
            {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "x-access-token": localStorage.getItem("token")
                }
            }
        )
            .catch(err => {
                dispatch(uiStopLoading());
            })
            .then(parsedRes => {
                if (parsedRes.status === 200) {
                    dispatch(uiStopLoading());
                    parsedRes.json().then(data => dispatch(Puzzles(data.data)));
                }
            });
    };
};

export const Puzzles = value => {
    return {
        type: PUZZLE_LIST,
        PuzzleList: value
    };
};

export const GetPuzzlePieces = value => {
    return {
        type: GET_PIECES,
        puzzlePieces: value
    };

};
