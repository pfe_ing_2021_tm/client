
import {
    SET_INFOQUIZ_DATA,
    SET_ANSWERQUIZINFO_DATA,
} from "./actionTypes";
import { history } from "../../history";
import { uiStartLoading, uiStopLoading } from "./index";
//quiz inf
export const getQuizInfoById = quizzid => {
    return dispatch => {
        dispatch(uiStartLoading());
        fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/quizinfo/getbyid`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("token")
            },
            body: JSON.stringify({
                quizzid
            })
        })
            .catch(err => { dispatch(uiStopLoading()); })
            .then(parsedRes => {
                dispatch(uiStopLoading());
                if (parsedRes.status === 200) {
                    parsedRes.json().then(async data => {
                        if (data.status === "error") {
                            history.push("/home");
                        } else {
                            dispatch(quizInfoResult(data.data));
                        }
                    });
                }
            });
    };
};

export const quizInfoResult = value => {
    return {
        type: SET_INFOQUIZ_DATA,
        quizInfoResult: value
    };
};

export const setAnswerQuizInfo = data => {
    return dispatch => {
        fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/quizInfo/answer`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("token")
            },
            body: JSON.stringify({
                quizzid: data.id,
                answer: data.reponse
            })
        })
            .catch(err => { })
            .then(parsedRes => {
                if (parsedRes.status === 200) {
                    parsedRes
                        .json()
                        .then(data => dispatch(quizAnswerResult(data)));
                }
            });
    };
};

export const quizAnswerResult = value => {
    return {
        type: SET_ANSWERQUIZINFO_DATA,
        quizInfoAnswerResult: value
    };
};