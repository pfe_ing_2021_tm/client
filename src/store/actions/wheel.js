/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-26 11:59:11
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-03-13 10:41:37
 */
import {
  SET_GIFT_VALUE,
  SET_GIFT_WIN,
  SET_USER_INFO_REDS
} from "./actionTypes";
import { decode } from "../cryptage/index";
export const getGifts = () => {
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/gifts`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {})
      .then(res => {
        if (res.status === 200) {
          res.json().then(async function(data) {
            if (data.data !== "token_error") {
              //resolve(data.data);
              let C = await decode(JSON.stringify(data.data));
              var value = JSON.parse(C);
              data.data = value;

              dispatch(wheelGift(data));
            }
          });
        }
      });
  };
};

export const getGiftsWin = () => {
  //const crypto = require('crypto');
  //const sessionId = crypto.createHash('md5').update(localStorage.getItem("token")).digest('hex');
  return dispatch => {
    dispatch(giftWin(null));
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/gift/win`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {})
      .then(res => {
        if (res.status === 200) {
          res.json().then(function(data) {
            if (data.data !== "token_error") {
              //resolve(data.data);
              dispatch(
                giftWin(data)

                // && dispatch(setUserInforReds(data))
              );
            }
          });
        }
      });
  };
};

// export const getUserInforeds = () => {
//   return dispatch => {
//     fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/cusotmer/profile`, {
//       method: "GET",
//       headers: {
//         "Content-Type": "application/json",
//         "x-access-token": localStorage.getItem("token")
//       }
//     })
//       .catch(err => { })
//       .then(parsedRes => {
//         if (parsedRes.status === 200) {
//           parsedRes.json().then(data => dispatch(setUserInfo(data.data)));
//         }
//       });
//   };
// };

const wheelGift = value => {
  return {
    type: SET_GIFT_VALUE,
    wheelgift: value
  };
};

const giftWin = value => {
  return {
    type: SET_GIFT_WIN,
    giftWin: value
  };
};

export const setUserInforReds = value => {
  return {
    type: SET_USER_INFO_REDS,
    userInfo: value
  };
};
