/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-06-11 10:38:36
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-26 16:09:46
 */
import { USER_ACS, ACS_FAIL, DETECT_ACS, DETECT_ACS_FAIL, DETECT_ACS_RESET } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
// age detect 

export const acsDetect = (data) => {
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/acs`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        //"x-access-token": localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        dispatch(uiStopLoading());
        if (parsedRes.status === 200) {
          parsedRes.json().then(async (data) => {
            if (data.message) {
              if (data.message.status_code === "FAILED" && data.message.error_code === "TooYoung") {

                dispatch(detectionFailACS(data.message.error_message));
              } else if (data.message.status_code === "FAILED" && data.message.error_code === "ImageNotRecognized") {
                dispatch(detectionFailACS("Image Not Recognized"))
              }
              else if (data.message.status_code === "SUCCESS") {
                dispatch(detectionACS(data.message.status_code));
              }
            }
          });
        }
      });
  };
};

export const detectionACS = (value) => {
  return {
    type: DETECT_ACS,
    detectionACS: value,
  };
};

export const detectionFailACS = (value) => {
  return {
    type: DETECT_ACS_FAIL,
    detectionFailACS: value,
  };
};

export const acsDetectReset = (value) => {
  return {
    type: DETECT_ACS_RESET,
    detectionFailACS: value,
    detectionACS: value,
  };
};

//
export const registerToSF = (data) => {
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/register-acs`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        //"x-access-token": localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        dispatch(uiStopLoading());
        if (parsedRes.status === 200) {
          parsedRes.json().then(async (data) => {
            if (data.message) {
              if (data.message.Error) {
                if (data.message.Error[0]) {
                  //console.log("data.message.Error[0] " , data.message.Error[0])
                  dispatch(registrationFailACS(data.message.Error[0].message , data.status ));
                }
              } else if (data.message.status === "Success") {
                dispatch(userRegisterToSF(data.message.status , data.status));
              }
            }
          });
        }
      });
  };
};

export const registerToSFResetSucc = (value) => {
  return {
    type: USER_ACS,
    registerToSf: value,
    status : ""
  };
};
export const registerToSFReset = (value) => {
  return {
    type: ACS_FAIL,
    registrationFailACS: value,
    status : ""
  };
};
export const registrationFailACS = (value , status ) => {
  return {
    type: ACS_FAIL,
    registrationFailACS: value,
    status : status
  };
};

export const userRegisterToSF = (value , status) => {
  return {
    type: USER_ACS,
    registerToSf: value,
    status : status
  };
};
