/**
 * @author Wathek KAHLA
 * @email wathek.kahla@chifco.com
 * @create date 2020-10-16 11:14:56
 * @modify date 2020-10-16 11:40:10
 * @desc [description]
 */

import { BRAND_LIST, BRAND_DETAIL } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
import { decode } from "../cryptage/index";

export const getBrandList = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/brand/enabledbrands`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(async parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());

          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);
            dispatch(brandList(value));
          });
        }
      });
  };
};

export const brandList = value => {
  return {
    type: BRAND_LIST,
    brandList: value
  };
};

export const getActiveBrand = id => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/brand/details`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        id: id
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);
            if(typeof value === "string"){
              localStorage.removeItem("brand");
              dispatch(brandData({}));
              window.location.reload()
            }else{
              dispatch(brandData(value));
            }
          });
        }
      });
  };
};

export const brandData = value => {
  return {
    type: BRAND_DETAIL,
    brandData: value
  };
};