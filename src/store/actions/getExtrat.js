import { SET_EXTRA_BYID } from "./actionTypes";
import { uiStopLoading, uiStartLoading } from "./index";
import { decode } from "../cryptage/index";
export const getExtrabyid = extraid => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/getbyid/${extraid}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);
            dispatch(infoExtra(value));
          });
        }
      });
  };
};

const infoExtra = data => {
  return {
    type: SET_EXTRA_BYID,
    videoExtra: data
  };
};
