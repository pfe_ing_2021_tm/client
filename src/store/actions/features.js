/** @format */

import { SET_LIST_FEATURES } from "./actionTypes";
import { uiStopLoading, uiStartLoading } from "./index";
import { history } from "../../history";
export const getFeatures = () => {
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/features`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token"),
      },
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then((data) => dispatch(setFeatures(data.data)));
        }
        if (parsedRes.status === 401) {
          history.push("/");
          // window.location.reload();
        }
      });
  };
};

const setFeatures = (value) => {
  return {
    type: SET_LIST_FEATURES,
    features: value,
  };
};
