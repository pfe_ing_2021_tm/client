/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Hayfa MRAD
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.²
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Mon Apr 29 2019
 */

import { 
  LIST_SETTINGS,
  DATA_FROM_WD_SETTINGS,
} from "./actionTypes"
import {
  
  SUCCESS_UPDATE_CITY
} from "./actionTypes"; 
import axios from "axios";
// const token = localStorage.getItem("token");






 
export const getSettings = (data) => {
  const config = {
    headers: {
      enctype: "multipart/form-data",
      "x-access-token": localStorage.getItem("token"),
    },
  }
  return (dispatch) => {
    axios
      .get(process.env.REACT_APP_DOMAIN + `/v1/getSettings`, config)
      .then(function (response) {
        dispatch(setListSettings(response.data.data))
      })
      .catch(function (error) {
        console.log(error)
      })
  }
}
export const setListSettings = (value) => {
  return {
    type: LIST_SETTINGS,
    settings: value,
  }
}

 

export const dataSettingsFromWSocket = (value) => {
 
  return {
    type: DATA_FROM_WD_SETTINGS,
    settings_ws: value,
    settings:value,
  }
}

