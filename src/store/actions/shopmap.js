/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-02 16:50:59
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-03-06 17:38:30
 */

import { SET_SHOP_MAP, SHOPMAP_DATA, CHECKINLIMITE } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";

export const getShop = (position) => {
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/shopmap/all`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        latitude: position.latitude,
        longitude: position.longitude
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(res => res.json())
      .then(parsedRes => {
        dispatch(uiStopLoading());
        if (parsedRes.status === "success") {
          //history.push('./home');

          dispatch(setShopMap(parsedRes.data)); //webservice result
        }
      });
  };
};

export const getShopChecks = shopmapid => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/shopmapproduct/findchecks`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        shopmap: shopmapid
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(res => res.json())
      .then(parsedRes => {
        dispatch(uiStopLoading());
        if (parsedRes.status === "success") {
          //history.push('./home');

          dispatch(shopmapData(parsedRes.data)); //webservice result
        }
      });
  };
};

export const setShopMap = value => {
  return {
    type: SET_SHOP_MAP,
    shopmapList: value
  };
};

export const shopmapData = value => {
  return {
    type: SHOPMAP_DATA,
    shopmapData: value
  };
};

export const setCheckInProduct = data => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/shopmapproduct/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        shopmap: data.shopmap,
        products: data.products,
        customer: data.customer
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          //dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(setCheckinLimite(data.data)));
        }
      });
  };
};

export const setCheckinLimite = value => {
  return {
    type: CHECKINLIMITE,
    checkinLimite: value
  };
};
