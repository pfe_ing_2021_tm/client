/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 11:47:37
 * @Last Modified by:   Aymen ZAOUALI
 * @Last Modified time: 2019-07-30 11:47:37
 */

import { UI_START_LOADING, UI_STOP_LOADING } from './actionTypes';

export const uiStartLoading = () => {
  return {
    type: UI_START_LOADING
  };
};
export const uiStopLoading = () => {
  return {
    type: UI_STOP_LOADING
  };
};
