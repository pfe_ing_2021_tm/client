/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-22 18:28:21
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-03-13 11:48:38
 */
import {
  RESULT_CODE,
  SET_SHOTQUIZ_DATA,
  SET_ANSWERQUIZ_DATA,
  SET_LIST_GAMES
} from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
import { decode } from "../cryptage/index";
//List games
export const getListGames = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/quiz`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        customerid: localStorage.getItem("_id")
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(setListGames(data.data)));
        }
      });
  };
};

//Code challenge
export const setCodeResult = data => {
  data.login = localStorage.getItem("login");
  if (localStorage.getItem("city")) data.idcity = localStorage.getItem("city"); else data.idcity = null
  if (localStorage.getItem("brand")) data.brand = localStorage.getItem("brand"); else data.brand = null
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/coderesult`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify(data)
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(data => dispatch(setResultCode(data.message)));
        }
      });
  };
};

//quiz shot
export const getQuizShotById = quizzid => {
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/quiz/getbyid`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        quizzid
      })
    })
      .catch(err => { })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);
            dispatch(quizShotResult(value));
          });
        }
      });
  };
};

export const setAnswerQuizShot = (questionid, answer, idvideo, idcity) => {
  let data = { questionid: questionid, answer: answer, idvideo: idvideo, customerid: localStorage.getItem("_id") };
  if (localStorage.getItem("city")) data.idcity = localStorage.getItem("city"); else data.idcity = null
  if (localStorage.getItem("brand")) data.brand = localStorage.getItem("brand"); else data.brand = null
  return dispatch => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/quiz/answer`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify(data)
    })
      .catch(err => { })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          parsedRes
            .json()
            .then(data => dispatch(quizAnswerResult(data.message)));
        }
      });
  };
};

export const quizShotResult = value => {
  return {
    type: SET_SHOTQUIZ_DATA,
    quizShotResult: value
  };
};

export const quizAnswerResult = value => {
  return {
    type: SET_ANSWERQUIZ_DATA,
    quizAnswerResult: value
  };
};

export const setResultCode = data => {
  return {
    type: RESULT_CODE,
    resultcode: data
  };
};

export const setListGames = data => {
  return {
    type: SET_LIST_GAMES,
    listGames: data
  };
};
