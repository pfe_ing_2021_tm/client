import { GET_CHALLENGE, GET_CLASSEMENT_TOP10 } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";

export const getChallenge = data => {
    return dispatch => {
        dispatch(uiStartLoading());
        fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/get/challenge`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("token")
            }
        })
            .catch(err => {
                dispatch(uiStopLoading());
            })
            .then(parsedRes => {
                if (parsedRes.status === 200) {
                    dispatch(uiStopLoading());
                    parsedRes.json().then(data => {
                        dispatch(challengeData(data))
                        if (data.data.length > 0) {
                            dispatch(getClassement(data.data[0].id))
                        }
                    });
                }
            });
    };
};

export const getClassement = data => {
    return dispatch => {
        dispatch(uiStartLoading());
        fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/get/getClassementTop10`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("token")
            },
            body: JSON.stringify({
                challenge: data
            })
        })
            .catch(err => {
                dispatch(uiStopLoading());
            })
            .then(parsedRes => {
                if (parsedRes.status === 200) {
                    dispatch(uiStopLoading());
                    parsedRes.json().then(data => dispatch(classementData(data)));
                }
            });
    };
};

export const challengeData = value => {
    if (value.data.length > 0) {
        if (value.data[0].id !== localStorage.getItem("challenge")) {
            localStorage.setItem("challenge", value.data[0].id);
            localStorage.removeItem("clicked");
        } else {
            localStorage.setItem("challenge", value.data[0].id);
        }
    } else {
        localStorage.removeItem("challenge");
        localStorage.removeItem("clicked");
    }
    return {
        type: GET_CHALLENGE,
        challenge: value
    };
};

export const classementData = value => {
    return {
        type: GET_CLASSEMENT_TOP10,
        classement: value
    };
};