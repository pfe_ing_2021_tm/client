/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-13 12:30:44
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-09-19 17:27:59
 */
import { SET_POSITION } from './actionTypes';
import { uiStopLoading, uiStartLoading } from "./index";
//import { geolocated } from 'react-geolocated';

export const getCurrentLocation = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    navigator.geolocation.getCurrentPosition(
      lastPosition => {
        dispatch(setPosition(lastPosition.coords));
        dispatch(uiStopLoading());
      },
      error => console.log(JSON.stringify(error), dispatch(uiStopLoading())),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 0 }
    );

  };
};

export const setPosition = value => {
  return {
    type: SET_POSITION,
    position: value
  };
};
