/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-08-19 14:44:21
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-03-13 14:41:48
 */
import { CITY_DATA, CITY_LIST } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
import { decode } from "../cryptage/index";
export const getActiveCity = cityid => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/details`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      },
      body: JSON.stringify({
        id: cityid
      })
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);
            dispatch(cityData(value));
          });
        }
      });
  };
};

export const getCityList = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/city/enabledcities`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(async parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());

          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);

            dispatch(cityList(value));
          });
        }
      });
  };
};

export const cityData = value => {
  return {
    type: CITY_DATA,
    cityData: value
  };
};

export const cityList = value => {
  return {
    type: CITY_LIST,
    cityList: value
  };
};
