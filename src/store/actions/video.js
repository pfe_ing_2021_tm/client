/** @format */

import { SET_VIDEO_BYID } from "./actionTypes";
import { history } from "../../history";
import { decode } from "../cryptage/index";

export const getVideoById = (id) => {
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/video/getbyid/${id}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "x-access-token": localStorage.getItem("token"),
      },
    })
      .catch((err) => {})
      .then(async (res) => {
        if (res.status === 200) {
          res.json().then(async function (data) {
            if (data.data !== "token_error") {
              let C = await decode(JSON.stringify(data.data));
              var value = JSON.parse(C);
              dispatch(setVideoItem(value));
            }
          });
        } else {
          await localStorage.removeItem("login");
          await localStorage.removeItem("refreshtoken");
          await localStorage.removeItem("fcmtoken");
          await localStorage.removeItem("_id");
          await localStorage.removeItem("uid");
          await localStorage.removeItem("token");
          console.log("LOGIN");
          window.location.reload();
          return history.push("/");
        }
      });
  };
};

export const setVideoItem = (data) => {
  return {
    type: SET_VIDEO_BYID,
    videoItem: data,
  };
};
