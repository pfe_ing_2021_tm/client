import { SET_BANNER_DATA } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
import { decode } from "../cryptage/index";
export const getBanner = () => {
  return dispatch => {
    dispatch(uiStartLoading());
    fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/slider/all`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": localStorage.getItem("token")
      }
    })
      .catch(err => {
        dispatch(uiStopLoading());
      })
      .then(parsedRes => {
        if (parsedRes.status === 200) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async data => {
            let C = await decode(JSON.stringify(data.data));
            var value = JSON.parse(C);
            dispatch(setBannerData(value));
          });
        }
      });
  };
};

export const setBannerData = value => {
  return {
    type: SET_BANNER_DATA,
    bannerData: value
  };
};
