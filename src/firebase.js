/*
 * Author: <> with ❤ tarek.mdemegh 
 * -----
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 * -----
 * Last Modified: Friday, 10th September 2021 12:53:57 am
 * Modified By: tarek.mdemegh
 * -----
 * Copyright , Chifco - 2021 
 */

import firebase from "firebase"