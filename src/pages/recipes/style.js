/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-06-13T15:39:02+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-18T12:07:03+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import backgroundImg from "../../assets/img/background.jpg";
import bgrecipe from "../../assets/img/bgrecipe.png";

export const styles = theme => ({
  background: {
    background: `url(${backgroundImg})`,
    width: "100%",
    position: "relative",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundAttachment: "fixed"
  },
  likeDislike: {
    position: "fixed",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    bottom: "40%",
    //right: "0%",
    [theme.breakpoints.between("sm", "sm")]: {
      //ipod ( 600px => 950px)
      right: 0
    }
  },
  tabsTitle: {
    fontFamily: "NeulandGroteskCondensedBold",
    fontSize: "3.5vw",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25
    }
  },
  root: {
    width: "100%"
  },
  dialogPaper: {
    overflow: "hidden"
  },
  sliderContainer: {
    width: "100%",
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "center",
    alignContent: "center"
  },
  sliderPoster: {
    width: "80%",
    marginBottom: -15
  },
  videoDescripCont: {
    display: "flex",
    justifyContent: "space-between",
    width: "70%",
    alignItems: "center",
    position: "absolute"
  },

  videoTitle: {
    color: "#fff",
    fontWeight: "500",
    textTransform: "uppercase"
  },
  videoAuthor: {
    color: "#fff"
  },
  playVideoAuthor: {
    color: "#999",
    fontSize: 13
  },
  iconPlay: {
    width: 35,
    color: "#fff"
  },
  btnNext: {
    display: "flex",
    flexDirection: "row",
    marginTop: 25,
    color: "#fff",
    justifyContent: "flex-end",
    width: "90%"
  },
  titleNext: {
    color: "#fff"
  },
  iconNext: {
    fontSize: 20,
    color: "#C70013",
    marginLeft: 10
  },
  bannerContainer: {
    width: "100%",
    justifyContent: "center",
    display: "flex",
    marginTop: 25
  },
  bannerItem: {
    display: "flex",
    flexDirection: "column"
  },
  bannerIndicator: {
    width: 10,
    margin: "2px 5px"
  },
  bannerCarousel: {
    display: "flex",
    width: "80%"
  },
  playlistDescripCont: {
    display: "flex",
    justifyContent: "space-between",
    width: "90%",
    alignItems: "center",
    position: "absolute",
    paddingBottom: 15,
    zIndex: 99
  },
  playListTitle: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "500",
    textTransform: "uppercase"
  },
  playVideoTitle: {
    color: "#fff",
    fontWeight: "500",
    textTransform: "uppercase"
  },
  iconPlayAudio: {
    width: 30,
    color: "#fff"
  },
  iconOff: {
    width: 20,
    color: "#fff"
  },
  drivWhi: {
    position: "relative",
    top: -2,
    fontSize: 10,
    color: "#fff"
  },
  galerieSmallItem: {
    height: 100,
    marginBottom: 8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  progressBarCont: {
    background: "rgba(000, 000, 000, 0.8)",
    height: 35,
    padding: "0 0px 40px 0px",
    position: "absolute",
    bottom: 0,
    width: "100%"
  },
  progressLine: {
    width: "100%",
    height: 3,
    position: "relative",
    top: -17
  },
  videoProgreesTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    width: " 40%",
    height: " 116%"
  },
  videoProgressBoxDesc: {
    display: "flex",
    alignItems: "flex-start"
  },
  progressImg: {
    width: 50,
    position: "relative",
    top: -8,
    left: 10
  },
  progressIcn: {
    color: "#fff",
    position: "relative",
    top: -10
  },
  videoCloseCont: {
    position: "absolute",
    top: 10,
    // background: "#000",
    borderRadius: 0,
    right: 0,
    padding: "5px 7px",
    zIndex: 1
  },
  papereffect: {
    height: 7,
    width: "97%",
    background: "rgba(255,255,255,0.8)",
    top: -11,
    position: "relative",
    padding: 5,
    borderRadius: 4,
    marginLeft: 0
  },
  paperbackground: {
    background: `url(${bgrecipe})`,
    margin: "0 -15px",
    position: "relative",
    padding: "0 15px",
    backgroundColor: "#fff",
    backgroundPosition: "bottom",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat"
  },
  listcount: {
    color: "#ed2a1c",
    fontSize: 30,
    padding: " 0 10px",
    fontWeight: 500,
    fontFamily: "NeulandGroteskCondensedBold"
  },
  videoCloseIcn: {
    color: "#fff",
    border: "1px solid #fff",
    borderRadius: 50,
    fontSize: 16
  },
  videoBack: {
    color: "#ed2a1c",
    position: "absolute",
    top: 15,
    marginLeft: 15,
    borderRadius: 50,
    fontSize: 16,
    zIndex: 999
  },
  btndowpdf: {
    marginBottom: 15,
    marginTop: 15,
    background: "#ed2a1c",
    color: "#fff",
    textTransform: "uppercase",
    padding: "12px 15px",
    borderRadius: 15
  },
  paperprops: { background: "#000" }
});
