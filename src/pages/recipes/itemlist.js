/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-06-13T15:39:02+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-18T12:05:54+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './style';
import Typography from '@material-ui/core/Typography';
import Details from './details';
import IcnPlay from '../../assets/img/playaudio.png';
import Icnoff from '../../assets/img/off.png';
