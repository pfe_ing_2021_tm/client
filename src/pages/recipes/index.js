/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-06-17T14:57:06+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-18T12:10:09+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./style";

import Icon from "@material-ui/core/Icon";
import SwipeableViews from "react-swipeable-views";
import { history } from "../../history";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
//import VideoPlayer from "./videoPlayer";
import dislike_grey from "../../assets/img/DISLIKE_grey.png";
import dislike from "../../assets/img/DISLIKE.png";
import Like_grey from "../../assets/img/LIKE_grey.png";
import like from "../../assets/img/like1.png";
import VideoPlayer from "../videoPlayer";
import {
  getRecipesbyid,
  Recipeslikedislike,
  RecipesPdf,
  getRecipesbyidPdf
} from "../../store/actions";

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

class Recipe extends React.Component {
  state = {
    open: false,
    openimagelike: false,
    openimagedislike: false,
    value: 0,
    index: 0,
    openvideo: false,
    ingredients: [],
    preparation: [],
    linkimage: "",
    videoimage: "",
    name: "",
    linkpdf: ""
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };
  componentDidUpdate(prevProps) {
    if (prevProps.infoRecipesid !== this.props.infoRecipesid) {
      if (this.props.infoRecipesid.like === null) {
        this.setState({
          openimagelike: false,
          openimagedislike: false
        });
      }
    }
  }
  componentDidMount() {
    this.props.getRecipesbyid(this.props.match.params.idrecipe);
    this.props.getRecipesbyidPdf(this.props.match.params.idrecipe);
    this.props.RecipesPdf(this.props.match.params.idrecipe);
  }

  componentWillReceiveProps(prevProps) {
    if (prevProps.infoRecipesid !== this.props.infoRecipesid) {
      // console.log(prevProps.infoRecipesid)
      this.setState({
        ingredients: prevProps.infoRecipesid.ingredients,
        preparation: prevProps.infoRecipesid.preparation,
        linkimage: prevProps.infoRecipesid.image,
        videoimage: prevProps.infoRecipesid.videorecetteid ? prevProps.infoRecipesid.videorecetteid.link : "",
        name: prevProps.infoRecipesid.name,
        // linkpdf: prevProps.infoRecipesid.pdf,
        openimagelike: prevProps.infoRecipesid.like,
        openimagedislike: !prevProps.infoRecipesid.like
      });
    }

    if (prevProps.infoRecipesid !== this.props.infoRecipesid) {
      this.setState({
        linkpdf: prevProps.pdfrecipeid
      });
    }
  }

  setImageNamelike = () => {
    if (this.state.openimagelike !== this.state.openimagedislike) {
      this.setState(state => ({
        openimagelike: true,
        openimagedislike: false
      }));
    } else {
      this.setState(state => ({
        openimagelike: !this.state.openimagedislike,
        openimagedislike: this.state.openimagelike
      }));
    }
    const data = {
      recipeid: this.props.match.params.idrecipe,
      status: true
    };
    this.props.Recipeslikedislike(data);
  };
  setImageNamedislike = () => {
    if (this.state.openimagelike !== this.state.openimagedislike) {
      this.setState(state => ({
        openimagedislike: true,
        openimagelike: false
      }));
    } else {
      this.setState(state => ({
        openimagelike: this.state.openimagedislike,
        openimagedislike: !this.state.openimagelike
      }));
    }
    const data = {
      recipeid: this.props.match.params.idrecipe,
      status: false
    };
    this.props.Recipeslikedislike(data);
  };
  downloadPdf = () => {
    const data = {
      recipeid: this.props.match.params.idrecipe,
      status: false
    };
    //  return this.props.getRecipesbyidPdf(data)
    this.props.RecipesPdf(data);
  };

  handleClose = () => {
    this.props.onClose();
  };
  onChangeIndex = value => {
    this.setState({ value });
  };

  clickVideo = () => {
    history.push(
      `/recipe/${this.props.match.params.idrecipe}/videoplayer/${this.props.videoid._id}`
    );
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div>
        <div classes={classes.paperprops} className={classes.root}>
          <div
            style={{
              background: "#000"
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end"
              }}
            >
              <div style={{ alignSelf: "self-start", zIndex: 1101 }}>
                <Link to="/home">
                  <Icon
                    className={classes.videoBack}
                    color="secondary"
                    aria-label="Close"
                  >
                    arrow_back_ios
                  </Icon>
                </Link>
              </div>
              <AppBar position="static" color="default">
                <Tabs
                  value={value}
                  onChange={this.handleChange}
                  indicatorColor="primary"
                  centered
                >
                  <Tab
                    className={classes.tabsTitle}
                    id={`Show_INGREDIENTS_${this.state.name}`}
                    label="INGREDIENTS"
                  />
                  <Tab
                    className={classes.tabsTitle}
                    id={`Show_PREPARATION_${this.state.name}`}
                    label="PREPARATION"
                  />
                </Tabs>
              </AppBar>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  width: "100%"
                }}
              >
                <img
                  src={process.env.REACT_APP_DOMAIN + this.state.linkimage}
                  width="100%"
                  alt=""
                />
                {this.props.videoid._id && <Icon
                  id={`Recipes_Video_${this.state.name}`}
                  className={classes.icon}
                  onClick={this.clickVideo}
                  style={{
                    position: "relative",
                    right: "50%",
                    marginTop: -15,
                    fontSize: 35,
                    color: "#fff"
                  }}
                >
                  play_circle_outline
                </Icon>}
              </div>
              {/* <VideoPlayer
                openVideo={this.state.openvideo}
                handleClose={() => this.setState({ openvideo: false })}
                videoimage={this.state.videoimage}
              /> */}
              <SwipeableViews
                index={this.state.value}
                onChangeIndex={this.onChangeIndex}
                style={{ marginTop: -45 }}
              >
                <TabContainer style={{ padding: 0 }}>
                  <Paper className={classes.paperbackground} elevation={1}>
                    <div className={classes.papereffect} />
                    <Typography
                      variant="h5"
                      component="h3"
                      style={{ fontFamily: "NeulandGroteskCondensedBold" }}
                    >
                      {this.state.name}
                    </Typography>
                    {this.state.ingredients.map((
                      item,
                      index ///TODO conv to array ingrd
                    ) => (
                        <div
                          key={item.key}
                          style={{
                            display: "flex",
                            borderBottom: "1px solid #ddd",
                            alignItems: "center"
                          }}
                        >
                          {
                            <p className={classes.listcount}>
                              {index < 9 ? `0${index + 1}` : `${index + 1}`}
                            </p>
                          }
                          <p
                            style={{
                              fontFamily: "NeulandGroteskCondensedRegular"
                            }}
                          >
                            {item.data}
                          </p>
                        </div>
                      ))}
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <Button
                        id={`downloadPdf_${this.state.name}`}
                        variant="contained"
                        onClick={this.downloadPdf}
                        className={classes.btndowpdf}
                      >
                        <a
                          id={`Download_Recipes_Pdf_${this.state.name}`}
                          style={{
                            color: "white",
                            fontFamily: "NeulandGroteskCondensedRegular"
                          }}
                          href={
                            process.env.REACT_APP_DOMAIN + this.state.linkpdf
                          }
                          target="_blank"
                          rel="noopener noreferrer"
                          download
                        >
                          Télécharger le pdf
                        </a>
                      </Button>
                    </div>
                  </Paper>
                </TabContainer>

                <TabContainer style={{ padding: 0 }}>
                  <Paper className={classes.paperbackground} elevation={1}>
                    <div className={classes.papereffect} />
                    <Typography
                      variant="h5"
                      component="h3"
                      style={{ fontFamily: "NeulandGroteskBold" }}
                    >
                      {this.state.name}
                    </Typography>
                    {this.state.preparation.map((item, index) => (
                      <div
                        style={{
                          display: "flex",
                          borderBottom: "1px solid #ddd",
                          alignItems: "center"
                        }}
                      >
                        <p className={classes.listcount}>
                          {index < 9 ? `0${index + 1}` : `${index + 1}`}
                        </p>
                        <p style={{ fontFamily: "NeulandGroteskLight" }}>
                          {" "}
                          {item.data}
                        </p>
                      </div>
                    ))}
                  </Paper>
                </TabContainer>
              </SwipeableViews>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end"
                }}
              >
                <div className={classes.likeDislike}>
                  <img
                    id={`Like_Recipes_${this.state.name}`}
                    src={this.state.openimagelike ? like : Like_grey}
                    onClick={this.setImageNamelike}
                    width="71px%"
                    alt=""
                  />
                  <img
                    id={`Dislike_Recipes_${this.state.name}`}
                    src={this.state.openimagedislike ? dislike : dislike_grey}
                    onClick={this.setImageNamedislike}
                    width="71px%"
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  //console.log("recipes state : ", state.watchVideo.infoRecipesid.videorecetteid)
  return {
    infoRecipesid: state.infoRecipesid.infoRecipesid,
    videoextrat: state.videoExtra,
    videoid: state.watchVideo.infoRecipesid.videorecetteid
      ? state.watchVideo.infoRecipesid.videorecetteid
      : state.videoExtra.videoExtra,
    pdfrecipeid: state.infoRecipesid.infoRecipesid.pdf
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRecipesbyid: data => dispatch(getRecipesbyid(data)),
    getRecipesbyidPdf: data => dispatch(getRecipesbyidPdf(data)),
    Recipeslikedislike: data => dispatch(Recipeslikedislike(data)),
    RecipesPdf: data => dispatch(RecipesPdf(data))
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Recipe);
