/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-18 10:11:23
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2020-01-16 15:29:42
 */

import React from "react";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { styles } from "./style";
import "video-react/dist/video-react.css"; // import css
import { Player, ControlBar } from "video-react";
import "./stylePlayer.css";
import { connect } from "react-redux";
import { compose } from "redux";
import {
  getCanPlayQuizz,
  canplayquizzreset,
  RecipeWatchVideo
} from "../../store/actions";
import Dialog from "@material-ui/core/Dialog";
import { watchVideo } from "../../store/actions/getRecipes";

const sources = {
  sintelTrailer: "http://media.w3.org/2010/05/sintel/trailer.mp4",
  bunnyTrailer: "http://media.w3.org/2010/05/bunny/trailer.mp4",
  bunnyMovie: "http://media.w3.org/2010/05/bunny/movie.mp4",
  test: "http://media.w3.org/2010/05/video/movie_300.webm"
};

class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    // window.screen.orientation.lock("landscape");
    this.state = {
      sources: null,
      open: false,
      openNotAction: false,
      url: null,
      pip: false,
      playing: false,
      controls: false,
      light: false,
      volume: 0.8,
      muted: false,
      played: 0,
      loaded: 0,
      duration: 0,
      playbackRate: 1.0,
      loop: false,
      portrait: "",
      videoend: false,
      progressBar: true,
      playstart: false,
      videoIndex: null, //this.props.match.params.id,
      timeToShow: false,
      currentTime: 0,
      seeking: false
    };

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.load = this.load.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
    this.seek = this.seek.bind(this);
    this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
    this.changeVolume = this.changeVolume.bind(this);
    this.setMuted = this.setMuted.bind(this);
  }

  componentDidMount() {
    //this.setState({ videoend: false });
    //this.play();
    // this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
  }

  handleStateChange = (state, prevState) => {
    // copy player state to this component's state
    this.setState({
      player: state,
      currentTime: state.currentTime
    });
    // parseFloat(3.7) >= parseFloat(this.state.currentTime.toFixed(2))
  };

  handleClickOpen = async () => {
    await this.setState({ open: true });
    await this.play();
  };

  handleClose = () => {
    this.setState({ open: false, playing: false });
  };

  onDuration = duration => {
    this.setState({ duration });
  };

  onProgress = state => {
    // We only want to update time slider if we are not currently seeking
    if (!this.state.seeking) {
      this.setState(state);
    }
  };
  onClickVideo = () => {
    this.setState({ progressBar: true });
    /*setTimeout(
      function() {
        this.setState({ progressBar: false });
      }.bind(this),
      5000
    );*/
  };

  ref = player => {
    this.player = player;
  };

  onSeekMouseDown = e => {
    this.setState({ seeking: true });
  };

  onSeekChange = e => {
    this.setState({ played: e.target.value });
  };

  onSeekMouseUp = e => {
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(e.target.value));
  };

  onSeekMouseUp = e => {
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(e.target.value));
  };

  play() {
    this.refs.player.play();
    this.props.getCanPlayQuizz(
      this.props.cityData.videos[this.state.videoIndex]._id
    );
  }

  pause() {
    this.refs.player.pause();
  }

  load() {
    this.refs.player.load();
  }

  changeCurrentTime(seconds) {
    return () => {
      const { player } = this.refs.player.getState();
      const currentTime = player.currentTime;
      this.refs.player.seek(currentTime + seconds);
    };
  }

  seek(seconds) {
    return () => {
      this.refs.player.seek(seconds);
    };
  }

  changePlaybackRateRate(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const playbackRate = player.playbackRate;
      this.refs.player.playbackRate = playbackRate + steps;
    };
  }

  changeVolume(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const volume = player.volume;
      this.refs.player.volume = volume + steps;
    };
  }

  setMuted(muted) {
    return () => {
      this.refs.player.muted = muted;
    };
  }

  changeSource(name) {
    return () => {
      this.setState({
        source: sources[name]
      });
      this.refs.player.load();
    };
  }

  VideoEnd = () => {
    let data = {
      customer_id: this.props.userData._id,
      videoid: this.props.infoRecipesid._id,
      idcity: this.props.cityData._id,
      spenttime: 20
    };
    this.props.RecipeWatchVideo(data);

    if (this.props.canplayquizz === "can play quizz") {
      this.setState({ open: true });
    } else {
      this.setState({ openNotAction: true });
    }
    this.props.canplayquizzreset();
  };

  startPlay = () => {
    this.setState({ playstart: true });
  };

  onReplayVideo = async () => {
    //this.setState({ open: true });
    this.props.getCanPlayQuizz(
      this.props.cityData.videos[this.state.videoIndex]._id
    );
    //this.refs.player.play();
  };

  onCloseEnd = () => {
    this.setState({ open: false });
    this.props.getCanPlayQuizz(
      this.props.cityData.videos[this.state.videoIndex]._id
    );
  };

  render() {
    const { classes, videoimage } = this.props;

    return (
      <Dialog
        fullScreen
        open={this.props.openVideo}
        PaperProps={{ classes: { root: classes.dialogPaper } }}
        style={{ overflow: "hidden " }}
      >
        <div className={classes.vidcont}>
          <IconButton
            style={{ zIndex: 10 }}
            color="inherit"
            onClick={this.props.handleClose}
            aria-label="Close"
            className={classes.videoCloseCont}
          >
            <CloseIcon
              className={classes.videoCloseIcn}
              style={{ zIndex: 10 }}
            />
          </IconButton>

          <div>
            <Player playsInline ref="player" onEnded={this.VideoEnd} autoPlay>
              <source src={videoimage} />
              <ControlBar>
                <div className={classes.videoProgreesTitle}>
                  <div className={classes.videoProgressBoxDesc}>
                    <div>
                      <Typography
                        component="p"
                        className={classes.playVideoTitle}
                      >
                        {this.props.titleplay}
                      </Typography>
                      <Typography
                        component="p"
                        className={classes.playVideoAuthor}
                      >
                        {/* title */}
                      </Typography>
                    </div>
                  </div>
                </div>
              </ControlBar>
            </Player>
          </div>
        </div>
      </Dialog>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    cityData: state.cityData.cityData,
    canplayquizz: state.canplayquizz.canplayquizz,
    watchVideo: state.watchVideo.watchVideo,
    userData: state.User.userInfo,
    infoRecipesid: state.infoRecipesid.infoRecipesid
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getCanPlayQuizz: idvideo => dispatch(getCanPlayQuizz(idvideo)),
    canplayquizzreset: () => dispatch(canplayquizzreset()),
    RecipeWatchVideo: data => dispatch(RecipeWatchVideo(data))
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(VideoPlayer);
