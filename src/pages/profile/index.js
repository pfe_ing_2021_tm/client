/**
 * /*
 *
 * @format
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-08 18:33:56
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2021-03-31 12:26:42
 */

import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Link } from "react-router-dom";
import Redxp from "../../assets/img/redxp.svg";
import { withStyles } from "@material-ui/core/styles";
// import myaccount from "../../assets/img/myaccount.png";
import myaccount from "../../assets/img/updateAccount.png";
import backprofile from "../../assets/img/backprofile.png";
import countdownimg from "../../assets/img/countdown.png";
import avatarPlaceholder from "../../assets/img/avatar-placeholder.png";

import REDSIconWheel from "../../assets/img/wheel/REDSIconWheel.png";

import { styles } from "./style";
import MyCheckin from "./myCheckin";
import MyEarnings from "./myEarnings";
import { compose } from "redux";
import { connect } from "react-redux";
import Icon from "@material-ui/core/Icon";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import {
  getUserInfo,
  getUserCheckIn,
  updatePseudo,
  getUserAwards,
  setResponseUpdate,
  updateAvatar,
  getVouchers,
} from "../../store/actions";

import AddPhotoAlternateIcon from "@material-ui/icons/AddPhotoAlternate";
import { red } from "@material-ui/core/colors";
import { history } from "../../history";
const token = localStorage.getItem("token");
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: undefined,
      hours: undefined,
      minutes: undefined,
      seconds: undefined,
      value: 0,
      username: "",
      city: "",
      token,
      avatar: "",
      checkins: [],
      userAwards: [],
      countdown: undefined,
      pageCheckIn: 1,
      pageEarning: 1,
      showCount: true,
      namestatus: true,
      newpseudo: null,
      open: false,
      reds: "",
    };
    this.refInputUpload = React.createRef();
  }

  handleClick = () => {
    this.setState({ open: true });
  };

  handleClose = (event, reason) => {
    this.props.setResponseUpdate();
    if (reason === "clickaway") {
      return;
    }

    this.setState({ open: false });
  };

  componentDidMount() {
    if (this.state.token === null) {
      return history.push("/");
    }

    this.props.getUserInfo();
    this.props.getUserCheckIn(1);
    this.props.getUserAwards(1);
    //this.props.getVouchers();
    // this.setCountdown();
  }

  setCountdown = () => {
    let eventTime = this.state.countdown * 1000; // Timestamp - Sun, 21 Apr 2013 13:00:00 GMT
    var now = new Date();
    let currentTime = now.getTime(); // Timestamp - Sun, 21 Apr 2013 12:30:00 GMT
    let diffTime = eventTime - currentTime;
    setInterval(() => {
      diffTime -= 1000;
      if (diffTime > 0) {
        let diffTimeDays = Math.floor(diffTime / (1000 * 3600 * 24));
        let diffTimeHours = Math.floor(
          (diffTime - diffTimeDays * 1000 * 3600 * 24) / (1000 * 3600),
        );
        let diffTimeMinutes = Math.floor(
          (diffTime -
            diffTimeDays * 1000 * 3600 * 24 -
            diffTimeHours * 1000 * 3600) /
            (1000 * 60),
        );

        const days = diffTimeDays > 9 ? diffTimeDays : "0" + diffTimeDays;
        const hours = diffTimeHours > 9 ? diffTimeHours : "0" + diffTimeHours;
        const minutes =
          diffTimeMinutes > 9 ? diffTimeMinutes : "0" + diffTimeMinutes;
        this.setState({ days, hours, minutes });
      } else {
        const days = "00";
        const hours = "00";
        const minutes = "00";
        this.setState({ days, hours, minutes });
      }
    }, 1000);
  };

  async componentWillReceiveProps(prevProps) {
    if (
      prevProps.userInfoData !== null &&
      prevProps.userInfoData !== this.props.userInfoData
    ) {
      this.setState(
        {
          username: prevProps.userInfoData.pseudo,
          newpseudo: prevProps.userInfoData.pseudo,
          city: prevProps.userInfoData.city,
          avatar: prevProps.userInfoData.image,
          countdown: prevProps.userInfoData.countdown,
          reds: prevProps.userInfoData.reds,
        },
        function () {
          // console.log("setState completed ");
          this.setCountdown();
        },
      );
    }
    if (prevProps.userCheckIn !== this.props.userCheckIn) {
      this.setState({ maxPageCheckin: prevProps.userCheckIn.maxpage });
      for (let key in prevProps.userCheckIn.checkins) {
        await this.setState({
          checkins: [
            ...this.state.checkins,
            prevProps.userCheckIn.checkins[key],
          ],
        });
        //console.log("this.state.userAwards", this.state.userAwards);
      }
    }
    if (prevProps.userAwards !== this.props.userAwards) {
      this.setState({ maxPageEarning: prevProps.userAwards.maxpage });
      for (let key in prevProps.userAwards.awards) {
        await this.setState({
          userAwards: [
            ...this.state.userAwards,
            prevProps.userAwards.awards[key],
          ],
        });
      }
    }

    if (
      prevProps.responseUpdateUser !== this.props.responseUpdateUser &&
      prevProps.responseUpdateUser === "duplicate"
    ) {
      this.handleClick();
    }

    if (prevProps.vouchers !== this.props.vouchers) {
      this.setState({
        vouchers: this.props.vouchers,
      });
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  loadMoreCheckIn = async () => {
    this.setState({ pageCheckIn: this.state.pageCheckIn + 1 });
    this.props.getUserCheckIn(this.state.pageCheckIn + 1);
  };

  loadMoreEarning = async () => {
    this.setState({ pageEarning: this.state.pageEarning + 1 });
    this.props.getUserAwards(this.state.pageEarning + 1);
  };

  editAvatar = (event) => {
    if (
      event.target.files[0].type == "image/png" ||
      event.target.files[0].type == "image/jpg" ||
      event.target.files[0].type == "image/jpeg"
    ) {
      this.props.updateAvatar(event.target.files[0]);
    } else {
      console.log("type invalide");
    }
  };

  editName = () => {
    this.setState({ namestatus: false });
  };

  handleSaveName = () => {
    this.setState({ namestatus: true });
    this.props.updatePseudo(this.state.newpseudo);
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            paddingLeft: 40,
            marginTop: 25,
            width: "35%",
          }}
        >
          <Link
            to="/"
            style={{
              display: "contents",
            }}
          >
            <img
              src={backprofile}
              style={{ width: 35, marginRight: 15 }}
              alt=""
            />
            <p style={{ color: "#fff", fontSize: 18 }}>&nbsp;</p>
          </Link>
        </div>

        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginLeft: "10%",
            marginRight: "10%",
          }}
        >
          <div>
            <img
              alt={this.state.username}
              src={
                this.state.avatar !== null
                  ? process.env.REACT_APP_DOMAIN + this.state.avatar
                  : avatarPlaceholder
              }
              className={classes.bigAvatar}
            />
            <input
              type="file"
              style={{ display: "none" }}
              accept=".png, .jpg, .jpeg"
              id="input"
              onChange={this.editAvatar}
              ref={this.refInputUpload}
            ></input>
            <AddPhotoAlternateIcon
              className={classes.iconEdit}
              onClick={() => this.refInputUpload.current.click()}
            />
          </div>

          <div style={{ marginLeft: 20 }}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginBottom: 15,
                fontFamily: "NeulandGroteskCondensedBold",
                fontSize: "3vw",
              }}
            >
              <p className={classes.pseudo}>
                {this.state.namestatus ? (
                  this.state.username
                ) : (
                  <input
                    style={{
                      fontFamily: "NeulandGroteskCondensedBold",
                      fontSize: "3vw",
                    }}
                    type="text"
                    className={classes.inputName}
                    value={this.state.newpseudo}
                    onChange={(event) =>
                      this.setState({ newpseudo: event.target.value })
                    }
                  />
                )}
              </p>

              {this.state.namestatus ? (
                <Icon className={classes.iconEditName} onClick={this.editName}>
                  edit
                </Icon>
              ) : (
                <Icon
                  className={classes.iconSaveName}
                  onClick={this.handleSaveName}
                >
                  save
                </Icon>
              )}
            </div>

            {/* <img src={myaccount} className={classes.gererCompte} alt='' /> */}
          </div>
        </div>

        {/* <div className={classes.tirageIcn}>
          {this.state.showCount && (
            <div
              style={{
                display: "flex",
                margin: "35px 10%",
                justifyContent: "flex-end",
              }}
            >
              <p
                style={{ color: "#fff", margin: "7px 20px", letterSpacing: 1 }}
              >
                Prochain tirage{" : "}
              </p>
              <div>
                <img
                  style={{
                    position: "absolute",
                    width: 200,
                    zIndex: -1,
                    marginTop: -25,
                    height: 80,
                    marginLeft: -24,
                  }}
                  src={countdownimg}
                  alt=''
                />
                <span
                  style={{
                    color: "#fff",
                    fontSize: 18,
                    //opacity: 0.8,
                    marginTop: -11,
                    marginLeft: 27,
                    marginRight: 30,
                  }}
                >
                  <span style={{ marginRight: 8, fontWeight: "bold" }}>
                    <span>{this.state.days}</span>
                    <span
                      style={{
                        marginLeft: -22,
                        position: "absolute",
                        marginTop: 22,
                        fontSize: 9,
                        color: "#d3a201",
                      }}
                    >
                      Jours
                    </span>
                  </span>
                  <span
                    style={{
                      position: "relative",
                      top: -4,
                      fontWeight: "bold",
                    }}
                  >
                    .
                  </span>
                  <span
                    style={{
                      marginLeft: 8,
                      marginRight: 8,
                      fontWeight: "bold",
                    }}
                  >
                    <span>{this.state.hours}</span>
                    <span
                      style={{
                        marginLeft: -25,
                        position: "absolute",
                        marginTop: 22,
                        fontSize: 9,
                        color: "#d3a201",
                      }}
                    >
                      Heures
                    </span>
                  </span>
                  <span
                    style={{
                      position: "relative",
                      top: -4,
                      fontWeight: "bold",
                    }}
                  >
                    .
                  </span>
                  <span style={{ marginLeft: 8, fontWeight: "bold" }}>
                    <span>{this.state.minutes}</span>
                    <span
                      style={{
                        marginLeft: -23,
                        position: "absolute",
                        marginTop: 22,
                        fontSize: 9,
                        color: "#d3a201",
                      }}
                    >
                      Minutes
                    </span>
                  </span>
                </span>
              </div>
            </div>
          )}
        </div> */}
        <div className={classes.itemIcn}>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: " center",
              flexDirection: "column",
            }}
          >
            <img
              alt="REDSIconWheel"
              style={{
                width: "27%",
                marginTop: "11%",
              }}
              src={REDSIconWheel}
            />
            {"  "}
            &nbsp;&nbsp;
            <p
              style={{
                color: "white",
                fontSize: "16px",
                fontFamily: "NeulandGroteskCondensedBold",
                marginTop: "-5%",
              }}
            >
              {this.state.reds} 
            </p>
          </div>
          {/* <Link to='/badge'>
            <img src={Redxp} alt='' style={{ width: 100 }} />
          </Link> */}
        </div>
        <Snackbar
          style={{ fontFamily: "NeulandGroteskCondensedBold" }}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
          ContentProps={{
            "aria-describedby": "message-id",
          }}
          message={<span id="message-id">Pseudo déjà utilisé !</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
        >
          <Tab
            label="Mes gains"
            classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
          />
          <Tab
            label="Cadeaux reçu"
            classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
          />
        </Tabs>
        {value === 0 && (
          <MyEarnings
            data={this.state.userAwards}
            vouchers={this.state.vouchers}
            maxPage={this.state.maxPageEarning}
            pageEarning={this.state.pageEarning}
            LoadMore={(p) => this.loadMoreEarning(p)}
          />
        )}
        {value === 1 && (
          <MyCheckin
            data={this.state.checkins}
            maxPage={this.state.maxPageCheckin}
            pageCheckIn={this.state.pageCheckIn}
            LoadMore={(p) => this.loadMoreCheckIn(p)}
          />
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInfoData: state.userInfoData.userInfoData,
    userCheckIn: state.userCheckIn.userCheckIn,
    userAwards: state.userAwards.userAwards,
    responseUpdateUser: state.responseUpdateUser.responseUpdateUser,
    vouchers: state.vouchers.vouchers,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUserInfo: () => dispatch(getUserInfo()),
    getUserCheckIn: (page) => dispatch(getUserCheckIn(page)),
    getUserAwards: (page) => dispatch(getUserAwards(page)),
    updateAvatar: (avatar) => dispatch(updateAvatar(avatar)),
    getVouchers: () => dispatch(getVouchers()),
    updatePseudo: (pseudo) => dispatch(updatePseudo(pseudo)),
    setResponseUpdate: () => dispatch(setResponseUpdate(null)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(Profile);
