export const styles = (theme) => ({
  itemIcn: {
    display: "flex",
    justifyContent: " space-around",
    alignItems: "center",
    alignContent: "center",
    borderTop: "1px dashed #fff",
    borderBottom: "1px dashed #fff",
    //padding: "14px 0",
    flexDirection: "row",
    margin: 15,
  },
  tirageIcn: {
    //display: "flex",
    justifyContent: " space-around",
    alignItems: "center",
    alignContent: "center",

    //padding: "14px 0",
    flexDirection: "row",
    margin: 15,
  },
  root: {
    color: "red",
    "&$checked": {
      color: "black",
    },
  },
  btnpopup: {
    background: "#FFBE04 0% 0% no-repeat padding-box",
    borderRadius: "12px",
    opacity: 1,
    width: "205px",
    height: "48px",
    position: "absolute",
    bottom: -21,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    font: "Black 25px/30px Roboto",
    fontFamily: "Roboto",
    letterSpacing: 0,
    fontWeight: 900,
    color: "black",
    letterSpacing: "0px",
    fontSize: "140%",

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      left: "22%",
    },
  },
  pseudo: {
    color: "yellow",
    margin: "5px 0px 0 16px",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },
    checkin: {
      [theme.breakpoints.between("xs", "sm")]: {
        //Galaxi s3 jusqu'a ipod ( 0px => 600px)
        fontSize: 22,
        color: "white !important",
      },

      [theme.breakpoints.between("sm", "md")]: {
        //ipod ( 600px => 950px)
        marginLeft: 20,
        fontSize: 45,
        color: "white !important",
      },
      [theme.breakpoints.between("md", "xl")]: {
        //ipod jusqu'a xl screen ( 950px => 1920px)
        marginLeft: 20,
        fontSize: 25,
        color: "white !important",
      },
    },
    dispo: {
      color: "#E4AC00",
      margin: "5px 0",
      textTransform: "uppercase",
      [theme.breakpoints.between("xs", "sm")]: {
        //Galaxi s3 jusqu'a ipod ( 0px => 600px)
        fontSize: 22,
      },

      [theme.breakpoints.between("sm", "md")]: {
        //ipod ( 600px => 950px)

        fontSize: 45,
      },
      [theme.breakpoints.between("md", "xl")]: {
        //ipod jusqu'a xl screen ( 950px => 1920px)

        fontSize: 25,
      },
    },
    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      marginLeft: 20,
      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      marginLeft: 20,
      fontSize: 25,
    },
  },

  width: "50%",
  [theme.breakpoints.between("lg", "xl")]: {
    width: 200,
  },
  [theme.breakpoints.between("xs", "sm")]: { width: "50%" },
  // dialogPaper: {
  //   [theme.breakpoints.between("lg", "xl")]: {
  //     display: "flex",
  //     justifyContent: "center",
  //     alignItems: "center",
  //     backgroundColor: "#96010F",
  //     backgroundSize: "cover",
  //     width: 400
  //   },
  //   [theme.breakpoints.between("xs", "sm")]: {
  //     //backgroundImage: `url("https://cdn.futura-sciences.com/buildsv6/images/mediumoriginal/6/5/2/652a7adb1b_98148_01-intro-773.jpg")`,

  //     display: "flex",
  //     justifyContent: "center",
  //     alignItems: "center",
  //     backgroundColor: "#96010F",
  //     backgroundSize: "cover"
  //   }
  // },
  btn: {
    position: "relative",
    [theme.breakpoints.between("xs", "sm")]: {
      //fontSize: 22,
      bottom: 150,
      width: 124,
    },

    [theme.breakpoints.between("sm", "md")]: {
      bottom: 300,
      width: 200,
      //  fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      bottom: 165,
      width: 115,
      // fontSize: 25
    },
  },
  image: {
    width: "50%",
    [theme.breakpoints.between("lg", "xl")]: {
      width: 200,
    },
    [theme.breakpoints.between("xs", "sm")]: { width: "50%" },
  },
  avatar: {
    margin: 10,
  },
  redsText: {
    color: "red",
    fontFamily: "NeulandGroteskCondensedBold",
    position: "relative",
    [theme.breakpoints.between("xs", "sm")]: {
      fontSize: 22,
      bottom: 140,
    },

    [theme.breakpoints.between("sm", "md")]: {
      bottom: 270,
      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      bottom: 145,
      fontSize: 25,
    },
  },
  bigAvatar: {
    margin: 10,
    borderRadius: "50%",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)

      width: "20vw",
      height: "20vw",
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      width: "20vw",
      height: "20vw",
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      width: 80,
      height: 80,
    },
  },
  tabsRoot: {
    // borderBottom: '1px solid #fff'
  },
  tabsIndicator: {
    backgroundColor: theme.palette.secondary.main,
  },
  tabRoot: {
    textTransform: "initial",
    minWidth: 72,
    // fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "NeulandGroteskCondensedBold",
    marginTop: 30,
    color: "#999",
    "&:hover": {
      color: theme.palette.secondary.main,
      opacity: 1,
    },
    "&$tabSelected": {
      color: theme.palette.secondary.main,
      fontWeight: theme.typography.fontWeightMedium,
      fontFamily: "NeulandGroteskCondensedBold",
    },
    "&:focus": {
      color: theme.palette.secondary.main,
    },
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  tabSelected: {},
  typography: {
    padding: theme.spacing(3),
  },
  titleDialog: {
    textAlign: "center ",
    font: "Black 25px/30px Roboto",
    fontFamily: "Roboto",
    letterSpacing: 0,
    fontWeight: 900,
    color: "black",
    letterSpacing: "0px",
    /* margin: 27px; */
    marginTop: "34px",
    marginLeft: "40px",
    marginRight: "40px",
    fontSize: "140%",
  },

  titleDialogerr: {},
  root: {
    display: "flex",
    justifyContent: "center",
  },
  formControl: {
    margin: theme.spacing(3),
  },
  label: {
    //backgroundColor: "red",
    marginTop: "14px !important",
    marginBottom: "36px !important",
  },
  labelTT: {
    marginTop: "10px",
    marginBottom: "12px",
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
  gererCompte: {
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)

      width: 90,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      width: 140,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      width: 90,
    },
  },
  iconEdit: {
    color: "#bd2132",
    position: "absolute",
    marginLeft: -30,
    marginTop: 7,
    cursor: "pointer",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)

      width: 40,
      height: 50,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      width: 90,
      height: 90,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      width: 50,
      height: 50,
    },
  },
  iconEditName: {
    color: "#bd2132",
    fontSize: 20,
    marginRight: -10,
    marginTop: 5,
    cursor: "pointer",
  },
  iconSaveName: {
    color: "#bd2132",
    marginRight: -15,
    marginTop: 5,
    cursor: "pointer",
  },
  inputName: {
    fontSize: 16,
    margin: "0 5px",
    color: theme.palette.secondary.main,
    background: "none",
    border: "none",
    borderBottom: "1px solid #999",
    width: 120,
  },
  containerOper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      marginLeft: "80px !important",
      marginRight: "80px !important",
    },
  },
});
