/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-09 11:21:57
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-02-20 14:49:54
 */
import React, { Component } from "react";
import TimeAgo from "react-timeago";
import frenchStrings from "react-timeago/lib/language-strings/fr";
import buildFormatter from "react-timeago/lib/formatters/buildFormatter";
import pin from "../../assets/img/pin.png";
import reds from "../../assets/img/reds.png"

import { withStyles } from "@material-ui/core/styles";
import { styles } from "./style";
import { compose } from "redux";

const formatter = buildFormatter(frenchStrings);
class MyCheckin extends Component {
  state = {
    page: 1
  };

  LoadMore = () => {
    this.props.LoadMore();
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.props.data.map((item, index) => (
          <div
            key={index}
            style={{
              display: "flex",
              justifyContent: "space-between",
              height: 130,
              margin: 20,
              padding: "0 15px",
              alignItems: "center",
              backgroundImage: `url(${
                process.env.REACT_APP_DOMAIN + item.background
              })`,
              backgroundSize: "cover",
            }}
          >
            <div style={{ zIndex: 1 }}>
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  marginBottom: 13,
                }}
              >
                {/* <img src={pin} alt="" style={{ width: 15, marginRight: 5 }} /> */}

                <p
                  style={{
                    margin: "0",
                    fontSize: 22,
                    color: "#fff",
                    fontFamily: "NeulandGroteskCondensedRegular",
                  }}
                >
                  <TimeAgo date={item.createdAt} formatter={formatter} />
                </p>
              </div>

              <p style={{ color: "white", marginLeft: 20, fontSize: 20 }}>
                {item.nameOfsender}
              </p>
              {/* {item.disponible ? (
                <p
                  style={{
                    color: "#E4AC00",
                    marginLeft: "9%",
                    textTransform: "uppercase",
                  }}
                >
                  disponible !
                </p>
              ) : (
                <p
                  style={{
                    color: "red",
                    marginLeft: "9%",
                    fontSize: 11,
                    textTransform: "uppercase",
                  }}
                >
                  non disponible
                </p>
              )} */}
            </div>
            <p style={{ color: "#cdd281", marginLeft: 20, fontSize: 55 }}>
              {item.startsToSend}
            </p>
            <img src={reds} style={{ width: 50, zIndex: 1 }} alt="" />
          </div>
        ))}
        <div>
          {this.props.maxPage > 1 &&
            this.props.maxPage > this.props.pageCheckIn && (
              <p
                style={{
                  color: "#fff",
                  textAlign: "center",
                  cursor: "pointer",
                }}
                onClick={() => this.LoadMore()}
              >
                VOIR PLUS
              </p>
            )}
        </div>
      </div>
    )
  }
}
export default compose(withStyles(styles))(MyCheckin);
