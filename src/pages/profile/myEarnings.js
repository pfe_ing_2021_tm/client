/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-09 11:21:51
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2020-01-30 10:53:12
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-08-21 14:33:39
 */
import React, { Component } from "react";
import award from "../../assets/img/award.png";
import View_recharge from "../../assets/img/View_recharge.svg";
import View_check from "../../assets/img/check.png";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import OrangeIcons from "../../assets/img/orange.png";
import ooredoooIcons from "../../assets/img/ooredooo.png";
import TTIcons from "../../assets/img/tt.png";
import {
  uiStartLoading,
  getCodeRecharge,
  getVouchers,
} from "../../store/actions";
import PopupRch from "./popupRch";
import { history } from "../../history";

// function Transition(props) {
//   return <Slide direction="down" {...props} mountOnEnter unmountOnExit />;
// }

class MyEarnings extends Component {
  state = {
    page: 1,
    open: false,
    activeStep: -1,
    value: "",
    err: false,
    valid: false,
    idItem: "",
    coderecharges: "",
    typevoucher: "",
    seen: false,
    points: 0,
    selectedItem: {},
    vouchersp: "",
    existvouchers: false,
    ORANGE: false,
    TT: false,
    OOREDOO: false,
    orange: false,
  };
  componentDidMount() {
    //this.props.getVouchers();
  }
  validations(valid) {
    this.setState({
      valid: valid,
      open: false,
      activeStep: -1,
      value: "",
      coderecharges: "",
      typevoucher: "",
      seen: false,
    });
  }
  LoadMore = (p) => {
    this.props.LoadMore();
  };
  handleOpen = (item) => {
    //console.log("item : ", item)
    if (item.giftid.votcherType === "achat") {
      let data = {
        operateur: this.state.value.toUpperCase(),
        idGiftWin: item._id,
        typevoucher: "achat",
        points: item.giftid.points,
      };
      this.props.getCodeRecharge(data);
      this.setState({
        open: true,
        coderecharges: this.props.coderecharge,
        activeStep: 1,
        err: false,
        typevoucher: item.giftid.votcherType,
        seen: item.seen,
      });
    } else {
      this.props.getVouchers();
      this.setState({ selectedItem: item });
    }
  };
  handleOpenPopInfo = (item) => {
    if (
      item.giftid.votcherType === "telecom" &&
      item.voucher !== 0 &&
      item.voucher !== "0" &&
      item.voucher !== ""
    ) {
      this.setState({
        open: true,
        idItem: item._id,
        coderecharges: item.voucher,
        activeStep: 1,
        typevoucher: item.giftid.votcherType,
        seen: item.seen,
      });
    } else if (
      item.giftid.votcherType === "achat" &&
      item.voucher !== 0 &&
      item.voucher !== "0" &&
      item.voucher !== ""
    ) {
      let data = {
        operateur: this.state.value.toUpperCase(),
        idGiftWin: item._id,
        typevoucher: "achat",
        points: this.state.points,
      };
      if (!item.seen) {
        this.props.getCodeRecharge(data);
      }
      this.setState({
        open: true,
        coderecharges: item.voucher,
        activeStep: 1,
        err: false,
        typevoucher: item.giftid.votcherType,
        seen: item.seen,
      });
    } /*else {
      this.setState({ open: true, coderecharges: "code non disponible", activeStep: 1, err: false });
    }*/
  };
  handleClose = () => {
    this.setState({ open: false });
    if (!this.state.seen) {
      history.push("/");
    }
  };
  handleAnnule = () => {
    this.setState({ open: false });
  };
  handleChange = (event) => {
    this.setState({ value: event.target.value, err: false });
  };
  handleNext = () => {
    if (this.state.value != "") {
      this.setState({ activeStep: 1, err: false });
    } else {
      this.setState({ err: true });
    }
    if (this.state.value != "" && this.state.activeStep === 1) {
    }
    let data = {
      operateur: this.state.value.toUpperCase(),
      idGiftWin: this.state.idItem,
      typevoucher: "telecom",
      points: this.state.points,
    };
    this.props.getCodeRecharge(data);
    //return history.push("/profile");
  };
  getIdItem = (v) => {};
  /*componentWillReceiveProps(prevProps) {
    if (prevProps.coderecharge !== this.props.coderecharge) {
      this.setState({ coderecharges: this.props.coderecharge });
      this.setState({ vouchers: this.props.vouchers.vouchers, orange: this.props.vouchers.orange });
      this.props.getVouchers();
    }
  }*/

  componentDidUpdate(prevProps) {
    if (prevProps.coderecharge !== this.props.coderecharge) {
      this.setState({ coderecharges: this.props.coderecharge });
    }
    if (prevProps.vouchers !== this.props.vouchers) {
      this.setState({
        vouchers: this.props.vouchers.vouchers,
        orange: this.props.vouchers.orange,
      });
      let exits = false;
      let vouchers = [];
      const item = this.state.selectedItem;
      if (
        this.props.vouchers.vouchers.length > 0 &&
        typeof this.props.vouchers.vouchers !== "string"
      ) {
        let orange = this.props.vouchers.orange;
        vouchers = this.props.vouchers.vouchers.map(function (obj) {
          if (obj._id === "ORANGE" && item.giftid.points === 2000) {
            if (orange) {
              exits = true;
              return obj;
            }
          }
          if (obj.TND.includes(item.giftid.points.toString())) {
            exits = true;
            return obj;
          }
        });
        vouchers = vouchers.filter(Boolean);
      }
      if (exits === true) {
        this.setState({
          open: true,
          idItem: item._id,
          typevoucher: item.giftid.votcherType,
          seen: item.seen,
          points: item.giftid.points,
          activeStep: 0,
          vouchers: vouchers,
          existvouchers: exits,
          err: false,
        });

        vouchers.map((item, index) => {
          if (item.client == "ORANGE") {
            this.setState({ ORANGE: true });
          } else if (item.client == "OOREDOO") {
            this.setState({ OOREDOO: true });
          } else if (item.client == "TT") {
            this.setState({ TT: true });
          }
        });
      } else {
        this.setState({
          open: true,
          idItem: item._id,
          typevoucher: item.giftid.votcherType,
          seen: item.seen,
          points: item.giftid.points,
          activeStep: 0,
          existvouchers: exits,
          err: true,
        });
      }
    }
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.props.data.map((item, index) => (
          <div
            key={index}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              padding: "20px 15px",
              margin: 13,
              borderBottom: "1px dashed #fff",
            }}
          >
            <div
              style={{ display: "flex", alignItems: "center" }}
              onClick={() => this.handleOpenPopInfo(item)}
            >
              <img src={award} alt="" style={{ width: 50 }} />
              <div style={{ marginLeft: 30 }}>
                <p
                  style={{
                    color: "#FFBE00",
                    fontSize: 25,
                    fontWeight: 700,
                    margin: 0,
                  }}
                >
                  {item.giftid.title}
                </p>
                <p style={{ color: "#fff", margin: 0, fontSize: 11 }}>
                  {item.createdAt}
                </p>
              </div>
            </div>
            <div>
              {(item.giftid.votcherType === "telecom" ||
                item.giftid.votcherType === "achat") &&
              !item.seen ? (
                <img
                  onClick={() => this.handleOpen(item)}
                  src={View_recharge}
                  alt=""
                  style={{ width: 42 }}
                />
              ) : (item.giftid.votcherType === "telecom" ||
                  item.giftid.votcherType === "achat") &&
                item.seen ? (
                <img src={View_check} alt="" style={{ width: 42 }} />
              ) : (
                ""
              )}
            </div>

            <Dialog
              //modal={false}
              disableBackdropClick
              aria-labelledby="simple-dialog-title"
              open={this.state.open}
              onClose={this.handleClose}
              //TransitionComponent={Transition}
              PaperProps={{
                style: {
                  display: "flex",

                  justifyContent: "center",
                  overflowY: "visible",
                },
              }}
            >
              {this.state.activeStep == 0 ? (
                <div>
                  <div
                    className={classes.titleDialog}
                    style={{
                      color: this.state.err ? "red" : "black",
                      transition: this.state.err ? "color 1s" : "color 1s",
                      transform: this.state.err ? "scale(1.1,1.1)" : null,
                      transition: this.state.err ? "transform 0.5s ease" : null,
                      height: !this.state.existvouchers ? "18vh" : null,
                    }}
                  >
                    {
                      /*this.state.vouchers.length > 0 && typeof this.state.vouchers !== 'string'*/ this
                        .state.existvouchers
                        ? "Choisissez votre opérateur"
                        : "Vouchers non disponibles"
                    }
                  </div>

                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      overflowY: "visible",
                    }}
                  >
                    {this.state.existvouchers ? (
                      <FormControl
                        component="fieldset"
                        className={classes.formControl}
                      >
                        <FormLabel component="legend"></FormLabel>
                        <div
                          className={classes.containerOper}
                          //style={{ width: "34px" }}
                        >
                          <div>
                            <RadioGroup
                              aria-label="Operateur"
                              name="op1"
                              className={classes.group}
                              value={this.state.value}
                              onChange={this.handleChange}
                            >
                              <FormControlLabel
                                value="Orange"
                                control={<Radio color="primary" />}
                                //label="Orange"
                                className={classes.label}
                                disabled={!this.state.ORANGE}
                              />
                              <FormControlLabel
                                value="Ooredoo"
                                control={<Radio color="primary" />}
                                className={classes.label}
                                //label="Ooredoo"
                                disabled={!this.state.OOREDOO}
                              />
                              <FormControlLabel
                                value="TT"
                                control={<Radio color="primary" />}
                                className={classes.labelTT}
                                // label="TT"
                                disabled={!this.state.TT}
                              />
                            </RadioGroup>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              //   display:
                              //     this.state.vouchers.length > 1 &&
                              //     typeof this.state.vouchers !== "string" &&
                              //     this.state.existvouchers
                              //       ? "flex"
                              //       : "",
                              flexDirection: "column",
                              //   height:
                              //     typeof this.state.vouchers !== "string" &&
                              //     this.state.existvouchers
                              //       ? "300px"
                              //       : "100px",
                              justifyContent: "space-around",
                              height: 275,
                            }}
                          >
                            <div>
                              <img
                                src={OrangeIcons}
                                alt=""
                                style={{
                                  filter: this.state.ORANGE
                                    ? "opacity(1)"
                                    : "opacity(0.3)",
                                }}
                              />
                            </div>
                            <div>
                              <img
                                src={ooredoooIcons}
                                alt=""
                                style={{
                                  filter: this.state.OOREDOO
                                    ? "opacity(1)"
                                    : "opacity(0.3)",
                                }}
                              />
                            </div>
                            <div>
                              <img
                                src={TTIcons}
                                alt=""
                                style={{
                                  filter: this.state.TT
                                    ? "opacity(1)"
                                    : "opacity(0.3)",
                                }}
                              />
                            </div>
                          </div>
                        </div>
                      </FormControl>
                    ) : (
                      ""
                    )}

                    {/*<div style={{ position: "absolute", bottom: 22 }}>
                      <MobileStepper
                        variant="dots"
                        steps={2}
                        position="static"
                        activeStep={this.state.activeStep}
                        className={classes.root}
                      />
                    </div> */}
                    {this.state.existvouchers ? (
                      <div
                        className={classes.btnpopup}
                        onClick={this.handleNext}
                      >
                        SUIVANT
                      </div>
                    ) : (
                      <div
                        className={classes.btnpopup}
                        onClick={this.handleAnnule}
                      >
                        Annuler
                      </div>
                    )}
                  </div>
                </div>
              ) : (
                <PopupRch
                  idItem={this.state.idItem}
                  operateur={this.state.value}
                  typevoucher={this.state.typevoucher}
                  validation={this.validations.bind(this)}
                  coderecharge={this.state.coderecharges}
                  seen={this.state.seen}
                />
              )}
              {/* <img className={classes.image} src={PopupResd} alt="" />
              <h2 className={classes.title}>VOUS AVEZ CUMULÉ</h2>
              <p className={classes.redsText}>{this.state.reds} &nbsp;STARS</p>
              <img
                src={CONFIRMERPOPUP}
                onClick={this.handleClose}
                alt="IconReds"
                className={classes.btn}
              /> */}
            </Dialog>
          </div>
        ))}

        <div>
          {this.props.maxPage > 1 &&
            this.props.maxPage > this.props.pageEarning && (
              <p
                style={{
                  color: "#fff",
                  textAlign: "center",
                  cursor: "pointer",
                }}
                onClick={() => this.LoadMore()}
              >
                VOIR PLUS
              </p>
            )}
        </div>
      </div>
    );
  }
}
MyEarnings.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
  return {
    isLoading: state.ui.isLoading,
    coderecharge: state.coderecharge.coderecharge,
    vouchers: state.vouchers.vouchers,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    uiStartLoading: () => dispatch(uiStartLoading()),
    getVouchers: (data) => dispatch(getVouchers(data)),
    getCodeRecharge: (data) => dispatch(getCodeRecharge(data)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(MyEarnings);
