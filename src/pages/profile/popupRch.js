/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-01-31 12:04:08
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-09-18 11:55:28
 */

import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Button from "@material-ui/core/Button";
import { uiStartLoading, getCodeRecharge } from "../../store/actions";
import CircularProgress from "@material-ui/core/CircularProgress";
import { history } from "../../history";
class PopupRch extends React.Component {
  state = {
    codeOper: 0,
    copied: false,
    valid: false,
    codeRecharge: {},
  };
  componentDidMount() {
    if (this.props.operateur === "Orange") {
      this.setState({ codeOper: 101 });
    } else if (this.props.operateur === "Ooredoo") {
      this.setState({ codeOper: 100 });
    } else {
      this.setState({ codeOper: 124 });
    }

    this.setState({ codeRecharge: this.props.coderecharge });

    // let data = {
    //   operateur: this.props.operateur.toUpperCase(),
    //   idGiftWin: this.props.idItem
    // };
    // this.props.getCodeRecharge(data);
  }

  componentWillUpdate(prevProps) {
    if (prevProps.coderecharge !== this.props.coderecharge) {
      this.setState({ codeRecharge: this.props.coderecharge });
    }
  }
  // componentWillReceiveProps(prevProps) {
  //   if (prevProps.coderecharge !== this.props.coderecharge) {
  //     console.log("inside if ");
  //     this.setState({ codeRecharge: this.props.coderecharge });
  //   }
  // }

  handleValider = () => {
    this.setState({ valid: true });
    this.props.validation(this.state.valid);
    if (!this.props.seen) {
      window.location.reload();
    }
  };
  render() {
    const { classes, operateur } = this.props;
    let code = "";
    let arrayCode = [];
    if (this.props.coderecharge) {
      //console.log("this.props.coderecharge : ", this.props.coderecharge)
      if (this.props.coderecharge.voucher) {
        if (
          this.props.coderecharge.voucher !== 0 &&
          this.props.coderecharge.voucher !== ""
        ) {
          code = this.props.coderecharge.voucher;
          if (code.includes(",")) {
            arrayCode = code.split(",");
          } else {
            arrayCode.push(code);
          }
        }
      } else {
        if (
          this.props.coderecharge !== 0 &&
          this.props.coderecharge !== "0" &&
          this.props.coderecharge !== ""
        ) {
          code = this.props.coderecharge.toString();
          if (code.includes(",")) {
            arrayCode = code.split(",");
          } else {
            arrayCode.push(code);
          }
        }
      }
    }
    //console.log("arrayCode : ", arrayCode)
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        {arrayCode.length > 0 ? (
          <div className={classes.titleDialog}>
            {this.props.typevoucher === "telecom" &&
              `Votre code de recharge ${operateur} `}
            {this.props.typevoucher === "achat" && `Votre code de bon d'achat `}
            {this.props.typevoucher === "" && `Votre code `}
          </div>
        ) : (
          ""
        )}
        {this.props.isLoading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
            }}
          >
            <CircularProgress color="primary" />
          </div>
        ) : (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            {arrayCode.length > 0 ? (
              <div>
                {arrayCode.map((item, key) => {
                  return (
                    <div key={key} style={{ display: "contents" }}>
                      <div
                        className={classes.titleDialog}
                        style={{ marginBottom: "30px" }}
                        // ${this.state.codeRecharge.voucher}
                      >
                        {/*` *${this.state.codeOper}*${this.props.coderecharge.voucher}#`*/}
                        {`${
                          typeof code === "object"
                            ? code.voucher === "" || code.voucher === 0
                              ? "code non disponible"
                              : code.voucher
                            : item
                        }`}
                      </div>
                      <div
                        style={{ marginBottom: "30%", position: "relative" }}
                      >
                        <span
                          style={{
                            display: !this.state.copied ? "none" : null,
                            position: "absolute",
                            transform: "translate(-50%,-50%)",
                            bottom: -30,
                            left: "50%",
                          }}
                        >
                          code copied !
                        </span>
                        {this.props.typevoucher === "telecom" &&
                        code.voucher !== 0 ? (
                          <div style={{ textAlign: "center" }}>
                            <CopyToClipboard
                              //text={` *${this.state.codeOper}*${this.props.coderecharge.voucher}#`}
                              text={`${item}`}
                              onCopy={() => this.setState({ copied: true })}
                            >
                              <span></span>
                            </CopyToClipboard>

                            <CopyToClipboard
                              //text={` *${this.state.codeOper}*${this.props.coderecharge.voucher}#`}
                              text={`${item}`}
                              onCopy={() => this.setState({ copied: true })}
                            >
                              <Button variant="contained" color="primary">
                                Copier le code
                              </Button>
                            </CopyToClipboard>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  );
                })}
              </div>
            ) : (
              ""
            )}
          </div>
        )}
        {/*<div style={{ position: "absolute", bottom: 22 }}>
          <MobileStepper
            variant="dots"
            steps={2}
            position="static"
            activeStep={1}
            className={classes.root}
          />
        </div> */}
        {arrayCode.length > 0 ? (
          <div onClick={this.handleValider} className={classes.btnpopup}>
            VALIDER
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.ui.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    uiStartLoading: () => dispatch(uiStartLoading()),
    getCodeRecharge: (data) => dispatch(getCodeRecharge(data)),
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(PopupRch);
