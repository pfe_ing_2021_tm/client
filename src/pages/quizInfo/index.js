/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Mon Apr 29 2019
 */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { styles } from "./style";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogRes from "./dialog";
//import respQuiz from "../../assets/img/resp-quiz.png";
//import check from "../../assets/img/check.png";
import answerOption from "../../assets/img/optionAnswer.png";
import { compose } from "redux";
import { connect } from "react-redux";

import {
  getQuizInfoById,
  setAnswerQuizInfo,
  quizShotResult,
  quizAnswerResult
} from "../../store/actions";
import { history } from "../../history";
const token = localStorage.getItem("token");

class QuizInfo extends React.Component {
  state = {
    open: false,
    currentResponse: [],
    question: null,
    resData: null,
    actionButton: null,
    suggestions: [],
    image: null,
    token
  };

  componentDidMount() {
    if (this.state.token === null) {
      return history.push("/");
    }
    //set background color
    //document.body.style.backgroundColor = "rgb(33, 175, 184)";
    this.props.getQuizInfoById(this.props.match.params.quizid);
    //const quizinfo = 
  }

  async componentWillUnmount() {
    //reset background color
    document.body.style.backgroundColor = "#000";
    this.props.quizShotResultreset();
    this.props.quizAnswerResultreset();
  }

  componentWillReceiveProps(prevProps) {
    if (
      prevProps.quizInfoResult !== null &&
      prevProps.quizInfoResult !== this.props.quizInfoResult
    ) {
      this.setState({
        question: prevProps.quizInfoResult.question,
        suggestions: prevProps.quizInfoResult.suggestions,
        image: prevProps.quizInfoResult.image
      });
    }

    if (prevProps.quizInfoAnswerResult !== this.props.quizInfoAnswerResult) {
      this.setState({ resData: prevProps.quizInfoAnswerResult.message, actionButton: prevProps.quizInfoAnswerResult.ressayer, open: true });
    }
  }

  handleClose = async () => {
    this.setState({
      open: false
    });
    //TO DO

    this.setState({ resData: null, actionButton: null });
  };

  handleAddResponse = id => {
    this.setState({ currentResponse: [id] });
  };

  handleRemoveResponse = id => {
    const newArray = this.state.currentResponse.filter(item => item !== id);
    this.setState({ currentResponse: newArray });
  };

  handleResponse = async (id) => {
    let data = {
      id: this.props.match.params.quizid,
      reponse: id//this.state.currentResponse[0]
    };
    this.props.setAnswerQuizInfo(data);
    this.props.quizAnswerResultreset();
  };

  render() {
    const { classes } = this.props;
    const { currentResponse, image } = this.state;
    return (
      <div>
        <div>
          <DialogRes
            onClose={this.handleClose}
            open={this.state.open}
            data={this.state.resData}
            actionButton={this.state.actionButton}
          />
          <div className={classes.container} style={{
            backgroundImage: `url(${process.env.REACT_APP_DOMAIN + image})`,
            backgroundSize: `cover`,
            backgroundPositionY: "80%",
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "flex-start"
          }}>
            <div className={classes.sectionHeader}>
              <Link to="/home" className={classes.closebtn}>
                <IconButton
                  color="inherit"
                  aria-label="Close"
                  style={{ padding: 0 }}
                >
                  <CloseIcon className={classes.videoCloseIcn} />
                </IconButton>
              </Link>
            </div>
            {/*Content*/}
            <div
              style={{
                alignSelf: "flex-end",
                display: "flex",
                position: "relative",
                alignItems: "center",
                marginLeft: "auto",
                marginRight: "auto",
                marginBottom: "20px"
              }}
            >
              {this.state.suggestions && this.state.suggestions.map((item, index) => (
                <div
                  style={{ margin: "10px" }}
                  key={index}
                  onClick={e => this.handleResponse(index)}
                >
                  {item.image.length > 0 ? (
                    <div style={{ width: "80px" }}>
                      <img src={process.env.REACT_APP_DOMAIN + item.image} style={{ width: "80px" }} />
                      <p style={{ color: "#fff", textAlign: "center", margin: "auto", /*fontSize: "25px",*/ fontFamily: "initial" }}>{item.answerOption}</p>
                    </div>
                  ) : (
                      <div style={{ width: "80px" }}>
                        <img src={answerOption} style={{ width: "100px" }} />
                        <p style={{
                          color: "#d7182a",
                          margin: "auto",
                          fontSize: "20px",
                          top: "40%",
                          fontSize: "18px",
                          marginLeft: "28px",
                          marginBottom: "20px",
                          //left: "50%",
                          //transform: "translate(80%, -50%)",
                          position: "absolute",
                        }}>{item.answerOption}</p>
                      </div>
                    )}

                </div>
              ))}
            </div>
          </div>
        </div>
      </div >
    );
  }
}

const mapStateToProps = state => {
  return {
    quizInfoResult: state.quizInfoResult.quizInfoResult,
    quizInfoAnswerResult: state.quizInfoAnswerResult.quizInfoAnswerResult
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getQuizInfoById: data => dispatch(getQuizInfoById(data)),
    quizShotResultreset: () => dispatch(quizShotResult()),
    setAnswerQuizInfo: data => dispatch(setAnswerQuizInfo(data)),
    quizAnswerResultreset: () => dispatch(quizAnswerResult())
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(QuizInfo)