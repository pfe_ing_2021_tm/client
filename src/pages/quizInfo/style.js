import quizhead from "../../assets/img/quiz-shot-header.png";

export const styles = theme => ({
  container: {
    height: "100vh",
    //background: "rgb(33, 175, 184)"
  },
  closebtn: {
    color: theme.palette.secondary.main,
    border: "3px solid",
    padding: 0,
    margin: 15,
    borderRadius: "50%",
    cursor: "pointer"
  },
  section: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 25
  },
  sectionHeader: {
    height: 145,
    //background: `url(${quizhead})`,
    position: "absolute",
    marginTop: "10px",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "flex-start",
    backgroundSize: "cover"
  },
  question: {
    color: theme.palette.secondary.main,
    textAlign: "center",
    width: "80%",
    fontWeight: "600"
  },
  questionInd: {
    background: theme.palette.secondary.main,
    height: 60,
    display: "flex",
    alignItems: "center",
    padding: "0 15px",
    borderRadius: 6,
    margin: "15px 0"
  },
  checkbox: {
    width: 25,
    height: 25,
    background: "#BEE2C2",
    borderRadius: "50%",
    border: "2px solid #B7DABB"
  },
  checkboxdis: {
    width: 25,
    height: 25,
    background: theme.palette.secondary.main,
    borderRadius: "50%",
    border: "2px solid #B7DABB"
  },
  inditext: {
    padding: "0 0px",
    color: "#0F013F",
    fontWeight: "500"
  },
  imgcheck: {
    width: 25,
    padding: "2px 3px"
  },
  btncontain: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  btnimg: {
    width: 200,
    marginTop: 30
  }
});
