/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-07-09T09:52:00+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-16T14:04:53+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import blue from "@material-ui/core/colors/blue";
import RedsIcon from "../../assets/img/endstore.png";
import { Link } from "react-router-dom";

const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600]
  },
  dialogPaper: {
    alignItems: "center",
    overflow: "visible",
    width: "100%"
  }
};

class DialogRes extends React.Component {
  state = {
    open: false
  };

  handleClose = () => {
    this.props.onClose();
  };

  handleListItemClick = () => {
    this.props.onClose();
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog
        open={this.props.open}
        PaperProps={{ className: classes.dialogPaper }}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <img src={RedsIcon} alt="" style={{ width: 245, marginBottom: 5 }} />
        <p
          style={{
            textAlign: "center",
            color: "#919191",
            textTransform: "uppercase"
          }}
        >
          {this.props.data === "correct answer" ? (
            <span>
              Bonne réponse ! <br />
              Vous avez ganger 300 Reds
            </span>
          ) : (
            <span>Mauvaise réponse !</span>
          )}

          <br />
          {/* <span style={{ color: '#ed2a1c', fontSize: 22, fontWeight: 700 }}>
            50 reds
          </span> */}
        </p>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 40
          }}
        >
          <Link to="/home" id={`Reponse : ${this.props.data}`}>
            <Button
              id={`Reponse : ${this.props.data}`}
              variant="contained"
              style={{
                position: "absolute",
                bottom: -15,
                background: "#ed2a1c",
                color: "#fff",
                marginLeft: -32
              }}
            >
              <div id={`Reponse Quiz : ${this.props.data}`}>ok</div>
            </Button>
          </Link>
        </div>
        <div />
      </Dialog>
    );
  }
}

export default withStyles(styles)(DialogRes);
