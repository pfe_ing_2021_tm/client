/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Mon Apr 29 2019
 */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { styles } from "./style";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogRes from "./dialog";
import respQuiz from "../../assets/img/resp-quiz.png";
import check from "../../assets/img/check.png";
import { compose } from "redux";
import { connect } from "react-redux";

import {
  getQuizShotById,
  setAnswerQuizShot,
  quizShotResult,
  quizAnswerResult
} from "../../store/actions";
import { history } from "../../history";
const token = localStorage.getItem("token");

class QuizShot extends React.Component {
  state = {
    open: false,
    currentResponse: [],
    question: null,
    resData: null,
    suggestions: [],
    idvideo: null,
    idcity: null,
    brand: null,
    token
  };

  componentDidMount() {
    if (this.state.token === null) {
      return history.push("/");
    }
    //set background color
    //document.body.style.backgroundColor = "rgb(33, 175, 184)";
    this.props.getQuizShotById(this.props.match.params.quizid);
  }

  async componentWillUnmount() {
    //reset background color
    document.body.style.backgroundColor = "#000";
    this.props.quizShotResultreset();
    this.props.quizAnswerResultreset();
  }

  componentWillReceiveProps(prevProps) {
    if (
      prevProps.quizShotResult !== null &&
      prevProps.quizShotResult !== this.props.quizShotResult
    ) {
      this.setState({
        question: prevProps.quizShotResult.question,
        suggestions: prevProps.quizShotResult.suggestions,
        idvideo: prevProps.quizShotResult.video._id,
        idcity: prevProps.quizShotResult.video.city ? prevProps.quizShotResult.video.city._id : null,
        brand: prevProps.quizShotResult.video.brand
      });
    }

    if (prevProps.quizAnswerResult !== this.props.quizAnswerResult) {
      this.setState({ resData: prevProps.quizAnswerResult, open: true });
    }
  }

  handleClose = async () => {
    this.setState({
      open: false
    });
    //TO DO

    this.setState({ resData: null });
  };

  handleAddResponse = id => {
    this.setState({ currentResponse: [id] });
  };

  handleRemoveResponse = id => {
    const newArray = this.state.currentResponse.filter(item => item !== id);
    this.setState({ currentResponse: newArray });
  };

  handleResponse = async () => {
    this.props.setAnswerQuizShot(
      this.props.match.params.quizid,
      this.state.currentResponse[0],
      this.state.idvideo,
      this.state.idcity,
      this.state.brand
    );
    this.props.quizAnswerResultreset();
  };

  render() {
    const { classes } = this.props;
    const { currentResponse } = this.state;
    return (
      <div>
        <div>
          <DialogRes
            onClose={this.handleClose}
            open={this.state.open}
            data={this.state.resData}
          />
          <div className={classes.container}>
            <div className={classes.sectionHeader}>
              <Link to="/home" className={classes.closebtn}>
                <IconButton
                  color="inherit"
                  aria-label="Close"
                  style={{ padding: 0 }}
                >
                  <CloseIcon className={classes.videoCloseIcn} />
                </IconButton>
              </Link>
            </div>
            {/*Content*/}
            <div className={classes.section}>
              <p className={classes.question}>{this.state.question}</p>
              <div style={{ width: "80%" }}>
                {this.state.suggestions.map((item, index) => (
                  <div
                    key={index}
                    onClick={() => this.handleAddResponse(index)}
                    className={classes.questionInd}
                    style={{
                      boxShadow: currentResponse.some(id => id === index)
                        ? "2px 9px 7px -7px"
                        : ""
                    }}
                  >
                    {currentResponse.some(id => id === index) ? (
                      <span className={classes.checkbox}>
                        <img src={check} className={classes.imgcheck} alt="" />
                      </span>
                    ) : (
                        <span className={classes.checkboxdis}></span>
                      )}

                    <p className={classes.inditext}>{item.value}</p>
                  </div>
                ))}
                <div
                  className={classes.btncontain}
                  onClick={this.handleResponse}
                >
                  <img
                    src={respQuiz}
                    className={classes.btnimg}
                    alt=""
                    id={`Reponse_Quiz_ChoixNumero:${this.state.currentResponse[0]}`}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    quizShotResult: state.QuizShot.quizShotResult,
    quizAnswerResult: state.quizAnswerResult.quizAnswerResult
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getQuizShotById: data => dispatch(getQuizShotById(data)),
    quizShotResultreset: () => dispatch(quizShotResult()),
    setAnswerQuizShot: (quizid, answer, video, city) =>
      dispatch(setAnswerQuizShot(quizid, answer, video, city)),
    quizAnswerResultreset: () => dispatch(quizAnswerResult())
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(QuizShot);
