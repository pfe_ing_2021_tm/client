import React from "react";
import ParticlesBg from "particles-bg";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import result from "./result/result.jpeg";
import log1 from "./result/section-1.jpeg";
import log2 from "./result/section-2.jpeg";
import log3 from "./result/section-3.jpeg";
import log4 from "./result/section-4.jpeg";
import log5 from "./result/section-5.jpeg";
import log6 from "./result/section-6.jpeg";
import log7 from "./result/section-7.jpeg";
import log8 from "./result/section-8.jpeg";
import log9 from "./result/section-9.jpeg";
import PropTypes from "prop-types";
import "./index.css";
const list = [
  { id: "0", mv: false, top: "0px", left: "0px", src: log8 },
  { id: "1", mv: false, top: "-100px", left: "100px", src: log4 },
  { id: "2", mv: false, top: "-200px", left: "200px", src: log7 },
  { id: "3", mv: false, top: "-200px", left: "0px", src: log6 },
  { id: "4", mv: false, top: "-300px", left: "100px", src: log2 },
  { id: "5", mv: false, top: "-400px", left: "200px", src: log1 },
  { id: "6", mv: false, top: "-400px", left: "0px", src: log3 },
  { id: "7", mv: false, top: "-500px", left: "100px", src: log9 },
  { id: "8", mv: false, top: "-600px", left: "200px", src: log5 },
];
const list0 = [
  { id: "0", mv: false, top: "0px", left: "0px", src: log1 },
  { id: "1", mv: false, top: "-100px", left: "100px", src: log2 },
  { id: "2", mv: false, top: "-200px", left: "200px", src: log3 },
  { id: "3", mv: false, top: "-200px", left: "0px", src: log4 },
  { id: "4", mv: false, top: "-300px", left: "100px", src: log5 },
  { id: "5", mv: false, top: "-400px", left: "200px", src: log6 },
  { id: "6", mv: false, top: "-400px", left: "0px", src: log7 },
  { id: "7", mv: false, top: "-500px", left: "100px", src: log8 },
  { id: "8", mv: false, top: "-600px", left: "200px", src: log9 },
];

export default class Puzzle extends React.Component {
  constructor(props) {
    super(props);
    this.onSwipe = this.onSwipe.bind(this);
    this.state = {
      tableInit: list,
      firstclick: false,
      secondclick: true,
      tabImage: list0.slice(),
      topPosSec: "",
      topPosFir: "",
      isfinished: false,
      leftPosSec: "",
      opendiag: false,
      leftPosFir: "",
      image1: "",
      image2: "",
      id1: 0,
      id2: 0,
      count: 0,
    };
  }

  componentDidMount() {}

  handleClose() {
    this.setState({ opendiag: !this.state.opendiag });
  }
  Reorder(list, startIndex, endIndex) {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  }
  onSwipe(box) {
    let tabs = list.slice();
    let item = box;
    let id1 = 0;
    if (item !== undefined) {
      if (this.state.firstclick === false && this.state.secondclick === true) {
        //firstclick
        var d = document.getElementById(item.id);
        this.state.ind1 = this.state.tableInit.findIndex(
          (t) => t.id === item.id
        );
        this.state.image1 = this.state.tableInit[this.state.ind1].src;

        this.setState({ firstclick: !this.state.firstclick });
        this.setState({ secondclick: !this.state.secondclick });
        if (
          this.state.tableInit[this.state.ind1].src ===
          this.state.tabImage[this.state.ind1].src
        ) {
          this.state.tableInit[this.state.ind1].mv = true;
        }
      } else {
        if (this.state.secondclick === false) {
          //secondclick
          var d = document.getElementById(item.id);
          this.state.ind2 = this.state.tableInit.findIndex(
            (t) => t.id === item.id
          );
          this.state.image2 = this.state.tableInit[this.state.ind2].src;

          this.state.tableInit[this.state.ind2].src = this.state.image1;
          this.state.tableInit[this.state.ind1].src = this.state.image2;
          if (
            this.state.tableInit[this.state.ind1].src ===
            this.state.tabImage[this.state.ind1].src
          ) {
            this.state.tableInit[this.state.ind1].mv = true;
          }
          if (
            this.state.tableInit[this.state.ind2].src ===
            this.state.tabImage[this.state.ind2].src
          ) {
            this.state.tableInit[this.state.ind2].mv = true;
          }
          let count = 0;
          let p = count + 1;
          for (let i = 0; i < 9; i++) {
            if (this.state.tableInit[i].mv === true) count++;
            this.state.count++;
          }
          if (count === 9) {
            setTimeout(() => this.setState({ opendiag: true }), 500);
          }
          //this.state.tableInit[this.state.ind2].left=this.state.leftPosFir;
          // this.state.tableInit.splice(this.state.ind1,0,this.state.tableInit[this.state.ind2]);
          //  this.state.tableInit.splice(this.state.ind2,0,this.state.tableInit[this.state.ind1]);

          this.setState({ firstclick: false });
          this.setState({ secondclick: !this.state.secondclick });
        }
      }

      this.state.tableInit = tabs;
    } else {
      return;
    }
    //switch between two values
  }
  dragStart = (v) => {};
  dragging = (v) => {};
  handleDrag(event) {}

  render() {
    const { imageUrl } = this.props;
    const list = this.state.tableInit;

    return (
      <>
        <Dialog
          className="dialog-box"
          open={this.state.opendiag}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle className="dialog-title" id="responsive-dialog-title">
            Félicitation &#128079;{" "}
          </DialogTitle>
          <DialogContent className="diag-content">
            <DialogContentText>
              félicitation! vous avez finis la partie du Puzzel avec succées.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClose.bind(this)}
              color="#2664A3"
              autoFocus
            >
              Confirmer
            </Button>
          </DialogActions>
        </Dialog>
        <ParticlesBg type="random" bg={true} />
        <div className="container-puzzle">
          <img
            className="box-item"
            style={{
              marginLeft: "15px",
              marginTop: "2px",
              boxShadow: "2px  2px 2px 2px #484848",
            }}
            src={`${result}`}
            alt="puzzle"
          />
          <div
            className="box-puzzle"
            style={{
              width: "300px",
              height: "300px",
              border: "1px solid #484848",
            }}
          >
            {list.map((item, index) => (
              <div
                id={index}
                className="box-item"
                onClick={(it) => this.onSwipe(item)}
                style={{
                  width: "100px",
                  height: "100px",
                  position: "relative",
                  top: `${item.top}`,
                  left: `${item.left}`,
                  backgroundSize: "100px 100px ",
                }}
              >
                <img src={`${props.imageUrl}`} alt="puzzle" />
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
Puzzle.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  pieces: PropTypes.shape({
    url: PropTypes.string,
    id: PropTypes.number,
  }),
};

Puzzle.defaultProps = {
  imageUrl: `${result}`,
};

//C:\Users\hp\Downloads\puzzlegame\src
