import React from "react";
//import ParticlesBg from "particles-bg";
import { compose } from "redux";
import { connect } from "react-redux";
import {
  getPuzzlelist,
  setPuzzleResultById,
  GetPuzzleResult,
  setPuzzleById,
} from "../../store/actions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import RedsIcon from "../../assets/img/endstore.png";
import { Link } from "react-router-dom";
import { history } from "../../history";
//import result from './result/result.jpeg';
import log1 from "./result/section-1.jpeg";
import log2 from "./result/section-2.jpeg";
import log3 from "./result/section-3.jpeg";
import log4 from "./result/section-4.jpeg";
import log5 from "./result/section-5.jpeg";
import log6 from "./result/section-6.jpeg";
import log7 from "./result/section-7.jpeg";
import log8 from "./result/section-8.jpeg";
import log9 from "./result/section-9.jpeg";
import "./index.css";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
const token = localStorage.getItem('token');
//const listzz = [{ id: "0", mv: false, top: '0px', left: '0px', src: log8 }, { id: "1", mv: false, top: '-100px', left: '100px', src: log4 }, { id: "2", mv: false, top: '-200px', left: '200px', src: log7 }, { id: "3", mv: false, top: '-200px', left: '0px', src: log6 }, { id: "4", mv: false, top: '-300px', left: '100px', src: log2 }, { id: "5", mv: false, top: '-400px', left: '200px', src: log1 }, { id: "6", mv: false, top: '-400px', left: '0px', src: log3 }, { id: "7", mv: false, top: '-500px', left: '100px', src: log9 }, { id: "8", mv: false, top: '-600px', left: '200px', src: log5 }];
const list = [
  { id: "0", mv: false, top: "0px", left: "0px", src: log8 },
  { id: "1", mv: false, top: "-100px", left: "100px", src: log4 },
  { id: "2", mv: false, top: "-200px", left: "200px", src: log7 },
  { id: "3", mv: false, top: "-200px", left: "0px", src: log6 },
  { id: "4", mv: false, top: "-300px", left: "100px", src: log2 },
  { id: "5", mv: false, top: "-400px", left: "200px", src: log1 },
  { id: "6", mv: false, top: "-400px", left: "0px", src: log3 },
  { id: "7", mv: false, top: "-500px", left: "100px", src: log9 },
  { id: "8", mv: false, top: "-600px", left: "200px", src: log5 },
];
let list0 = [
  { id: "0", mv: false, top: "0px", left: "0px", src: log1 },
  { id: "1", mv: false, top: "-100px", left: "100px", src: log2 },
  { id: "2", mv: false, top: "-200px", left: "200px", src: log3 },
  { id: "3", mv: false, top: "-200px", left: "0px", src: log4 },
  { id: "4", mv: false, top: "-300px", left: "100px", src: log5 },
  { id: "5", mv: false, top: "-400px", left: "200px", src: log6 },
  { id: "6", mv: false, top: "-400px", left: "0px", src: log7 },
  { id: "7", mv: false, top: "-500px", left: "100px", src: log8 },
  { id: "8", mv: false, top: "-600px", left: "200px", src: log9 },
];

class Puzzle extends React.Component {
  constructor(props) {
    super(props);
    this.onSwipe = this.onSwipe.bind(this);
    this.state = {
      tableInit: list,
      firstclick: false,
      secondclick: true,
      tabImage: list0.slice(),
      topPosSec: "",
      topPosFir: "",
      isfinished: false,
      leftPosSec: "",
      opendiag: false,
      leftPosFir: "",
      image1: "",
      image2: "",
      id1: 0,
      id2: 0,
      count: 0,
      cout0: "",
      cout1: "",
      title: "",
      origine: "",
      puzzles: list,
      imagelist: [],
      tablepuzzle: [],
      tabResult: [],
      initialTab: [],
      slices: [],
      finish: false,
      orientationScreen: "",
      message: "",
      maxresponse: false,
      start: false,
      puzzleid: this.props.match.params.puzzleid,
      token,
      videoItem:""
    };
  }

  componentWillMount() {
    if (this.state.token === null) {
      return history.push("/");
    }
    this.props.setPuzzleById(this.state.puzzleid);
    if (this.props.PuzzleList !== undefined) {
      this.setState({
        tablepuzzle: this.props.PuzzleList,
        videoItem: this.props.location.state.videoid
      });
      this.onSwipe();
    }
  }
  Close = () => {
    history.push("/home");
  };
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.puzzlePieces !== this.props.puzzlePieces) {
      let tempArray = [];
      let initialTab = [];
      this.props.puzzlePieces.pictures.forEach((element, index) => {
        list0[index].src = element.link;
        tempArray[index] = element.link;
        initialTab.push(element.link);
      });
      tempArray = this.shuffle(tempArray);
      tempArray.forEach((element, index) => {
        list[index].src = element;
      });
      this.setState({
        tabImage: list0.slice(),
        tableInit: list,
        origine: this.props.puzzlePieces.image,
        initialTab: initialTab,
        title: this.props.puzzlePieces.title,
      });
    } //result
    if (prevProps.result !== this.props.result) {
      this.setState({ opendiag: true, message: this.props.result.message });
    }
  }
  shuffle = (array) => {
    var currentIndex = array.length,
      temporaryValue,
      randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  };
  handleClose() {
    this.setState({ opendiag: !this.state.opendiag, finish: true });
    localStorage.setItem("finish", this.state.finish);
    history.push("/");
  }
  Reorder(list, startIndex, endIndex) {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  }

  isEqual(answerCustomer, answerTab) {
    // if length is not equal
    if (answerCustomer.length !== answerTab.length) return false;
    else {
      // comapring each element of array
      for (var i = 0; i < answerCustomer.length; i++)
        if (answerCustomer[i] !== answerTab[i]) return false;
      return true;
    }
  }
  onSwipe(box) {
    let tabs = list.slice();
    let item = box;
    if (item !== undefined) {
      if (this.state.firstclick === false && this.state.secondclick === true) {
        //firstclick
        let tableInit = this.state.tableInit;
        let index = tableInit.findIndex((t) => t.id === item.id);
        this.setState({
          firstclick: !this.state.firstclick,
          secondclick: !this.state.secondclick,
          image1: tableInit[index].src,
          index1: index,
        });
        if (tableInit[index].src === this.state.tabImage[index].src) {
          tableInit[index].mv = true;
          this.setState({ tableInit: tableInit });
        }
      } else {
        if (this.state.secondclick === false) {
          //secondclick
          let tableInit = this.state.tableInit;
          let index = tableInit.findIndex((t) => t.id === item.id);
          let image2 = tableInit[index].src;
          tableInit[index].src = this.state.image1;
          tableInit[this.state.index1].src = image2;
          if (
            tableInit[this.state.index1].src ===
            this.state.tabImage[this.state.index1].src
          ) {
            tableInit[this.state.index1].mv = true;
          }
          if (tableInit[index].src === this.state.tabImage[index].src) {
            tableInit[index].mv = true;
          }
          let tempTab = [];
          tableInit.forEach((t) => {
            tempTab.push(t.src);
          });

          if (this.isEqual(tempTab, this.state.initialTab)) {
            this.props.setPuzzleResultById(this.state.puzzleid, tempTab, this.props.location.state ? this.props.location.state.videoid : "");
          }
          this.setState({
            secondclick: !this.state.secondclick,
            firstclick: false,
            tableInit: tableInit,
          });
        }
      }
      this.setState({ tableInit: tabs });
    } else {
      return;
    }
  }

  render() {
    const { classes } = this.props;
    let orientation = "";
    var mql = window.matchMedia("(orientation: portrait)");
    if (mql.matches) {
    } else {
      orientation = "landscape";
    }
    return (
      <>
        <Dialog
          open={this.state.opendiag}
          aria-labelledby="simple-dialog-title"
          PaperProps={{
            classes: {
              root: classes.dialogPaper,
            },
            style: {
              backgroundColor: "transparent",
              boxShadow: "none",
              justifyContent: "center",
              overflow: "visible",
            },
          }}
        >
          <div
            style={{
              backgroundColor: "white",
              borderRadius: "6px",
              padding: "10px 10px 0px 10px",
            }}
          >
            <img
              src={RedsIcon}
              alt=""
              style={{
                width: 245,
                marginBottom: 5,
              }}
            />
            <p
              style={{
                textAlign: "center",
                color: "#919191",
                textTransform: "uppercase",
              }}
            >
              <span>
                Bonne réponse ! <br />
                Vous avez gagné <br />
                <span style={{ color: "#ed2a1c", lineHeight: 1.5 }}>
                  300 Reds
                </span>
              </span>

              <br />
            </p>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Link to="/home" id={`Reponse Reponse puzzel correct`}>
                <Button
                  id={`Reponse Reponse puzzel correct`}
                  variant="contained"
                  style={{
                    position: "relative",
                    bottom: "-15px",
                    background: "rgb(237, 42, 28)",
                    color: "rgb(255, 255, 255)",
                    marginLeft: "-32px",
                    borderRadius: "15px",
                    left: "16px",
                    padding: "7px 24px",
                  }}
                >
                  <div id={`Reponse puzzel correct`}>Valider</div>
                </Button>
              </Link>
            </div>
          </div>
        </Dialog>
        <IconButton
          style={{ zIndex: 10 }}
          color="inherit"
          onClick={this.Close}
          aria-label="Close"
          className={classes.videoCloseCont}
        >
          <CloseIcon className={classes.videoCloseIcn} style={{ zIndex: 10 }} />
        </IconButton>
        <div
          className="container-puzzle"
          style={{
            display: orientation === "portrait" ? "block" : "flex",
            flexDirection: orientation === "portrait" ? "column" : "row",
            justifyContent: "space-around",
            paddingTop: orientation === "portrait" ? "10px" : "5px",
          }}
        >
          {/*<img className="box-item" style={{ width: '300px', height: '300px', boxShadow: '2px  2px 2px 2px #484848' }} src={`${process.env.REACT_APP_Prefix_CF}${this.state.origine}`} alt="puzzle" /> */}
          <div
            className="box-puzzle"
            style={{
              width: "300px",
              height: "300px",
              border: "1px solid #484848",
              top: orientation === "portrait" ? 100 : 40,
            }}
          >
            {this.state.tableInit.map((item, index) => (
              <div
                className={
                  this.state.firstclick === true
                    ? "box-active"
                    : "box-nonactive"
                }
                draggable="true"
                onDragStart={(e) => {
                  this.dragStart(e);
                }}
                onClick={() => this.onSwipe(item)}
                style={{
                  width: "100px",
                  height: "100px",
                  position: "relative",
                  top: `${item.top}`,
                  left: `${item.left}`,
                  backgroundSize: "100px 100px ",
                }}
              >
                <img
                  className="box-contain"
                  id={index}
                  src={`${process.env.REACT_APP_DOMAIN}${item.src}`}
                  alt="puzzle"
                />
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    videoItem: state.videoItem.videoItem,
    PuzzleList: state.PuzzleList.PuzzleList,
    result: state.resultPuzzel.resultPuzzel,
    puzzlePieces: state.puzzlePieces.puzzlePieces,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPuzzlelist: () => dispatch(getPuzzlelist()),
    setPuzzleResultById: (id, table,videoid) =>
      dispatch(setPuzzleResultById(id, table,videoid)),
    GetPuzzleResult: () => dispatch(GetPuzzleResult()),
    setPuzzleById: (id) => dispatch(setPuzzleById(id)),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Puzzle);
