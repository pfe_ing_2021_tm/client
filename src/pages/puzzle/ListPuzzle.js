import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { getPuzzlelist, setPuzzleById } from "../../store/actions";
import "./index.css";
import Puzzle from "./index.js";
import { history } from "../../history";

class ListPuzzle extends React.Component {
  constructor(props) {
    super(props);
    this.handleMovetoPuzzle0 = this.handleMovetoPuzzle0.bind(this);

    this.state = {
      image1: "",
      image2: "",
      puzzles: [],
      imagelist: [],
      tablepuzzle: [],
      id1: 0,
      id2: 0,
      clicked0: false,
      clicked1: false,
      count: 0,
      piecesList: [],
    };
  }

  componentWillMount() {
    this.props.getPuzzlelist();

    // this.setState({ imagelist: this.props.PuzzleList })
    if (this.props.PuzzleList !== undefined) {
      //pieacesList
      this.setState({
        tablepuzzle: this.props.PuzzleList,
      });
    }
  }

  handleMovetoPuzzle0(id) {
    alert(id);
    this.setState({
      clicked0: !this.state.clicked0,
    });
    history.push(`/puzzle/${id}`);
    this.props.setPuzzleById(id);

    localStorage.setItem("numero", id);
  }

  render() {
    const { PuzzleList, puzzlePieces } = this.props;
    const tablepuzzle = puzzlePieces;
    const list = this.state.tableInit;
    let contain = this.state.tablepuzzle;
    let table0 = this.state.tablepuzzle[0];
    let table1 = this.state.tablepuzzle[1];
    let imagetitle1;
    let imagetitle0;
    let imagetitle2;
    let cout0 = 0;
    let cout1 = 1;
    let tabs = [];
    if (tablepuzzle !== undefined) {
      this.state.piecesList.push(tablepuzzle.pictures);
      localStorage.setItem("imageOrigine", tablepuzzle.image);
      localStorage.setItem("item0", tablepuzzle.pictures[0].link);
      localStorage.setItem("item1", tablepuzzle.pictures[1].link);
      localStorage.setItem("item2", tablepuzzle.pictures[2].link);

      localStorage.setItem("item3", tablepuzzle.pictures[3].link);
      localStorage.setItem("item4", tablepuzzle.pictures[4].link);
      localStorage.setItem("item5", tablepuzzle.pictures[5].link);

      localStorage.setItem("item6", tablepuzzle.pictures[6].link);
      localStorage.setItem("item7", tablepuzzle.pictures[7].link);
      localStorage.setItem("item8", tablepuzzle.pictures[8].link);
    }

    if (this.state.clicked0) {
      localStorage.setItem("idImage0", cout0);
    } else {
      if (this.state.clicked1) {
        localStorage.setItem("idImage1", cout1);
      }
    }
    for (let i = 0; i < this.state.tablepuzzle.length; i++) {
      if (this.state.tablepuzzle[i] !== undefined) {
      }
    }
    if (table0 !== undefined || table1 !== undefined) {
      let tablepuzzle = table0.pictures;
      imagetitle0 = this.state.tablepuzzle[0].image;
      imagetitle1 = this.state.tablepuzzle[1].image;
    }
    let listp = PuzzleList;
    console.table(" list of puzzle 1 ", table1);
    let cond = this.state.clicked;
    return (
      <>
        {this.state.clicked0 == true ? (
          <div>
            <Puzzle />
          </div>
        ) : (
          <>
            {contain.map((item, index) => (
              <div
                id={index}
                onClick={() => this.handleMovetoPuzzle0(item._id)}
              >
                <img
                  className="imagebox"
                  src={`${process.env.REACT_APP_Prefix_CF}${item.image}`}
                  alt="puzzle"
                />
              </div>
            ))}
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    PuzzleList: state.PuzzleList.PuzzleList,
    puzzlePieces: state.puzzlePieces.puzzlePieces,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPuzzlelist: () => dispatch(getPuzzlelist()),
    setPuzzleById: (id) => dispatch(setPuzzleById(id)),
  };
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ListPuzzle
);
