/**
 * @Author: <> with ❤ by Tarek MDIMAGH
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Tarek MDIMAGH
 * @Last modified time: 2019-07-03T09:48:47+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import backgroundImg from "../../assets/img/background.jpg";

export const styles = (theme) => ({
  root: {
    width: "100%",
  },
  backdrops: {
    background: "#000",
  },
  inputEntryCode: {
    width: 42,
    height: 45,
    background: "transparent",
    border: "1px solid #fff",
    margin: "0 5px",
    borderRadius: 5,
    color: theme.palette.secondary.main,
    fontSize: 36,
    textAlign: "center",
    lineHeight: "40px",
  },
  returnToHomebtn: {
    color: "rgb(255, 255, 255)",
    background: "none",
    border: "1px solid #666",
    padding: "5px 10px",
    borderRadius: 15,
    width: 240,
    cursor: "pointer",
    fontSize: 18,
    textAlign: "center",
  },
  paperprops: {
    width: "100%",
    background: "#000",
    justifyContent: "space-between",
    alignItems: "flex-start",
    display: "contents",
  },
  scrollPaper: {
    justifyContent: "flex-end",
  },
  imgLike: {
    width: 90,
    top: 25,
    position: "relative",
  },
  liketext: {
    color: "#0ede29",
    fontSize: 18,
    marginTop: 30,
    fontWeight: 800,
    textAlign: "center",
  },
  imgdLike: {
    width: 90,
    transform: "rotate(180deg)",
    top: 25,
    position: "relative",
  },
  btnnumb: {
    border: "1px solid #fff",
    borderRadius: 5,
    height: 42,
    width: 35,
    textAlign: "center",
    fontSize: 30,
    color: theme.palette.secondary.main,
    margin: "15px 10px",
    cursor: "pointer",
  },
  dliketext: {
    color: "#fe1100",
    fontSize: 18,
    marginTop: 30,
    fontWeight: 800,
    textAlign: "center",
  },
  bottomend: {
    color: theme.palette.secondary.main,
    display: "flex",
    position: "absolute",
    bottom: -79,
    left: 0,
    justifyContent: "space-between",
    width: "100%",
  },
  background: {
    background: `url(${backgroundImg})`,
    width: "100%",
    position: "relative",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundAttachment: "fixed",
  },
  sliderContainer: {
    width: "100%",
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "center",
    alignContent: "center",
  },
  sliderPoster: {
    width: "85%",
    marginBottom: -15,
  },
  vidcont: {},
  videoDescripCont: {
    display: "flex",
    justifyContent: "space-between",
    width: "70%",
    alignItems: "center",
    position: "absolute",
  },
  videoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  videoAuthor: {
    color: theme.palette.secondary.main,
  },
  playVideoAuthor: {
    color: "#999",
    fontSize: 13,
  },
  iconPlay: {
    width: 50,
    color: theme.palette.secondary.main,
    marginBottom: -7,
  },
  dialogPaper: {
    overflow: "hidden",
  },
  btnNext: {
    display: "flex",
    flexDirection: "row",
    marginTop: 25,
    color: theme.palette.secondary.main,
    justifyContent: "flex-end",
    width: "90%",
  },
  titleNext: {
    color: theme.palette.secondary.main,
  },
  iconNext: {
    fontSize: 20,
    color: "#C70013",
    marginLeft: 10,
  },
  bannerContainer: {
    width: "100%",
    justifyContent: "center",
    display: "flex",
    marginTop: 25,
  },
  bannerItem: {
    flexDirection: "column",
  },
  bannerIndicator: {
    width: 10,
    margin: "2px 5px",
  },
  bannerCarousel: {
    width: "85%",
  },
  playlistDescripCont: {
    display: "flex",
    justifyContent: "space-between",
    width: "90%",
    alignItems: "center",
    position: "absolute",
    paddingBottom: 15,
  },
  icntabhead: {
    color: theme.palette.secondary.main,
    position: "relative",
    top: 2,
  },
  playListTitle: {
    color: theme.palette.secondary.main,
    fontSize: 30,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  playVideoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  iconPlayAudio: {
    width: 30,
    color: theme.palette.secondary.main,
  },
  iconOff: {
    width: 20,
    color: theme.palette.secondary.main,
  },
  drivWhi: {
    position: "relative",
    top: -2,
    fontSize: 10,
    color: theme.palette.secondary.main,
  },
  galerieSmallItem: {
    height: 100,
    overflow: "hidden",
    marginBottom: 8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  progressBarCont: {
    background: "rgba(000, 000, 000, 0.8)",
    height: 35,
    padding: "0 0px 40px 0px",
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
  progressLine: {
    width: "100%",
    height: 3,
    position: "relative",
    top: -17,
  },
  videoProgreesTitle: {
    // display: "flex",
    // flexDirection: "row",
    // alignItems: "center",
    // justifyContent: "space-around",
    // width: " 30%",
    // height: " 90%"
    position: "absolute",
    left: "30px",
    top: "30%",
  },
  videoProgressBoxDesc: {
    display: "flex",
    alignItems: "flex-start",
  },
  progressImg: {
    width: 50,
    position: "relative",
    top: -8,
    left: 10,
  },
  progressIcn: {
    color: theme.palette.secondary.main,
    position: "relative",
    top: -10,
  },
  videoCloseCont: {
    position: "absolute",
    top: 10,

    borderRadius: 0,
    right: 0,
    padding: "5px 7px",
    zIndex: 1,
  },
  gobackOrClose: {},

  videoCloseIcn: {
    color: theme.palette.secondary.main,
    border: "1px solid #fff",
    borderRadius: 50,
    fontSize: 16,
  },
  boxcategory: {
    transformOrigin: "0 0 0",
    background: theme.palette.secondary.main,
    height: 125,
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    margin: 5,
    flex: 1,
  },
  headtabcont: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  conttab: {
    display: "flex",
    justifyContent: "space-between",
    width: "90%",
    alignItems: "center",
    alignContent: "center",
  },
  headtab: {
    color: theme.palette.secondary.main,
    textTransform: "uppercase",
    fontWeight: 500,
    borderBottom: "3px solid #ed2a1c",
    paddingBottom: 5,
  },
  slide: {
    perspective: 1000, // create perspective
    overflow: "hidden",
    // relative is a must if you want to create overlapping layers in children
    position: "relative",
    paddingTop: 8,
    /*[theme.breakpoints.up('sm')]: {
      paddingTop: 10
    },
    [theme.breakpoints.up('md')]: {
      paddingTop: 14
    }*/
  },
  imageContainer: {
    display: "flex",
    position: "relative",
    zIndex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    display: "block",
    width: "85%",
    objectFit: "cover",

    /*  [theme.breakpoints.up('sm')]: {
      marginLeft: '4%'
    }*/
  },
  arrow: {
    display: "none",
    position: "absolute",
    top: "50%",
    transform: "translateY(-50%)",
    /*  [theme.breakpoints.up('sm')]: {
      display: 'inline-flex'
    }*/
  },
  arrowLeft: {
    left: 0,
    // [theme.breakpoints.up("lg")]: {
    //   left: "-64px"
    // }
  },
  arrowRight: {
    right: 0,
    /*  [theme.breakpoints.up('lg')]: {
      right: -64
    }*/
  },
  text: {
    // shared style for text-top and text-bottom
    fontWeight: 900,
    position: "absolute",
    zIndex: 1,
    color: theme.palette.secondary.main,
    padding: "0 8px",
    lineHeight: 1.2,
    /*  [theme.breakpoints.up('sm')]: {
      padding: '0 16px'
    },
    [theme.breakpoints.up('md')]: {
      padding: '0 24px'
    }*/
  },
  title: {
    bottom: 20,
    zIndex: 9999,
    fontSize: 17,
    fontWeight: "500",
    textTransform: "uppercase",
    /*[theme.breakpoints.up('sm')]: {
      top: 40,
      fontSize: 72
    },
    [theme.breakpoints.up('md')]: {
      top: 52,
      fontSize: 72
    }*/
  },
  subtitle: {
    top: 60,
    left: "0%",
    height: "52%",
    fontSize: 56,
    background: "linear-gradient(0deg, rgba(255,255,255,0) 0%, #888888 100%)",
  },
  gameimg: {
    width: 200,
    marginLeft: 50,
  },
  dlikecont: {
    display: "flex",
    flexDirection: "column",
    margin: 20,
  },
  likecont: {
    display: "flex",
    flexDirection: "column",
    margin: 20,
  },
  endvideobox: {
    display: "flex",
    flexDirection: "row",
    marginRight: 10,
  },
  videoendcont: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  dividers: {
    width: 4,
  },
  btnreplay: {
    display: "flex",
    alignItems: "center",
  },
  btnreplayimg: {
    width: 30,
    marginRight: 15,
    transform: "rotate(-70deg)",
  },
  btnreplaytext: {
    textTransform: "uppercase",
    fontWeight: 700,
    fontSize: 20,
  },
  btnnextvideo: {
    display: "flex",
    alignItems: "center",
  },
  btnnextvideotext: {
    textTransform: "capitalize",
    fontSize: 17,
  },
  btnnextvideoicn: {
    width: 25,
    marginLeft: 15,
  },
  btnvalider: {
    width: 150,
    marginLeft: 15,
  },
});
