import Button from "@material-ui/core/Button"
import Checkbox from "@material-ui/core/Checkbox"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import IconButton from "@material-ui/core/IconButton"
import InputBase from "@material-ui/core/InputBase"
import Snackbar from "@material-ui/core/Snackbar"
import SnackbarContent from "@material-ui/core/SnackbarContent"
import { withStyles } from "@material-ui/core/styles"
import CheckCircleIcon from "@material-ui/icons/CheckCircle"
import CloseIcon from "@material-ui/icons/Close"
import ErrorIcon from "@material-ui/icons/Error"
import InfoIcon from "@material-ui/icons/Info"
import WarningIcon from "@material-ui/icons/Warning"
import classNames from "classnames"
import React from "react"
import { connect } from "react-redux"
import { compose } from "redux"
import bgLogin from "../../assets/img/bg-login.jpg"
import logoDiscovred from "../../assets/img/logoDiscovred.png"
import LOGOEPI from "../../assets/img/Logo_EPI_Sousse.png"
import { history } from "../../history"
import {
  dataSettingsFromWSocket, tryLogin, tryRegisterNickname, userData
} from "../../store/actions"
import "../home/styleSheet.css"
import { styles } from "./style"



const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
}

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props
  const Icon = variantIcon[variant]

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  )
}

const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent)

class Login extends React.Component {
  constructor(props) {
    super(props)
    const token = localStorage.getItem("token")
    this.state = {
      token,
      username: "",
      password: "",
      savepass: false,
      age: false,
      use: false,
      nextStep: true,
      open: false,
      errorvalue: "",
      nickname: "",
      placeholder: "",
      settings: null,
      presettings: null,
      pg: null,
      logo: logoDiscovred,
    }
  }

  componentDidMount() {
    if (this.state.token !== null) {
      return history.push("home")
    } else {
//       console.log("this.props.settings_ws", this.props.settings_ws)
//       console.log("this.props.settings", this.props.settings)

//        if (this.props.settings_ws && this.props.settings_ws.backgroundLogin)
//        {  this.setState({
//            bg:
//              process.env.REACT_APP_DOMAIN +
//              this.props.settings_ws.backgroundLogin,
//            logo: this.props.settings_ws.logo,
//          })
// }
// else{
//     if (this.props.settings && this.props.settings.backgroundLogin)
//       this.setState({
//         bg: process.env.REACT_APP_DOMAIN + this.props.settings.backgroundLogin,
//         logo: this.props.settings.logo,
//       })
//     this.props.dataSettingsFromWSocket(this.props.settings)
// }
      // var socket = socketIOClient(`${process.env.REACT_APP_DOMAIN}`, {
      //   query: `token=${this.state.token}`,
      // })
      // socket.on("backgrounLoginSend", (data) => {
      //   console.log("data", data)
      //   this.setState({
      //     settings: data,
      //     bg: process.env.REACT_APP_DOMAIN + data.backgroundLogin,
      //   })
      //   localStorage.setItem("extraColor", data.extraColor)
      //   localStorage.setItem("primaryColor", data.primaryColor)
      //   localStorage.setItem("secondaryColor", data.secondaryColor)
      // })
    }
  }

  handlepdf = (ch) => {
    if (ch == "cond") {
      let path = `https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/t%26c.pdf`
      return (window.location.href = path)
    } else if (ch == "conf") {
      let path = `https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/Consumer+privacy+notice+FRv2.pdf`
      return (window.location.href = path)
    } else if (ch == "sante") {
      let path = `https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/health-effects-of-smoking_fr.pdf`
      return (window.location.href = path)
    }
  }
  componentDidUpdate(prevProps, nextProps) {
  
    // if (prevProps.settings_ws !== this.props.settings_ws) {
    //   if (this.props.settings_ws && this.props.settings_ws.backgroundLogin)
    //     this.setState({
    //       bg:
    //         process.env.REACT_APP_DOMAIN +
    //         this.props.settings_ws.backgroundLogin,
    //       logo: this.props.settings_ws.logo,
    //     })
    // } else if (prevProps.settings !== this.props.settings || true) {
    //   console.log("iciiiiii")
    //   if (this.props.settings && this.props.settings.backgroundLogin)
    //    { this.setState({
    //       bg:
    //         process.env.REACT_APP_DOMAIN + this.props.settings.backgroundLogin,
    //       logo: this.props.settings.logo,
    //     })
    //   this.props.dataSettingsFromWSocket(this.props.settings)}
    // }

    if (
      prevProps.status !== this.props.status &&
      prevProps.status === "error"
    ) {
      this.handleClick("vous n'avez pas de pseudo")
    }
    if (
      prevProps.status !== this.props.status &&
      this.props.status === "errorLogin"
    ) {
      this.handleClick("Impossible de se connecter")
    }
    if (
      prevProps.nickname !== this.props.nickname &&
      this.props.isLoading === false &&
      this.props.nickname !== null &&
      this.props.status === "success"
      // this.props.status !== "error" &&
      // this.props.status !== "errorLogin" &&
      // this.props.status !== "Le Pseudo existe déjà"
    ) {
      let name = this.props.nickname
      this.setState({ nextStep: false, placeholder: name })
    } else {
      if (
        prevProps.status !== this.props.status &&
        this.props.status === "success"
      )
        window.location.reload()
    }

    if (
      prevProps.nickname !== this.props.nickname &&
      prevProps.nickname !== null &&
      this.props.status !== "error" &&
      this.props.status !== "errorLogin" &&
      this.props.status !== "Le Pseudo existe déjà"
    ) {
      let name = prevProps.nickname
      this.setState({ nextStep: false, placeholder: name })
    }

    if (
      prevProps.status !== this.props.status &&
      this.props.status === "Le Pseudo existe déjà"
    ) {
      this.handleClick("Le Pseudo existe déjà")
    }
  }

  handleChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
    })
  }
  //   handleChangeName =name => event=>{
  //     (typeof(this.state.username)==="number" && this.state.username.length<=8  )?
  //  this.setState({
  //       [name]:event.target.value
  //     }):this.setState({ [name]:''})

  //   }

  handleSavePass = (event) => {
    this.setState({ savepass: event.target.checked })
  }
  handleAgeCheck = (event) => {
    this.setState({ age: event.target.checked })
  }
  handleUseCheck = (event) => {
    this.setState({ use: event.target.checked })
  }
  /** This is a connection function. */
  handleConnection = () => {
    const data = {
      email: this.state.username,
      password: this.state.password,
    }
    this.props.tryLogin(data)
  }

  handleLastStep = () => {
    //to do register nickname
    if (this.state.nickname.trim() === "") {
      history.push("/home")
    } else {
      const data = {
        login: this.state.username,
        pseudo: this.state.nickname,
      }
      this.props.tryRegisterNickname(data)
    }
  }

  handleClick = (value) => {
    this.setState({ errorvalue: value, open: true })
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return
    }

    this.props.userData(null)
    this.setState({ open: false })
  }

  render() {
    const { classes } = this.props
    const { nextStep, settings, bg } = this.state
   
  

    return (
      <div
        style={{
          background:
            this.props.settings.backgroundLogin !== undefined
              ? `url(${
                  process.env.REACT_APP_DOMAIN +
                  this.props.settings?.backgroundLogin?.replace(/\\/g, "/")
                })`
              : `url(${bgLogin}`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        }}
        className={classes.container}
      >
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
        >
          <MySnackbarContentWrapper
            onClose={this.handleClose}
            variant="error"
            className={classes.margin}
            message={this.state.errorvalue}
          />
        </Snackbar>
         
          <img alt="logoEPI" src={LOGOEPI} className={classes.logo} />
        
        <div>
          <img
            src={
              this.props.settings.logo !== undefined
                ? process.env.REACT_APP_DOMAIN +
                  this.props.settings?.logo?.replace(/\\/g, "/")
                : logoDiscovred
            }
            alt=""
            className={classes.logo}
          />
        </div>
        {nextStep ? (
          <div>
            <div className={classes.forms}>
              <div className={classes.section} style={{ marginTop: 10 }}>
                <InputBase
                  id="user"
                  placeholder="Numéro de Telephone"
                  className={classes.textField}
                  value={this.state.username}
                  onChange={this.handleChange("username")}
                  classes={{
                    root: classes.bootstrapRoot,
                    input: classes.bootstrapInput,
                  }}
                  inputProps={{
                    maxLength: 8,
                  }}
                />

                <InputBase
                  id="password"
                  placeholder="Mot de passe"
                  type="password"
                  className={classes.textField}
                  value={this.state.password}
                  onChange={this.handleChange("password")}
                  classes={{
                    root: classes.bootstrapRoot,
                    input: classes.bootstrapInput,
                  }}
                />

                <div className={classes.checkContainer1}>
                  {/* <FormControlLabel
                    control={
                      <Checkbox
                        color="primary"
                        className={classes.checkbox}
                        checked={this.state.age}
                        onChange={this.handleAgeCheck}
                        value="age"
                      />
                    }
                    classes={{
                      label: classes.label,
                    }}
                    label="Je confirme être fumeur(se) agé(e) de plus de 18 ans"
                  /> */}
                  <FormControlLabel
                    control={
                      <Checkbox
                        color="primary"
                        className={classes.checkbox}
                        checked={this.state.use}
                        onChange={this.handleUseCheck}
                        value="use"
                      />
                    }
                    classes={{
                      label: classes.label,
                    }}
                    label="J'ai lu et j'accepte les conditions d'utilisation"
                  />
                </div>
                <Button
                  id="Se connecter"
                  variant="contained"
                  color="primary"
                  style={!this.state.use ? { backgroundColor: "gray" } : {}}
                  className={classes.button}
                  onClick={() =>
                    this.state.username !== "" &&
                    this.state.password !== "" &&
                    this.state.use
                      ? this.handleConnection()
                      : null
                  }
                >
                  <div id="Se connecter">Se connecter</div>
                </Button>
                <a
                  className={[classes.lien, classes.label].join(" ")}
                  //href="https://cloud.pmiclicks.com/login"
                >
                  Mot de passe oublié ?
                </a>

                <FormControlLabel
                  className={classes.checkContainer2}
                  control={
                    <Checkbox
                      color="primary"
                      className={classes.checkbox}
                      checked={this.state.savepass}
                      onChange={this.handleSavePass}
                      value="savepass"
                    />
                  }
                  classes={{
                    label: classes.label,
                  }}
                  label="Se souvenir de moi"
                />

                <>
                  <a
                    className={classes.label}
                    style={{
                      fontSize: 15,
                      marginBottom: "-5px",
                      cursor: "pointer",
                    }}
                    onClick={() => history.push("/signup")}
                  >
                    Vous n'avez pas un compte ?
                    {/* <span className={classes.redsText}> cliquez ici </span> */}
                  </a>
                  <div>
                    <Button
                      id="s'inscrire"
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      style={{ marginBottom: 20 }}
                      onClick={() => history.push("/signup")}
                    >
                      <div id="s'inscrire">s'inscrire</div>
                    </Button>
                  </div>
                </>
              </div>
            </div>
            <div className={classes.f}>
              <div className={classes.bottomLightContainer}>
                {/* <p className={classes.largeText}>
                  Philip Morris services S.A © 2020. tous droits réservés.
                  Prière de ne pas partager sur aucun canal de média social ces
                  publications (y compris des images et des textes) ou des
                  hastags liés à nos marques et produits. Pour plus
                  d’informations, visitez notre site: https://www.pmi.com
                </p> */}
              </div>
            </div>
            <div className={classes.bottomContainer}></div>
          </div>
        ) : (
          <div className={classes.section}>
            <p
              style={{
                color: "#fff",
                fontFamily: "NeulandGroteskCondensedRegular",
                textAlign: "center",
                fontSize: 36,
              }}
            >
              Félicitations !
              <br /> Vous avez gagné <br />
              <span
                style={{
                  color: "#ED2A1C",
                  marginTop: 10,
                  // fontFamily: "NeulandGroteskCondensedRegular",
                  // textAlign: "center",
                  // fontSize: 36
                }}
              >
                700 STARS
              </span>
            </p>
            <p
              style={{
                color: "#fff",
                fontFamily: "NeulandGroteskCondensedRegular",
                textAlign: "center",
                fontSize: 36,
              }}
            >
              Merci de choisir
              <br /> votre nom d'utilisateur
            </p>
            <InputBase
              id="outlined-name"
              placeholder="Nom d'utilisateur"
              className={classes.textField}
              placeholder={this.state.placeholder}
              value={this.state.nickname}
              onChange={this.handleChange("nickname")}
              classes={{
                root: classes.bootstrapRoot,
                input: classes.bootstrapInput,
              }}
            />

            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={this.handleLastStep}
              style={{ marginBottom: 35 }}
            >
              Valider
            </Button>
            <p className={classes.footerNextStep}>
              avis important : fumer nuit à la santé
            </p>
          </div>
        )}
      </div>
    )
  }
}
const mapStateToProps = (state) => {
 
  return {
    isLoading: state.ui.isLoading,
    status: state.status.status,
    userInfo: state.User.userInfo,
    token: state.token.token,
    nickname: state.nickname.nickname,
    settings: state.settings.settings,
    settings_ws: state.settings_ws.settings_ws,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    tryLogin: (data) => dispatch(tryLogin(data)),
    userData: () => dispatch(userData(null, null, null, null)),
    tryRegisterNickname: (data) => dispatch(tryRegisterNickname(data)),
    dataSettingsFromWSocket: () => dispatch(dataSettingsFromWSocket()),
  }
}
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Login)
