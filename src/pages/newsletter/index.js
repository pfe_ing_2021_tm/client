import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import backGround from "../../assets/img/newsLetterBackGround.png";
import YouDecide from "../../assets/img/you_decide.png";
import Tel from "../../assets/img/tel.png";
import footerbg from "../../assets/img/footer.png";
import newsletterBG from "../../assets/img/newsletterBG.png";

const NewsLetter = (props) => {
  const { classes } = props;
  //const [loading, setLoading] = React.useState(false);
  return (
    <div className={classes.Container}>
      <div className={classes.HeaderContainer}>
        {" "}
        <img src={backGround} style={{ width: "100%" }} />
        <div className={classes.header}>
          <div className={classes.label}>Cher(e) Membre , </div>
          <div className={classes.label2}>
            Quelques pas vous séparent de l'univers DiscoveRED!{" "}
          </div>
        </div>
      </div>

      <div
        style={{
          backgroundImage: `url(${newsletterBG})`,
          backgroundSize: "cover",
        }}
        className={classes.Container}
      >
        <div className={classes.paragraphContainer}>
          <div className={classes.paragraph}>
            <div>Entrez sur www.scanpack.com</div>
            <div>Prenez en photo la face de votre paquet</div>
            <div>Confirmez que vous avez l'âge requis</div>
            <div>Regardez ce que votre paquet a à vous raconter</div>
            <div>Cliquez sur le lien "S'INSCRIRE"</div>
            <div>Inscrivez-vous avec votre numéro de téléphone</div>
            <div>Insérez le code reçue par SMS</div>
            <div>Valider votre mot de passe</div>
          </div>
          <img src={Tel} style={{ width: "22%", marginTop: "4%" }} />
        </div>
        <div className={classes.center}>
          Et le monde DiscoveRed S'ouvre a vous !
        </div>
        <div className={classes.separator} />
        <div className={classes.paragraph} style={{ marginTop: "2%" }}>
          Une fois connecté (e), vous aurez la possibilité de :
        </div>
        <div className={classes.paragraph}>
          <div>Vous désabonner des communications reçues</div>
          <div>
            Sélectionner votre moyen de communication préféré (email/SMS)
          </div>
          <div>Changer votre mot de passe</div>
          <div>Consulter vos données stockées</div>
          <div>Supprimer votre compte et vos données</div>
          <div>
            Revoir les conditions d'utilisation et politique de confidentalité
          </div>
        </div>
        <img className={classes.YouDecide} src={YouDecide} />
        <div className={classes.btnScanpack}>
          <center>Aller sur Scanpack.com </center>
        </div>
      </div>
    </div>
  );
};

export default withStyles(styles)(NewsLetter);
