// import React, { Component } from 'react';
// import { Player, ControlBar } from 'video-react';
// import Typography from "@material-ui/core/Typography";
// import "./stylePlayer.css";
// import { withStyles } from "@material-ui/core/styles";
// import { styles } from "./style";
// import "video-react/dist/video-react.css"; // import css


// const sources = {
//     sintelTrailer: "http://media.w3.org/2010/05/sintel/trailer.mp4",
//     bunnyTrailer: "http://media.w3.org/2010/05/bunny/trailer.mp4",
//     bunnyMovie: "http://media.w3.org/2010/05/bunny/movie.mp4",
//     test: "http://media.w3.org/2010/05/video/movie_300.webm"
// };

// class VideoExtra extends Component {
//     constructor(props) {
//         super(props);
//         window.screen.orientation.lock("landscape");
//         this.state = {
//             sources: null,
//             open: false,
//             openNotAction: false,
//             url: null,
//             pip: false,
//             playing: false,
//             controls: false,
//             light: false,
//             volume: 0.8,
//             muted: false,
//             played: 0,
//             loaded: 0,
//             duration: 0,
//             playbackRate: 1.0,
//             loop: false,
//             portrait: "",
//             videoend: false,
//             progressBar: true,
//             playstart: false,
//             videoIndex: null, //this.props.match.params.id,
//             timeToShow: false,
//             currentTime: 0,
//             seeking: false
//         }
//         this.play = this.play.bind(this);
//         this.pause = this.pause.bind(this);
//         this.load = this.load.bind(this);
//         this.changeCurrentTime = this.changeCurrentTime.bind(this);
//         this.seek = this.seek.bind(this);
//         this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
//         this.changeVolume = this.changeVolume.bind(this);
//         this.setMuted = this.setMuted.bind(this);
//     }
//     handleStateChange = (state, prevState) => {
//         this.setState({
//             player: state,
//             currentTime: state.currentTime
//         });
//     };
//     handleClickOpen = async () => {
//         await this.setState({ open: true });
//         await this.play();
//     };
//     handleClose = () => {
//         this.setState({ open: false, playing: false });
//     };
//     onDuration = duration => {
//         this.setState({ duration });
//     };
//     onProgress = state => {
//         // We only want to update time slider if we are not currently seeking
//         if (!this.state.seeking) {
//             this.setState(state);
//         }
//     };
//     onClickVideo = () => {
//         this.setState({ progressBar: true });
//         /*setTimeout(
//           function() {
//             this.setState({ progressBar: false });
//           }.bind(this),
//           5000
//         );*/
//     };

//     ref = player => {
//         this.player = player;
//     };

//     onSeekMouseDown = e => {
//         this.setState({ seeking: true });
//     };

//     onSeekChange = e => {
//         this.setState({ played: e.target.value });
//     };

//     onSeekMouseUp = e => {
//         this.setState({ seeking: false });
//         this.player.seekTo(parseFloat(e.target.value));
//     };

//     onSeekMouseUp = e => {
//         this.setState({ seeking: false });
//         this.player.seekTo(parseFloat(e.target.value));
//     };

//     play() {
//         this.refs.player.play();


//     }

//     pause() {
//         this.refs.player.pause();
//     }

//     load() {
//         this.refs.player.load();
//     }

//     changeCurrentTime(seconds) {
//         return () => {
//             const { player } = this.refs.player.getState();
//             const currentTime = player.currentTime;
//             this.refs.player.seek(currentTime + seconds);
//         };
//     }

//     seek(seconds) {
//         return () => {
//             this.refs.player.seek(seconds);
//         };
//     }

//     changePlaybackRateRate(steps) {
//         return () => {
//             const { player } = this.refs.player.getState();
//             const playbackRate = player.playbackRate;
//             this.refs.player.playbackRate = playbackRate + steps;
//         };
//     }

//     changeVolume(steps) {
//         return () => {
//             const { player } = this.refs.player.getState();
//             const volume = player.volume;
//             this.refs.player.volume = volume + steps;
//         };
//     }

//     setMuted(muted) {
//         return () => {
//             this.refs.player.muted = muted;
//         };
//     }

//     changeSource(name) {
//         return () => {
//             this.setState({
//                 source: sources[name]
//             });
//             this.refs.player.load();
//         };
//     }

//     render() {
//         const { classes, title, id, video } = this.props
//         return (
//             <Player>
//                 <source ref="player" src={video} />
//                 <ControlBar>
//                     <div className={classes.videoProgreesTitle}>
//                         <div className={classes.videoProgressBoxDesc}>
//                             <div style={{ marginLeft: 20 }}>
//                                 <Typography
//                                     component="p"
//                                     className={classes.playVideoTitle}
//                                 >
//                                     {this.props.titleplay}
//                                 </Typography>
//                                 <Typography
//                                     component="p"
//                                     className={classes.playVideoAuthor}
//                                 >
//                                     {title}
//                                 </Typography>
//                             </div>
//                         </div>
//                     </div>
//                 </ControlBar>
//             </Player>


//         );
//     }
// }

// export default withStyles(styles)(VideoExtra);
