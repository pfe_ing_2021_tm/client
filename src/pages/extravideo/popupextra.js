import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import ligne from "../../assets/img/ligne.png";
import replayIcn from "../../assets/img/replay.png";
import nextIcn from "../../assets/img/next.png";
import { styles } from "./style";
import "./stylePlayer.css";
import { history } from "../../history";
import { Link } from "react-router-dom";
import { Typography } from "@material-ui/core";

class EndVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      nextAction: this.props.typeplay
    };
  }
  componentDidMount() {
    const data = {
      idcustomer: localStorage.getItem("_id"),
      idvideo: this.props._id
    };
  }
  handleLangChangeStart = type => {
    if (type === "extra") {
      localStorage.setItem("sectionSelected", "recette");
      history.push("/home");
      this.setState({ open: false });
      //this.props.onStartPlay();
    }
  };
  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;
    const { nextAction } = this.state;
    let actionType;

    // if (nextAction === "extra") {
    //     actionType = (
    //         <p style={{ color: 'white' }}
    //             onClick={() => this.handleLangChangeStart("extra")}

    //         >vous avez une recette a voir</p>
    //     );
    // }

    return (
      <Dialog
        BackdropProps={{
          classes: {
            root: classes.backdrops
          }
        }}
        PaperProps={{
          classes: {
            root: classes.paperprops
          }
        }}
        open={this.props.open}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <div className="rota">
          <div className={classes.discoverContainer}>
            <div>
              <Typography component="h5" className={classes.discoverText}>
                Merci d'avoir regardé la vidéo ! Découvrer ces recettes
                incroyables !
              </Typography>
            </div>

            <div onClick={() => this.handleLangChangeStart("extra")}>
              <Typography component="h4" className={classes.discoverButton}>
                Découvrir
              </Typography>
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}
EndVideo.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string
};

export default withStyles(styles)(EndVideo);
