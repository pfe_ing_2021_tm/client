/**
 * @Author: <> with ❤ by Nessrine Khedhiri
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Nessrine Khedhiri
 * @Last modified time: 2019-11-03T09:48:47+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 **/

import React, { Component } from "react";

import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Dialog from "@material-ui/core/Dialog";

import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./style";
import "video-react/dist/video-react.css";
import { Player, ControlBar, ForwardControl } from "video-react";

import EndVideo from "./popupextra";
import { compose } from "redux";
import "./stylePlayer.css";
import { history } from "../../history";
import CloseIcon from "@material-ui/icons/Close";

import IconButton from "@material-ui/core/IconButton";
import {
  getExtrabyid,
  setVideoItem,
  setvideohistory
} from "../../store/actions";

const sources = {
  sintelTrailer: "http://media.w3.org/2010/05/sintel/trailer.mp4",
  bunnyTrailer: "http://media.w3.org/2010/05/bunny/trailer.mp4",
  bunnyMovie: "http://media.w3.org/2010/05/bunny/movie.mp4",
  test: "http://media.w3.org/2010/05/video/movie_300.webm"
};
class ExtraVideo extends Component {
  constructor(props) {
    super(props);
    // window.screen.orientation.lock("landscape");
    this.state = {
      videoTitle: null,
      videoId: null,
      videoExtraTo: null,
      image: "",
      source: sources.bunnyMovie,
      currentSrc: "http://media.w3.org/2010/05/bunny/movie.mp4",
      duration: 596.48,
      currentTime: 596.48,
      seekingTime: 0,
      buffered: {},
      waiting: false,
      seeking: false,
      paused: true,
      autoPaused: false,
      ended: false,
      playbackRate: 2.1,
      muted: false,
      volume: 1,
      readyState: 4,
      networkState: 1,
      videoWidth: 853,
      videoHeight: 480,
      hasStarted: true,
      userActivity: false,
      isActive: false,
      isFullscreen: false,
      activeTextTrack: null,
      error: null,
      src: "",
      srcObject: null,
      crossOrigin: null,
      preload: " auto",
      defaultPlaybackRate: 1,
      played: {},
      seekable: {},
      autoplay: true,
      loop: false,
      controls: false,
      defaultMuted: false,
      textTracks: {},
      width: 0,
      height: 0,
      poster: ""
    };

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.load = this.load.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
    this.seek = this.seek.bind(this);
    this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
    this.changeVolume = this.changeVolume.bind(this);
    this.setMuted = this.setMuted.bind(this);
  }

  componentDidMount() {
    // subscribe state change
    // this.player.subscribeToStateChange(this.handleStateChange.bind(this));

    this.props.getExtrabyid(this.props.match.params.extraid);
  }
  async componentWillReceiveProps(prevProps) {
    if (prevProps.videoExtra !== this.props.videoExtra) {
      await this.setState({
        videoTitle: prevProps.videoExtra.title,
        videoId: prevProps.videoExtra._id,
        videoType: prevProps.videoExtra.type,
        videoExtraTo: prevProps.videoExtra.link,
        image: prevProps.videoExtra.image,
        videoMetaType: prevProps.videoExtra.metaType,
        videoQuizz: prevProps.videoExtra.quizz,
        videoExtra: prevProps.videoExtra.video, //id for video
        videoLink: prevProps.videoExtra.link,
        videoHauteur: prevProps.videoExtra.hauteur,
        videoLargeur: prevProps.videoExtra.largeur,
        videotime: prevProps.videoExtra.time,
        videoDuration: prevProps.videoExtra.duration
      });
      await this.load();
      await this.play();
      await this.refs.player.subscribeToStateChange(
        this.handleStateChange.bind(this)
      );
    }
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    // this.load = this.load.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
    this.seek = this.seek.bind(this);
    this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
    this.changeVolume = this.changeVolume.bind(this);
    this.setMuted = this.setMuted.bind(this);
  }

  setMuted(muted) {
    return () => {
      this.refs.player.muted = muted;
    };
  }

  handleStateChange(state) {
    // copy player state to this component's state
    this.setState({
      player: state,
      currentTime: state.currentTime
    });
  }

  play() {
    this.refs.player.play();
  }

  pause() {
    this.refs.player.pause();
  }

  load() {
    this.refs.player.load();
  }

  changeCurrentTime(seconds) {
    return () => {
      const { player } = this.refs.player.getState();
      const currentTime = player.currentTime;
      this.refs.player.seek(currentTime + seconds);
    };
  }

  seek(seconds) {
    return () => {
      this.refs.player.seek(seconds);
    };
  }

  changePlaybackRateRate(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const playbackRate = player.playbackRate;
      this.refs.player.playbackRate = playbackRate + steps;
    };
  }

  changeVolume(steps) {
    return () => {
      const { player } = this.player.getState();
      this.player.volume = player.volume + steps;
    };
  }

  changeVolume(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const volume = player.volume;
      this.refs.player.volume = volume + steps;
    };
  }
  componentWillUnmount() {
    this.props.setVideoItem();
    localStorage.removeItem("lastScreen");
    this.load();
  }
  VideoEnd = () => {
    let video = {};
    video.id = this.state.videoId;
    video.timespent = this.state.currentTime;
    video.type = this.state.videoType
    this.props.setvideohistory(video);
    //this.props.setvideohistory(this.state.videoId);
    //if user from section codeChallenge goBack()

    const lastscreen = localStorage.getItem("lastScreen");
    if (lastscreen) {
      history.goBack();
    } else if (this.state.videoExtraTo) {
      this.setState({ open: true });
    } else {
      this.setState({ openNotAction: true });
    }

    // this.props.canplayquizzreset();
  };
  gobackOrClose = () => {
    let video = {};
    video.id = this.state.videoId;
    video.timespent = this.state.currentTime;
    video.type = this.state.videoType
    this.props.setvideohistory(video);
    if (localStorage.getItem("lastScreen")) {
      history.goBack();
    } else {
      history.push("/home");
    }
  };

  render() {
    const { classes } = this.props;
    // console.log('type video', this.state.videoType)
    // console.log('link source', sources.bunnyMovie)
    // console.log('id', this.props.videoExtra)
    // console.log('props', this.props.videoExtra)
    // console.log('videolik', this.state.videoExtraTo)
    return this.state.videoLink !== null ? (
      <div>
        <Dialog
          fullScreen
          open={true}
          PaperProps={{ classes: { root: classes.dialogPaper } }}
          style={{ overflow: "hidden " }}
        >
          <div className={classes.vidcont}>
            <IconButton
              style={{ zIndex: 10 }}
              color="inherit"
              onClick={this.gobackOrClose}
              aria-label="Close"
              className={classes.videoCloseCont}
            >
              <CloseIcon
                className={classes.videoCloseIcn}
                style={{ zIndex: 10 }}
              />
            </IconButton>
            <EndVideo
              open={this.state.open}
              typeplay={this.state.videoType}
              _id={this.state.videoId}
            />

            <div className={classes.vidcont}>
              <div>
                <div>
                  <Player
                    playsInline
                    ref="player"
                    autoPlay
                    //src={this.state.videoExtraTo}
                    onEnded={this.VideoEnd}
                  >
                    <source src={this.state.videoLink} />

                    {/* <div
                      onClick={this.gobackOrClose}
                      style={{
                        zIndex: 10,
                        marginTop: "2%"
                      }}
                    >
                      <IconButton
                        style={{
                          zIndex: 10,
                          width: "auto",
                          height: "1px"
                        }}
                        color="inherit"
                        aria-label="Close"
                        className={classes.videoCloseCont}
                      >
                        <CloseIcon
                          className={classes.videoCloseIcn}
                          style={{ zIndex: 10 }}
                        />
                      </IconButton>
                    </div> */}

                    <ControlBar>
                      <div className={classes.videoProgreesTitle}>
                        <div className={classes.videoProgressBoxDesc}>
                          <div style={{ marginLeft: 20 }}>
                            <Typography
                              component="p"
                              className={classes.playVideoTitle}
                            >
                              {this.props.titleplay}
                            </Typography>
                            <Typography
                              component="p"
                              className={classes.playVideoAuthor}
                            >
                              {this.state.videoTitle}
                            </Typography>
                          </div>
                        </div>
                      </div>
                    </ControlBar>
                  </Player>
                </div>
              </div>
            </div>
          </div>
        </Dialog>
      </div>
    ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh"
          }}
        >
          <CircularProgress className={classes.progress} color="secondary" />
        </div>
      );
  }
}
const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    videoExtra: state.videoExtra.videoExtra
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getExtrabyid: extraid => dispatch(getExtrabyid(extraid)),
    // getCanPlayQuizz: idvideo => dispatch(getCanPlayQuizz(idvideo)),
    //   canplayquizzreset: () => dispatch(canplayquizzreset()),
    setvideohistory: id => dispatch(setvideohistory(id)),
    setVideoItem: () => dispatch(setVideoItem({}))
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(ExtraVideo);
