/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-03-02 18:45:08
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-19 14:12:58
 */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputBase from "@material-ui/core/InputBase";
import logoDiscovred from "../../assets/img/logoDiscovred.png";
import { compose } from "redux";
import classNames from "classnames";
import { connect } from "react-redux";
import {
  tryLogin,
  userData,
  tryRegisterNickname,
  sendContact,
  getUserInfo,
  sendGifts,
} from "../../store/actions"
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";
import CloseIcon from "@material-ui/icons/Close";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import WarningIcon from "@material-ui/icons/Warning";
import { history } from "../../history";
import { styles } from "./style";
import bgContact from "../../assets/img/bgContact.png";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import backprofile from "../../assets/img/backprofile.png";
import InputAdornment from "@material-ui/core/InputAdornment";
import "../home/styleSheet.css";
import FormHelperText from "@material-ui/core/FormHelperText";
import { CircularProgress } from "@material-ui/core";

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent);

class SendGifts extends React.Component {
  constructor(props) {
    super(props)
    const token = localStorage.getItem("token")
    this.state = {
      token,
      username: "",
      mobile: "",
      sujet: "",
      textarea: "",
      savepass: false,
      age: false,
      use: false,
      nextStep: true,
      open: false,
      error: false,
      errorvalue: "",
      nickname: "",
      placeholder: "",
      usernameValid: false,
      mobileValid: false,
      startsToSend: "",
      textareaValid: false,
      startsToSendValid: false,
      mobileErrMsg: "",
    }
  }
  checkValidation = () => {

    if (this.state.mobile === "") {
      this.setState({
        mobileValid: true,
        mobileErrMsg: "Numéro téléphone champ obligatoire !",
      })
    } else if (this.state.mobile.length !== 8) {
      this.setState({
        mobileValid: true,
        mobileErrMsg:
          " le numéro de téléphone doit être composé de 8 chiffres !",
      })
    }
    if (this.state.startsToSend === "") {
      this.setState({ startsToSendValid: true })
    }
    
  }
  componentDidMount() {
    if (this.state.token !== null) {
      //return history.push("home");
    }
    this.setState({ error: false })
    this.props.getUserInfo()
  }

  componentDidUpdate(prevProps, nextProps) {
    //console.log("prevProps : ", prevProps);
    //console.log("nextProps : ", nextProps);

     if (prevProps.sendGiftsP !== this.props.sendGiftsP) {
       console.log("this.props.sendGifts", this.props.sendGiftsP)
       this.handleClick(this.props.sendGiftsP.message , true)
     }

     if (prevProps.userInfo !== this.props.userInfo) {
       console.log("userInfo", this.props.userInfo)
     }
    
    if (
      prevProps.status !== this.props.status &&
      prevProps.status === "error"
    ) {
      this.handleClick("vous n'avez pas de pseudo")
    }
    if (prevProps.status !== this.props.status && prevProps.status === null) {
      this.handleConnection("")
    }

    if (
      prevProps.status !== this.props.status &&
      this.props.status === "errorLogin"
    ) {
      this.handleClick("Impossible de se connecter")
    }

    if (prevProps.contact !== this.props.contact) {
      //console.log("this.props.contact : ", this.props.contact)
      if (this.props.contact.status === "success") {
        return history.push("/")
      }
    }

    if (
      prevProps.nickname !== this.props.nickname &&
      prevProps.nickname !== null &&
      this.props.status !== "error" &&
      this.props.status !== "errorLogin" &&
      this.props.status !== "Le Pseudo existe déjà"
    ) {
      let name = "Guest-" + prevProps.nickname
      this.setState({ nextStep: false, placeholder: name })
    }
    if (
      prevProps.status !== this.props.status &&
      this.props.status === "Le Pseudo existe déjà"
    ) {
      this.handleClick("Le Pseudo existe déjà")
    }
  }

  handleChange = (name) => (event) => {
    //console.log("this.state.mobile.length", this.state.mobile.length);
   
    if (name === "mobile") {
      this.setState({ mobileValid: false })
    }

    if (name === "startsToSend") {
      this.setState({ startsToSendValid: false })
    }
   
    this.setState({
      [name]: event.target.value,
    })
  }
  //   handleChangeName =name => event=>{
  //     (typeof(this.state.username)==="number" && this.state.username.length<=8  )?
  //  this.setState({
  //       [name]:event.target.value
  //     }):this.setState({ [name]:''})

  //   }

  handleSavePass = (event) => {
    this.setState({ savepass: event.target.checked })
  }
  handleAgeCheck = (event) => {
    this.setState({ age: event.target.checked })
  }
  handleUseCheck = (event) => {
    this.setState({ use: event.target.checked })
  }
  /** This is a connection function. */
  handleConnection = () => {
    const data = {
      mobile: this.state.mobile,
      startsToSend: this.state.startsToSend,
    }
    this.props.sendGifts(data)
  }

  handleLastStep = () => {
    //to do register nickname
    const data = {
      login: this.state.username,
      pseudo:
        this.state.nickname === ""
          ? this.state.placeholder
          : this.state.nickname,
    }
    this.props.tryRegisterNickname(data)
  }

  handleClick = (value , flag) => {
    this.setState({ errorvalue: value, open: true  })
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return
    }

    this.props.userData(null)
    this.setState({ open: false })
  }

  render() {
    const { classes } = this.props
    const { nextStep } = this.state
    //console.log("this.state.mobile.length", this.state.mobile.length);
    return (
      <div
        style={{
          background: `url(${bgContact})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          height: "100%",
        }}
        className={classes.container}
      >
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
        >
          <MySnackbarContentWrapper
            onClose={this.handleClose}
            variant={
              this.props.sendGiftsP && this.props.sendGiftsP.status
                ? "success"
                : "error"
            }
            className={classes.margin}
            message={this.state.errorvalue}
          />
        </Snackbar>
        <div>
          <Link to="/">
            <div
              style={{
                display: "flex",
                alignItems: "center",
                paddingLeft: "7%",
                marginTop: 25,
                marginBottom: 15,
              }}
            >
              <img
                src={backprofile}
                style={{ width: 35, marginRight: 15 }}
                alt=""
              />
            </div>
          </Link>
          <div className={classes.Titletext}>
            <p>Aidez vos amis en envoyant des "STARTS"</p>
            <p className={classes.Titletextsub}>
              Mes Starts ={" "}
              {this.props.userInfoData && this.props.userInfoData.reds}
            </p>
          </div>
        </div>

        <div style={{ width: "100%" }}>
          <div className={classes.forms}>
            <div className={classes.section}>
              {/* <InputBase
                  id="username"
                  placeholder="Nom d'utilisateur"
                  className={classes.textField}
                  value={this.state.username}
                  onChange={this.handleChange("username")}
                  classes={{
                    root: classes.bootstrapRoot,
                    input: classes.bootstrapInput
                  }}
                /> */}

              <TextField
                error={this.state.mobileValid ? true : false}
                helperText={
                  this.state.mobileValid ? this.state.mobileErrMsg : null
                }
                id="mobile"
                label="Numéro téléphone"
                placeholder="Numéro téléphone"
                className={classes.textField}
                margin="normal"
                value={this.state.mobile}
                onInput={(e) => {
                  if (!e.target.value) {
                    e.target.value = ""
                  } else {
                    if (parseInt(e.target.value)) {
                      e.target.value = Math.max(0, parseInt(e.target.value))
                        .toString()
                        .slice(0, 8)
                    } else {
                      e.target.value = ""
                      //console.log("mobile");
                      this.setState({ mobileValid: false })
                    }
                  }
                }}
                onChange={this.handleChange("mobile")}
                InputProps={{
                  className: "digitsOnly",
                  classes: {
                    underline: classes.input,
                    input: classes.textInputf,
                  },
                  style: {
                    color: "white",
                    borderBottom: "1px solid",
                  },
                }}
                InputLabelProps={{
                  classes: {
                    root: classes.input,
                    focused: classes.input,
                  },
                }}
              />
              <TextField
                error={this.state.startsToSendValid ? true : false}
                type="number"
                helperText={
                  this.state.startsToSendValid
                    ? "stars  champ obligatoire !"
                    : null
                }
                id="subject"
                label="stars a envoyer "
                placeholder="stars "
                className={classes.textField}
                value={this.state.startsToSend}
                onChange={this.handleChange("startsToSend")}
                margin="normal"
                InputProps={{
                  classes: {
                    underline: classes.input,
                    input: classes.textInputf,
                  },
                  style: {
                    color: "white",
                    borderBottom: "1px solid",
                  },
                }}
                InputLabelProps={{
                  classes: {
                    root: classes.input,
                    focused: classes.input,
                  },
                }}
              />
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              marginRight: "10%",
              marginTop: "16%",
              marginBottom: "10%",
            }}
          >
            
              <div
                onClick={() =>
                  this.state.mobile !== "" &&
                  this.state.mobile.length === 8 &&
                  this.state.startsToSend !== ""
                    ? this.handleConnection()
                    : this.checkValidation()
                }
                className={classes.btnpopup}
              >
                Envoyer
              </div>
 
          </div>
          <div className={classes.f}>
            <div className={classes.bottomDarkContainer}></div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                position: "fixed",
                bottom: 0,
                width: "100%",
                zIndex: 1300,
              }}
            ></div>
          </div>
          <div className={classes.bottomContainer}></div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.ui.isLoading,
    status: state.status.status,
    userInfo: state.User.userInfo,
    token: state.token.token,
    nickname: state.nickname.nickname,
    contact: state.contact.contact,
    userInfoData: state.userInfoData.userInfoData,
    sendGiftsP: state.sendGifts.sendGifts,
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    getUserInfo: () => dispatch(getUserInfo()),
    tryLogin: (data) => dispatch(tryLogin(data)),
    sendContact: (data) => dispatch(sendContact(data)),
    userData: () => dispatch(userData(null, null, null, null)),
    tryRegisterNickname: (data) => dispatch(tryRegisterNickname(data)),
    sendGifts: (data) => dispatch(sendGifts(data)),
  }
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(SendGifts)
