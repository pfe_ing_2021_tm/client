import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";

export const styles = (theme) => ({
  container: {
    display: "flex",
    [theme.breakpoints.between("sm", "md")]: {
      height: "50vh",
    },
    height: "90vh",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  logo: {
    width: 200,
    marginBottom: 40,
    marginTop: 60,
    [theme.breakpoints.between("sm", "md")]: {
      marginTop: 100,
      width: 250,
    },
  },
  // multilineColor: {
  //   color: "#FFFFFF"
  // },
  // textField: {
  //   color: "#FFFFFF !important",
  //   marginLeft: theme.spacing.unit,
  //   marginRight: theme.spacing.unit,
  //   marginBottom: 20
  // },
  // bootstrapRoot: {
  //   color: "#FFFFFF",
  //   color: "#FFFFFF !important",
  //   "label + &": {
  //     color: "#FFFFFF !important",
  //     color: "#FFFFFF",
  //     marginTop: theme.spacing.unit * 3
  //   }
  // },
  textField: {
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto",
    paddingBottom: 0,
    marginTop: 0,
    fontWeight: 500,
  },

  input: {
    color: "white !important",
    fontFamily: "NeulandGroteskCondensedRegular",
    letterSpacing: "3px",
    "&:after": {
      border: "1px solid white  !important",
      color: "white !important",
    },
  },
  btnpopup: {
    cursor: "pointer",
    background: "#D7182A 0% 0% no-repeat padding-box",
    borderRadius: "24px",
    opacity: 1,
    width: "90px",
    height: "25px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontFamily: "NeulandGroteskCondensedRegular",
    letterSpacing: 0,
    color: "black",
    letterSpacing: "0px",
    fontSize: "1em",

    padding: 5,
    color: "white",
    textAlign: "left",
    letterSpacing: "1.75px",
    color: "#F3F3F3",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      width: "100px",
      height: "15px",
      fontSize: "1em",
      fontFamily: "NeulandGroteskCondensedBold",
      letterSpacing: "0.75px",
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      width: "150px",
      height: "25px",
      fontSize: "1.5em",
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
    },
  },
  textInputf: {
    color: "#afafb8",
    letterSpacing: 1,
  },
  section: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "94%",
    marginLeft: "2%",
  },
  bootstrapInput: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "auto",
    [theme.breakpoints.between("sm", "md")]: {
      width: 190,
    },
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "NeulandGroteskCondensedRegular",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
  label: {
    color: theme.palette.secondary.main,
    fontSize: 12,
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: 20,
    },
    fontFamily: "NeulandGroteskCondensedRegular",
  },
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: "flex",
    alignItems: "center",
  },
  button: {
    marginTop: 20,
    marginBottom: 20,
  },
  checkbox: {
    color: theme.palette.primary.main,
    padding: 2,
  },
  bottomContainer: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    position: "fixed",
    bottom: 0,
  },
  forms: {
    [theme.breakpoints.between("sm", "md")]: {
      marginBottom: "7vh",
    },
  },
  bottomLightContainer: {
    backgroundColor: "#222222",
    display: "flex",
    color: "white",
    textAlign: "center",
    flexDirection: "column",
    width: "100%",
  },
  bottomDarkContainer: {
    backgroundColor: "#111112",
    height: "10vh",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  footer: {
    height: "10vh",
    background: "rgb(255, 255, 255)",
    textAlign: "center",
    width: "100%",
    margin: 0,
    textTransform: "uppercase",
    fontSize: 11,
    fontWeight: "700",
    lineHeight: "28px",
    // position: "fixed",
    // bottom: 0
    boxShadow: "inset 0px 0px 0px 10px #000",
    paddingTop: 15,
  },

  footerNextStep: {
    position: "fixed",
    bottom: 0,
    height: 25,
    background: "rgb(255, 255, 255)",
    textAlign: "center",
    width: "100%",
    margin: 0,
    textTransform: "uppercase",
    fontSize: 11,
    fontWeight: "700",
    lineHeight: "24px",
  },
  largeText: {
    paddingRight: 20,
    paddingLeft: 20,
    [theme.breakpoints.between("sm", "md")]: {
      paddingRight: 80,
      paddingLeft: 80,
    },
    fontSize: 12,
    textWrap: "break-word",
    //color: "white"
  },
  bottomTypography: {
    marginRight: 15,
    marginLeft: 15,
    color: "white",
    // fontWeight: "heavy",
    fontFamily: "NeulandGroteskCondensedBold",
  },
  checkContainer2: {
    // marginTop: 20,
    marginLeft: 0,
    marginBottom: 10,
  },
  checkContainer1: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  lien: {
    color: theme.palette.secondary.main,
    fontSize: " 12px",
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: " 22px",
    },
    fontFamily: "NeulandGroteskCondensedRegular",
    padding: "5%",
  },
  textarea: {
    backgroundColor: theme.palette.secondary.main,
    padding: 18.5,
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginBottom: 20,
  },
  Titletext: {
    fontFamily: "NeulandGroteskCondensedRegular",
    color: theme.palette.secondary.main,
    letterSpacing: "5px",
    textAlign: "left",
    fontSize: 60,
    marginLeft: "7%",
    marginTop: "8vh",
  },
  Titletextsub: {
    fontFamily: "NeulandGroteskCondensedRegular",
    color: theme.palette.primary.main,
    letterSpacing: "5px",
    textAlign: "left",
    fontSize: 20,
    marginLeft: "7%",
    marginTop: "8vh",
  },
})
