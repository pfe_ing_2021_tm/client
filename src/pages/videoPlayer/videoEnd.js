/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-06-19T15:02:57+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-03T09:50:13+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import like from "../../assets/img/jaime_icone.png";
import dislike from "../../assets/img/jaimepas_icone.png";
import dislike_grey from "../../assets/img/jaime_icone-grey.png";
import Like_grey from "../../assets/img/jaime_icone-grey.png";

import puzzleTime from "../../assets/img/puzzle_time.png";

import codeChallenge from "../../assets/img/code_challenge.png";
import quizShot from "../../assets/img/quiz.png";
import extraVideo from "../../assets/img/extra_video.png";
import ligne from "../../assets/img/ligne.png";
import replayIcn from "../../assets/img/replay.png";
import nextIcn from "../../assets/img/next.png";
import { styles } from "./style";
import { connect } from "react-redux";
import { compose } from "redux";
import { setDidLikeVideo, getCanLikeVideo ,getActiveCity , getActiveBrand} from "../../store/actions";
import "./stylePlayer.css";
import { history } from "../../history";
import { Link } from "react-router-dom";

class VideoEnd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      nextAction: this.props.typeplay,
      openimagelike: false,
      openimagedislike: false,
      canlikevideo: true,
      oldLike: false,
      flag: false,
      currentIndexVideo: 1,
      nextVideoId: null
    };
  }

  async componentWillMount() {
    const cityid = localStorage.getItem("city");

    if (cityid !== null) {
      await this.props.getActiveCity(cityid);
    }
    const data = {
      idcustomer: localStorage.getItem("_id"),
      idvideo: this.props._id
    };
    this.props.getCanLikeVideo(data);
  }

  componentDidMount() {
    if(this.props.brandData.lenght > 0){
    this.props.cityData.videos.filter((item, index) => {
      this.setState({
        currentIndexVideo: this.props._id === item._id ? index : 0,
        nextVideoId: this.props.cityData.videos[this.state.currentIndexVideo - 1]
          ._id
      });
    });
  }else if(this.props.brandData.lenght > 0){
      this.props.brandData.videos.filter((item, index) => {
        this.setState({
          currentIndexVideo: this.props._id === item._id ? index : 0
        });
      });
      this.setState({
        nextVideoId: this.props.brandData.videos[this.state.currentIndexVideo - 1]
          ._id
      });
    }else {
      const cityid = localStorage.getItem("city");
      const brand = localStorage.getItem("brand");
      if (cityid !== null) {
        this.props.getActiveCity(cityid);
      }
      if (brand !== null) {
        this.props.getActiveBrand(brand);
      }
    }
    const data = {
      idcustomer: localStorage.getItem("_id"),
      idvideo: this.props._id
    };
    this.props.getCanLikeVideo(data);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.canlikevideo != this.props.canlikevideo) {
      if (
        this.props.canlikevideo !== null &&
        this.props.canlikevideo.message === "can like video"
      ) {
        this.setState({ flag: false });
      } else {
        this.setState({ flag: true });
      }
    }
    if (prevProps.cityData !== this.props.cityData) {
      this.props.cityData.videos.map((item, index) => {
        if (item._id === this.props._id) {
          {
            this.setState({ currentIndexVideo: index }, () =>
              this.setState({
                nextVideoId: this.props.cityData.videos[
                  this.state.currentIndexVideo + 1
                ]
                  ? this.props.cityData.videos[this.state.currentIndexVideo + 1]
                      ._id
                  : null,
              })
            );
          }
        }
      });
    }
    if (prevProps.brandData !== this.props.brandData) {
      this.props.brandData.videos.map((item, index) => {
        if (item._id === this.props._id) {
          {
            this.setState({ currentIndexVideo: index }, () =>
              this.setState({
                nextVideoId: this.props.brandData.videos[
                  this.state.currentIndexVideo + 1
                ]
                  ? this.props.brandData.videos[this.state.currentIndexVideo + 1]
                      ._id
                  : null,
              })
            );
          }
        }
      });
    }
  }

  onNextVideoClick = () => {
    if (this.state.nextVideoId !== undefined) {
      history.push(`/videoplayer/${this.state.nextVideoId}`);
    } else {
      history.push(`/home`);
    }
    window.location.reload();
  };

  onNextVideoShowButton = () => {
    if (this.props.cityData && this.props.cityData.videos) {
      return this.state.currentIndexVideo ===
        this.props.cityData.videos.length - 1
        ? false
        : true;
    }
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = value => {
    this.setState({ open: true });
  };

  handleLangChangeStart = type => {
    if (type === "forward_to_code") {
      this.setState({ open: false });
      //this.props.onStartPlay();
      history.push(`/entrycode/${this.props._id}`);
    } else if (type === "forward_to_quizz") {
      this.setState({ open: false });
      //this.props.onStartPlay();
      history.push(`/quizshot/${this.props.quizid}`);
    } else if (type === "forward_to_extra") {
      this.setState({ open: false });
      //this.props.onStartPlay();
      history.push(`/extrat/${this.props.extraid}`);
    }else if (type === "forward_to_puzzle") {
      this.setState({ open: false });
      history.push(`/puzzleGame/${this.props.puzzleid}`,{videoid : this.props.videoIndex});
    }
    // else if (type === "extra") {
    //   this.setState({ open: false });
    //   //this.props.onStartPlay();
    //   history.push(`/home/${this.props.afterextraid}`);
    // }
  };

  canLikeVideo = value => {
    const data = {
      idcustomer: localStorage.getItem("_id"),
      idvideo: this.props._id,
      status: value
    };
    this.props.setDidLikeVideo(data);
  };
  onLike = () => {
    this.setState({
      openimagelike: true,
      openimagedislike: this.state.openimagedislike ? false : false
    });
  };

  onDisLike = () => {
    this.setState({
      openimagedislike: true,
      openimagelike: this.state.openimagelike ? false : false
    });
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;
    const { nextAction } = this.state;
    let actionType;
    if (nextAction === "forward_to_puzzle") {
      actionType = (
        <img
          onClick={() => this.handleLangChangeStart("forward_to_puzzle")}
          src={puzzleTime}
          alt=""
          className={classes.gameimg}
        />
      );
    } else if (nextAction === "forward_to_code") {
      actionType = (
        <img
          onClick={() => this.handleLangChangeStart("forward_to_code")}
          src={codeChallenge}
          alt=""
          className={classes.gameimg}
        />
      );
    } else if (nextAction === "forward_to_extra") {
      actionType = (
        <img
          onClick={() => this.handleLangChangeStart("forward_to_extra")}
          src={extraVideo}
          alt=""
          className={classes.gameimg}
        />
      );
    } else if (nextAction === "forward_to_quizz") {
      actionType = (
        <img
          onClick={() => this.handleLangChangeStart("forward_to_quizz")}
          src={quizShot}
          alt=""
          className={classes.gameimg}
        />
      );
    }
    return (
      <Dialog
        BackdropProps={{
          classes: {
            root: classes.backdrops
          }
        }}
        PaperProps={{
          classes: {
            root: classes.paperprops
          }
        }}
        fullScreen
        open={this.props.open}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <IconButton
          color="inherit"
          onClick={this.props.onclose}
          aria-label="Close"
          className={classes.videoCloseCont}
        >
          <CloseIcon className={classes.videoCloseIcn} />
        </IconButton>
        <div className="rota">
          <div className={classes.endvideobox}>
            <div className={classes.likecont}>
              <img
                src={
                  this.state.flag
                    ? this.props.canlikevideo &&
                      this.props.canlikevideo.data.status
                      ? like
                      : Like_grey
                    : this.state.openimagelike
                      ? like
                      : Like_grey
                }
                alt=""
                className={classes.imgLike}
                onClick={() => {
                  if (this.state.flag === false) {
                    this.canLikeVideo(true);
                    this.onLike();
                  }
                }}
              />

              <p className={classes.liketext}>J'AIME </p>
            </div>

            <div className={classes.dlikecont}>
              <img
                src={
                  this.state.flag
                    ? this.props.canlikevideo &&
                      !this.props.canlikevideo.data.status
                      ? dislike
                      : dislike_grey
                    : this.state.openimagedislike
                      ? dislike
                      : dislike_grey
                }
                alt=""
                className={classes.imgdLike}
                onClick={() => {
                  if (this.state.flag === false) {
                    this.canLikeVideo(false);
                    this.onDisLike();
                  }
                }}
              />
              <p className={classes.dliketext}>J'AIME PAS</p>
            </div>
          </div>

          <img src={ligne} alt="" className={classes.dividers} />

          {actionType}
          <div className={classes.bottomend}>
            <div className={classes.btnreplay} onClick={this.props.onclose}>
              <img src={replayIcn} alt="" className={classes.btnreplayimg} />

              <p className={classes.btnreplaytext}>Replay</p>
            </div>
            <div className={classes.btnnextvideo}>
              {this.onNextVideoShowButton() && (
                <div
                  className={classes.btnnextvideo}
                  onClick={this.onNextVideoClick}
                >
                  <p className={classes.btnnextvideotext}>épisode suivant</p>
                  <img
                    src={nextIcn}
                    alt=""
                    className={classes.btnnextvideoicn}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}

VideoEnd.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string
};

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    cityData: state.cityData.cityData,
    brandData: state.brandData.brandData,
    canlikevideo: state.canlikevideo.canlikevideo
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setDidLikeVideo: data => dispatch(setDidLikeVideo(data)),
    getCanLikeVideo: data => dispatch(getCanLikeVideo(data)),
    getActiveBrand: (id) => dispatch(getActiveBrand(id)),
    getActiveCity: (id) => dispatch(getActiveCity(id))
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(VideoEnd);
