/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-02-12 16:53:39
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-03-05 11:52:11
 */

import React from "react";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { styles } from "./style";
import "video-react/dist/video-react.css"; // import css
import {
  Player,
  ControlBar,
  ForwardControl,
  ReplayControl,
  VolumeMenuButton,
  CurrentTimeDisplay,
  PlaybackRateMenuButton,
  TimeDivider
} from "video-react";
import CircularProgress from "@material-ui/core/CircularProgress";
import VideoEnd from "./videoEnd";
import NotActionEndVideo from "./notActionEndVideo";
// import "./stylePlayer.css";
import { connect } from "react-redux";
import { compose } from "redux";
import { history } from "../../history";
import MediaQuery from "react-responsive";
import Dialog from "@material-ui/core/Dialog";
import { Button } from "reactstrap";
import {
  getCanPlayQuizz,
  canplayquizzreset,
  setvideohistory,
  getVideoById,
  setVideoItem,
  getListOfQuiz
} from "../../store/actions";
const token = localStorage.getItem("token");
const sources = {
  sintelTrailer: "http://media.w3.org/2010/05/sintel/trailer.mp4",
  bunnyTrailer: "http://media.w3.org/2010/05/bunny/trailer.mp4",
  bunnyMovie: "http://media.w3.org/2010/05/bunny/movie.mp4",
  test: "http://media.w3.org/2010/05/video/movie_300.webm"
};

class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    // window.screen.orientation.lock("landscape");
    this.state = {
      sources: null,
      open: false,
      openNotAction: false,
      url: null,
      token,
      pip: false,
      playing: false,
      controls: false,
      light: false,
      volume: 0.8,
      muted: false,
      played: 0,
      loaded: 0,
      duration: 0,
      playbackRate: 1.0,
      loop: false,
      portrait: "",
      videoend: false,
      progressBar: true,
      playstart: false,
      videoIndex: this.props.match.params.id,
      timeToShow: false,
      currentTime: 0,
      seeking: false,
      videoTitle: null,
      videoId: null,
      videoType: null,
      videoQuizz: null,
      videoExtra: null,
      videoLink: null,
      videoHauteur: null,
      videoLargeur: null,
      videotime: null,
      videoDuration: null,
      videoMetaType: null,
      videoExtraTo: null,
      canPlayQuiz: ""
    };

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.load = this.load.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
    this.seek = this.seek.bind(this);
    this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
    this.changeVolume = this.changeVolume.bind(this);
    this.setMuted = this.setMuted.bind(this);
  }

  componentWillMount() {
    if (this.state.token === null) {
      return history.push("/");
    }
    this.props.getVideoById(this.props.match.params.id);
    this.setState({ videoend: false });
  }

  componentWillUnmount() {
    if (this.state.token === null) {
      return history.push("/");
    }
    this.props.setVideoItem();
    localStorage.removeItem("lastScreen");
    this.load();
  }

  async componentWillReceiveProps(prevProps) {
    if (prevProps.videoItem !== this.props.videoItem) {
      await this.props.getCanPlayQuizz(prevProps.videoItem._id);
      await this.setState({
        videoTitle: prevProps.videoItem.title,
        videoId: prevProps.videoItem._id,
        videoType: prevProps.videoItem.type,
        videoMetaType: prevProps.videoItem.metaType,
        videoQuizz: prevProps.videoItem.quizz,
        videoExtra: prevProps.videoItem.video,
        // videoExtraTo: prevProps.videoItem.video.city,
        videoLink: prevProps.videoItem.link,
        videoHauteur: prevProps.videoItem.hauteur,
        videoLargeur: prevProps.videoItem.largeur,
        videotime: prevProps.videoItem.time,
        videoDuration: prevProps.videoItem.duration,
        puzzelId: prevProps.videoItem.puzzel,
      });
      await this.load();
      await this.play();
      await this.refs.player.subscribeToStateChange(
        this.handleStateChange.bind(this)
      );
    }

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    // this.load = this.load.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
    this.seek = this.seek.bind(this);
    this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
    this.changeVolume = this.changeVolume.bind(this);
    this.setMuted = this.setMuted.bind(this);
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.canplayquizz !== this.props.canplayquizz) {
      this.setState({ canPlayQuiz: this.props.canplayquizz });
    }
  }
  handleStateChange = (state, prevState) => {
    // copy player state to this component's state
    this.setState({
      player: state,
      currentTime: state.currentTime
    });
    // parseFloat(3.7) >= parseFloat(this.state.currentTime.toFixed(2))
  };

  handleClickOpen = async () => {
    await this.setState({ open: true });
    await this.play();
  };

  handleClose = () => {
    this.setState({ open: false, playing: false });
  };

  onDuration = duration => {
    this.setState({ duration });
  };

  onProgress = state => {
    // We only want to update time slider if we are not currently seeking
    if (!this.state.seeking) {
      this.setState(state);
    }
  };

  onClickVideo = () => {
    this.setState({ progressBar: true });
  };

  ref = player => {
    this.player = player;
  };

  onSeekMouseDown = e => {
    this.setState({ seeking: true });
  };

  onSeekChange = e => {
    this.setState({ played: e.target.value });
  };

  onSeekMouseUp = e => {
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(e.target.value));
  };

  onSeekMouseUp = e => {
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(e.target.value));
  };

  play() {
    this.refs.player.play();
    this.props.getCanPlayQuizz(this.state.videoId);
    this.props.getListOfQuiz();
  }

  pause() {
    this.refs.player.pause();
  }

  load() {
    this.refs.player.load();
  }

  changeCurrentTime(seconds) {
    return () => {
      const { player } = this.refs.player.getState();
      const currentTime = player.currentTime;
      this.refs.player.seek(currentTime + seconds);
    };
  }

  seek(seconds) {
    return () => {
      this.refs.player.seek(seconds);
    };
  }

  changePlaybackRateRate(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const playbackRate = player.playbackRate;
      this.refs.player.playbackRate = playbackRate + steps;
    };
  }

  changeVolume(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const volume = player.volume;
      this.refs.player.volume = volume + steps;
    };
  }

  setMuted(muted) {
    return () => {
      this.refs.player.muted = muted;
    };
  }

  changeSource(name) {
    return () => {
      this.setState({
        source: sources[name]
      });
      this.refs.player.load();
    };
  }

  startPlay = () => {
    this.setState({ playstart: true });
  };

  onReplayVideo = async () => {
    //this.setState({ open: true });
    this.props.getCanPlayQuizz(this.state.videoId);
    //this.refs.player.play();

    this.refs.player.play();
  };

  onCloseEnd = () => {
    this.setState({ open: false });
    // this.setState({ playstart: true });
    this.props.getCanPlayQuizz(this.state.videoId);
    this.refs.player.play();
  };
  gobackOrClose = () => {
    let video = {};
    video.id = this.state.videoId;
    video.timespent = this.state.currentTime;
    video.type = this.state.videoType;
    this.props.setvideohistory(video);
    if (localStorage.getItem("lastScreen")) {
      history.goBack();
    } else {
      if (this.props.match.params.idrecipe) {
        history.push(`/recipe/${this.props.match.params.idrecipe}`);
      } else {
        history.push("/home");
      }
    }
  };
  VideoEnd = () => {
    let video = {};
    video.id = this.state.videoId;
    video.timespent = this.state.currentTime;
    video.type = this.state.videoType;
    this.props.setvideohistory(video);
    //this.setState({ open: true });
    //if user from section codeChallenge goBack()

    // const lastscreen = localStorage.getItem("lastScreen");
    // if (lastscreen) {
    //   history.goBack();
    // }
    //--------------------------------------------
    if (this.props.canplayquizz === "can play quizz") {
      this.setState({ open: true });
    } else {
      this.setState({ openNotAction: true });
      this.setState({ open: false });
    }
    //--------------------------------------------
    // if (this.state.videoExtra) {
    //   this.setState({ open: true });
    // }
    //this.props.canplayquizzreset();
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.state.videoLink !== null ? ( // style={{ overflow: "hidden " }}
          <Dialog
            fullScreen
            open={true}
            PaperProps={{ classes: { root: classes.dialogPaper } }}
            style={{ overflow: "hidden " }}
          >
            <div className={classes.vidcont}>
              <IconButton
                style={{ zIndex: 10 }}
                color="inherit"
                onClick={this.gobackOrClose}
                aria-label="Close"
                className={classes.videoCloseCont}
              >
                <CloseIcon
                  className={classes.videoCloseIcn}
                  style={{ zIndex: 10 }}
                />
              </IconButton>
              <NotActionEndVideo open={this.state.openNotAction} />
              <VideoEnd
                open={this.state.open}
                typeplay={this.state.videoType}
                _id={this.state.videoId}
                videoIndex={this.state.videoIndex}
                quizid={this.state.videoQuizz}
                extraid={this.state.videoExtra}
                puzzleid={this.state.puzzelId}
                onStartPlay={this.startPlay}
                onReplayVideo={this.onReplayVideo}
                onclose={this.onCloseEnd}
              />

              <div className={classes.vidcont}>
                <div>
                  <div>
                    <Player
                      //muted={true}
                      playsInline
                      ref="player"
                      onEnded={this.VideoEnd}
                      // onFullscreenPlayerDidDismiss={() =>
                      //   this.handleFullScreenToPortrait()}

                      // src={this.state.videoLink}
                    >
                      <source src={this.state.videoLink} />

                      <ControlBar>
                        <div className={classes.videoProgreesTitle}>
                          <div className={classes.videoProgressBoxDesc}>
                            <div style={{ marginLeft: 20 }}>
                              <Typography
                                component="p"
                                className={classes.playVideoTitle}
                              >
                                {this.props.titleplay}

                                {/* {this.state.currentTime} */}
                              </Typography>
                              <Typography
                                component="p"
                                className={classes.playVideoAuthor}
                              >
                                {this.state.videoTitle}
                              </Typography>
                            </div>
                          </div>
                        </div>
                      </ControlBar>
                    </Player>
                  </div>
                  {parseFloat(this.state.videotime) <=
                    parseFloat(this.state.currentTime.toFixed(2)) &&
                    parseFloat(this.state.videoDuration) >=
                      parseFloat(this.state.currentTime.toFixed(2)) &&
                    this.state.videoType === "forward_to_code" &&
                    this.props.canplayquizz === "can play quizz" && (
                      <div>
                        <MediaQuery query="(orientation: portrait)">
                          <div>
                            You are portrait
                            <p
                              style={{
                                position: "absolute",
                                fontFamily: "NeulandGroteskCondensedRegular",
                                right: `${this.state.videoHauteur}%`,
                                color: "#fff",
                                zIndex: 999,
                                top: `${this.state.videoLargeur}%`,
                                transform: "rotate(90deg)"
                              }}
                            >
                              {this.state.videoMetaType}
                            </p>
                          </div>
                        </MediaQuery>
                        <MediaQuery query="(orientation: landscape)">
                          <div>
                            You are landscape
                            <p
                              style={{
                                position: "absolute",
                                top: `${this.state.videoHauteur}%`,
                                color: "#fff",
                                zIndex: 999,
                                left: `${this.state.videoLargeur}%`
                              }}
                            >
                              {this.state.videoMetaType}
                            </p>
                          </div>
                        </MediaQuery>
                      </div>
                    )}
                </div>
              </div>
            </div>
          </Dialog>
        ) : (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh"
            }}
          >
            <CircularProgress className={classes.progress} color="secondary" />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    cityData: state.cityData.cityData,
    canplayquizz: state.canplayquizz.canplayquizz,
    videoItem: state.videoItem.videoItem
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getVideoById: id => dispatch(getVideoById(id)),
    getCanPlayQuizz: idvideo => dispatch(getCanPlayQuizz(idvideo)),
    canplayquizzreset: () => dispatch(canplayquizzreset()),
    setvideohistory: id => dispatch(setvideohistory(id)),
    setVideoItem: () => dispatch(setVideoItem({})),
    getListOfQuiz: () => dispatch(getListOfQuiz())
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(VideoPlayer);
