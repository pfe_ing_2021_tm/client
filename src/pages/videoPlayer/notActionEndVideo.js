/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-03 12:54:09
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-07 19:09:56
 */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import { styles } from './style';
import './stylePlayer.css';

import { Link } from 'react-router-dom';

class NotActionEndVideo extends React.Component {
  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog
        BackdropProps={{
          classes: {
            root: classes.backdrops
          }
        }}
        PaperProps={{
          classes: {
            root: classes.paperprops
          }
        }}
        open={this.props.open}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <div className="rota">
          <div
            className={classes.bottomend}
            style={{ justifyContent: 'center' }}
          >
            <Link to={'/'}>
              <button
                onClick={this.props.onclose}
                className={classes.returnToHomebtn}
              >
                Retour à la page d'accueil
              </button>
            </Link>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default withStyles(styles)(NotActionEndVideo);
