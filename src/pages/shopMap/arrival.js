/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-07-09T09:52:00+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-16T14:04:53+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import CheckSelection from "./checkSelection";
import blue from "@material-ui/core/colors/blue";
import RedsIcon from "../../assets/img/endstore.png";

const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600]
  },
  dialogPaper: {
    alignItems: "center",
    overflow: "visible",
    width: "100%"
  }
};

class ArrivalDialog extends React.Component {
  state = {
    open: false
  };

  handleClose = () => {
    this.props.onClose();
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog
        PaperProps={{ className: classes.dialogPaper }}
        onClose={() => this.handleClose()}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <img src={RedsIcon} alt="" style={{ width: 245, marginBottom: 35 }} />
        <p
          style={{
            textAlign: "center",
            color: "#919191",
            textTransform: "uppercase"
          }}
        >
          félicitations! <br />
          vous êtes arrivé
          <br />
          {/* <span style={{ color: '#ed2a1c', fontSize: 22, fontWeight: 700 }}>
            50 reds
          </span> */}
        </p>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 40
          }}
        >
          <Button
            variant="contained"
            onClick={() => this.setState({ open: true })}
            style={{
              position: "absolute",
              bottom: -15,
              background: "#ed2a1c",
              color: "#fff"
            }}
          >
            ok
          </Button>
        </div>
        <div />
        <CheckSelection
          open={this.state.open}
          handleClose={this.handleClose}
          shopId={this.props.shopId}
        />
      </Dialog>
    );
  }
}

ArrivalDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string
};

export default withStyles(styles)(ArrivalDialog);
