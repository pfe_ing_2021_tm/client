/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:43:15+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-16T10:54:19+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import kiosquebg from "../../assets/img/kiosquebg.png";

export const styles = (theme) => ({
  imageProducts: {
    width: "100%",
    "@media (min-width: 300px)": {
      width: "35vw",
    },
  },
  containdialog: {
    position: "fixed",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    background: `url(${kiosquebg})`,
    backgroundSize: "cover",
  },
  sectionkiosque: {
    height: 70,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  sectiontitle: {
    color: "#fff",
    fontSize: 18,
    textTransform: "uppercase",
    margin: "15px 15px -5px 15px",
  },
  sectionscroll: {
    margin: "10px 15px",

    position: "relative",
    height: "80%",
  },
  storeitem: {
    margin: "5px 15px",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "bottom",
    height: 111,
  },

  closeicon: {
    width: 25,
    marginRight: 20,
  },
  allchechbtncont: {
    background: "-webkit-linear-gradient(bottom, #000, rgba(0,0,0,0.1))",
    height: 175,
    width: "100%",
    position: "absolute",
    bottom: 130,
  },
  allchechbtncontOneBtn: {
    background: "-webkit-linear-gradient(bottom, #000, rgba(0,255,0,0))",
    height: 80,
    width: "100%",
    position: "absolute",
    bottom: 40,
  },
  sectionbtn: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    position: "absolute",
    bottom: 0,
    width: "100%",
    paddingBottom: 20,
    zIndex: 5,
  },
  btnalcheck: {
    background: "#ed2a1c",
    bottom: 0,
    zIndex: 99,
    width: 200,
    textAlign: "center",
    padding: 17,
    borderRadius: 15,
    margin: 10,
    color: "#fff",
    fontSize: 17,
    textTransform: "uppercase",
    fontWeight: 600,
  },
  btnalcheckdisb: {
    background: "#333333",
    bottom: 0,
    zIndex: 99,
    width: 200,
    textAlign: "center",
    padding: 17,
    borderRadius: 15,
    margin: 10,
    color: "#7d7c7c",
    fontSize: 17,
    textTransform: "uppercase",
    fontWeight: 600,
  },
  footallcheck: {
    height: 130,
    background: "#000",
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
  footallcheckOneBtn: {
    height: 40,
    background: "#000",
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
});
