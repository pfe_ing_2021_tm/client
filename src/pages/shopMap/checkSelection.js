/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-07-10T13:39:32+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-12T13:09:09+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import TimeAgo from "react-timeago";

import frenchStrings from "react-timeago/lib/language-strings/fr";
import buildFormatter from "react-timeago/lib/formatters/buildFormatter";

// import kiosqueicn from "../../assets/img/kiosque.png";
import kiosqueicn from "../../assets/img/kiosque_1.png";
import closeicn from "../../assets/img/closemap.png";
import pin from "../../assets/img/pin.png";
import { styles } from "./style";

import Carousel from "./carousel";

import Pack1 from "../../assets/img/Pack-1.png";
import Pack2 from "../../assets/img/pack-2.png";
import Pack3 from "../../assets/img/pack-3.png";
import Pack4 from  "../../assets/img/Pack-4.png"

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

var items = [
  { key: "5d94bcdbdbd2aa8c6fea66a7", image: Pack1 },
  { key: "5d94bcfbdbd2aa8c6fea66a8", image: Pack2 },
  { key: "5d94bd08dbd2aa8c6fea66a9", image: Pack3 },
  { key: "5e427498a5104f5850fcc7bb", image: Pack4 }
];
const formatter = buildFormatter(frenchStrings);
class CheckSelection extends React.Component {
  state = {
    allList: false,
    inScroll: false,
    positionShow: false,
    selectedStore: null,
    open: false
  };
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false, playing: false });
  };
  showAllList() {
    this.setState({ allList: true });
  }

  handleScroll = e => {
    if (e.target.scrollTop > 5) {
      this.setState({ inScroll: true });
    } else {
      this.setState({ inScroll: false });
    }

    let element = e.target;
    if (element.scrollHeight - element.scrollTop === element.clientHeight) {
      // do something at end of scroll
    }
  };

  getStorePostion = store => {
    this.setState({
      inScroll: false,
      positionShow: true,
      selectedStore: store
    });
  };

  render() {
    const { classes } = this.props;
   
    return (
      <React.Fragment>
        <Dialog
          fullScreen
          open={this.props.open}
          onClose={this.props.handleClose}
          TransitionComponent={Transition}
        >
          <div className={classes.containdialog}>
            <div className={classes.sectionkiosque}>
              <img src={kiosqueicn} alt="" style={{ width: 220 }} />
              <img
                onClick={this.props.handleClose}
                src={closeicn}
                alt=""
                className={classes.closeicon}
              />
            </div>
            <p
              className={classes.sectiontitle}
              style={{
                marginLeft: 30,
                marginBottom: 10,
                fontFamily: "NeulandGroteskCondensedRegular",
                color: "white"
              }}
            >
              Vous êtes ici
            </p>
            <div className={classes.storeitem} style={{ margin: "5px 30px" }}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  padding: "10px 15px"
                }}
              >
                <img src={pin} alt="" style={{ width: 20, marginRight: 10 }} />

                <div style={{}}>
                  <p
                    style={{
                      margin: 0,
                      fontSize: 16,
                      textTransform: "uppercase",
                      fontFamily: "NeulandGroteskBold",
                      color: "#bc2132"
                    }}
                  >
                    {this.props.shopId.code}
                  </p>
                </div>
              </div>
              <div style={{
                padding: "10px 15px"
              }}>
                <p style={{ margin: 0, fontSize: 13, fontFamily: 'NeulandGroteskLight', color: "white" }}>
                  <TimeAgo
                    date={this.props.shopId.createdAt}
                    formatter={formatter}
                  />
                </p>
              </div>
            </div>
          </div>

          <Carousel items={items} active={0} shopId={this.props.shopId} />
        </Dialog>
      </React.Fragment>
    );
  }
}

CheckSelection.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CheckSelection);
