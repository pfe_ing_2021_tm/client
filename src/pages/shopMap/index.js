/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-09T09:33:28+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React, { useState, useEffect } from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";
import TagManager from "react-gtm-module";
import AllCheck from "./allCheck";
import markerbox from "../../assets/img/markerbox.png";
import markerboxGomreg from "../../assets/img/markerboxGomreg.png";
import markerboxGomregOff from "../../assets/img/markerboxOffGomreg.png";

import markerboxOff from "../../assets/img/markerboxOff.png";
import malboroRed from "../../assets/img/malboroRed.png";
import malboroBlue from "../../assets/img/malboroBlue.png";
import malboroGold from "../../assets/img/malboroGold.png";
import merit from "../../assets/img/merit.png";
import { compose } from "redux";
import { connect } from "react-redux";
import {
  getShop,
  getShopChecks,
  getCurrentLocation,
} from "../../store/actions";
import { setshopmapViewistory } from "../../store/actions/history";
import closeeMap from "../../assets/img/closeeMap.png";
import { Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import { history } from "../../history";
const token = localStorage.getItem("token");
let URI = `https://maps.googleapis.com/maps/api/js?key=AIzaSyC7s7L47twQvDe_uGgtdBI1u7dAG5m_i1s&v=3.exp&libraries=geometry,drawing,places`;
//URI = decodeURI(URI)
const defaultMapOptions = {
  fullscreenControl: false,
  minZoom: 2,
  maxZoom: 20,
  //mapTypeControl: false,
  //streetViewControl: false,
  //zoomControl: false
};
const tagManagerArgs = {
  gtmId: process.env.REACT_APP_IDGTM,

  dataLayer: {
    userProject: "Home_Discovered_page",
    checkin: "click_shopmap_view",
  },
};

const MapWithMaker = withScriptjs(
  withGoogleMap((props) => {
    const [zoom, setZoom] = useState(18);
    const [latVariable, setLatVariable] = useState(0.00023);
    const [longVariable, setLongVariable] = useState(0.0002);
    const [leftVariable, setLeftVariable] = useState(-0.0001);
    const [diffVariable, setDiffVariable] = useState(0.0001);
    const [activeZoom, setActiveZoom] = useState(true);
    function handleZoomChanged() {
      let zoom = this.getZoom();
      //setZoom(zoom);
      if (zoom === 20) {
        setLatVariable(0.00006);
        setLongVariable(0.00005);
        setLeftVariable(-0.00004);
        setDiffVariable(0.00001);
        setActiveZoom(true);
      } else if (zoom === 19) {
        setLatVariable(0.00012);
        setLongVariable(0.0001);
        setLeftVariable(-0.00005);
        setDiffVariable(0.00005);
        setActiveZoom(true);
      } else if (zoom === 18) {
        setLatVariable(0.00023);
        setLongVariable(0.0002);
        setLeftVariable(-0.0001);
        setDiffVariable(0.0001);
        setActiveZoom(true);
      } else if (zoom <= 16) {
        setActiveZoom(false);
      }
    }
    const markerImg = (store) => {
      if (
        store.timeEnd !== undefined &&
        store.timeEnd !== "" &&
        store.timeStart !== undefined &&
        store.timeStart !== ""
      ) {
        // ***********************
        let start =
          store.timeStart.slice(0, 2) +
          store.timeStart.slice(3, store.timeStart.length);
        let end =
          store.timeEnd.slice(0, 2) === "00"
            ? "24" + store.timeEnd.slice(3, store.timeEnd.length)
            : store.timeEnd.slice(0, 2) +
              store.timeEnd.slice(3, store.timeEnd.length);
        // ***********************

        if (start < props.dateForma && props.dateForma < end) {
          // shop open

          if (store.code.toUpperCase().search("GOMREG") !== -1) {
            return markerboxGomreg;
          } else {
            return markerbox;
          }
        } else {
          // shop close
          if (store.code.toUpperCase().search("GOMREG") !== -1) {
            return markerboxGomregOff;
          } else {
            return markerboxOff;
          }
        }
      } else if (store.timeStart) {
      }
      return markerboxOff;
    };
    return (
      <GoogleMap
        defaultZoom={zoom}
        onZoomChanged={handleZoomChanged}
        defaultCenter={{
          lat: props.center.latitude
            ? parseFloat(props.center.latitude)
            : parseFloat(props.center.coords.latitude),
          lng: props.center.longitude
            ? parseFloat(props.center.longitude)
            : parseFloat(props.center.coords.longitude),
        }}
        defaultOptions={defaultMapOptions}
      >
        {props.isMarkerShown && (
          <div>
            <React.Fragment>
              <Marker
                position={{
                  lat: props.center.latitude
                    ? parseFloat(props.center.latitude)
                    : parseFloat(props.center.coords.latitude),
                  lng: props.center.longitude
                    ? parseFloat(props.center.longitude)
                    : parseFloat(props.center.coords.longitude),
                }}
              />

              {props.shopmapList.map((store, index) => {
                return (
                  <>
                    <Marker
                      key={index}
                      position={{
                        lat: parseFloat(store.latitude),
                        lng: parseFloat(store.longitude),
                      }}
                      onClick={() => props.onpressmarker(store)}
                      icon={{
                        url: markerImg(store),
                        scaledSize: new window.google.maps.Size(45, 52),
                      }}
                    />
                    {activeZoom === true &&
                      store.active === true &&
                      store.products.length > 0 &&
                      store.products.map((p, i) => {
                        return (
                          <Marker
                            key={i + 999}
                            position={{
                              lat: parseFloat(store.latitude) + latVariable,
                              lng:
                                store.products.length < 3
                                  ? parseFloat(store.longitude) +
                                    (leftVariable + i * longVariable)
                                  : store.products.length === 3
                                  ? parseFloat(store.longitude) +
                                    (leftVariable -
                                      diffVariable +
                                      i * longVariable)
                                  : parseFloat(store.longitude) +
                                    (leftVariable -
                                      diffVariable * 2 +
                                      i * longVariable),
                              //lng: store.products.length < 3 ? parseFloat(store.longitude) + (0.0001 + i * 0.00005) : store.products.length === 3 ? parseFloat(store.longitude) + (-0.0002 + i * 0.0002) : parseFloat(store.longitude) + (-0.0003 + i * 0.0002),
                            }}
                            //onClick={() => props.onpressmarker(store)}
                            icon={{
                              url:
                                p.name === "Marlboro red"
                                  ? malboroRed
                                  : p.name === "Marlboro blue"
                                  ? malboroBlue
                                  : p.name === "Marlboro gold"
                                  ? malboroGold
                                  : merit,
                              scaledSize: new window.google.maps.Size(30, 50),
                              origin: new window.google.maps.Point(0, 0),
                            }}
                          />
                        );
                      })}
                  </>
                );
              })}
            </React.Fragment>
          </div>
        )}
      </GoogleMap>
    );
  })
);

class Shopmap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shopmapList: [],
      open: false,
      event: null,
      token,
      checksAllList: false,
      currentposition: this.props.position,
      isLoading: true,
      date: new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1"),
      dateForma: "",
    };
  }

  componentDidMount() {
    this.setState({
      dateForma: this.state.date.substring(0, 5).replace(/:/gi, ""),
    });

    this.props.getCurrentLocation();
    if (this.state.token === null) {
      return history.push("/");
    }
    //this.props.getShop();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.position !== this.props.position) {
      this.setState({ currentposition: this.props.position });
      this.props.getShop(this.props.position);
    }
    if (prevProps.isLoading !== this.props.isLoading) {
      this.setState({ isLoading: this.props.isLoading });
    }
  }
  componentWillUnmount() {
    this.setState({ currentposition: {} });
  }

  handleClickOpen = (item, e) => {
    this.setState({ open: true });
    this.setState({ event: item });
    this.props.getShopChecks(item);
    this.props.setshopmapViewistory(item);
  };

  handleClose = () => {
    this.setState({ open: false, playing: false, checksAllList: false }, () => {
      this.props.getShop(this.props.position);
    });
  };

  /*getCurrentLocation() {
    navigator.geolocation.watchPosition(
      lastPosition => {
        this.setState({ currentposition: lastPosition });
      },
      error => console.log(JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 0 }
    );
  }*/

  render() {
    return (
      <div key={1}>
        <div key={2}>
          <div
            key={3}
            style={{
              position: "absolute",
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
            }}
          >
            <Link
              to={"/home"}
              style={{
                width: "25 px",
                height: "25px",
                position: "absolute",
                zIndex: "10",
                marginTop: "6px",
                right: "0px",
              }}
            >
              <IconButton color="inherit" aria-label="Menu">
                <img
                  src={closeeMap}
                  alt=""
                  style={{
                    width: "25 px",
                    height: "25px",
                  }}
                />
              </IconButton>
            </Link>

            {!this.state.isLoading && (
              <>
                <MapWithMaker
                  key={4}
                  isMarkerShown
                  center={this.state.currentposition}
                  googleMapURL={URI}
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={<div style={{ height: `100%`, top: 20 }} />}
                  mapElement={<div style={{ height: `100%` }} />}
                  onpressmarker={(e) => this.handleClickOpen(e)}
                  shopmapList={this.props.shopmapList}
                  dateForma={this.state.dateForma}
                />
              </>
            )}
          </div>
        </div>
        {/*Allcheck*/}
        <AllCheck
          onSelectEvent={this.state.event}
          currentposition={this.state.currentposition}
          open={this.state.open}
          allList={this.state.checksAllList}
          handleClose={() => this.handleClose()}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.ui.isLoading,
    position: state.position.position,
    shopmapList: state.shopmapList.shopmapList,
    shopmapData: state.shopmapData.shopmapData,
  };
};

const mapDispatchToProps = (dispatche) => {
  return {
    getShop: (data) => dispatche(getShop(data)),
    getShopChecks: (id) => dispatche(getShopChecks(id)),
    setshopmapViewistory: (item) => dispatche(setshopmapViewistory(item)),
    getCurrentLocation: () => dispatche(getCurrentLocation()),
  };
};
TagManager.initialize(tagManagerArgs);
export default compose(connect(mapStateToProps, mapDispatchToProps))(Shopmap);
