/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-07-08T09:26:31+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-10T15:28:19+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React from "react";
import { compose, withProps, lifecycle } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker
} from "react-google-maps";
import ArrivalDialog from "./arrival";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import currentuser from "../../assets/img/currentuser.svg";
import { connect } from "react-redux";
import closeeMap from "../../assets/img/closeeMap.png";
import { Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";

function Transition(props) {
  return <Slide direction="up" {...props} />;
}
const defaultMapOptions = {
  fullscreenControl: false
};

function calculateDistance(lat1, lon1, lat2, lon2) {
  var R = 6371; // km
  var dLat = ((lat2 - lat1) * Math.PI) / 180;
  var dLon = ((lon2 - lon1) * Math.PI) / 180;
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos((lat1 * Math.PI) / 180) *
    Math.cos((lat2 * Math.PI) / 180) *
    Math.sin(dLon / 2) *
    Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
}

const MapWithADirectionsRenderer = compose(
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyC7s7L47twQvDe_uGgtdBI1u7dAG5m_i1s&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentDidMount() {
      const DirectionsService = new window.google.maps.DirectionsService();

      DirectionsService.route(
        {
          origin: new window.google.maps.LatLng(
            this.props.position.latitude,
            this.props.position.longitude
          ),
          destination: new window.google.maps.LatLng(
            this.props.destination.lat,
            this.props.destination.lng
          ),
          travelMode: window.google.maps.TravelMode.DRIVING
        },
        (result, status) => {
          if (status === window.google.maps.DirectionsStatus.OK) {
            this.setState({
              directions: result
            });
          } else {
            console.error(`error fetching directions ${result}`);
          }
        }
      );
    }
  })
)(props => (
  <GoogleMap
    defaultOptions={defaultMapOptions}
    defaultZoom={7}
    defaultCenter={
      new window.google.maps.LatLng(
        props.position.latitude,
        props.position.longitude
      )
    }
  >
    <Marker
      position={{
        lat: props.position.latitude,
        lng: props.position.longitude
      }}
      icon={{
        url: currentuser,
        scaledSize: new window.google.maps.Size(25, 25)
      }}
    />
    <DirectionsRenderer directions={props.directions} />
  </GoogleMap>
));
class Route extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      finish: false,
      currentposition: this.props.position
    };
  }

  componentDidMount() {
    this.getCurrentLocation();
  }
  UNSAFE_componentWillMount() {
    setTimeout(
      function () {
        // this.getCurrentLocation();
        // console.log("get current");
        // // this.setState({ finish: true });
      }.bind(this),
      5000
    );
  }

  getCurrentLocation() {
    navigator.geolocation.watchPosition(
      lastPosition => {
        //console.log("currentposition : ", this.state.currentposition);
        //console.log("lastPosition : ", lastPosition);

        this.setState({ currentposition: { latitude: lastPosition.coords.latitude, longitude: lastPosition.coords.longitude } });
      },
      error => console.log(JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 0 }
    );
  }

  //TODO handle dialog in radius store
  componentDidUpdate(prevProps, prevState) {

    // console.log("currentposition ", prevState.currentposition, " VS ", this.state.currentposition);

    if (prevState.currentposition !== this.state.currentposition) {
      let distance = calculateDistance(this.state.currentposition.latitude, this.state.currentposition.longitude,
        this.props.destination.lat, this.props.destination.lng);
      //console.log("distance ", distance)
      if (distance < 0.05) {
        this.setState({ finish: true });
      }
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };
  handleFinishOpen = () => {
    this.setState({ finish: true });
  };
  handleFinishClose = () => {
    this.setState({ finish: false });
  };
  handleClose = () => {
    this.setState({ open: false, playing: false });
  };

  render() {
    return (
      <div>
        <Dialog
          fullScreen
          open={this.props.open}
          onClose={this.props.handleClose}
          TransitionComponent={Transition}
        >
          <div
            style={{
              position: "absolute",
              top: 0,
              bottom: 0,
              right: 0,
              left: 0
            }}
          >
            <ArrivalDialog
              open={this.state.finish}
              onClose={this.handleFinishClose}
              shopId={this.props.shopId}
            />
            <Link
              to={"/home"}
              style={{
                width: "25 px",
                height: "25px",
                position: "absolute",
                zIndex: "10",
                marginTop: "6px",
                right: "0px"
              }}
            >
              <IconButton color="inherit" aria-label="Menu">
                <img
                  src={closeeMap}
                  alt=""
                  style={{
                    width: "25 px",
                    height: "25px"
                  }}
                />
              </IconButton>
            </Link>

            <MapWithADirectionsRenderer
              position={this.state.currentposition}
              destination={this.props.destination}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    position: state.position.position
  };
};

export default compose(
  connect(
    mapStateToProps,
    null
  )
)(Route);
