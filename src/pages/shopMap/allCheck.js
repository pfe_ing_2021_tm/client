/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-07-03T10:37:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-09T09:25:31+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
// import kiosqueicn from "../../assets/img/kiosque.png";
import kiosqueicn from "../../assets/img/kiosque_1.png";
import closeicn from "../../assets/img/closemap.png";
import pin from "../../assets/img/pin.png";
import { styles } from "./style";
import Route from "./route";
import TimeAgo from "react-timeago";
import frenchStrings from "react-timeago/lib/language-strings/fr";
import buildFormatter from "react-timeago/lib/formatters/buildFormatter";
import { compose } from "redux";
import { connect } from "react-redux";
import CheckSelection from "./checkSelection";
import clock from "../../assets/img/clock.png";
function Transition(props) {
  return <Slide direction="up" {...props} />;
}
const formatter = buildFormatter(frenchStrings);
class AllCheck extends React.Component {
  state = {
    allList: false,
    inScroll: false,
    positionShow: false,
    selectedStore: null,
    open: false,
    destination: null,
    currentPosition: null,
    shopmapData: [],
    shopId: null,
    canCheck: false,
    CheckSelection: false,
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.currentposition !== this.props.currentposition) {
      this.setState({ currentposition: prevProps.currentposition });
    }
    if (prevProps.onSelectEvent !== this.props.onSelectEvent) {
      this.setState({
        destination: {
          lat: this.props.onSelectEvent.latitude,
          lng: this.props.onSelectEvent.longitude,
        },
      });
    }

    if (this.state.destination != prevState.destination) {
      let distance = this.calculateDistance(
        this.state.currentposition.latitude,
        this.state.currentposition.longitude,
        this.state.destination.lat,
        this.state.destination.lng
      );
      if (distance < 0.05) {
        this.setState({ canCheck: true });
      } else {
        this.setState({ canCheck: false });
      }
    }
  }

  calculateDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = ((lat2 - lat1) * Math.PI) / 180;
    var dLon = ((lon2 - lon1) * Math.PI) / 180;
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos((lat1 * Math.PI) / 180) *
        Math.cos((lat2 * Math.PI) / 180) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
  }

  componentWillReceiveProps() {
    this.setState({
      allList: this.props.allList,
      currentposition: this.props.currentposition,
    });
    if (!this.state.allList) {
      this.setState({
        inScroll: false,
      });
    }
    if (this.props.onSelectEvent) {
      this.setState({
        destination: {
          lat: this.props.onSelectEvent.latitude,
          lng: this.props.onSelectEvent.longitude,
        },
      });
    }
  }

  handleClickOpen = (store) => {
    this.setState({
      open: true,
      shopId: this.props.onSelectEvent,
      selectedStore: { lat: 35.8283991, lng: 10.583035 },
      // destination: { lat: 35.8283991, lng: 10.583035 }
      destination: {
        lat: this.props.onSelectEvent.latitude,
        lng: this.props.onSelectEvent.longitude,
      },
      // destination: this.props.position
    });
    // console.log("handleClickOpen", this.state.destination);
  };
  handleCheckIn = (store) => {
    this.setState({
      CheckSelection: true,
    });
  };

  handleClose = () => {
    this.setState(
      {
        open: false,
        playing: false,
        allList: false,
        CheckSelection: false,
      }
      // () => {
      //   window.location.reload();
      // }
    );
  };
  showAllList() {
    this.setState({ allList: true });
  }

  handleScroll = (e) => {
    if (e.target.scrollTop > 5) {
      this.setState({ inScroll: true });
    } else {
      this.setState({ inScroll: false });
    }

    let element = e.target;
    if (element.scrollHeight - element.scrollTop === element.clientHeight) {
      // do something at end of scroll
      this.setState({ inScroll: false });
    }
  };

  getStorePostion = (store) => {
    this.setState({
      inScroll: false,
      selectedStore: store,
      destination: store.position,
    });
  };

  render() {
    const { classes, ...other } = this.props;
    const { allList, inScroll } = this.state;
    return (
      <React.Fragment>
        <Dialog
          fullScreen
          open={this.props.open}
          onClose={this.props.handleClose}
          TransitionComponent={Transition}
        >
          <div className={classes.containdialog}>
            <div className={classes.sectionkiosque}>
              <img src={kiosqueicn} alt="" style={{ width: 220 }} />
              <img
                onClick={() => this.props.handleClose()}
                src={closeicn}
                alt=""
                className={classes.closeicon}
              />
            </div>
            <p
              style={{
                fontFamily: "NeulandGroteskCondensedRegular",
              }}
              className={classes.sectiontitle}
            >
              Derniers check-ins du
              <span
                style={{
                  fontFamily: "NeulandGroteskBold",
                  color: "#bc2132",
                  fontSize: "12px",
                }}
              >
                {this.props.onSelectEvent
                  ? " " + this.props.onSelectEvent.code
                  : ""}
              </span>
            </p>
            {this.props.onSelectEvent &&
            this.props.onSelectEvent.timeStart != "" &&
            this.props.onSelectEvent.timeStart != null &&
            this.props.onSelectEvent &&
            this.props.onSelectEvent.timeEnd != null &&
            this.props.onSelectEvent.timeEnd != "" ? (
              <p
                style={{
                  fontFamily: "NeulandGroteskCondensedRegular",
                }}
                className={classes.sectiontitle}
              >
                <img
                  style={{
                    width: "20px",
                    filter: "invert(1)",
                    marginBottom: "-3px",
                    marginRight: "10px",
                  }}
                  src={clock}
                  alt="clock"
                />
                Horaire &nbsp;
                <span
                  style={{
                    fontFamily: "NeulandGroteskBold",
                    color: "#bc2132",
                    fontSize: "16px",
                  }}
                >
                  {this.props.onSelectEvent
                    ? " " + this.props.onSelectEvent.timeStart
                    : ""}
                </span>
                &nbsp; à &nbsp;
                <span
                  style={{
                    fontFamily: "NeulandGroteskBold",
                    color: "#bc2132",
                    fontSize: "16px",
                  }}
                >
                  {this.props.onSelectEvent
                    ? " " + this.props.onSelectEvent.timeEnd
                    : ""}
                </span>
              </p>
            ) : (
              ""
            )}
            <div
              className={classes.sectionscroll}
              style={{ overflow: allList ? "scroll" : "hidden" }}
              onScroll={this.handleScroll}
            >
              {this.props.shopmapData.length !== 0 &&
                this.props.shopmapData.map((item, index) => {
                  return (
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        height: 130,
                        margin: 20,
                        padding: "0 15px",
                        alignItems: "center",
                        backgroundImage: `url(${
                          process.env.REACT_APP_DOMAIN + item.product.background
                        })`,
                        backgroundSize: "cover",
                        fontFamily: "NeulandGroteskLight",
                      }}
                    >
                      <div style={{ zIndex: 1 }}>
                        <div
                          style={{
                            display: "flex",
                            alignItems: "flex-end",
                            marginBottom: 13,
                          }}
                        >
                          <img
                            src={pin}
                            alt=""
                            style={{ width: 15, marginRight: 5 }}
                          />

                          <p
                            style={{
                              margin: "0",
                              fontSize: 11,
                              color: "#fff",
                            }}
                          >
                            <TimeAgo
                              date={item.checkInDate}
                              formatter={formatter}
                            />
                          </p>
                        </div>

                        <p
                          style={{
                            fontSize: 17,
                            color: "#fff",
                            margin: "7px 0",
                            fontWeight: "100",
                            textTransform: "uppercase",
                          }}
                        >
                          {item.customerPseudo}
                        </p>
                        {item.disponible ? (
                          <p
                            style={{
                              color: "#E4AC00",
                              margin: "5px 0",
                              fontSize: 11,
                              textTransform: "uppercase",
                            }}
                          >
                            disponible !
                          </p>
                        ) : (
                          <p
                            style={{
                              color: "red",
                              margin: "5px 0",
                              fontSize: 11,
                              textTransform: "uppercase",
                            }}
                          >
                            non disponible !
                          </p>
                        )}
                      </div>
                      <img
                        src={process.env.REACT_APP_DOMAIN + item.product.icon}
                        style={{ width: 50 }}
                        alt=""
                      />
                    </div>
                  );
                })}
            </div>

            {!inScroll && (
              <Slide
                direction="up"
                appear={true}
                in={!inScroll}
                mountOnEnter
                unmountOnExit
              >
                <div className={classes.sectionbtn}>
                  {!allList ? (
                    <div className={classes.allchechbtncont} />
                  ) : (
                    <div className={classes.allchechbtncontOneBtn} />
                  )}
                  {!allList && (
                    <p
                      className={classes.btnalcheck}
                      onClick={() => this.showAllList()}
                      id="Tous les check in"
                    >
                      "Tous les check in"
                    </p>
                  )}
                  {!this.state.canCheck && (
                    <p
                      className={classes.btnalcheck}
                      onClick={() => this.handleClickOpen()}
                      id={`Itinéraire`}
                    >
                      Itinéraire
                    </p>
                  )}
                  {this.state.canCheck && (
                    <p
                      className={classes.btnalcheck}
                      onClick={() => this.handleCheckIn()}
                      id={`Check-in_${this.props.onSelectEvent.code}`}
                    >
                      Check-in
                    </p>
                  )}
                  {!allList ? (
                    <div className={classes.footallcheck} />
                  ) : (
                    <div className={classes.footallcheckOneBtn} />
                  )}

                  {this.state.open && (
                    <Route
                      open={this.state.open}
                      handleClose={this.handleClose}
                      shopId={this.props.onSelectEvent}
                      destination={this.state.destination}
                    />
                  )}
                </div>
              </Slide>
            )}
          </div>
          {this.props.onSelectEvent && (
            <CheckSelection
              open={this.state.CheckSelection}
              handleClose={this.handleClose}
              shopId={this.props.onSelectEvent}
            />
          )}
        </Dialog>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    shopmapData: state.shopmapData.shopmapData,
  };
};

AllCheck.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps))(AllCheck);
