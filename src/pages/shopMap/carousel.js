/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-07-10T15:15:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-15T09:54:34+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import React from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import "./style.scss";
import citynext from "../../assets/img/citynext.png";
import cityback from "../../assets/img/cityback.png";
import disponible from "../../assets/img/disponible.png";
import nondisponible from "../../assets/img/nondisponible.png";
import Validercheck from "../../assets/img/Valider-check.png";
import NoOneChecked from "../../assets/img/NoOneChecked.png";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { setCheckInProduct } from "../../store/actions";
import { setshopmapViewistory } from "../../store/actions/history";

class Carousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.items,
      active: this.props.active,
      selectedIndex: 1,
      selectedItem: {},
      direction: "",
      activeIndex: 0,
      productsdispo: [],
      submit: false,
      checkinHistory: true,
      limitecheckin: this.props.checkinLimite,
    };
    this.rightClick = this.moveRight.bind(this);
    this.leftClick = this.moveLeft.bind(this);
  }
  //setshopmapViewistory => add in table history action 15
  componentDidMount() {
    this.setState({ limitecheckin: {} });
    let item = {};
    item._id = this.props.shopId._id;
    item.checkinHistory = this.state.checkinHistory;
    this.props.setshopmapViewistory(item);
    this.state.items.forEach((element, index) => {
      if (this.state.selectedIndex === index) {
        this.setState({
          selectedItem: element,
        });
      }
    });
  }
  componentDidUpdate(PrevProps) {
    if (PrevProps.checkinLimite !== this.props.checkinLimite) {
      this.setState({ limitecheckin: this.props.checkinLimite });
    }
  }
  generateItems() {
    var items = [];
    var level;
    for (var i = this.state.active - 2; i < this.state.active + 3; i++) {
      var index = i;
      if (i < 0) {
        index = this.state.items.length + i;
      } else if (i >= this.state.items.length) {
        index = i % this.state.items.length;
      }
      level = this.state.active - i;

      items.push(
        <Item
          key={index}
          id={this.state.items[index]}
          image={this.state.items[index].image}
          active={this.state.active}
          level={level}
          addProduct={(id) => this.addProduct(id)}
          removeProduct={(id) => this.removeProduct(id)}
          productsdispo={this.state.productsdispo}
        />
      );
    }

    return items;
  }

  addProduct = (product) => {
    const arr = this.state.productsdispo.filter(
      (obj) => obj.product !== product.product
    );
    const obj = this.state.productsdispo.filter(
      (obj) =>
        obj.product === product.product && obj.disponible === product.disponible
    );
    if (obj.length === 0) {
      this.setState({
        productsdispo: [...arr, product],
      });
    } else {
      this.setState({ productsdispo: arr });
    }
    //window.location.reload();
  };

  removeProduct = async (id) => {
    const arr = this.state.productsdispo.filter(
      (obj) => obj.product !== id.product
    );
    this.setState({ productsdispo: arr });
  };

  moveLeft() {
    var newActive = this.state.active;
    newActive--;
    let active = newActive < 0 ? this.state.items.length - 1 : newActive;
    let selectedIndex =
      active + 1 <= this.state.items.length - 1 ? active + 1 : 0;
    this.state.items.forEach((element, index) => {
      if (selectedIndex === index) {
        this.setState({
          selectedItem: element,
        });
      }
    });
    this.setState({
      active: newActive < 0 ? this.state.items.length - 1 : newActive,
      direction: "left",
      selectedIndex: selectedIndex,
    });
  }

  moveRight() {
    var newActive = this.state.active;
    let active = (newActive + 1) % this.state.items.length;
    let selectedIndex =
      active + 1 <= this.state.items.length - 1 ? active + 1 : 0;
    this.state.items.forEach((element, index) => {
      if (selectedIndex === index) {
        this.setState({
          selectedItem: element,
        });
      }
    });
    this.setState({
      active: (newActive + 1) % this.state.items.length,
      direction: "right",
      selectedIndex: selectedIndex,
    });
  }

  handleTouchMove = (value) => {
    // console.log(value);
  };

  handleSendCheckProducts = () => {
    const data = {
      shopmap: this.props.shopId._id,
      products: this.state.productsdispo,
      customer: localStorage.getItem("_id"),
    };
    this.props.setCheckInProduct(data);
    this.setState({ submit: true });
  };

  render() {
    //console.log("state : ",this.state)
    return (
      <div>
        {this.state.submit ? (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <div
              style={{
                position: "absolute",
                bottom: 80,
                alignItems: "center",
                display: "flex",
                flexDirection: "column",
              }}
            >
              {this.state.limitecheckin &&
              this.state.limitecheckin.limite == false ? (
                <p
                  style={{
                    fontFamily: "NeulandGroteskCondensedRegular",
                    textAlign: "center",
                    color: "#919191",
                    textTransform: "uppercase",
                  }}
                >
                  félicitations! <br />
                  <span
                    style={{
                      fontFamily: "NeulandGroteskCondensedRegular",
                      color: "#ed2a1c",
                      fontSize: 22,
                      fontWeight: 700,
                    }}
                  >
                    50 reds
                  </span>
                </p>
              ) : this.state.limitecheckin &&
                this.state.limitecheckin.limite ? (
                <p style={{ marginBottom: "50%" }}>
                  <span
                    style={{
                      fontFamily: "NeulandGroteskCondensedRegular",
                      color: "#ed2a1c",
                      fontSize: 22,
                      fontWeight: 700,
                    }}
                  >
                    Merci pour votre checkin !
                  </span>
                </p>
              ) : (
                ""
              )}

              <Link
                to="/"
                style={{
                  marginTop: "50%",
                  fontFamily: "NeulandGroteskLight",
                  color: "#fff",
                  textDecoration: "none",
                  cursor: "pointer",
                }}
              >
                Retour à la page d'accueil
              </Link>
            </div>
          </div>
        ) : (
          <div>
            <div id="carousel" className="noselect">
              <p className="titledesc">sélectionner les produits disponibles</p>
              <div className="arrow arrow-left" onClick={this.leftClick}>
                <img
                  src={cityback}
                  alt="imgproduct"
                  style={{ width: "100%" }}
                />
              </div>
              <ReactCSSTransitionGroup
                transitionAppear={true}
                transitionName={this.state.direction}
                onTouchMove={(touchMoveEvent) =>
                  this.handleTouchMove(touchMoveEvent)
                }
              >
                {this.generateItems()}
              </ReactCSSTransitionGroup>
              <div className="arrow arrow-right" onClick={this.rightClick}>
                <img src={citynext} alt="" style={{ width: "100%" }} />
              </div>
            </div>
            <div
              style={{
                display: "flex",
                alignContent: "center",
                justifyContent: "center",
              }}
            >
              {this.state.productsdispo.length === 0 ? (
                <>
                  <img src={NoOneChecked} className="btnSubmitCheck" alt="" />
                </>
              ) : (
                <>
                  <img
                    id={`Valider_check_${this.state.productsdispo.length}_produits`}
                    src={Validercheck}
                    className="btnSubmitCheck"
                    onClick={this.handleSendCheckProducts}
                    alt=""
                  />
                </>
              )}
              <div style={{ zIndex: 99, position: "absolute", bottom: 70 }}>
                {/* <div
                  style={{
                    backgroundColor: "red",
                    padding: "10px 20px",
                    color: "rgb(255, 255, 255)",
                    borderRadius: "10px",
                    textAlign: "center",
                    fontFamily: "NeulandGroteskLight",
                    fontSize: "15px",
                  }}
                >
                  Non <br></br> Disponible
                </div> */}
                <img
                  onClick={() =>
                    this.addProduct({
                      product: this.state.selectedItem.key,
                      disponible: false,
                    })
                  }
                  src={nondisponible}
                  style={{
                    zIndex: 99,
                    width: 150,
                  }}
                />
                <img
                  onClick={() =>
                    this.addProduct({
                      product: this.state.selectedItem.key,
                      disponible: true,
                    })
                  }
                  src={disponible}
                  style={{
                    zIndex: 99,
                    width: 150,
                  }}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    checkinLimite: state.checkinLimite.checkinLimite,
  };
};
const mapToDispatch = (dispatch) => {
  return {
    setCheckInProduct: (data) => dispatch(setCheckInProduct(data)),
    setshopmapViewistory: (data) => dispatch(setshopmapViewistory(data)),
  };
};
export default connect(mapStateToProps, mapToDispatch)(Carousel);

class Item extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      level: this.props.level,
    };
  }

  handleAddProduct = (id) => {
    this.props.addProduct({ product: id, disponible: true });
  };

  handleRemoveProduct = (id) => {
    this.props.removeProduct({ product: id, disponible: false });
  };

  render() {
    const className = "item level" + this.props.level;
    let check = "disablecheck";
    if (
      this.props.productsdispo.some(
        (item) => item.product === this.props.id.key && item.disponible === true
      )
    ) {
      check = "enablecheckdisp";
    }
    if (
      this.props.productsdispo.some(
        (item) =>
          item.product === this.props.id.key && item.disponible === false
      )
    ) {
      check = "enablechecknondips";
    }
    return (
      <div className={className}>
        {this.props.level === -1 && <span className={check}></span>}
        <img
          src={this.props.image}
          alt={this.props.id.key}
          className={"imageProducts"}

          /*onClick={() =>
            this.props.productsdispo.some(
              item =>
                item.product === this.props.id.key && item.disponible === true
            )
              ? this.handleRemoveProduct(this.props.id.key)
              : this.handleAddProduct(this.props.id.key)
          }*/
        />
      </div>
    );
  }
}
