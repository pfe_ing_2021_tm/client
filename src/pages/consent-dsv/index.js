import React, { useState } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import headerBg from "../../assets/img/pmipg.png";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import vedioPlayerContainer from "../../assets/img/vedio.png";
import playbtn from "../../assets/img/playbtn.png";
import Button from "@material-ui/core/Button";
import Videoplayer from "./vedioPlayer";
import Footer from "./footer";
import { history } from "../../history";

const Consent_dsv = (props) => {
  const [age, setAge] = useState(false);
  const [accept, setAccept] = useState(false);
  const [vedio, setVedio] = useState(false);

  const { classes } = props;

  const handleCheckAge = (event) => {
    setAge(event.target.checked);
  };

  const handleCheckAccept = (event) => {
    setAccept(event.target.checked);
  };
  const handleVedioPlayClick = () => {
    setVedio(true);
  };

  const handleCloseVedioPlay = () => {
    setVedio(false);
  };

  const clickAccepter = () => {
    if (age && accept) history.push("/");
  };

  return (
    <React.Fragment>
      {!vedio ? (
        <div className={classes.Container}>
          <img src={headerBg} style={{ width: "100%" }} />
          <div className={classes.vedioPlayerContainer}>
            <img src={vedioPlayerContainer} style={{ width: "100%" }} />
            <img
              src={playbtn}
              className={classes.playbtn}
              id="PLAY_VIDEO_CONSENT"
              alt="play"
              onClick={handleVedioPlayClick}
            />
          </div>

          <div className={classes.paragraph}>
            <p>
              Vous avez consenti dans le passé que Philip Morris Services S.A à
              travers des parties tierces à la réception de communication à des
              fins commerciales, à la collecte, traitement et archivage de vos
              données dans les limites des lois applicables, notamment la loi
              sur la protection de données. Le but de ce message est de vérifier
              non seulement votre âge et statut mais également votre volonté
              continue de rester en communication avec nous.
            </p>
          </div>
          <div className={classes.order}>
            <center>
              Veuillez cocher les conditions ci-dessous et appuyer sur accepter{" "}
            </center>
          </div>
          <div className={classes.checkboxeContainer}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  className={classes.checkbox}
                  checked={age}
                  onChange={handleCheckAge}
                  id="CONSENT_AGE_CHECK"
                />
              }
              classes={{
                label: classes.label,
              }}
              label="Je suis un adulte fumeur de plus de 18 ans"
            />
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  className={classes.checkbox}
                  checked={accept}
                  onChange={handleCheckAccept}
                  id="CONSENT_CONTACT_CHECK"
                />
              }
              classes={{
                label: classes.label,
              }}
              label="J’accepte d’être contacté par Philip Morris Services S.A "
            />
          </div>
          <Button
            id="ACCEPTER_CONSENT"
            variant="contained"
            color="primary"
            style={!(age && accept) ? { backgroundColor: "gray" } : {}}
            className={classes.btnAccept}
            onClick={clickAccepter}
          >
            <center>ACCEPTER</center>
          </Button>
          <div className={classes.bottomParagraph}>
            <p>
              PHILIP MORRIS SERVICES S.A 2020. TOUS DROITS RÉSERVÉS. PRIÈRE DE
              NE PAS PARTAGER SUR AUCUN CANAL DE MÉDIAS SOCIAL CES PUBLICATIONS
              (Y COMPRIS DES IMAGES ET DES TEXTES) OU DES HASHTAGS LIÉS A NOS
              MARQUES ET PRODUITS. <br />
              POUR PLUS D'INFORMATION, VISITEZ NOTRE SITE: HTTPS://WWW.PMI.COM
            </p>
          </div>
          <Footer />
          <div>
            <p className={classes.footerNextStep}>
              avis important : fumer nuit à la santé
            </p>
          </div>
        </div>
      ) : (
        <Videoplayer classes={classes} onclose={handleCloseVedioPlay} />
      )}
    </React.Fragment>
  );
};

export default withStyles(styles)(Consent_dsv);
