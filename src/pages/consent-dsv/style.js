import backgroundImg from "../../assets/img/background.jpg";

export const styles = (theme) => ({
  Container: {
    display: "flex",
    flexDirection: "column",
    [theme.breakpoints.down("xl")]: {
      marginBottom: "50px",
    },
    [theme.breakpoints.down("md")]: {
      fontSize: 15.5,
      marginBottom: "50px",
    },
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: 18.5,
    },
    justifyContent: "flex-start",
    width: "100vw",
    color: theme.palette.secondary.main,
    fontSize: "9px",
    fontFamily: "NeulandGroteskCondensedLight",
    fontWeight: "100",
    lineHeight: "1.4",
    letterSpacing: "1.3px",
    width: "fit-content",
  },
  vedioPlayerContainer: {
    margin: "0px 15px",
    position: "relative",
  },
  playbtn: {
    width: "10%",
    margin: "-5px -20px",
    position: "absolute",
    top: "40%",
    left: "50%",
    transform: "translate(-50% -50%)",
    cursor: "pointer",
  },
  paragraph: {
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
    alignItems: "flex-start",
    alignContent: "flex-start",
    margin: "0px 5% 0px 5%",
    textAlign: "center",
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: 18.5,
      //marginBottom: "50px",
    },
    [theme.breakpoints.between("md", "xl")]: {
      fontSize: 16.5,
      letterSpacing: "1px",
    },
    fontSize: "13px",
  },

  order: {
    textAlign: "center",
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: 26.5,
      marginBottom: "50px",
      letterSpacing: "1px",
    },
    [theme.breakpoints.between("md", "xl")]: {
      fontSize: 17.5,
      letterSpacing: "0.8px",
    },
    WordWrap: '"break-word"',
    fontSize: 16,
    margin: "0px 0% 20px 0%",
    letterSpacing: "0.8px",
    padding: "0px 15px 0px 15px",
  },
  btnAccept: {
    color: "#e8e8e8",
    width: "30%",
    border: "none",
    cursor: "pointer",
    padding: "0px 9px",
    fontSize: "20px",
    alignSelf: "center",
    fontFamily: "NeulandGroteskCondensedRegular",
    borderRadius: "4px",
    marginBottom: "10px",
    letterSpacing: "1px",
    textTransform: "uppercase",
    backgroundColor: "#9a0c1a",
  },
  checkboxeContainer: {
    margin: "0px 20px 15px",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
    alignItems: "flex-start",
    alignContent: "flex-start",
    // [theme.breakpoints.between("md", "xl")]: {
    //   fontSize: 20.5,
    //   margin: "30px 70px",
    // },
  },
  label: {
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: 18.5,
    },
    [theme.breakpoints.between("md", "xl")]: {
      fontSize: 16.5,
      letterSpacing: "1px",
    },
    fontSize: "13px",
    fontFamily: "NeulandGroteskCondensedLight",
    fontWeight: "100",
    lineHeight: "1.6",
    letterSpacing: "1.1px",
  },
  checkbox: {
    color: "#9a0c1a",
    padding: 2,
  },
  bottomParagraph: {
    backgroundColor: "#222222",
    textAlign: "center",
    fontSize: "8px",
    letterSpacing: "1px",
    color: "#787878",
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: 10.5,
    },
    [theme.breakpoints.between("md", "xl")]: {
      fontSize: 12.5,
    },
    marginTop: "10px",
  },
  footerNextStep: {
    width: "100%",
    bottom: "0",
    height: "35px",
    margin: "0",
    position: "fixed",
    fontSize: "10px",
    background: "rgb(255 255 255)",
    textAlign: "center",
    fontWeight: "500",
    lineHeight: "35px",
    textTransform: "uppercase",
    color: "black",
    fontFamily: "NeulandGroteskBold",
    [theme.breakpoints.up(817)]: {
      width: "40%",
    },
    [theme.breakpoints.up(1024)]: {
      width: "24%",
    },
    [theme.breakpoints.between(1024, 1440)]: {
      width: "38%",
    },
  },

  videoProgreesTitle: {
    position: "absolute",
    left: "30px",
    top: "30%",
  },
  videoProgressBoxDesc: {
    display: "flex",
    alignItems: "flex-start",
  },
  playVideoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  playVideoAuthor: {
    color: "#999",
    fontSize: 13,
  },

  // ------------------------------ footer style

  bottomDarkContainer: {
    width: "100%",
    height: "3vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "7px",
    flexDirection: "row",
    backgroundColor: "#111112",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      justifyContent: "space-around",
      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      width: 386,
      margin: "0 0 0 0",
    },
  },
  bottomTypography: {
    color: "white",
    fontFamily: "NeulandGroteskCondensedBold",
    textTransform: "uppercase",
    [theme.breakpoints.between(580, 800)]: {
      // 800px screen change to mobile style
      marginTop: "2%",
      fontSize: "1.2rem",
      letterSpacing: "0.2rem",
    },
  },

  // ------------------------------ vedio player style
  root: {
    width: "100%",
  },
  backdrops: {
    background: "#000",
  },
  inputEntryCode: {
    width: 42,
    height: 45,
    background: "transparent",
    border: `1px solid ${theme.palette.secondary.main} `,
    margin: "0 5px",
    borderRadius: 5,
    color: theme.palette.secondary.main,
    fontSize: 36,
    textAlign: "center",
    lineHeight: "40px",
  },
  returnToHomebtn: {
    color: "rgb(255, 255, 255)",
    background: "none",
    border: "1px solid #666",
    padding: "5px 10px",
    borderRadius: 15,
    width: 240,
    cursor: "pointer",
    fontSize: 18,
    textAlign: "center",
  },
  paperprops: {
    width: "100%",
    background: "#000",
    justifyContent: "space-between",
    alignItems: "flex-start",
    display: "contents",
  },
  scrollPaper: {
    justifyContent: "flex-end",
  },
  imgLike: {
    width: 90,
  },
  liketext: {
    color: "#0ede29",
    fontSize: 18,
    marginTop: 30,
    fontWeight: 800,
    textAlign: "center",
  },
  imgdLike: {
    width: 90,
    transform: "rotate(180deg)",
    top: 22,
    position: "relative",
  },
  btnnumb: {
    border: `1px solid ${theme.palette.secondary.main} `,
    borderRadius: 5,
    height: 42,
    width: 35,
    textAlign: "center",
    fontSize: 30,
    color: theme.palette.secondary.main,
    margin: "15px 10px",
    cursor: "pointer",
  },
  dliketext: {
    color: "#fe1100",
    fontSize: 18,
    marginTop: 30,
    fontWeight: 800,
    textAlign: "center",
  },
  bottomend: {
    color: theme.palette.secondary.main,
    display: "flex",
    position: "absolute",
    bottom: -79,
    left: 0,
    justifyContent: "space-between",
    width: "100%",
  },
  background: {
    background: `url(${backgroundImg})`,
    width: "100%",
    position: "relative",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundAttachment: "fixed",
  },
  sliderContainer: {
    width: "100%",
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "center",
    alignContent: "center",
  },
  sliderPoster: {
    width: "85%",
    marginBottom: -15,
  },
  vidcont: {},
  videoDescripCont: {
    display: "flex",
    justifyContent: "space-between",
    width: "70%",
    alignItems: "center",
    position: "absolute",
  },
  videoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  videoAuthor: {
    color: theme.palette.secondary.main,
  },
  playVideoAuthor: {
    color: "#999",
    fontSize: 13,
  },
  iconPlay: {
    width: 50,
    color: theme.palette.secondary.main,
    marginBottom: -7,
  },
  dialogPaper: {
    overflow: "hidden",
  },
  btnNext: {
    display: "flex",
    flexDirection: "row",
    marginTop: 25,
    color: theme.palette.secondary.main,
    justifyContent: "flex-end",
    width: "90%",
  },
  titleNext: {
    color: theme.palette.secondary.main,
  },
  iconNext: {
    fontSize: 20,
    color: "#C70013",
    marginLeft: 10,
  },
  bannerContainer: {
    width: "100%",
    justifyContent: "center",
    display: "flex",
    marginTop: 25,
  },
  bannerItem: {
    flexDirection: "column",
  },
  bannerIndicator: {
    width: 10,
    margin: "2px 5px",
  },
  bannerCarousel: {
    width: "85%",
  },
  playlistDescripCont: {
    display: "flex",
    justifyContent: "space-between",
    width: "90%",
    alignItems: "center",
    position: "absolute",
    paddingBottom: 15,
  },
  icntabhead: {
    color: theme.palette.secondary.main,
    position: "relative",
    top: 2,
  },
  playListTitle: {
    color: theme.palette.secondary.main,
    fontSize: 30,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  playVideoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  iconPlayAudio: {
    width: 30,
    color: theme.palette.secondary.main,
  },
  iconOff: {
    width: 20,
    color: theme.palette.secondary.main,
  },
  drivWhi: {
    position: "relative",
    top: -2,
    fontSize: 10,
    color: theme.palette.secondary.main,
  },
  galerieSmallItem: {
    height: 100,
    overflow: "hidden",
    marginBottom: 8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  progressBarCont: {
    background: "rgba(000, 000, 000, 0.8)",
    height: 35,
    padding: "0 0px 40px 0px",
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
  progressLine: {
    width: "100%",
    height: 3,
    position: "relative",
    top: -17,
  },
  videoProgreesTitle: {
    position: "absolute",
    left: "30px",
    top: "30%",
  },
  videoProgressBoxDesc: {
    display: "flex",
    alignItems: "flex-start",
  },
  progressImg: {
    width: 50,
    position: "relative",
    top: -8,
    left: 10,
  },
  progressIcn: {
    color: theme.palette.secondary.main,
    position: "relative",
    top: -10,
  },
  videoCloseCont: {
    position: "absolute",
    top: 10,

    borderRadius: 0,
    right: 0,
    padding: "5px 7px",
    zIndex: 1,
  },
  gobackOrClose: {},

  videoCloseIcn: {
    color: theme.palette.secondary.main,
    border: `1px solid ${theme.palette.secondary.main} `,
    borderRadius: 50,
    fontSize: 16,
  },
  boxcategory: {
    transformOrigin: "0 0 0",
    background: theme.palette.secondary.main,
    height: 125,
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    margin: 5,
    flex: 1,
  },
  headtabcont: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  conttab: {
    display: "flex",
    justifyContent: "space-between",
    width: "90%",
    alignItems: "center",
    alignContent: "center",
  },
  headtab: {
    color: theme.palette.secondary.main,
    textTransform: "uppercase",
    fontWeight: 500,
    borderBottom: "3px solid #ed2a1c",
    paddingBottom: 5,
  },
  slide: {
    perspective: 1000,
    overflow: "hidden",

    position: "relative",
    paddingTop: 8,
  },
  imageContainer: {
    display: "flex",
    position: "relative",
    zIndex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    display: "block",
    width: "85%",
    objectFit: "cover",
  },
  arrow: {
    display: "none",
    position: "absolute",
    top: "50%",
    transform: "translateY(-50%)",
  },
  arrowLeft: {
    left: 0,
  },
  arrowRight: {
    right: 0,
  },
  text: {
    fontWeight: 900,
    position: "absolute",
    zIndex: 1,
    color: theme.palette.secondary.main,
    padding: "0 8px",
    lineHeight: 1.2,
  },
  title: {
    bottom: 20,
    zIndex: 9999,
    fontSize: 17,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  subtitle: {
    top: 60,
    left: "0%",
    height: "52%",
    fontSize: 56,
    background: "linear-gradient(0deg, rgba(255,255,255,0) 0%, #888888 100%)",
  },
  gameimg: {
    width: 200,
    marginLeft: 50,
  },
  dlikecont: {
    display: "flex",
    flexDirection: "column",
    margin: 20,
  },
  likecont: {
    display: "flex",
    flexDirection: "column",
    margin: 20,
  },
  endvideobox: {
    display: "flex",
    flexDirection: "row",
    marginRight: 10,
  },
  videoendcont: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  dividers: {
    width: 4,
  },
  btnreplay: {
    display: "flex",
    alignItems: "center",
  },
  btnreplayimg: {
    width: 30,
    marginRight: 15,
    transform: "rotate(-70deg)",
  },
  btnreplaytext: {
    textTransform: "uppercase",
    fontWeight: 700,
    fontSize: 20,
  },
  btnnextvideo: {
    display: "flex",
    alignItems: "center",
  },
  btnnextvideotext: {
    textTransform: "capitalize",
    fontSize: 17,
  },
  btnnextvideoicn: {
    width: 25,
    marginLeft: 15,
  },
  btnvalider: {
    width: 150,
    marginLeft: 15,
  },
})
