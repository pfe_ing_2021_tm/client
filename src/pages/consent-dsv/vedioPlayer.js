import React, { useState, useEffect } from "react";
import {
  Player,
  ControlBar,
  VolumeMenuButton,
  CurrentTimeDisplay,
  TimeDivider,
  ReplayControl,
  ForwardControl,
} from "video-react";
import { styles } from "./style";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { withStyles } from "@material-ui/core/styles";
import "./styles/scss/video-react.scss";

const VedioPlayer = (props) => {
  const { classes } = props;

  return (
    <Dialog
      fullScreen
      open={true}
      PaperProps={{ classes: { root: classes.dialogPaper } }}
      style={{ overflow: "hidden " }}
    >
      <div className={classes.vidcont}>
        <IconButton
          style={{ zIndex: 10 }}
          color="inherit"
          onClick={props.onclose}
          aria-label="Close"
          className={classes.videoCloseCont}
          id="ClOSE_CONSENT_VIDEOPLAYER"
        >
          <CloseIcon className={classes.videoCloseIcn} style={{ zIndex: 10 }} />
        </IconButton>
        <div className={classes.vidcont}>
          <div>
            <div>
              <Player playsInline>
                <source
                  src={
                    "https://discoveereprd.s3-eu-west-1.amazonaws.com/videos/Marlboro_Discovered_Trailer_non+smoking_video_fr_Tunisia_AE002.mp4"
                  }
                />
                <ControlBar autoHide={true}>
                  <CurrentTimeDisplay order={4.1} />
                  <TimeDivider order={4.2} />
                  <ReplayControl seconds={5} order={2.1} />
                  <ForwardControl seconds={5} order={3.1} />
                  <VolumeMenuButton />
                </ControlBar>
              </Player>
            </div>
          </div>
        </div>
      </div>
    </Dialog>
  );
};
export default withStyles(styles)(VedioPlayer);
