import React, { useState, useEffect } from "react";
import { styles } from "./style";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const Footer = (props) => {
  const { classes } = props;
  return (
    <div className={classes.bottomDarkContainer}>
      <a href="https://www.pmiprivacy.com/fr/consumer">
        <Typography component="h4" className={classes.bottomTypography}>
          notre avis de confidentialité
        </Typography>
      </a>
    </div>
  );
};

export default withStyles(styles)(Footer);
