import React, { Component } from 'react';
import { styles } from './styles'
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import Fade from '@material-ui/core/Fade';


class Cardpopup extends Component {
    state = {
        open: this.props.boolean,
    }
    render() {
        const classes = this.props
        return (<div>
            <Dialog
                className={classes.modal}
                open={this.state.open}


            >
                <Fade in={this.state.open}>
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            padding: '4%',
                            marginTop: '81%',
                            backgroundColor: "#42160c"
                        }}
                    >

                        <div
                            style={{
                                color: 'white',
                                fontSize: '20px',

                            }}>
                            <h4 >VOUS AVEZ CUMMULE MOINS QUE {process.env.REACT_APP_REDS} STARS</h4>

                            <Link to='/home'>
                                <Button>retour</Button>
                            </Link>
                        </div>
                    </div>


                </Fade>

            </Dialog>
        </div>
        )
    }
}
export default withStyles(styles)(Cardpopup)
