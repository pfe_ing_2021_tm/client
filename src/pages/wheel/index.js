/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-25 12:44:37
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-09-18 10:44:06
 */
import "./_Wheel.css";
import React from "react";
import { withStyles } from "@material-ui/core/styles";

import denimg from "../../assets/img/wheel/403-error.png";
import WheelBack from "../../assets/img/wheel/R4.png";
import end from "../../assets/img/wheel/end.png";
import TweenMax, { Power4 } from "gsap";
import pinfix from "../../assets/img/wheel/pinfix.png";
import pinfixRed from "../../assets/img/wheel/pinfixRed.png";
import header from "../../assets/img/wheel/head.png";
import btnstart from "../../assets/img/wheel/btnstart.png";
import REDSIconWheel from "../../assets/img/wheel/REDSIconWheelBrown.png";
import DialogWin from "./dialogwin";
import loadingGif from "../../assets/img/loading.gif";
import CircularProgress from "@material-ui/core/CircularProgress";
import { history } from "../../history";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { styles } from "./styles";
import Cardpopup from "./popup";
import WHEEL_light from "../../assets/img/WHEEL_light.png";
import { getGifts, getGiftsWin } from "../../store/actions";
import { compose } from "recompose";
import SoundWheel from "../../assets/Wheel+Sound.mp3";
import LoseWheel from "../../assets/Lose+Sound.mp3";
import WinWheel from "../../assets/Win+Sound.mp3";

// const SoundWheel =
//   "https://discoveereprd.s3-eu-west-1.amazonaws.com/sounds/Wheel+Sound.mp3";

let tick = new Audio(SoundWheel);
class Wheel extends React.Component {
  constructor(props) {
    super(props);
    const token = localStorage.getItem("token");
    this.state = {
      gifts: [],
      awardHidden: true,
      winvalue: 0,
      open: false,
      canvas: "",
      start: true,
      token,
      loading: true,
      title: "",
      status: "error",
      isNotGift: false,
      reds: "",
      popup: false,
      points: 0,
      sound: false,
    };
  }
  componentDidMount() {
    if (this.state.token === null) {
      return history.push("/");
    }

    tick = new Audio(SoundWheel);
    this.props.getGifts();
  }

  UNSAFE_componentWillMount() {
    document.getElementsByTagName("html")[0].style.background = "#fff";
  }

  componentWillUnmount() {
    document.getElementsByTagName("html")[0].style.background = "#000";
    tick.pause();
    tick = new Audio("");
  }
  componentWillReceiveProps() {
    // if (this.props.giftWin !== null) {
    //   this.setState({
    //     winvalue: this.props.giftWin.data.code,
    //     loading: false,
    //     reds: this.props.giftWin.data.score,
    //     title: this.props.giftWin.data.title,
    //     status: this.props.giftWin.status,
    //   });
    // }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.wheelgift.data !== this.props.wheelgift.data) {
      this.setState({
        gifts: this.props.wheelgift.data,
        loading: false,
        reds: this.props.wheelgift.score,
      });
      if (this.props.wheelgift.data.length !== 8) {
        return history.push("/");
        // this.setState({ isNotGift: true });
      }
    }
    if (prevProps.giftWin !== this.props.giftWin) {
      if (this.props.giftWin !== null) {
        this.setState(
          {
            winvalue: this.props.giftWin.data.code,
            loading: false,
            reds: this.props.giftWin.data.score,
            title: this.props.giftWin.data.title,
            status: this.props.giftWin.status,
            points: this.props.giftWin.data.points,
          },
          () => {
            if (this.state.status === "success") {
              this.handleSpin();
            } else {
              this.setState({
                popup: true,
              });
            }
          }
        );
      }
    }
  }
  resultWheelSound = (sound) => {
    var tick = new Audio(sound);
    const promisse = tick.play();
    if (promisse !== undefined) {
      promisse
        .then(() => {
          tick.play();
        })
        .catch((error) => console.error);
    }
  };

  handleTourner = () => {
    tick.play();

    if (this.state.reds < process.env.REACT_APP_REDS) {
      this.setState({ popup: true });
    } else {
      this.props.getGiftsWin();
    }
  };

  handleSpin = () => {
    let that = this;
    that.setState({ start: false });

    var spin = () => {
      //change valuewin onclose dialog
      var wheel = that.refs.wheel;
      var awards = [
        { prize: 1, startAngle: 0, endAngle: 45 }, // 0 : 300+
        { prize: 2, startAngle: 45, endAngle: 90 }, // 50dt bon d'achat
        { prize: 3, startAngle: 90, endAngle: 135 }, // 90 : 5Dt recharge
        { prize: 4, startAngle: 135, endAngle: 180 }, // 135 : nouvelle chance
        { prize: 5, startAngle: 180, endAngle: 225 }, // 180 : 500 reds
        { prize: 6, startAngle: 225, endAngle: 270 }, // 50+ milles
        { prize: 7, startAngle: 270, endAngle: 315 }, // 270 : 2 dt recharge
        { prize: 8, startAngle: 315, endAngle: 360 }, //  315  : pas de chance
      ];
      var fullspin = Math.floor(Math.random() * (360 - 1 + 1)) + 1 + 360 * 5;

      var prizeValue = null;
      var spin = (fullspin % 360) - 30;

      //awards

      awards.forEach(function (element, index, array) {
        if (
          element.startAngle <= Math.abs(spin) &&
          element.endAngle >= Math.abs(spin)
        ) {
          prizeValue = element.prize;
        }
      });

      TweenMax.to(wheel, 10, {
        rotation: 360 * 100 + Number(that.state.winvalue), //that.state.winvalue,
        transformOrigin: "50% 50%",
        ease: Power4.easeOut,
        onUpdateParams: [wheel],
        onComplete: function () {
          setTimeout(() => {
            that.handleOpenDialog();
          }, 600);

          that.setState({ start: true, sound: true });
          that.setState({ awardHidden: false, prize: prizeValue });
        },
        onStart: function () {
          //tick.play();

          const promisse = tick.play();
          if (promisse !== undefined) {
            promisse
              .then(() => {
                tick.play();
              })
              .catch((error) => console.error);
          }
        },

        onUpdate: function (data) {
          var currentRotation = this.target._gsTransform.rotation;
          if (
            (Math.round(currentRotation) % (360 / 12) <= 7 &&
              Math.round(currentRotation) % (360 / 12) !== 0) ||
            Math.round(currentRotation) % 100 == 0
          ) {
            // const promisse = tick.play();
            // if (promisse !== undefined) {
            //   promisse
            //     .then(() => {
            //       tick.play();
            //     })
            //     .catch(error => console.error);
            // }
          }
        },
      });
    };

    spin();
  };

  reset = () => {
    var wheel = this.refs.wheel;
    TweenMax.to(wheel, 0, {
      rotation: 360,
      transformOrigin: "50% 50%",
      onUpdateParams: [wheel],
      onComplete: () => {
        if (this.state.sound) {
          if (this.state.points > 0) {
            this.resultWheelSound(WinWheel);
          } else if (this.state.points === 0) {
            //console.log("lose");
            this.resultWheelSound(LoseWheel);
          }
        }
      },
    });
  };

  handleCloseDialog = () => {
    this.setState({
      open: false,
    });
  };

  handleOpenDialog = async () => {
    this.reset();
    if (this.props.giftWin.status === "success") {
      this.setState({ open: true });
    }
  };

  render() {
    if (this.state.loading) {
      return <CircularProgress color="secondary" />;
    } else {
      if (this.state.gifts.length > 0) {
        return (
          <div className={this.props.classes.wheelRoot}>
            {this.state.start && (
              <Link to="/home">
                <IconButton
                  color="inherit"
                  onClick={this.handleClose}
                  aria-label="Close"
                  className={this.props.classes.videoCloseCont}
                >
                  <CloseIcon className={this.props.classes.videoCloseIcn} />
                </IconButton>
              </Link>
            )}

            {this.state.open && (
              <DialogWin
                open={this.state.open}
                prize={this.state.winvalue}
                title={this.state.title}
                handleClose={this.handleCloseDialog}
              />
            )}

            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItem: "center",
                filter: this.state.open ? "blur(8px)" : "none",
                position: "absolute",
                left: "50%",
                transform: "translate(-50% , -50%)",
                top: 30,
              }}
            >
              <img
                className={this.props.classes.titleGame}
                src={header}
                alt=""
              />
            </div>

            <div
              id="wrapper"
              className="wrapper"
              ref="wrapper"
              style={{
                filter: this.state.open ? "blur(8px)" : "none",
                backgroundImage: `url(${WheelBack})`,
                zIndex: "99",
                width: "315px",
                height: "430px",
                backgroundPosition: "center center",
                backgroundSize: "contain",
                backgroundRepeat: "no-repeat",
                marginTop: "0px",
                position: "absolute",
                top: "-155px",
                transform: "translate(-50%, 50%)",
                left: "50%",
              }}
            >
              {/* <img src={WHEEL_light} alt="" className="wheelLight" /> */}
              <div id="wheel" className="wheel" ref="wheel">
                <div id="inner-wheel" className="innerwheel">
                  {this.state.gifts.map((item, index) => (
                    <div key={index} ref="sec" className="sec">
                      <span className="fa" id="fa">
                        <div
                          style={{
                            transform: "rotate(90deg)",
                            marginTop: -39,
                            position: "absolute",
                            marginLeft: 15,
                            height: 86,
                            display: "flex",
                            alignItems: "center",
                            flexDirection: "column",
                            justifyContent: "space-around",
                          }}
                        >
                          <img
                            src={index % 2 == 0 ? pinfixRed : pinfix}
                            alt=""
                            style={{ width: 15 }}
                          />
                          <img
                            src={index % 2 == 0 ? pinfixRed : pinfix}
                            alt=""
                            style={{ width: 15, marginLeft: -6 }}
                          />
                          <img
                            src={index % 2 == 0 ? pinfixRed : pinfix}
                            alt=""
                            style={{ width: 15 }}
                          />
                        </div>
                        <p
                          style={{
                            color: "#f8d214",
                            fontSize: "16px",
                            position: "absolute",
                            top: "-4px",
                            right: "-43px",
                            width: "80px",
                            textShadow:
                              "1px 1px 2px red, 0 0 1em blue, 0 0 0.2em blue",
                          }}
                        >
                          {item.title}
                        </p>
                        <img
                          src={`${process.env.REACT_APP_DOMAIN}${item.imageroue}`}
                          alt={item.title}
                          className="imggift"
                          style={{
                            width: "20px !important",
                            height: "49px",
                            paddingTop: "25px",
                            marginLeft: "-2px",
                          }}
                        />
                      </span>
                    </div>
                  ))}
                </div>
              </div>
              <div
                style={{
                  background: "#000",
                  width: 20,
                  zIndex: 999,
                  height: 20,
                  top: "47%",
                  position: "absolute",
                  left: "46.6%",
                  borderRadius: "50%",
                }}
              />
              <img
                src={end}
                alt=""
                style={{
                  width: 53,
                  zIndex: 999,
                  top: 55,
                  position: "absolute",
                  left: "42%",
                }}
              />
            </div>
            <div className={this.props.classes.footerWheel}>
              <div className={this.props.classes.reds}>
                <img
                  alt="REDSIconWheel"
                  style={{
                    width: "40px",
                  }}
                  src={REDSIconWheel}
                />
                <p className={this.props.classes.redstxt}>
                  {this.state.reds} &nbsp;STARS
                </p>
              </div>
              {/* add test number reds<1000 */}
              {this.state.popup && (
                <div
                  style={{
                    backgroundColor: "#42160c",
                    boxShadow: "12px 12px 2px 1px rgba(0, 0, 255, .2)",
                  }}
                >
                  <Cardpopup boolean={this.state.popup} />
                </div>
              )}
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  filter: this.state.open ? "blur(8px)" : "none",
                }}
              >
                {this.state.start && (
                  <img
                    id="Tourner wheel"
                    src={btnstart}
                    alt=""
                    onClick={this.handleTourner}
                    className={this.props.classes.tournerbnt}
                  />
                )}
              </div>
            </div>
            {/* <p className={this.props.classes.avisfumer}>avis important : fumer nuit à la santé</p> */}
            <div id="shine" className="shine" />
          </div>
        )
      } else {
        return (
          <div
            style={{
              background: "#fff",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <img src={loadingGif} width="150px" alt="" />
          </div>
        );
      }
    }
  }
}
const mapStateToProps = (state) => {
  return {
    wheelgift: state.Wheel.wheelgift,
    giftWin: state.Gift.giftWin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGifts: () => dispatch(getGifts()),
    getGiftsWin: () => dispatch(getGiftsWin()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Wheel);
