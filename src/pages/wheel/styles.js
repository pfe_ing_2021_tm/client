export const styles = (theme) => ({
  videoCloseCont: {
    position: "absolute",
    borderRadius: 0,
    top: 5,
    padding: "5px 7px",
    zIndex: 99999,
  },
  videoCloseIcn: {
    color: "#000",
    border: "1px solid #000",
    fontSize: 20,
    borderRadius: 50,
  },
  footerWheel: {
    marginTop: 350,
    position: "relative",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      bottom: 5,
      position: "absolute",
      left: "50%",
      transform: "translate(-50% , -50%)",
      marginTop: 0,
    },
  },
  avisfumer: {
    width: "100%",
    bottom: 0,
    height: "25px",
    margin: 0,
    position: "fixed",
    // fontSize: "11px",
    background: "rgb(255, 255, 255)",
    textAlign: "center",
    fontFamily: "NeulandGroteskCondensedBold",
    fontSize: 15,
    // fontWeight: "700",
    lineHeight: "24px",
    textTransform: "uppercase",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  redstxt: {
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 40,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
    color: "#42160c",

    margin: 0,
    fontFamily: "NeulandGroteskCondensedBold",
    textDecoration: "underline",
  },
  tournerbnt: {
    width: 190,
    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      width: 350,
      marginBottom: "11vh",
    },

    cursor: "pointer",
  },
  reds: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",

    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      marginTop: "-15%",
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      marginTop: "0%",
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      marginTop: "-15%",
    },
  },
  titleGame: {
    width: "120px",
    height: "90px",
    marginBottom: -40,
    marginTop: 25,
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      marginBottom: -80,
      marginTop: 0,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      width: "170px !important",
      height: "110px !important",
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
    },
  },
  wheelRoot: {
    background: "#fff",
    paddingBottom: 25,
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    position: "relative",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      height: "90vh",
    },
  },
});
