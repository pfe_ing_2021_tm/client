import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import dialogwin from "../../assets/img/wheel/dialogwin.png";
import btnsubmit from "../../assets/img/wheel/btnsubmit.png";
import { history } from "../../history";
function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const styles = {
  dialogPaper: {
    alignItems: "center",
    overflow: "visible"
  },
  backDropPaper: {
    background: "transparent"
  },
  btnsub: {
    position: "relative",
    bottom: -30,
    width: 200,
    cursor: "pointer"
  },
  text: {
    textTransform: "uppercase",
    textAlign: "center",
    fontSize: 19,
    fontWeight: 500,
    margin: 0
  }
};

class DialogWin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prize: this.props.prize,
      open: false
    };
  }

  handleClose = () => {
    this.props.handleClose();
    history.push(`/profile`);
    //history.push("/profile");
  };

  handleMsg = (classes, prize) => {
    if (Number(prize) === 315) {
      return <p className={classes.text}>Pas de chance</p>;
    } else if (Number(prize) === 135) {
      return <p>Nouvelle chance</p>;
    } else {
      return (
        <p className={classes.text}>
          Félicitations!
          <br /> vous avez gagné <br />
          {this.props.title}
        </p>
      );
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <Dialog
        open={this.props.open}
        TransitionComponent={Transition}
        PaperProps={{ className: classes.dialogPaper }}
        BackdropProps={{ className: classes.backDropPaper }}
        keepMounted
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <img src={dialogwin} style={{ width: 300 }} alt="" />
        {this.handleMsg(classes, this.props.prize)}
        <img
          src={btnsubmit}
          alt=""
          className={classes.btnsub}
          onClick={this.handleClose}
        />
      </Dialog>
    );
  }
}
export default withStyles(styles)(DialogWin);
