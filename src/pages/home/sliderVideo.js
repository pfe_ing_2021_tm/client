/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-01T09:34:27+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React from "react";
import SwipeableViews from "react-swipeable-views";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ExtraWithoutF from "./extrawithoutforward";
import { Link } from "react-router-dom";
import cx from "clsx";
import IcnPlay from "../../assets/img/play.svg";
import { styles } from "./style";
import Pagination from "./pagination";
import nextIcn from "../../assets/img/next.png";
const calculateMargin = (selfIndex, slideIndex, speed = 50) => {
  const diff = selfIndex - slideIndex;
  if (Math.abs(diff) > 1) return 0;
  return diff * speed + "%";
};

class SliderVideo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      fineIndex: 0,
      cityData: this.props.cityData
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.cityData !== this.props.cityData) {
      this.setState({ index: 0 });
    }
  }
  onNextVideoShowButton = (index) => {
    return this.state.fineIndex === index - 1 ? false : true;
  };
  onNextVideoClick = (index) => {
    this.setState({
      fineIndex: this.state.fineIndex + 1,
      index: this.state.index + 1,
    });
  };
  onChangeIndex = i => {
    this.setState({ index: i, fineIndex: i });
  };
  render() {
    const { index, fineIndex } = this.state;
    const {
      classes,
      transition,
      children,
      renderElements,
      cityData,
      ...props
    } = this.props;

    const createStyle = (slideIndex, fineIndex) => {
      const diff = slideIndex - fineIndex;
      if (Math.abs(diff) > 1) return {};
      return {
        transform: "rotateY" + (-diff + 1) * 45 + "deg"
      };
    };
    const onChangeIndex = i => {
      this.setState({ index: i, fineIndex: i });
    };
    const renderChildren = ({ injectStyle, fineIndex }) =>
      cityData.length !== 0 &&
      cityData.map((item, i) => (
        <div key={i} className={classes.slide}>
          <div>
            <Typography
              noWrap
              className={cx(classes.text, classes.title)}
              style={{
                ...injectStyle(i, 60),
                ...createStyle(i, fineIndex),
                left: "10%",
                bottom: item.vid.type === "forward_to_extra" ? "19%" : 80,
                fontSize: 35,
                width: "60%",
                fontFamily: "NeulandGroteskCondensedBold",
                textAlign: "left",
                textTransform: "none",
                // textTransform: "uppercase"
              }}
            >
              {item.vid.title}
            </Typography>
            <Typography
              noWrap
              className={cx(classes.text, classes.title)}
              style={{
                ...injectStyle(i, 60),
                ...createStyle(i, fineIndex),
                left: "10%",
                fontWeight: 300,
                fontFamily: "NeulandGroteskCondensedRegular",
                fontSize: 25,
                bottom: item.vid.type === "forward_to_extra" ? "15%" : 60,
                textTransform: "none",
                // textTransform: "capitalize"
              }}
            >
              {item.vid.subtitle}
            </Typography>
            <Typography
              noWrap
              className={cx(classes.text, classes.title)}
              style={{
                ...injectStyle(i, 40),
                ...createStyle(i, fineIndex),
                right: "10%",
                bottom: item.vid.type === "forward_to_extra" ? "13%" : 70,
              }}
            >
              <Link to={`/videoplayer/${item.vid._id}`}>
                <img
                  id={`Video_Principal_${item.vid.title}`}
                  src={IcnPlay}
                  alt="play"
                  className={classes.iconPlay}
                  onClick={this.handleClickOpen}
                />
              </Link>
            </Typography>
            <div className={classes.imageContainer}>
              <img
                className={classes.image}
                src={`${process.env.REACT_APP_DOMAIN}${item.vid.image}`}
                alt={"slide"}
                style={{
                  borderStyle: "solid",
                  borderWidth: "6px",
                  borderImage: item.watched
                    ? "linear-gradient(\n45deg\n, rgb(24 53 245), rgb(66 193 250)) 1"
                    : "linear-gradient(\n45deg\n, rgb(110 110 110), rgb(112 112 112)) 1 / 1 / 0 stretch",
                }}
              />
              {item.vid.type !== "" && item.vid.type !== "forward_to_extra" && (
                <>
                  <Link
                    to={`/videoplayer/${item.vid._id}`}
                    style={{ width: "85%" }}
                  >
                    <img
                      id={`Video_Principal_${item.vid.title}`}
                      src={require(`../../assets/img/forward_img/${item.vid.type}.png`)}
                      alt="play"
                      className={classes.imgforwad}
                      onClick={this.handleClickOpen}
                    />
                  </Link>
                </>
              )}
            </div>
          </div>
          <div>
            {item.vid.type === "forward_to_extra" && (
              <ExtraWithoutF
                title={item.vid.video.title}
                link={item.vid.video.link}
                idextra={item.vid.video._id}
              />
            )}
          </div>
        </div>
      ))
    return (
      <div>
        <SwipeableViews
          resistance
          springConfig={{
            duration: "0.6s",
            easeFunction: "",
            delay: "0s"
          }}
          enableMouseEvents
          {...props}
          index={index}
          onChangeIndex={this.onChangeIndex}
          onSwitching={i => {
            this.setState({ fineIndex: i });
          }}
        >
          {renderChildren({
            fineIndex,
            injectStyle: (slideIndex, speed) => ({
              marginLeft: calculateMargin(slideIndex, fineIndex, speed),
              transition: fineIndex === index ? transition : "none"
            })
          })}
        </SwipeableViews>
        {this.onNextVideoShowButton(cityData.length) && (
          <div
            className={classes.btnnextvideoHome}
            onClick={() => {
              this.onNextVideoClick();
            }}
          >
            <p className={classes.btnnextvideotextHome}>épisode suivant</p>
            <img
              src={nextIcn}
              alt="next video"
              className={classes.btnnextvideoicn}
            />
          </div>
        )}
        {renderElements({ index, onChangeIndex })}
        <Pagination
          dots={cityData.length}
          index={index}
          onChangeIndex={this.onChangeIndex}
        />
      </div>
    );
  }
}

SliderVideo.defaultProps = {
  transition: "0.8s",
  renderElements: () => { }
};

export default withStyles(styles)(SliderVideo);
