/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-19T16:39:47+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Typography from "@material-ui/core/Typography";
import { history } from "../../history";
import Fade from "@material-ui/core/Fade";
import GalerieList from "./galerieList";
import RecipeList from "./recipesList";
import GameList from "./gamesList";
import { styles } from "./style";
import { compose } from "redux";
import { connect } from "react-redux";
import BrandsSelection from "../brands";
function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

class FullWidthTabs extends React.Component {
  state = {
    value: 0,
    isShowMenu: true,
    isPlaylist: false,
    isGallery: false,
    isOpencityrecipe: true,
    openBrandsSelection: false,
    open: this.props.open,
  };

  componentDidMount() {
    const sectionSelected = localStorage.getItem("sectionSelected");
    if (sectionSelected === "jeux") {
      return this.handleJeuxTabs();
    } else if (sectionSelected === "recette") {
      return this.handleRecetteTabs();
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = (index) => {
    this.setState({ value: index });
  };
  componentDidUpdate(prevProps) {
    if (prevProps.scrolltosection !== this.props.scrolltosection) {
      this.goTo(this.props.scrolltosection);
    }
  }
  closeBrand = () => {
    this.setState({ openBrandsSelection: false, open: false });
  };
  goTo = (value) => {
    if (value === "jeux") {
      this.handleJeuxTabs();
    } else if (value === "galerie") {
      this.handleGalerrieTabs();
    } else if (value === "recette") {
      this.handleRecetteTabs();
    } else if (value === "Brand") {
      this.setState({ openBrandsSelection: true });
    } else if (value === "events") {
      history.push("/events");
    }
  };

  handleJeuxTabs = () => {
    localStorage.setItem("sectionSelected", "jeux");
    this.setState({
      isPlaylist: false,
      isGames: true,
      isGallery: false,
      isRecipes: false,
      isShowMenu: false,
    });
  };

  handleRecetteTabs = () => {
    localStorage.setItem("sectionSelected", "recette");
    this.setState({
      isPlaylist: false,
      isGames: false,
      isGallery: false,
      isRecipes: true,
      isShowMenu: false,
    });
  };

  handleGalerrieTabs = () => {
    localStorage.setItem("sectionSelected", "galerie");
    this.setState({
      isPlaylist: false,
      isGames: false,
      isGallery: true,
      isRecipes: false,
      isShowMenu: false,
    });
  };

  back = () => {
    localStorage.removeItem("sectionSelected");
    this.onGoToDetails();
    if (this.state.isDetails) {
      this.setState({
        isPlaylist: false,
        isGames: false,
        isGallery: false,
        isRecipes: false,
        isShowMenu: false,
        isOpencityrecipe: true,
      });
    } else {
      this.setState({
        isPlaylist: false,
        isGames: false,
        isGallery: false,
        isRecipes: false,
        isShowMenu: true,
      });
    }
  };

  onGoToDetails = (value) => {
    //  alert(value);
  };

  render() {
    const { classes } = this.props;
    const { isGallery, isGames, isRecipes, isShowMenu } = this.state;
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100%",
          marginTop: 10,
          marginBottom: 20,
        }}
      >
        {this.state.openBrandsSelection && (
          <BrandsSelection
            handleClose={() => this.closeBrand()}
            open={this.state.openBrandsSelection}
            notif={false}
          />
        )}
        <div className={classes.root}>
          <div style={{}}>
            {isShowMenu && (
              <Fade in={isShowMenu} mountOnEnter unmountOnExit>
                <div
                  style={{
                    marginBottom: "20vh",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  {this.props.data.map((item, index) => (
                    <div key={index}>
                      <img
                        src={process.env.REACT_APP_DOMAIN + item.image}
                        alt=""
                        style={{ width: "100%" }}
                        onClick={() => this.goTo(item.type)}
                        id={`Bannier_${item.type}`}
                      />
                    </div>
                  ))}
                  {/*<div>
                    <img
                      src={sectionRecettes}
                      alt=""
                      style={{ width: "100%" }}
                      onClick={() => this.goTo("recipes")}
                    />
                  </div>

                  <div>
                    <img
                      src={sectionJeux}
                      alt=""
                      style={{ width: "100%" }}
                      onClick={() => this.goTo("games")}
                    />
                  </div>

                  <div>
                    <img
                      src={sectionGalerie}
                      alt=""
                      style={{ width: "100%" }}
                      onClick={() => this.goTo("gallery")}
                    />
                  </div>*/}
                </div>
              </Fade>
            )}
          </div>
          <div>
            {isGallery && (
              <Fade in={isGallery} mountOnEnter unmountOnExit>
                <div className={classes.headtabcont}>
                  <div className={classes.conttab}>
                    <div
                      style={{ display: "flex", alignItems: "center" }}
                      onClick={this.back}
                    >
                      <Icon className={classes.icntabhead}>
                        keyboard_backspace
                      </Icon>
                      <p
                        style={{
                          color: "#fff",
                          marginLeft: 5,
                          fontFamily: "NeulandGroteskLight",
                        }}
                      >
                        Retour
                      </p>
                    </div>
                    <p
                      style={{ fontFamily: "NeulandGroteskLight" }}
                      className={classes.headtab}
                    >
                      Galerie
                    </p>
                  </div>
                  <GalerieList />
                </div>
              </Fade>
            )}

            {isRecipes && (
              <Fade in={isRecipes} mountOnEnter unmountOnExit>
                <div className={classes.headtabcont}>
                  <div className={classes.conttab}>
                    <div
                      style={{ display: "flex", alignItems: "center" }}
                      onClick={this.back}
                    >
                      <Icon className={classes.icntabhead}>
                        keyboard_backspace
                      </Icon>
                      <p
                        style={{
                          color: "#fff",
                          marginLeft: 5,
                          fontFamily: "NeulandGroteskLight",
                        }}
                      >
                        Retour
                      </p>
                    </div>
                    <p
                      className={classes.headtab}
                      style={{ fontFamily: "NeulandGroteskLight" }}
                    >
                      Recettes
                    </p>
                  </div>
                  <RecipeList
                    handleSubBack={this.onGoToDetails}
                    open={this.state.isOpencityrecipe}
                  />
                </div>
              </Fade>
            )}

            {isGames && (
              <Fade in={isGames} mountOnEnter unmountOnExit>
                <div
                  className={classes.headtabcont}
                  style={{ marginBottom: 0 }}
                >
                  <div className={classes.conttab}>
                    <div
                      style={{ display: "flex", alignItems: "center" }}
                      onClick={this.back}
                    >
                      <Icon className={classes.icntabhead}>
                        keyboard_backspace
                      </Icon>
                    </div>
                    <p
                      className={classes.headtab}
                      style={{ fontFamily: "NeulandGroteskLight" }}
                    >
                      Jeux
                    </p>
                  </div>
                  <GameList />
                </div>
              </Fade>
            )}
          </div>
        </div>
      </div>
    )
  }
}

FullWidthTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    scrolltosection: state.scrolltosection.scrolltosection,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(FullWidthTabs);
