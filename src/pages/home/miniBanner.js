/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-17T14:41:41+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React, { Component } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Carousel } from "react-responsive-carousel";
import { styles } from "./style";
import { Link } from "react-router-dom";

import indicator from "../../assets/img/indicator.png";
import indicatorAct from "../../assets/img/indicatorAct.png";

class MiniBanner extends Component {
  state = {
    item: 0,
    currentIndex: 0
  };

  pagination = index => {
    this.setState({ item: index });
  };

  change = e => {
    this.setState({ currentIndex: e });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.bannerContainer}>
        <div className={classes.bannerCarousel}>
          <Carousel
            infiniteLoop
            autoPlay
            showThumbs={false}
            showArrows={false}
            showIndicators={false}
            showStatus={false}
            selectedItem={this.state.item}
            onChange={e => this.change(e)}
          >
            {this.props.data.map((slide, index) => (
              <Link key={index} to={`${slide.link}`}>
                <div id={`slide_${slide.name}`}>
                  <img
                    src={process.env.REACT_APP_DOMAIN + slide.image}
                    alt=""
                  />
                </div>
              </Link>
            ))}
          </Carousel>

          <div className={classes.bannerItem}>
            {this.props.data.map((item, index) =>
              this.state.currentIndex === index ? (
                <img
                  className={classes.bannerIndicator}
                  key={index}
                  src={indicatorAct}
                  onClick={() => this.pagination(index)}
                  alt=""
                />
              ) : (
                <img
                  className={classes.bannerIndicator}
                  key={index}
                  src={indicator}
                  onClick={() => this.pagination(index)}
                  alt=""
                />
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}

MiniBanner.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MiniBanner);
