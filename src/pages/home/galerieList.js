/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Thu Apr 25 2019
 */

import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { styles } from "./style";
import PicImg from "../../assets/img/pic.png";
import PanImg from "../../assets/img/pan.png";
import PanlImg from "../../assets/img/panl.png";
import OffImg from "../../assets/img/offgr.png";

class GalerieList extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container spacing={8} style={{ marginBottom: "30vh" }}>
        <Grid
          container
          xs={12}
          spacing={8}
          style={{
            flexDirection: this.props.direction ? "row-reverse" : "row"
          }}
        >
          <Grid item xs={8}>
            <div
              className={classes.paper}
              style={{ height: 208, overflow: "hidden" }}
            >
              <img src={PicImg} alt="" style={{ width: "100%" }} />
            </div>
          </Grid>
          <Grid item xs={4}>
            {[1, 2].map(item => (
              <Grid item xs={12}>
                <div className={classes.galerieSmallItem}>
                  <img src={PanImg} alt="" style={{ width: "100%" }} />
                  <img
                    src={OffImg}
                    alt=""
                    style={{ position: "absolute", width: 25 }}
                  />
                </div>
              </Grid>
            ))}
          </Grid>
        </Grid>

        <Grid container xs={12} spacing={8}>
          {[1, 2, 3].map(item => (
            <Grid item xs={4}>
              <div className={classes.galerieSmallItem} style={{ height: 100 }}>
                <img src={PanlImg} alt="" style={{ width: "100%" }} />
                <img
                  src={OffImg}
                  alt=""
                  style={{ position: "absolute", width: 25 }}
                />
              </div>
            </Grid>
          ))}
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(GalerieList);
