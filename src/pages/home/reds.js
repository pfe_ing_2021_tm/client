/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-28 17:04:17
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-29 11:29:31
 */
import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import { withStyles } from "@material-ui/core/styles";
import { setUserChallengesStatus, setUserBadges } from "../../store/actions";
import RedsIcon from "../../assets/img/endstore.png";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";

const styles = {
  dialogPaper: {
    alignItems: "center",
    overflow: "visible",
    width: "100%"
  }
};

class Reds extends Component {
  state = {
    open: true,
    index: this.props.index
  };

  componentDidMount() {
    if (this.props.count !== 0) {
      document.getElementById("root").style.filter = "blur(4px)";
    }
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = id => {
    let obj = {
      id: id,
      reds: this.props.data.reds
    }
    this.props.setUserChallengesStatus(obj);
    this.setState({ open: false });

    if (this.state.index === 0) {
      document.getElementById("root").style.filter = null;
      //this.props.setUserBadges();
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <Dialog
        open={this.state.open}
        PaperProps={{ className: classes.dialogPaper }}
        aria-labelledby="simple-dialog-title"
      //{...other}
      >
        <img src={RedsIcon} alt="" style={{ width: 245, marginBottom: 5 }} />
        <p
          style={{
            //marginTop: "35px",
            textAlign: "center",
            color: "#919191",
            textTransform: "uppercase"
          }}
        >
          <span>
            Bravo <br /> Grace a votre classement dans le Challenge du mois  <br /> Vous avez gagné <span style={{ color: '#ed2a1c', fontSize: 22, fontWeight: 700 }}>{this.props.data.reds}</span> REDs
          </span>
        </p>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 40
          }}
        >
          {/*<Link to="/home" id={`Reponse : ${this.props.data}`}>*/}
          <Button
            id={`Reponse : ${this.props.data}`}
            onClick={() => this.handleClose(this.props.data.id)}
            variant="contained"
            style={{
              position: "absolute",
              bottom: -15,
              background: "#ed2a1c",
              color: "#fff",
              //marginLeft: -32
            }}
          >
            Ok
          </Button>
          {/*</Link>*/}
        </div>
        <div />
      </Dialog>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    setUserChallengesStatus: id => dispatch(setUserChallengesStatus(id)),
    setUserBadges: () => dispatch(setUserBadges([]))
  };
};
export default compose(
  withStyles(styles),
  connect(
    null,
    mapDispatchToProps
  )
)(Reds);
