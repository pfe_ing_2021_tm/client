/*
 * Created Date: Friday December 13th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Friday, December 13th 2019, 12:00:47 pm
 * Modified By: Amir Dorgham
 * -----
 */
import React from "react";
import PropTypes from "prop-types";

const styles = {
  root: {
    height: 25,
    width: 25,
    cursor: "pointer",
    border: 0,
    background: "none",
    padding: 0
    // padding: "0px 6px 0px 6px"
  },
  dot: {
    backgroundColor: "rgba(120,120,120,1)",
    height: 10,
    width: 10,
    borderRadius: 6,
    margin: 3,
    // padding: "10px 5px"
  },
  active: {
    backgroundColor: "white"
  }
};

class PaginationDot extends React.Component {
  handleClick = event => {
    this.props.onClick(event, this.props.index);
  };

  render() {
    const { active } = this.props;

    let styleDot;

    if (active) {
      styleDot = Object.assign({}, styles.dot, styles.active);
    } else {
      styleDot = styles.dot;
    }

    return (
      <button type="button" style={styles.root} onClick={this.handleClick}>
        <div style={styleDot} />
      </button>
    );
  }
}

PaginationDot.propTypes = {
  active: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
};

export default PaginationDot;
