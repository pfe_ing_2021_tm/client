/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-06-13T16:08:17+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-19T11:39:40+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import IcnPlay from "../../assets/img/playaudio.png";
import Icnoff from "../../assets/img/off.png";
import quizshotimg from "../../assets/img/game1.png";
import codegameimg from "../../assets/img/game2.png";
import quizinfoimg from "../../assets/img/fillquiz.png";
import puzzelimg from "../../assets/img/puzzle.png";
import { getListGames } from "../../store/actions";
import { history } from "../../history";
//List Component
class Item extends React.Component {
  state = {
    open: false,
    recipe: 0,
  };

  handleClose = () => {
    this.props.onClose();
  };

  handleClickOpen = (index) => {
    this.setState({ open: true, recipe: index });
  };
  handleClickClose = () => {
    this.setState({ open: false });
  };

  isActive = (item) => {
    return !item.didwin;
  };
  componentDidMount() {
    window.scrollTo(0, 5000);
  }

  render() {
    const { classes } = this.props;
    let imageGame;
    let linkto;
    if (this.props.type === "QUIZZ") {
      imageGame = quizshotimg;
      linkto = "quizshot";
    } else if (this.props.type === "CODE") {
      imageGame = codegameimg;
      linkto = "entrycode";
    } else if (this.props.type === "QUIZINFO") {
      imageGame = quizinfoimg;
      linkto = "quizinfo";
    }else if (this.props.type === "PUZZLE") {
      imageGame = puzzelimg;
      linkto = "puzzleGame";
    }
    const imagequizinfo = quizshotimg;

    return (
      <div style={{ marginLeft: -24, marginRight: -24, display: "contents" }}>
        {this.props.data.map((item, index) => (
          <div className={classes.Bannier} key={index}>
            <div
              onClick={() =>
                this.isActive(item)
                  ? history.push(
                      `/${linkto}/${item.idquizz ? item.idquizz : item._id}`,
                      { videoid: item.id_vedio }
                    )
                  : null
              }
              style={
                {
                  // display: "flex",
                  // justifyContent: "center",
                  // alignItems: "center"
                }
              }
            >
              <div
                className={classes.gamesImage}
                style={{ width: "80vw !important" }}
              >
                <div>
                  {item.videoTitle !== undefined ? (
                    <Typography
                      component="p"
                      className={classes.playListTitle}
                      style={{
                        textShadow:
                          "1px 1px 2px red, 0 0 1em blue, 0 0 0.2em blue",
                      }}
                    >
                      {item.videoTitle}
                    </Typography>
                  ) : (
                    <Typography
                      component="p"
                      className={classes.playListTitle}
                      style={{
                        textShadow:
                          "1px 1px 2px red, 0 0 1em blue, 0 0 0.2em blue",
                      }}
                      /*component="p"
                      style={{
                        fontSize: 16,
                        fontWeight: 500,
                        textTransform: "uppercase",
                        marginTop: 110,
                        marginLeft: 20
                      }}*/
                    >
                      {/*item.title*/}
                    </Typography>
                  )}
                </div>
                {this.isActive(item) ? (
                  <img
                    src={IcnPlay}
                    alt="play"
                    className={classes.iconPlayAudio}
                    onClick={() =>
                      this.isActive(item) ? this.handleClickOpen(index) : null
                    }
                  />
                ) : (
                  <img src={Icnoff} alt="play" className={classes.iconOff} />
                )}
              </div>
              <img
                src={
                  item.rubrique
                    ? process.env.REACT_APP_DOMAIN + item.rubrique
                    : imageGame
                }
                alt=""
                style={{
                  width: "100%",
                  //position: "absolute",
                  height: "100%",
                  //overflow: "hidden",
                  filter: this.isActive(item) ? "none" : "grayscale(1)",
                }}
              />
               
            </div>
          </div>
        ))}
      </div>
    )
  }
}

const List = withStyles(styles)(Item);

class GameList extends Component {
  state = {
    open: false,
    gameselected: null,
  };

  componentDidMount() {
    this.props.getListGames();
  }

  handleClickOpen = (value) => {
    this.setState({ open: true, gameselected: value });
  };

  handleClickClose = () => {
    this.setState({ open: false });
  };

  render() {
    let gameSelected;
    if (this.state.gameselected === "QUIZZ") {
      gameSelected = this.props.listGames[1].quizlist;
    } else if (this.state.gameselected === "CODE") {
      gameSelected = this.props.listGames[2].codelist;
    } else if (this.state.gameselected === "PUZZLE") {
      gameSelected = this.props.listGames[3].puzzlelist;
    } else if (this.state.gameselected === "QUIZINFO") {
      gameSelected = this.props.listGames[0].quizinfolist;
    }
    return (
      <div style={{ marginBottom: "28vh" }}>
        {!this.state.open ? (
          this.props.listGames.map((item, index) => (
            <div
              key={index}
              style={{
                margin: "1px 0",
                overflow: "hidden",
                display: "flex",
                justifyContent: "center",
                alignItems: "flex-end",
              }}
              onClick={() => this.handleClickOpen(item.gametitle)}
            >
              <img
                onClick={this.handleClickOpen}
                src={process.env.REACT_APP_DOMAIN + item.image}
                alt=""
                style={{
                  width: "100%",
                  overflow: "hidden",
                }}
              />
            </div>
          ))
        ) : (
            <List
              openList={this.state.open}
              data={gameSelected}
              type={this.state.gameselected}
              onClose={this.handleClickClose}
            />
          )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listGames: state.listGames.listGames,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListGames: () => dispatch(getListGames()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(GameList);
