import React, { Component } from "react";
import { styles } from "./style";
import Btn from "../../assets/img/btn_extra.png";
import { withStyles } from "@material-ui/core/styles";
import { history } from "../../history";
import BackImg from "../../assets/img/extra_video_home.png";
import Typography from "@material-ui/core/Typography";

class ExtraWithoutF extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  extravideo = () => {
    history.push(`/extrat/${this.props.idextra}`);
  };

  render() {
    const { classes, title } = this.props;
    return (
      <div className={classes.extraImageContainer} onClick={this.extravideo}>
        <div className={classes.containerBlocExtraTitle}>
          <img src={Btn} className={classes.extraPlayIcon} alt={"Extra Icon"} />
          <div className={classes.extraTextContainer}>
            <Typography className={classes.extra_title}>BONUS</Typography>
            <Typography noWrap className={classes.extraVideoTitle}>
              {title}
            </Typography>
          </div>
        </div>
        <img
          className={classes.extraImage}
          src={BackImg}
          alt={"Extra Banner"}
          id="Extra_BannerVideo_From_Home_Page"
        />
      </div>
    );
  }
}

export default withStyles(styles)(ExtraWithoutF);
