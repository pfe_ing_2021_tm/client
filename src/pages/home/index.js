/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-19T10:34:29+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React, { Component } from "react"
import { withStyles } from "@material-ui/core/styles"
import { styles } from "./style"
import { history } from "../../history"
import SliderVideo from "./sliderVideo"
import MiniBanner from "./miniBanner"
import { Link } from "react-router-dom"
import Tabs from "./tabs"
import { connect } from "react-redux"
import { compose } from "redux"
import Typography from "@material-ui/core/Typography"
import {
  getActiveCity,
  getRecipes,
  getBanner,
  getFeatures,
  addFCM,
  getCurrentLocation,
  getListGames,
  //setfcmtoken
  getUserSurvey,
  getActiveBrand,
  getSettings,
  dataSettingsFromWSocket,
} from "../../store/actions"
import Header from "../../components/header"
import Badge from "./badge"
import SpecialQuiz from "./specialquiz"
import TagManager from "react-gtm-module"
import "./styleSheet.css"
import GameList from "./galerieList"
import Reds from "./reds"
import Survey from "./survey"

//import TagManager from "react-gtm-module";
//import "./styleSheet.css";
import DialogRes from "./DialogReds"
// Firebase App (the core Firebase SDK) is always required and must be listed before other Firebase SDKs
var firebase = require("firebase/app")
// require("firebase/firestore");
require("firebase/analytics")
require("firebase/messaging")
const uid = localStorage.getItem("uid")
const tagManagerArgs = {
  gtmId: process.env.REACT_APP_IDGTM,

  dataLayer: {
    userId: uid,
    userProject: "Home_Discovered_page",
    event: "GYGIA_event",
  },
}
class Home extends Component {
  constructor(props) {
    super(props)
    const token = localStorage.getItem("token")
    // var fcmtoken;
    // var newfcmtoken;
    this.state = {
      token,
      fcmtoken: null, //localStorage.getItem('fcmtoken'),
      newfcmtoken: null,
      showBadges: true,
      showSurvey: false,
      specialquizpopup: localStorage.getItem("specialquiz") ? true : false,
      specialquizpopupEnable: false,
      open: false,
      userRedsCahllenge: [],
      selectedItem: null,
      backGround: null,
    }
  }

  async componentDidMount() {
    this.initFCM()
    //const _id = localStorage.getItem("_id");

    //this.props.getUserBadges();
    this.props.getUserSurvey()
    this.setState({ showSurvey: !this.state.showSurvey })
    // this.props.getUserBadges();
    this.setState({
      showBadges: this.state.showBadges && !this.state.showSurvey,
    })
    this.props.getSettings()
    this.props.getBanner()
    this.props.getRecipes()
    this.props.getFeatures()

    //this.props.getListGames();
    if (this.state.specialquizpopup === false) {
      localStorage.setItem("sectionSelected", "jeux")
    }
    //this.props.getCurrentLocation();
    /*navigator.geolocation.getCurrentPosition(
      async lastPosition => {
        await this.props.setPosition({
          latitude: lastPosition.coords.latitude,
          longitude: lastPosition.coords.longitude
        });
      },
      error => console.log(JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 0 }
    );*/
    if (this.props.userRedsCahllenge.length !== 0) {
      this.setState({ open: true })
    }

    const cityid = localStorage.getItem("city")
    const brandid = localStorage.getItem("brand")
    if (cityid !== null) {
      this.props.getActiveCity(cityid)
    } else if (brandid !== null) {
      this.props.getActiveBrand(brandid)
    } else {
      this.props.getActiveCity()
    }

    if (this.state.token === null) {
      return history.push("/")
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.settings_ws !== this.props.settings_ws) {
      this.setState({ backGround: this.props.settings_ws.backgroundHome })
    } else if (prevProps.settings !== this.props.settings) {
      this.setState({ backGround: this.props.settings.backgroundHome })
      this.props.dataSettingsFromWSocket(this.props.settings)
    }
    if (prevProps.listGames !== this.props.listGames) {
      if (
        this.props.listGames &&
        this.props.listGames[0] &&
        this.props.listGames[0].gametitle &&
        this.props.listGames[0].gametitle === "QUIZINFO"
      ) {
        this.setState({ specialquizpopupEnable: true })
      } else {
        this.setState({ specialquizpopupEnable: false })
      }
    }
    if (prevProps.userRedsCahllenge !== this.props.userRedsCahllenge) {
      if (this.props.userRedsCahllenge) {
        this.setState({
          open: true,
          userRedsCahllenge: this.props.userRedsCahllenge,
        })
      } else {
        this.setState({ open: false })
      }
    }
    if (prevProps.cityData !== this.props.cityData) {
      console.log("this.props.cityData", this.props.cityData)
      console.log(
        "----",
        this.props.cityData.filter((e) => e.vid.status)
      )
      this.setState({
        selectedItem: this.props.cityData.filter((e) => e.vid.status),
        activeItem: true,
      })
    }
    if (prevProps.brandData !== this.props.brandData) {
      this.setState({ selectedItem: this.props.brandData, activeItem: true })
    }
  }

  initFCM = () => {
   const firebaseConfig = {
     apiKey: "AIzaSyB3kdjJPlNhxL5vvEpB80cbXKPNB9bXM4c",
     authDomain: "pubdeal-pfe.firebaseapp.com",
     projectId: "pubdeal-pfe",
     storageBucket: "pubdeal-pfe.appspot.com",
     messagingSenderId: "182178917822",
     appId: "1:182178917822:web:cea90e82c7ed3151eb9def",
     measurementId: "G-D4QKZRRNDK",
   }
    // Initialize Firebase
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig)
    }
    firebase.analytics()
    if (firebase.messaging.isSupported()) {
      Notification.requestPermission().then((permission) => {
        //console.log("Notification.requestPermission => ", permission);
        if (permission === "granted") {
          // Get Instance ID token. Initially this makes a network call, once retrieved
          // subsequent calls to getToken will return from cache.
          const messaging = firebase.messaging()
          messaging
            .getToken()
            .then((currentToken) => {
               console.log("messaging.getToken => ", currentToken);
              if (currentToken) {
                localStorage.setItem("fcmtoken", currentToken)
                this.setState({ newfcmtoken: currentToken })
                this.props.addFCM()
              } else {
                console.log(
                  "No Instance ID token available. Request permission to generate one."
                )
              }
            })
            .catch((err) => {
              console.log("An error occurred while retrieving token.")
            })
          // messaging.onMessage(payload => {
          //   console.log("Message received. ", payload);
          //   // [START_EXCLUDE]
          //   // Update the UI to include the received message.
          //   // appendMessage(payload);
          //   // [END_EXCLUDE]
          // });
          // Callback fired if Instance ID token is updated.
          messaging.onTokenRefresh(() => {
            messaging
              .getToken()
              .then((refreshedToken) => {
                //console.log("Token refreshed.");
                localStorage.setItem("fcmtoken", refreshedToken)
                this.setState({ newfcmtoken: refreshedToken })
                this.props.addFCM()
              })
              .catch((err) => {
                console.log("Unable to retrieve refreshed token ")
              })
          })
        } else {
          console.log("Unable to get permission to notify.")
        }
      })
    }
  }

  gotoQuiz() {
    //let data = this.props.listGames;
    //data = data.filter(p => p.gametitle === "QUIZZ");
    // console.log(data)
    window.scroll(0, 550)
  }

  render() {
    const { classes } = this.props
    const { selectedItem, backGround } = this.state
    return (
      <div>
        <div className={classes.root}>
          {this.props.userSurvey &&
            this.props.userSurvey.length !== 0 &&
            this.state.showSurvey &&
            this.props.userSurvey.map((item, index) => (
              <Survey
                key={index}
                data={item}
                index={index}
                count1={this.props.userSurvey.length}
                count={this.props.userSurvey.length}
              />
            ))}

          {this.state.specialquizpopup === false &&
            this.state.specialquizpopupEnable && (
              <SpecialQuiz gotoQuizInfo={() => this.gotoQuiz()} />
            )}
          {this.state.open === true &&
            this.state.userRedsCahllenge &&
            this.state.userRedsCahllenge.map((item, index) => (
              <Reds
                key={index}
                data={item}
                index={index}
                count={this.state.userRedsCahllenge.length}
              />
            ))}
          {this.props.userBadge &&
            this.props.userBadge.length !== 0 &&
            this.state.showBadges &&
            this.props.userBadge.map((item, index) => (
              <Badge
                key={index}
                data={item}
                index={index}
                count={this.props.userBadge ? this.props.userBadge.length : 0}
              />
            ))}
          {selectedItem !== null && selectedItem.length !== 0 && (
            <div
              style={{
                background: `url(${
                  process.env.REACT_APP_DOMAIN + backGround?.replace(/\\/g, "/")
                })`,
              }}
              className={classes.bgHome}
            >
              <Header />
              <div style={{ textAlign: "center" }}>
                <SliderVideo cityData={selectedItem} />
                <MiniBanner data={this.props.bannerData} />
                <Tabs data={this.props.features} />
              </div>
            </div>
          )}

          {/* <FooterT /> */}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.ui.isLoading,
    cityData: state.cityData.cityData,
    bannerData: state.bannerData.bannerData,
    features: state.features.features,
    listGames: state.listGames.listGames,
    //setfcmtoken: state.setfcmtoken.setfcmtoken,
    userBadge: state.userBadge.userBadge,
    userRedsCahllenge: state.userBadge.userRedsChallenge,
    userSurvey: state.userSurvey.userSurvey,
    brandData: state.brandData.brandData,
    settings_ws: state.settings_ws.settings_ws,
    settings: state.settings.settings,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getActiveCity: (id) => dispatch(getActiveCity(id)),
    getCurrentLocation: () => dispatch(getCurrentLocation()),
    getRecipes: (data) => dispatch(getRecipes(data)),
    getBanner: () => dispatch(getBanner()),
    getFeatures: () => dispatch(getFeatures()),

    getListGames: () => dispatch(getListGames()),
    addFCM: () => dispatch(addFCM()),
    //setfcmtoken: data => dispatch(setfcmtoken(data)),
    getUserSurvey: () => dispatch(getUserSurvey()),
    getActiveBrand: (id) => dispatch(getActiveBrand(id)),
    getSettings: (id) => dispatch(getSettings(id)),
    dataSettingsFromWSocket: (data) => dispatch(dataSettingsFromWSocket(data)),
  }
}

TagManager.initialize(tagManagerArgs)

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Home)
