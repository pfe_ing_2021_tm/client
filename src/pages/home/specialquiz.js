/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-28 17:04:17
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-29 11:29:31
 */
import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import { withStyles } from "@material-ui/core/styles";
import specialquiz from "../../assets/img/specialquiz.png";
import jouer from "../../assets/img/jouer.png";
import { compose } from "redux";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const styles = theme => ({
  badg: {
    width: 125,
    margin: "0 auto",
    marginTop: 90
  },
  backdrops: {
    background: "none"
  },
  paperprops: {
    margin: "90px auto!important",
    width: 315,
    height: 363,

    boxShadow: theme.shadows[5],

    overflow: "initial"
  },
  cityname: {
    textAlign: "center",
    color: "#fff",
    fontSize: 30,
    margin: 0,
    textTransform: "uppercase",
    fontWeight: 700
  }
});

class SpecialQuiz extends Component {
  state = {
    open: true,
    index: this.props.index
  };

  componentDidMount() {

  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
    localStorage.setItem("specialquiz", "true")
    this.props.gotoQuizInfo();
  };
  handleCloseX = () => {
    this.setState({ open: false });
    localStorage.setItem("specialquiz", "true")
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          open={this.state.open}
          scroll={"body"}
          aria-labelledby="simple-dialog-title"
          PaperProps={{
            style: {
              background: `url(${specialquiz}) 36% 0% / cover`,
              //backgroundSize: "cover"
            },
            classes: {
              root: classes.paperprops
            }
          }}
          BackdropProps={{
            classes: {
              root: classes.backdrops
            }
          }}
        >
          <IconButton
                            style={{
                                justifyContent: "flex-start",
                                color: "#fff",
                                paddingBottom: "0px",
                                marginBottom: "0px",
                            }}
                            fontSize="large"
                            onClick={this.handleCloseX}
                            aria-label="Close"
                        >
                            <CloseIcon
                                style={{
                                    fontSize: 20,
                                    marginLeft: 5,
                                    marginTop: 5,
                                    padding: "0px"
                                }}
                            />
            </IconButton>
          <React.Fragment>
            <p className={classes.cityname}> </p>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <img
                onClick={() => this.handleClose()}
                src={jouer}
                alt=""
                style={{ position: "absolute", width: 200, bottom: -16 }}
              />
            </div>
          </React.Fragment>
        </Dialog>
      </div>
    );
  }
}
export default compose(
  withStyles(styles)
)(SpecialQuiz);
