/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-27 17:13:46
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-03-04 09:37:00
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import IcnPlay from "../../assets/img/playaudio.png";
import Icnoff from "../../assets/img/off.png";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { setVideoItem, setvideohistory } from "../../store/actions";
import { thisExpression } from "@babel/types";
import { history } from "../../history";

class Item extends React.Component {
  state = {
    open: false,
    recipe: 0,
  };

  handleClose = () => {
    this.props.onClose();
  };

  handleClickOpen = (index) => {
    this.setState({ open: true, recipe: index });
  };
  handleClickClose = () => {
    this.setState({ open: false });
  };
  render() {
    const { classes } = this.props;

    return (
      <div style={{ marginLeft: -24, marginRight: -24, display: "contents" }}>
        {this.props.recipes.map((item, index) => (
          <div
            key={index}
            onClick={
              item.active ? () => history.push(`/recipe/${item._id}`) : {}
            }
            style={{
              height: 150,
              margin: "1px 0",
              overflow: "hidden",
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-end",
              backgroundImage: `url(${
                process.env.REACT_APP_DOMAIN + item.image
              })`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              width: "100%",
              filter: !item.active ? "grayscale(1)" : "grayscale(0)",
            }}
            id={`Recipe_${item.name}`}
          >
            <div className={classes.playlistDescripCont}>
              <div id={`Recipe_${item.name}`}>
                <Typography
                  component="p"
                  className={classes.playListTitle}
                  srtyle={{ fontSize: 20 }}
                >
                  {item.name}
                </Typography>
              </div>

              {item.active ? (
                <img
                  src={IcnPlay}
                  alt="play"
                  className={classes.iconPlayAudio}
                  onClick={() => this.handleClickOpen(index)}
                />
              ) : (
                <img src={Icnoff} alt="play" className={classes.iconOff} />
              )}
            </div>
          </div>
        ))}
      </div>
    );
  }
}

const ItemList = withStyles(styles)(Item);

class RecipeList extends Component {
  state = {
    open: this.props.open,
    recipes: 0,
    videoTitle: "",
    videoId: " ",
    // videoExtraTo:,
    // videoExtra: ,
  };

  handleClickOpen = (index) => {
    this.setState({ open: false, recipes: index });
    this.handleSubBack();
  };
  handleClickClose = () => {
    this.setState({ open: false });
  };

  handleSubBack = () => {
    this.props.handleSubBack(this.state.open);
  };

  componentDidMount() {
    // const data = {
    //   // idcity: this.props.match.params.extratid,
    //   idvideo: this.state.videoId,
    //   login: "55123456"
    // }
    // this.props.setvideohistory(data)
    if (this.props.listRecipes && this.props.listRecipes.length > 0) {
      this.refs.recette.scrollIntoView({
        behavior: "smooth",
        block: "start",
      }); // scroll...
      
    }
    // Default
    if(process.env.REACT_APP_DEFAULT_RECIPIES === "true"){
      this.setState({ open: false, recipes: 0 });
    }
  }
  async componentWillReceiveProps(prevProps) {
    if (prevProps.videoExtra !== this.props.videoExtra) {
      await this.setState({
        videoTitle: prevProps.videoExtra.title,
        videoId: prevProps.videoExtra._id,
        videoExtra: prevProps.videoExtra.video, //id for video
      });
    }
  }
  render() {
    const { classes } = this.props;
    return (
      <div
        style={{
          marginLeft: -24,
          marginRight: -24,
          width: "100%",
          marginBottom: "20vh",
        }}
      >
        {this.state.open ? (
          this.props.listRecipes.map((item, index) => (
            <div
              id={`Recipe_City_${item.name}`}
              key={index}
              ref="recette"
              style={{
                height: 150,
                margin: "1px 0",
                overflow: "hidden",
                display: "flex",
                justifyContent: "center",
                alignItems: "flex-end",
                backgroundImage: `url(${
                  process.env.REACT_APP_DOMAIN + item.image
                })`,
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                filter: !item.active ? "grayscale(1)" : "grayscale(0)",
              }}
              onClick={item.active ? () => this.handleClickOpen(index) : null}
            >
              <div className={classes.playlistDescripCont}>
                <div>
                  <Typography component="p" className={classes.playListTitle}>
                    {item.name}
                  </Typography>
                  <Typography component="p" className={classes.videoAuthor}>
                    &nbsp;
                  </Typography>
                </div>
                {item.active ? (
                  <img
                    src={IcnPlay}
                    alt="play"
                    className={classes.iconPlayAudio}
                  />
                ) : (
                  <img src={Icnoff} alt="play" className={classes.iconOff} />
                )}
              </div>
            </div>
          ))
        ) : (
          (this.props.listRecipes && this.props.listRecipes.length > 0 && <ItemList
            openList={this.state.open}
            recipes={this.props.listRecipes[this.state.recipes].recipies}
            onClose={this.handleClickClose}
          />)
        )}
      </div>
    );
  }
}

RecipeList.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
  return {
    listRecipes: state.listRecipes.listRecipes,
    videoExtra: state.videoExtra.videoExtra,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    setvideohistory: (id) => dispatch(setvideohistory(id)),
    // setVideoItem: () => dispatch(setVideoItem({}))
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(RecipeList);
