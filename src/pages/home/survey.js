/*
 * @Author: KHEDHIRI Nessrine
 * @Date: 2019-10-28 17:04:17
 * @Last Modified by: KHEDHIRI Nessrine
 * @Last Modified time: 2019-10-29 11:29:31
 */
import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import SwipeableViews from "react-swipeable-views";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Confirmer from "../../assets/img/Confirmer.png";
import Grid from "@material-ui/core/Grid";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import { setUserSurvey, setUserSurveyAnswer } from "../../store/actions";
import Pagination from "./pagination";
import { compose } from "redux";
import { connect } from "react-redux";
import { thisTypeAnnotation } from "@babel/types";
import { yellow } from "@material-ui/core/colors";

const styles = (theme) => ({
  title: {
    textAlign: "center",
    color: theme.palette.secondary.main,
    fontSize: 15,
    // margin: 0,
    fontWeight: 500,
  },
  group: {
    padding: 0,
  },
  radios: {
    padding: 0,
    color: yellow,
  },
  input: {
    fontFamily: "NeulandGroteskCondensedRegular",
    padding: 5,
    fontSize: 14,
  },
  contpaggination: {
    display: "flex",
    margin: "0px auto 10px auto",
    alignItems: "center",
    textAlign: "Client",
    justifyContent: "center",
    width: "90%",
    zIndex: 9,
  },
  backdrops: {
    background: "none",
  },
  paperprops: {
    margin: "90px auto!important",
    width: 315,
    height: 378,
    boxShadow: theme.shadows[5],
    overflow: "initial",
  },
  titlequestionsurvey: {
    textAlign: "center",
    color: theme.palette.secondary.main,
    fontSize: 20,
    fontFamily: "NeulandGroteskCondensedBold",
    // margin: 0,
    textTransform: "uppercase",
  },
  questionListe: {
    color: theme.palette.secondary.main,
    marginTop: 0,
    marginBottom: "15px",
    // marginLeft: "6%"
  },
  // inputQuestion: {
  //   top: " 310px",
  //   left: " 63px",
  //   width: " 289px",
  //   height: "212px",
  //   background: "#FFFFFF 0% 0% no-repeat padding-box",
  //   borderRadius: " 17px",
  //   opacity: 1
  // },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    fontFamily: "NeulandGroteskCondensedRegular",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    margin: "0px 0px 8px 0px !important",
    width: 250,
    backgroundColor: theme.palette.secondary.main,
    borderRadius: " 8px",
    marginBottom: "5px",
  },
  formControl: {
    // margin: theme.spacing.unit
    textAlign: "center",
  },
});

class Survey extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();

    this.state = {
      open: true,
      index: 0,
      count: 0,
      pageIndex: 0,
      value: "",
      arr: [],
      answerarray: [],
      radioQuestionAnswers: [],
      questionAnswersArray: [],
      left: 0,
    };
  }

  componentDidMount() {
    if (this.props.count1 !== 0) {
      document.getElementById("root").style.filter = "blur(4px)";
    }
    if (this.props.userSurvey) {
      var chunk = 1;
      var obj = [];
      for (let i = 0; i < this.props.data.questions.length; i += chunk) {
        obj.push({ userSurvey: this.props.userSurvey.slice(i, i + chunk) });
      }
      this.setState({ arrslid: obj, count: obj.length }, () => {});
    }
    // console.log("my Index = ", this.state.index);
  }
  componentWillReceiveProps() {
    // console.log("componentWillReceiveProps");
    // if (this.props.userSurvey) {
    //   var chunk = 1;
    //   var obj = [];
    //   for (let i = 0; i < this.props.data.questions.length; i += chunk) {
    //     obj.push({ userSurvey: this.props.userSurvey.slice(i, i + chunk) });
    //   }
    //   this.setState({ arrslid: obj, count: obj.length }, () => {
    //     console.log(this.state.count);
    //   });
    // }
  }
  componentDidUpdate(prevProps) {
    // console.log("componentDidUpdate");
    // if (prevProps.userSurvey !== this.props.userSurvey) {
    //   var chunk = 1;
    //   var obj = [];
    //   for (let i = 0; i < this.props.data.questions.length; i += chunk) {
    //     obj.push({ userSurvey: this.props.userSurvey.slice(i, i + chunk) });
    //   }
    //   this.setState({ arrslid: obj, count: obj.length }, () => {
    //     console.log(this.state.count);
    //   });
    // }
    // if (this.myRef.current !== null && this.myRef.current !== undefined) {
    //   this.scrollToBottom();
    // }
  }

  handleOpen = () => {
    this.setState({ open: true });
  };
  handleChangeIndex = (index, indexLatest) => {
    this.setState({
      pageIndex: index,
    });
  };
  // handleBackIndex = () => {
  //   this.setState({
  //     index: this.state.index - 1,
  //     pageIndex: this.pageIndex - 1
  //   });
  // };

  handleAddOrUpdateAnswers = (event, id) => {
    let answer = event.target.value;
    let index = this.state.questionAnswersArray.findIndex(
      (answer) => answer.question === id
    );
    // console.log("Index : ", index);
    if (index >= 0) {
      if (answer === "") {
        this.deleteAnswer(index);
      } else {
        this.updateAnswer(answer, index);
      }
    } else {
      this.addAnswer(answer, id);
    }
  };
  deleteAnswer = (index) => {
    let prevStateQuestionAnswersArray = this.state.questionAnswersArray;
    prevStateQuestionAnswersArray.splice(index, 1);
    this.setState(
      {
        questionAnswersArray: prevStateQuestionAnswersArray,
      },
      () => {}
    );
  };

  updateAnswer = (answer, index) => {
    let prevStateQuestionAnswersArray = this.state.questionAnswersArray;
    prevStateQuestionAnswersArray[index].questionAnswers = [answer];
    this.setState(
      {
        questionAnswersArray: prevStateQuestionAnswersArray,
      },
      () => {}
    );
  };
  addAnswer = (answer, id) => {
    this.setState(
      {
        questionAnswersArray: [
          ...this.state.questionAnswersArray,
          {
            question: id,
            questionAnswers: [answer],
          },
        ],
      },
      () => {}
    );
  };

  findAnswerForQuestion = (questionId) => {
    let index = this.state.questionAnswersArray.findIndex(
      (answer) => answer.question === questionId
    );
    if (index >= 0) {
      return this.state.questionAnswersArray[index].questionAnswers[0];
    } else {
      return "";
    }
  };

  handleConfirmer = (surveyId) => {
    this.props.setUserSurveyAnswer(surveyId, this.state.questionAnswersArray);
  };

  handleClose = (id) => {
    this.setState({ open: false });

    if (this.state.index === 0) {
      document.getElementById("root").style.filter = null;
      this.props.setUserSurvey();
    }
  };
  scrollToBottom = () => {
    this.myRef.current.scrollIntoView({ behavior: "smooth" });
  };

  render() {
    const { classes, data } = this.props;
    // const { pageIndex } = this.state.pageIndex;
    return (
      <div>
        <Dialog
          open={this.state.open}
          scroll={"body"}
          aria-labelledby="simple-dialog-title"
          PaperProps={{
            style: {
              backgroundColor: "#A31932",
              backgroundSize: "cover",
            },
            classes: {
              root: classes.paperprops,
            },
          }}
          BackdropProps={{
            classes: {
              root: classes.backdrops,
            },
          }}
        >
          <React.Fragment>
            <SwipeableViews
              index={this.state.pageIndex}
              onChangeIndex={this.handleChangeIndex}
              enableMouseEvents
              containerStyle={{ height: "100%" }}
              // disabled={this.state.value.length === 0 ? true : false}
            >
              {data.questions.map((item, index) => (
                <Grid
                  // className={classes.swipeable}
                  key={item._id}
                  container
                  direction="column"
                  justify="space-evenly"
                  alignItems="center"
                  style={{ left: this.state.left, marginBottom: 40 }}
                  // onMouseLeave={() => this.handleNext(item._id, data._id)}
                >
                  <p className={classes.titlequestionsurvey}>
                    Question {index + 1}/{data.questions.length}
                  </p>
                  <div style={{ display: "inline-grid" }}>
                    <p className={classes.questionListe}>{item.question}</p>
                    <div
                      style={{
                        textAlign: "center",
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "start",
                        // minHeight: "230px"
                      }}
                    >
                      {item.questionType === "libre" && (
                        <TextField
                          id={item._id}
                          label="Tapez ici votre réponse"
                          multiline
                          rows="4"
                          className={classes.textField}
                          margin="normal"
                          // variant="outlined"
                          onChange={(e) =>
                            this.handleAddOrUpdateAnswers(e, item._id)
                          }
                          InputProps={{
                            className: classes.input,
                          }}
                          InputLabelProps={{
                            className: classes.input,
                          }}
                        />
                      )}
                      {item.questionType === "liste" &&
                        item.questionOptions.map((element, index) => (
                          <FormControl
                            component="fieldset"
                            className={classes.formControl}
                            key={element}
                          >
                            <RadioGroup
                              value={this.findAnswerForQuestion(item._id)}
                              onChange={(e) =>
                                this.handleAddOrUpdateAnswers(e, item._id)
                              }
                              // style={{ padding: 0 }}
                              className={classes.group}
                              key={index}
                            >
                              <FormControlLabel
                                value={element}
                                key={element}
                                control={
                                  <Radio
                                    style={{
                                      color: "white",
                                      padding: 7,
                                      // width: "5px",
                                      fontSize: "5px",
                                    }}
                                    // size="small"
                                  />
                                }
                                label={
                                  <span
                                    style={{
                                      color: "white",
                                      fontFamily:
                                        "NeulandGroteskCondensedRegular",
                                    }}
                                  >
                                    {element}
                                  </span>
                                }
                              />
                            </RadioGroup>
                          </FormControl>
                        ))}
                    </div>
                  </div>
                </Grid>
              ))}
            </SwipeableViews>
            {this.state.count !== 1 && (
              <div className={classes.contpaggination}>
                <Pagination
                  dots={this.state.count}
                  index={this.state.pageIndex}
                  onChangeIndex={this.handleChangeIndex}
                />
              </div>
            )}
            {this.state.questionAnswersArray.length === this.state.count &&
              this.state.count !== 0 && (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    marginRight: "1px",
                  }}
                  ref={this.myRef}
                >
                  <img
                    onClick={() => {
                      this.handleConfirmer(data._id);
                      this.handleClose(data._id);
                    }}
                    src={Confirmer}
                    alt=""
                    style={{
                      // position: "absolute",
                      width: 150,
                      // bottom: "0px",
                      position: "relative",
                    }}
                  />
                </div>
              )}
          </React.Fragment>
        </Dialog>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userSurvey: state.userSurvey.userSurvey,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    setUserSurveyAnswer: (id, data) => dispatch(setUserSurveyAnswer(id, data)),
    setUserSurvey: () => dispatch(setUserSurvey([])),
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Survey);
