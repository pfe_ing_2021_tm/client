/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-28 17:04:17
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-29 11:29:31
 */
import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import { withStyles } from "@material-ui/core/styles";
import { setUserBadgesStatus, setUserBadges } from "../../store/actions";

import redxp from "../../assets/img/redxp.svg";
import Confirmer from "../../assets/img/Confirmer.png";
import { compose } from "redux";
import { connect } from "react-redux";

const styles = (theme) => ({
  badg: {
    width: 125,
    margin: "0 auto",
    marginTop: 90,
  },
  backdrops: {
    background: "none",
  },
  paperprops: {
    margin: "90px auto!important",
    width: 315,
    height: 363,

    boxShadow: theme.shadows[5],

    overflow: "initial",
  },
  cityname: {
    textAlign: "center",
    color: theme.palette.secondary.main,
    fontSize: 30,
    margin: 0,
    textTransform: "uppercase",
    fontWeight: 700,
  },
})

class Badge extends Component {
  state = {
    open: true,
    index: this.props.index
  };

  componentDidMount() {
    if (this.props.count !== 0) {
      document.getElementById("root").style.filter = "blur(4px)";
    }
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = id => {
    this.props.setUserBadgesStatus(id);
    this.setState({ open: false });

    if (this.state.index === 0) {
      document.getElementById("root").style.filter = null;
      this.props.setUserBadges();
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          open={this.state.open}
          scroll={"body"}
          aria-labelledby="simple-dialog-title"
          PaperProps={{
            style: {
              background: `url(${process.env.REACT_APP_DOMAIN +
                this.props.data.background})`,
              backgroundSize: "cover"
            },
            classes: {
              root: classes.paperprops
            }
          }}
          BackdropProps={{
            classes: {
              root: classes.backdrops
            }
          }}
        >
          <React.Fragment>
            <img
              className={classes.badg}
              src={process.env.REACT_APP_DOMAIN + this.props.data.badge.icon}
              alt=""
            />
            <p className={classes.cityname}>{this.props.data.badge.name}</p>
            <div style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}>
              <p className={classes.cityname} style={{ margin: 0 }}>
                + {this.props.data.badge.point}
              </p>
              <img src={redxp} alt="" style={{ width: 90, marginLeft: 10 }} />
            </div>

            <div style={{ display: "flex", justifyContent: "center" }}>
              <img
                onClick={() => this.handleClose(this.props.data.badge._id)}
                src={Confirmer}
                alt=""
                style={{ position: "absolute", width: 200, bottom: -16 }}
              />
            </div>
          </React.Fragment>
        </Dialog>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    setUserBadgesStatus: id => dispatch(setUserBadgesStatus(id)),
    setUserBadges: () => dispatch(setUserBadges([]))
  };
};
export default compose(
  withStyles(styles),
  connect(
    null,
    mapDispatchToProps
  )
)(Badge);
