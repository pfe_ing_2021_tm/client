/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:33:03+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-03T09:48:47+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import BackImg from "../../assets/img/extra_video_home.png";
import { CoverageMap } from "istanbul-lib-coverage";
export const styles = (theme) => ({
  root: {
    width: "100%",
  },
  bgHome: {
    backgroundSize: "cover !important",
    backgroundRepeat: "no-repeat !important",
    height: "100vh",
  },
  background: {
    width: "100%",
    position: "relative",
  },
  backdrops: {
    background: "#000",
  },
  inputEntryCode: {
    width: 42,
    height: 45,
    background: "transparent",
    border: "1px solid #fff",
    margin: "0 5px",
    borderRadius: 5,
    color: theme.palette.secondary.main,
    fontSize: 36,
    textAlign: "center",
    lineHeight: "40px",
  },
  paperprops: {
    width: "100%",
    background: "#000",
    justifyContent: "space-between",
    alignItems: "flex-start",
    display: "contents",
  },
  scrollPaper: {
    justifyContent: "flex-start",
  },
  imgLike: {
    width: 90,
  },
  liketext: {
    color: "#0ede29",
    fontSize: 18,
    marginTop: 30,
    fontWeight: 800,
    textAlign: "center",
  },
  imgdLike: {
    width: 90,
    transform: "rotate(180deg)",
    top: 22,
    position: "relative",
  },
  btnnumb: {
    border: "1px solid #fff",
    borderRadius: 5,
    height: 42,
    width: 35,
    textAlign: "center",
    fontSize: 30,
    color: theme.palette.secondary.main,
    margin: "15px 10px",
    cursor: "pointer",
  },
  dliketext: {
    color: "#fe1100",
    fontSize: 18,
    marginTop: 30,
    fontWeight: 800,
    textAlign: "center",
  },
  bottomend: {
    color: theme.palette.secondary.main,
    display: "flex",
    position: "absolute",
    bottom: -79,
    justifyContent: "space-between",
    width: "100%",
  },

  sliderContainer: {
    width: "100%",
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "center",
    alignContent: "center",
  },
  sliderPoster: {
    width: "85%",
    marginBottom: -15,
  },
  vidcont: {},
  videoDescripCont: {
    display: "flex",
    justifyContent: "space-between",
    width: "70%",
    alignItems: "center",
    position: "absolute",
  },
  videoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  videoAuthor: {
    color: theme.palette.secondary.main,
  },
  playVideoAuthor: {
    color: "#999",
    fontSize: 13,
  },
  iconPlay: {
    width: "60px",
    color: theme.palette.secondary.main,
  },
  dialogPaper: {
    overflow: "hidden",
  },
  btnNext: {
    display: "flex",
    flexDirection: "row",
    marginTop: 25,
    color: theme.palette.secondary.main,
    justifyContent: "flex-end",
    width: "90%",
  },
  titleNext: {
    color: theme.palette.secondary.main,
  },
  iconNext: {
    fontSize: 20,
    color: "#C70013",
    marginLeft: 10,
  },
  bannerContainer: {
    width: "100%",
    justifyContent: "center",
    display: "flex",
    marginTop: 25,
  },
  bannerItem: {
    flexDirection: "column",
  },
  bannerIndicator: {
    width: 10,
    margin: "10px 5px",
  },
  bannerCarousel: {
    width: "85%",
  },
  playlistDescripCont: {
    // display: "flex",
    // justifyContent: "space-between",
    // width: "90%",
    // alignItems: "center",
    // position: 'absolute',
    paddingBottom: 15,
  },
  gamesImage: {
    //display: "flex",
    justifyContent: "space-between",
    width: "90%",
    alignItems: "center",
    position: "relative",
    bottom: 0,
    left: 15,
    top: "10vh",
    //paddingBottom: "20%",
    zIndex: 1,
    [theme.breakpoints.between("xs", "sm")]: {
      top: "15vh",
    },

    [theme.breakpoints.between("sm", "md")]: {},
    [theme.breakpoints.between("md", "xl")]: {},
  },
  icntabhead: {
    color: theme.palette.secondary.main,
    position: "relative",
    top: 2,
  },
  playListTitle: {
    color: theme.palette.secondary.main,
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 20,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 27,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 20,
    },
    fontWeight: "500",
    textTransform: "uppercase",
  },
  playVideoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  iconPlayAudio: {
    width: 30,
    color: theme.palette.secondary.main,
  },
  iconOff: {
    width: 20,
    color: theme.palette.secondary.main,
  },
  drivWhi: {
    position: "relative",
    top: -2,
    fontSize: 10,
    color: theme.palette.secondary.main,
  },
  galerieSmallItem: {
    height: 100,
    overflow: "hidden",
    marginBottom: 8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  progressBarCont: {
    background: "rgba(000, 000, 000, 0.8)",
    height: 35,
    padding: "0 0px 40px 0px",
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
  progressLine: {
    width: "100%",
    height: 3,
    position: "relative",
    top: -17,
  },
  videoProgreesTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    position: "relative",
  },
  videoProgressBoxDesc: {
    display: "flex",
    alignItems: "flex-start",
  },
  progressImg: {
    width: 50,
    position: "relative",
    top: -8,
    left: 10,
  },
  progressIcn: {
    color: theme.palette.secondary.main,
    position: "relative",
    top: -10,
  },
  videoCloseCont: {
    position: "absolute",
    top: 10,
    background: "#000",
    borderRadius: 0,
    right: 0,
    padding: "5px 7px",
    zIndex: 1,
  },
  videoCloseIcn: {
    color: theme.palette.secondary.main,
    border: "1px solid #fff",
    borderRadius: 50,
    fontSize: 16,
  },
  boxcategory: {
    transformOrigin: "0 0 0",
    background: theme.palette.secondary.main,
    height: 125,
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    margin: 5,
    flex: 1,
  },
  headtabcont: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  conttab: {
    display: "flex",
    justifyContent: "space-between",
    width: "90%",
    alignItems: "center",
    alignContent: "center",
  },
  headtab: {
    color: theme.palette.secondary.main,
    textTransform: "uppercase",
    fontWeight: 500,
    borderBottom: "3px solid #ed2a1c",
    paddingBottom: 5,
  },
  slide: {
    perspective: 1000, // create perspective
    overflow: "hidden",
    // relative is a must if you want to create overlapping layers in children
    position: "relative",
    paddingTop: 8,
    /*[theme.breakpoints.up('sm')]: {
      paddingTop: 10
    },
    [theme.breakpoints.up('md')]: {
      paddingTop: 14
    }*/
  },
  extraImageContainer: {
    width: "85%",
    position: "relative",
    alignItems: "center",
    justifyContent: "center",
    margin: "auto",
    "&&:after": {
      display: "block",
      borderRadius: "9999px",
      content: "''",
      width: "108%",
      height: "108%",
      position: "absolute",
      zIndex: "-1",
      left: "-4%",
      top: "-4%",
      backgroundImage: "conic-gradient(#00AEEF, #fff, #00AEEF, #fff, #00AEEF)",
      animation: "spin 5s linear infinite",
    },
  },

  imageContainer: {
    display: "flex",
    position: "relative",
    zIndex: 2,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  extraImage: {
    width: "100%",
    display: "block",
  },
  image: {
    display: "block",
    width: "82%",
    objectFit: "cover",

    /*  [theme.breakpoints.up('sm')]: {
      marginLeft: '4%'
    }*/
  },
  arrow: {
    display: "none",
    position: "absolute",
    top: "50%",
    transform: "translateY(-50%)",
    /*  [theme.breakpoints.up('sm')]: {
      display: 'inline-flex'
    }*/
  },
  arrowLeft: {
    left: 0,
    /*[theme.breakpoints.up('lg')]: {
      left: -64
    }*/
  },
  arrowRight: {
    right: 0,
    /*  [theme.breakpoints.up('lg')]: {
      right: -64
    }*/
  },
  text: {
    // shared style for text-top and text-bottom
    fontWeight: 900,
    position: "absolute",
    zIndex: 1,
    color: theme.palette.secondary.main,
    padding: "0 8px",
    lineHeight: 1.2,
    /*  [theme.breakpoints.up('sm')]: {
      padding: '0 16px'
    },
    [theme.breakpoints.up('md')]: {
      padding: '0 24px'
    }*/
  },
  title: {
    bottom: 20,
    zIndex: 9999,
    fontSize: 17,
    fontWeight: "500",
    textTransform: "uppercase",
    /*[theme.breakpoints.up('sm')]: {
      top: 40,
      fontSize: 72
    },
    [theme.breakpoints.up('md')]: {
      top: 52,
      fontSize: 72
    }*/
  },
  subtitle: {
    top: 60,
    left: "0%",
    height: "52%",
    fontSize: 56,
    background: "linear-gradient(0deg, rgba(255,255,255,0) 0%, #888888 100%)",
  },
  gameimg: {
    width: 200,
    marginLeft: 50,
  },
  dlikecont: {
    display: "flex",
    flexDirection: "column",
    margin: 20,
  },
  likecont: {
    display: "flex",
    flexDirection: "column",
    margin: 20,
  },
  endvideobox: {
    display: "flex",
    flexDirection: "row",
    marginRight: 10,
  },
  videoendcont: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  dividers: {
    width: 4,
  },
  btnreplay: {
    display: "flex",
    alignItems: "center",
  },
  btnreplayimg: {
    width: 30,
    marginRight: 15,
    transform: "rotate(-70deg)",
  },
  btnreplaytext: {
    textTransform: "uppercase",
    fontWeight: 700,
    fontSize: 20,
  },
  btnnextvideo: {
    display: "flex",
    alignItems: "center",
  },
  btnnextvideotext: {
    textTransform: "capitalize",
    fontSize: 17,
  },
  btnnextvideoicn: {
    width: 25,
    marginLeft: 15,
    cursor: "pointer",
  },
  btnvalider: {
    width: 150,
    marginLeft: 15,
  },

  Bannier: {
    height: "21vh",
    margin: "1px 0",
    //overflow: "hidden",
    display: "flex",
    justifyContent: "center",
  },
  // footer: {
  //   height: "10vh",
  //   background: "rgb(255, 255, 255)",
  //   textAlign: "center",
  //   width: "100%",
  //   margin: 0,
  //   textTransform: "uppercase",
  //   fontSize: 18,
  //   fontWeight: "700",
  //   lineHeight: "76px",
  //   // position: "fixed",
  //   // bottom: 0
  //   boxShadow: "inset 0px 0px 0px 10px #000",
  //   paddingTop: 15,
  //   [theme.breakpoints.between("xs", "sm")]: {
  //     //Galaxi s3 jusqu'a ipod ( 0px => 600px)
  //     width: "100%",
  //     lineHeight: "56px",
  //     fontSize: 12
  //   },

  //   [theme.breakpoints.between("sm", "md")]: {
  //     //ipod ( 600px => 950px)
  //     width: "100%",
  //     lineHeight: "80px",
  //     fontSize: 20
  //   },

  //   [theme.breakpoints.between("md", "xl")]: {
  //     //ipod jusqu'a xl screen ( 950px => 1920px)

  //     lineHeight: "66px",
  //     width: 386,
  //     margin: "0 0 0 0"
  //   }
  // },

  footerText: {
    padding: "0 15px",
  },
  footerBorder: {
    borderTop: "1px solid #888",
    width: 35,
    marginLeft: 18,
    marginBottom: 10,
  },
  footerlist: {
    marginLeft: 15,
    marginBottom: 30,
  },
  container: {
    backgroundImage: `url(${BackImg})`,
    backgroundSize: "contain",
    backgroundRepeat: " no-repeat",
    width: "83%",
    marginLeft: "8%",
  },
  containerBlocExtraTitle: {
    display: "flex",
    position: "absolute",
    margin: "auto",
    width: "65%",
    height: "100%",
    textAlign: "left",
  },
  extraPlayIcon: {
    height: "80%",
    margin: "3% 0% auto 2%",
  },
  extraTextContainer: {
    position: "absolute",
    margin: "3% 0% auto 25%",
    width: "75%",
    height: "100%",
    textAlign: "left",
  },
  extra_title: {
    margin: "0 0 0 0",
    color: "white",
    lineHeight: 0.5,
    fontFamily: "NeulandGroteskCondensedBold",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  extraVideoTitle: {
    margin: "0 0 0 0",
    color: "black",
    fontFamily: "NeulandGroteskCondensedBold",
    // fontWeight: "bold",
    whiteSpace: "nowrap",
    overflow: "Hidden",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  titlequestionsurvey: {
    top: " 100px",
    left: "134px",
    width: " 146px",
    height: " 29px",
    textAlign: "left",
    font: "Black 25px/40px Roboto",
    letterSpacing: " 0",
    color: theme.palette.secondary.main,
    opacity: " 1",
  },
  bottomDarkContainer: {
    backgroundColor: "#111112",
    height: "10vh",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    paddingTop: 15,
    fontSize: 45,
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      justifyContent: "space-around",
      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      width: 386,
      margin: "0 0 0 0",
    },
  },
  bottomTypography: {
    marginRight: 5,
    marginLeft: 8,
    color: "white",
    // fontWeight: "heavy",
    fontFamily: "NeulandGroteskCondensedBold",
    fontSize: 13,
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      marginRight: 0,
      marginLeft: 0,
    },

    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      marginRight: 5,
      marginLeft: 10,
    },
  },
  imgforwad: {
    width: "100%",
    height: 50,
  },
  btnnextvideoHome: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    marginRight: 20,
  },
  btnnextvideotext: {
    textTransform: "capitalize",
    fontSize: 17,
    color: "white",
    cursor: "pointer",
  },
  btnnextvideotextHome: {
    textTransform: "capitalize",
    fontSize: 17,
    color: "white",
    cursor: "pointer",
  },
})
