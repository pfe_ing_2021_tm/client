/*
 * Created Date: Wednesday October 30th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, November 7th 2019, 12:32:37 pm
 * Modified By: Amir Dorgham
 * -----
 */
import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import position from "../../assets/img/location.png";
class PositionDescription extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.positionContainer}>
        <div className={classes.positionTextContainer}>
          <span className={classes.positionText1}>Adresse</span>
          <span className={classes.positionText2}>{this.props.address}</span>
        </div>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={` http://maps.google.com/maps?z=18&q=${this.props.latitude},${this.props.longitude}`}
        //href={`https://www.google.com/maps/@${this.props.latitude},${this.props.longitude},17z`}
        >
          <img
            id={`Show_Adresse_Events_${this.props.title}`}
            src={position}
            alt="position"
            className={classes.positionIcon}
          ></img>
        </a>
      </div>
    );
  }
}
export default withStyles(styles)(PositionDescription);
