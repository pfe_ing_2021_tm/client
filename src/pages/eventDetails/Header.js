/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Wednesday, November 6th 2019, 5:54:13 pm
 * Modified By: Amir Dorgham
 * -----
 */
import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import backimg from "../../assets/img/backprofile.png";
import vyndimg from "../../assets/img/vynd.png";

import { Link } from "react-router-dom";

class Header extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.header}>
        <div className={classes.headerTop}>
          <Link to="/events">
            <div
              style={{
                display: "flex",
                alignItems: "center",
                paddingLeft: 40,
                marginTop: 15,
                marginBottom: 15
              }}
            >
              <img
                src={backimg}
                style={{ width: 35, marginRight: 15 }}
                alt=""
              />
            </div>
          </Link>
        </div>
        <div className={classes.headerBottom}>
          <span className={classes.headerBottomText1}>
            Prochains evenements
          </span>
          <div className={classes.headerBottomContainer}>
            <span className={classes.headerBottomText2}>Powered by </span>
            <img src={vyndimg} style={{ width: "9vw", marginLeft: 5 }} alt="" />
          </div>

          <div className={classes.headerBar}></div>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(Header);
