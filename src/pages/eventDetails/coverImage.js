/*
 * Created Date: Wednesday October 30th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, October 31st 2019, 2:06:37 pm
 * Modified By: Amir Dorgham
 * -----
 */
/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Wednesday, October 30th 2019, 11:41:26 am
 * Modified By: Amir Dorgham
 * -----
 */

import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";

class CoverImage extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.imageContainer}>
        <img src={this.props.photo} alt="eventImage" className={classes.eventImage}></img>
        <div className={classes.shadowGradient} />
      </div>
    );
  }
}
export default withStyles(styles)(CoverImage);
