/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, October 31st 2019, 2:12:47 pm
 * Modified By: Amir Dorgham
 * -----
 */

import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";

class DescriptionPanel extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.textPanelContainer}>
        <span className={classes.slideTitle}>{this.props.title}</span>
        <span className={classes.slideDescription}>
          A partir de {this.props.hours}H
        </span>
        <div className={classes.largeTextContainer}>
          <p className={classes.largeText}>{this.props.description}</p>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(DescriptionPanel);
