/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, October 31st 2019, 3:08:48 pm
 * Modified By: Amir Dorgham
 * -----
 */

import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Header from "./Header";
import EventDescription from "./eventDescription";
import CoverImage from "./coverImage";
import { connect } from "react-redux";
import { compose } from "redux";
import { getEventbyid, EventView } from "../../store/actions";

class EventDetails extends Component {
  state = {
    photo: null,
    title: null,
    latitude: null,
    longitude: null,
    description: null,
    address: null,
    hours: null,
    calendarDay: null,
    calendarMonth: null,
    loading: true
  };
  componentDidMount() {
    this.props.getEventInfo(this.props.match.params.eventid)
    //this.props.EventView(this.props.match.params.eventid);
  }
  componentWillReceiveProps(prevProps) {
    if (prevProps.eventInfo !== this.props.eventInfo) {
      this.setState({
        loading: false,
        photo: prevProps.eventInfo.event.photo,
        title: prevProps.eventInfo.event.title,
        latitude: prevProps.eventInfo.event.latitude,
        longitude: prevProps.eventInfo.event.longitude,
        description: prevProps.eventInfo.event.description,
        address: prevProps.eventInfo.event.address,
        hours: prevProps.eventInfo.event.date.hours,
        calendarDay: prevProps.eventInfo.event.date.day,
        calendarMonth: prevProps.eventInfo.event.date.month
      });
    }
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        {this.state.loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh"
            }}
          >
            <CircularProgress className={classes.progress} color="secondary" />
          </div>
        ) : (
            <div>
              <Header />
              <CoverImage photo={this.state.photo}
              />
              <EventDescription
                title={this.state.title}
                latitude={this.state.latitude}
                longitude={this.state.longitude}
                description={this.state.description}
                address={this.state.address}
                hours={this.state.hours}
                calendarDay={this.state.calendarDay}
                calendarMonth={this.state.calendarMonth}
              />
            </div>
          )}

        {/* <span style={{ color: "white" }}>
            {this.props.match.params.eventid}
          </span> */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    eventInfo: state.events.eventInfo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getEventInfo: id => dispatch(getEventbyid(id)),
    EventView: data => dispatch(EventView(data))
  };
};

export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(EventDetails);
