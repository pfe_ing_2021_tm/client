/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, November 7th 2019, 12:35:01 pm
 * Modified By: Amir Dorgham
 * -----
 */

export const styles = theme => ({
  container: {
    width: "100%"
  },
  description: {
    position: "relative",
    bottom: 125,
    width: "100%"
  },

  descriptionContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
    // marginLeft: "5%",
    marginTop: 10
  },
  textPanelContainer: {
    width: "70%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    marginTop: 12,
    marginRight: 15,
    paddingLeft: "5%"
    // paddingRight: "5%"
  },
  largeTextContainer: {
    marginTop: 10,
    position: "relative",
    right: "0%"
  },
  slideTitle: {
    color: "white",
    fontWeight: "bold",
    fontFamily: "NeulandGroteskCondensedRegular",

    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      marginLeft: 20,
      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      marginLeft: 20,
      fontSize: 25
    }
  },
  slideDescription: {
    color: "white",
    fontFamily: "NeulandGroteskCondensedRegular",

    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      marginLeft: 20,
      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      marginLeft: 20,
      fontSize: 25
    }

    // height: 40,
    // marginTop: 50
  },
  headerBottomContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  largeText: {
    color: "white",
    wordWrap: "break-word",
    lineHeight: 1.5,
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25
    }
  },
  header: {
    width: "100%",
    height: "20vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  headerTop: {
    width: "100%",
    height: "50%",
    display: "flex",
    // flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between"
  },
  headerBottom: {
    width: "100%",
    height: "50%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  headerBottomText1: {
    color: "white",
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25
    }
  },
  headerBottomText2: {
    color: "white",
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25
    }
  },
  headerBar: {
    width: "60%",
    height: 1,
    backgroundColor: "white",
    marginTop: 10,
    marginBottom: 10
  },
  eventImage: {
    width: "100%"
  },
  shadowGradient: {
    backgroundImage:
      "linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0.5), rgba(0,0,0,0.8),rgba(0,0,0,1), rgba(0,0,0,1));",
    position: "relative",
    bottom: 100,
    height: 100,
    width: "100%"
  },
  imageContainer: {
    marginTop: 20,
    width: "100%"
  },
  positionContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
    // height: 200
  },
  positionTextContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    color: "white",
    marginBottom: 50
  },
  positionText1: {
    textDecoration: "underline",
    fontWeight: "bold",
    marginTop: 40,
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25
    }
  },
  positionText2: {
    fontWeight: "bold",
    marginTop: 15,
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25
    }
  },
  positionIcon: {
    height: 100,
    width: 100
  }
});
