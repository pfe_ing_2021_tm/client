/*
 * Created Date: Wednesday October 30th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, October 31st 2019, 2:15:12 pm
 * Modified By: Amir Dorgham
 * -----
 */
/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Tuesday, October 29th 2019, 2:51:41 pm
 * Modified By: Amir Dorgham
 * -----
 */

import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import DatePanel from "../events/datePanel";
import DescriptionPanel from "./descriptionPanel";
import PositionDescription from "./positionDescription";

class EventDescription extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.description}>
        <div className={classes.descriptionContainer}>
          <div style={{ width: 50, marginLeft: "10%" }}>
            <DatePanel
              calendarDay={this.props.calendarDay}
              calendarMonth={this.props.calendarMonth}
            />
          </div>
          <DescriptionPanel
            title={this.props.title}
            hours={this.props.hours}
            description={this.props.description}
          />
        </div>
        <PositionDescription
          title={this.props.title}
          address={this.props.address}
          latitude={this.props.latitude}
          longitude={this.props.longitude}
        />
      </div>
    );
  }
}
export default withStyles(styles)(EventDescription);
