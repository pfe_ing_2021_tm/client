/**
 * @author Wathek KAHLA
 * @email wathek.kahla@chifco.com
 * @create date 2020-10-16 11:11:28
 * @modify date 2020-10-16 11:11:30
 * @desc [description]
 */

import React from "react";
import PropTypes from "prop-types";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./style";
import IconButton from "@material-ui/core/IconButton";
import SwipeableViews from "react-swipeable-views";
import Pagination from "./Pagination";
import cityon from "../../assets/img/cityon.png";
import cityoff from "../../assets/img/cityoff.png";
import cityback from "../../assets/img/cityback.png";
import citynext from "../../assets/img/citynext.png";
import closemap from "../../assets/img/closemap.png";
import { compose } from "redux";
import { connect } from "react-redux";
import { getBrandList, getActiveBrand } from "../../store/actions";

function Transition(props) {
  return <Slide direction="up" {...props} mountOnEnter unmountOnExit />;
}

class BrandsSelection extends React.Component {
  state = {
    open: false,
    count: 0,
    index: 0,
    arrslid: [],
    citySelected: 0,
    black: true,
  };

  componentDidMount() {
    this.props.getBrandList();
    const brandid = localStorage.getItem("brand");
    this.setState({ citySelected: brandid });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.brandList !== this.props.brandList) {
      var chunk = 4;
      var obj = [];
      for (let i = 0; i < this.props.brandList.length; i += chunk) {
        obj.push({ city: this.props.brandList.slice(i, i + chunk) });
      }
      this.setState({ arrslid: obj, count: obj.length });
    }
  }

  tryGetActiveCity = () => {
    if (this.state.citySelected !== 0) {
      this.props.getActiveBrand(this.state.citySelected);
    }

    this.handleClose();
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false, playing: false });
    this.props.handleClose();
  };

  handleChangeIndex = (index) => {
    this.setState({
      index,
      highResImageLoaded: false,
    });
  };

  handleNextIndex = () => {
    this.setState({
      index: this.state.index + 1,
    });
  };

  handleBackIndex = () => {
    this.setState({
      index: this.state.index - 1,
    });
  };

  onSetCity = (id) => {
    this.setState({ citySelected: id }, () => this.tryGetActiveCity());
    localStorage.removeItem("city");
    localStorage.setItem("brand", id);
    this.changeColor();
  };
  changeColor = () => {
    this.setState({ black: !this.state.black });
  };

  render() {
    const { classes } = this.props;
    const { index } = this.state;
    return (
      <div>
        {/*<IconButton
          onClick={this.handleClickOpen}
          className={classes.icnButton}
          color="inherit"
          aria-label="Menu"
        >
          {this.props.notif && <span className={classes.dot}></span>}
          <img src={IconMap} alt="" style={{ width: 22 }} id="Menu_Villes" />
        </IconButton> */}

        <Dialog
          fullScreen
          open={this.props.open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
          PaperProps={{ classes: { root: classes.dialogPaper } }}
        >
          <img
            src={closemap}
            alt=""
            className={classes.closebtn}
            onClick={this.handleClose}
          />

          {/*<img src={citybg} className={classes.container} />*/}

          <SwipeableViews
            index={index}
            onChangeIndex={this.handleChangeIndex}
            enableMouseEvents
            containerStyle={{ height: "100%" }}
            style={{ width: "85%" }}
          >
            {this.state.arrslid.map((item, index) => (
              <div key={index} style={Object.assign({}, styles.slide)}>
                {item.city.map((city, index) => (
                  <div
                    className={classes.contcityname}
                    onClick={
                      city.status ? () => this.onSetCity(city._id) : null
                    }
                    key={index}
                  >
                    <div style={{ width: "90%" }}>
                      <IconButton
                        className={classes.icnButton}
                        color="inherit"
                        aria-label="Menu"
                      >
                        <p
                          id={`Click_on_${city.name}_city_list`}
                          className={
                            city.status
                              ? classes.citytexton
                              : classes.citytextoff
                          }
                          style={{
                            // margin: "10px",
                            // border: 5,
                            textAlign: "start",
                            padding: 10,
                            fontFamily:
                              this.state.citySelected === city._id &&
                              city.status &&
                              "NeulandGroteskCondensedBold",
                            fontSize:
                              this.state.citySelected === city._id &&
                              city.status &&
                              40,
                          }}
                        >
                          {city.name}
                        </p>
                      </IconButton>
                    </div>
                    <div style={{ width: "10%" }}>
                      {city.status ? (
                        <img
                          src={cityon}
                          className={classes.citystatus}
                          alt=""
                        />
                      ) : (
                          <img
                            src={cityoff}
                            className={classes.citystatus}
                            alt=""
                          />
                        )}
                    </div>
                  </div>
                ))}
              </div>
            ))}
          </SwipeableViews>
          {this.state.count !== 1 && (
            <div className={classes.contpaggination}>
              <img
                onClick={this.state.index !== 0 ? this.handleBackIndex : null}
                src={cityback}
                style={{
                  width: 20,
                  filter: this.state.index !== 0 ? "none" : "brightness(0.5)",
                }}
                alt=""
              />
              <Pagination
                dots={this.state.count}
                index={index}
                onChangeIndex={this.handleChangeIndex}
              />

              <img
                src={citynext}
                onClick={
                  this.state.index + 1 !== this.state.count
                    ? this.handleNextIndex
                    : null
                }
                style={{
                  width: 20,
                  filter:
                    this.state.index + 1 !== this.state.count
                      ? "none"
                      : "brightness(0.5)",
                }}
                alt=""
              />
            </div>
          )}
          {/* <div
            style={{
              position: "absolute",
              bottom: 25
            }}
            onClick={this.tryGetActiveCity}
          >
            <img src={btndecouv} style={{ width: 160 }} alt="" />
          </div> */}
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    brandList: state.brandList.brandList,
  };
};

const mapDispatchToProps = (dispatche) => {
  return {
    getBrandList: () => dispatche(getBrandList()),
    getActiveBrand: (id) => dispatche(getActiveBrand(id)),
  };
};

BrandsSelection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(BrandsSelection);
