/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:59:39+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-19T11:52:38+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */
import citybg from "../../assets/img/citybg.png";
export const styles = (theme) => ({
  root: {
    width: "100%",
  },
  container: {
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundSize: "cover",
    position: "absolute",
  },
  // dialogPaper: {
  //   backgroundImage: `url(${citybg})`,
  //   display: "flex",
  //   justifyContent: "center",
  //   alignItems: "center",
  //   backgroundColor: "#96010F",
  //   backgroundSize: "cover"
  // },
  dialogPaper: {
    backgroundImage: `url(${citybg})`,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#96010F",
    backgroundSize: "cover",
    [theme.breakpoints.between("lg", "xl")]: {
      width: 400,
    },
    [theme.breakpoints.between("sm", "md")]: {
      width: "100%",
    },
    [theme.breakpoints.between("xs", "sm")]: {
      //backgroundImage: `url("https://cdn.futura-sciences.com/buildsv6/images/mediumoriginal/6/5/2/652a7adb1b_98148_01-intro-773.jpg")`,

      width: "100%",
    },
  },

  contcityname: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 23,
  },
  citystatus: {
    width: 20,
  },
  citytexton: {
    fontFamily: "NeulandGroteskCondensedRegular",
    color: theme.palette.secondary.main,
    fontSize: 30,
    margin: 0,
    "&:hover": {
      opacity: "0.5",
    },
  },
  citytextoff: {
    fontFamily: "NeulandGroteskCondensedRegular",
    color: "#000",
    fontSize: 40,
    margin: 0,
  },
  contpaggination: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "90%",
    zIndex: 9,
  },
  arrowpagination: {
    width: 20,
  },
  closebtn: {
    position: "absolute",
    right: 37,
    top: 25,
    width: 25,
  },
})
