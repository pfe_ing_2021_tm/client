/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-09-09 18:14:36
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-02-17 18:07:42
 */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import replayIcn from "../../assets/img/replay.png";
import btn_valider from "../../assets/img/btn_valider.png";
import { styles } from "./style";
import { connect } from "react-redux";
import { compose } from "redux";
import { Link } from "react-router-dom";
import { history } from "../../history";
import { useTheme } from "@material-ui/styles";
import "./stylePlayer.css";
import {
  setCodeResult,
  setResultCode,
  getVideoById,
  setVideoItem
} from "../../store/actions";

class EntryCode extends React.Component {
  constructor(props) {
    super(props);
    this.myRefNum1 = React.createRef();
    this.state = {
      open: true,
      newCode: [],
      arrcode: [],
      codeResult: null,
      idvideo: null,
      clearInput: true
    };
  }

  handleClose = () => {
    //this.setState({ open: false });
    this.props.setResultCode(null);
  };

  handleOpen = value => {
    this.setState({ open: true });
  };

  shuffle = a => {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  };

  componentWillMount() {
    this.props.getVideoById(this.props.match.params.videoid);
    //get item video from cityDataReducer
  }

  componentWillUnmount() {
    this.props.setVideoItem();
  }

  async componentWillReceiveProps(prevProps) {
    if (prevProps.videoItem !== this.props.videoItem) {
      const firsCode = ("" + prevProps.videoItem.metaType).split("");

      firsCode.push(Math.floor(Math.random() * Math.floor(9)));
      firsCode.push(Math.floor(Math.random() * Math.floor(9)));

      const newCode = await this.shuffle(firsCode);
      await this.setState({ newCode, idvideo: prevProps.videoItem._id });
    }
  }

  onClickInNumber = e => {
    if (this.state.arrcode.length < 6) {
      this.setState({ arrcode: [...this.state.arrcode, e], clearInput: true });
    } else {
      this.setState({ arrcode: [], clearInput: false });
    }
  };

  onSubmitNumber = () => {
    const data = {
      idvideo: this.props.match.params.videoid,
      code: this.state.arrcode.join("")
    };
    //this.setState({ codeResult: data.code });
    this.props.setCodeResult(data);
  };

  onReplayVideo = () => {
    history.push(`/videoplayer/${this.state.idvideo}`);
    localStorage.setItem("lastScreen", true);
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.paperprops}>
        {this.props.resultcode === null ? (
          <div className="rota">
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: -65
              }}
            >
              <p style={{ color: "#fff", textAlign: "center" }}>
                QUEL EST LE CODE QUE <br />
                VOUS AVEZ VUE DANS LA VIDÉO ?
              </p>

              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <p className={classes.inputEntryCode}>
                  {this.state.arrcode[0]}
                </p>
                <p className={classes.inputEntryCode}>
                  {this.state.arrcode[1]}
                </p>
                <p className={classes.inputEntryCode}>
                  {this.state.arrcode[2]}
                </p>
                <p className={classes.inputEntryCode}>
                  {this.state.arrcode[3]}
                </p>
                <p className={classes.inputEntryCode}>
                  {this.state.arrcode[4]}
                </p>
                <p className={classes.inputEntryCode}>
                  {this.state.arrcode[5]}
                </p>
              </div>

              <div style={{ display: "flex", justifyContent: "space-around" }}>
                {this.state.newCode.map((item, index) => (
                  <p
                    key={index}
                    onClick={() => this.onClickInNumber(item)}
                    className={classes.btnnumb}
                  >
                    {item}
                  </p>
                ))}
              </div>
            </div>
            {/* content */}
            <div className={classes.bottomend}>
              <div className={classes.btnreplay}>
                <img src={replayIcn} alt="" className={classes.btnreplayimg} />

                <p
                  className={classes.btnreplaytext}
                  style={{ fontSize: 17 }}
                  onClick={this.onReplayVideo}
                >
                  REVOIR LA VIDÉO
                </p>
              </div>

              <div
                className={classes.btnnextvideo}
                onClick={this.onSubmitNumber}
              >
                <img
                  src={btn_valider}
                  alt=""
                  className={classes.btnvalider}
                  id={"Reponse_GameCode"}
                />
              </div>
            </div>
          </div>
        ) : (
          <div className="rota" style={{ flexDirection: "column" }}>
            <p
              style={{
                color: "#fff",
                fontSize: 25,
                marginLeft: 16,
                textTransform: "uppercase"
              }}
            >
              {this.props.resultcode}
            </p>
            <Link to="/">
              <div className={classes.btnnextvideo} onClick={this.handleClose}>
                <img
                  src={btn_valider}
                  alt=""
                  className={classes.btnvalider}
                  id={`Reponse Game Code : ${this.props.resultcode}`}
                />
              </div>
            </Link>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    cityData: state.cityData.cityData,
    resultcode: state.resultcode.resultcode,
    videoItem: state.videoItem.videoItem
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setCodeResult: data => dispatch(setCodeResult(data)),
    setResultCode: data => dispatch(setResultCode(data)),
    getVideoById: id => dispatch(getVideoById(id)),
    setVideoItem: () => dispatch(setVideoItem({}))
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(EntryCode);
