import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";

export const styles = theme => ({
  container: {
    display: "flex",
    [theme.breakpoints.between("sm", "md")]: {
      height: "50vh"
    },
    height: "90vh",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  logo: {
    width: 200,
    marginBottom: 40,
    marginTop: 60,
    [theme.breakpoints.between("sm", "md")]: {
      marginTop: 100,
      width: 250
    }
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginBottom: 20
  },
  bootstrapRoot: {
    "label + &": {
      marginTop: theme.spacing.unit * 3
    }
  },
  section: { display: "flex", flexDirection: "column", alignItems: "center" },
  bootstrapInput: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "auto",
    [theme.breakpoints.between("sm", "md")]: {
      width: 190
    },
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "NeulandGroteskCondensedRegular",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)"
    }
  },
  label: {
    color: "#fff",
    fontSize: 12,
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: 20
    },
    fontFamily: "NeulandGroteskCondensedRegular"
  },
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  info: {
    backgroundColor: theme.palette.primary.dark
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit
  },
  message: {
    display: "flex",
    alignItems: "center"
  },
  button: {
    marginTop: 20
  },
  checkbox: {
    color: "#ED2A1C",
    padding: 2
  },
  bottomContainer: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    position: "fixed",
    bottom: 0
  },
  forms: {
    [theme.breakpoints.between("sm", "md")]: {
      marginBottom: "7vh"
    }
  },
  bottomLightContainer: {
    backgroundColor: "#222222",
    display: "flex",
    color: "white",
    textAlign: "center",
    flexDirection: "column",
    width: "100%"
  },
  bottomDarkContainer: {
    backgroundColor: "#111112",
    height: "10vh",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  footer: {
    height: "10vh",
    background: "rgb(255, 255, 255)",
    textAlign: "center",
    width: "100%",
    margin: 0,
    textTransform: "uppercase",
    fontSize: 18,
    fontWeight: "700",
    lineHeight: "56px",
    // position: "fixed",
    // bottom: 0
    boxShadow: "inset 0px 0px 0px 10px #000",
    paddingTop: 15
  },

  footerNextStep: {
    position: "fixed",
    bottom: 0,
    height: 25,
    background: "rgb(255, 255, 255)",
    textAlign: "center",
    width: "100%",
    margin: 0,
    textTransform: "uppercase",
    fontSize: 11,
    fontWeight: "700",
    lineHeight: "24px"
  },
  largeText: {
    paddingRight: 20,
    paddingLeft: 20,
    [theme.breakpoints.between("sm", "md")]: {
      paddingRight: 80,
      paddingLeft: 80
    },
    fontSize: 12,
    textWrap: "break-word"
    //color: "white"
  },
  bottomTypography: {
    marginRight: 15,
    marginLeft: 15,
    color: "white",
    cursor: "pointer",
    // fontWeight: "heavy",
    fontFamily: "NeulandGroteskCondensedBold",
    fontSize: "10px",
    textTransform: "uppercase"
  },
  checkContainer2: {
    // marginTop: 20,
    marginLeft: 0,
    marginBottom: 10
  },
  checkContainer1: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start"
  },
  lien: {
    color: " #fff",
    fontSize: " 12px",
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: " 22px"
    },
    fontFamily: "NeulandGroteskCondensedRegular",
    padding: "5%"
  }
});
