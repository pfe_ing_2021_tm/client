import React from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
//import result from "./result/result.jpeg";
import log1 from "./result/section-1.jpeg";
import log2 from "./result/section-2.jpeg";
import log3 from "./result/section-3.jpeg";
import log4 from "./result/section-4.jpeg";
import log5 from "./result/section-5.jpeg";
import log6 from "./result/section-6.jpeg";
import log7 from "./result/section-7.jpeg";
import log8 from "./result/section-8.jpeg";
import log9 from "./result/section-9.jpeg";
import $ from "jquery";
import "./App.css";
const list = [
  { id: "0", mv: false, top: "0px", left: "0px", src: log8 },
  { id: "1", mv: false, top: "-100px", left: "100px", src: log4 },
  { id: "2", mv: false, top: "-200px", left: "200px", src: log7 },
  { id: "3", mv: false, top: "-200px", left: "0px", src: log6 },
  { id: "4", mv: false, top: "-300px", left: "100px", src: log2 },
  { id: "5", mv: false, top: "-400px", left: "200px", src: log1 },
  { id: "6", mv: false, top: "-400px", left: "0px", src: log3 },
  { id: "7", mv: false, top: "-500px", left: "100px", src: log9 },
  { id: "8", mv: false, top: "-600px", left: "200px", src: log5 },
];
const list0 = [
  { id: "0", mv: false, top: "0px", left: "0px", src: log1 },
  { id: "1", mv: false, top: "-100px", left: "100px", src: log2 },
  { id: "2", mv: false, top: "-200px", left: "200px", src: log3 },
  { id: "3", mv: false, top: "-200px", left: "0px", src: log4 },
  { id: "4", mv: false, top: "-300px", left: "100px", src: log5 },
  { id: "5", mv: false, top: "-400px", left: "200px", src: log6 },
  { id: "6", mv: false, top: "-400px", left: "0px", src: log7 },
  { id: "7", mv: false, top: "-500px", left: "100px", src: log8 },
  { id: "8", mv: false, top: "-600px", left: "200px", src: log9 },
];

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.onSwipe = this.onSwipe.bind(this);
    this.state = {
      tableInit: list,
      firstclick: false,
      secondclick: true,
      tabImage: list0.slice(),
      topPosSec: "",
      topPosFir: "",
      isfinished: false,
      leftPosSec: "",
      opendiag: false,
      leftPosFir: "",
      image1: "",
      image2: "",
      id1: 0,
      id2: 0,
      count: 0,
      active: false,
    };
  }
  componentWillMount() {
    this.onSwipe();
  }
  componentDidMount() {
    setTimeout(function () {
      shuffleTiles();
      setInterval(function () {
        secs++;
      }, 1000);
    }, 1000);

    var tileClicked = false;
    var firstTileClicked;
    var secondTileClicked;
    var topPosFir = 0;
    var leftPosFir = 0;
    var topPosSec = 0;
    var leftPosSec = 0;
    var shuffle = Math.floor(Math.random() * 4 + 1);
    var moves = 0;
    var secs = 0;

    //  shuffle the tiles

    function shuffleTiles() {
      if (shuffle === 1) {
        $("#piece-1").css({ top: 100, left: 200 });
        $("#piece-2").css({ top: 0, left: 200 });
        $("#piece-3").css({ top: 100, left: 100 });
        $("#piece-4").css({ top: 0, left: 100 });
        $("#piece-5").css({ top: 100, left: 0 });
        $("#piece-6").css({ top: 0, left: 0 });
      } else if (shuffle === 2) {
        $("#piece-1").css({ top: 100, left: 0 });
        $("#piece-2").css({ top: 0, left: 0 });
        $("#piece-3").css({ top: 100, left: 100 });
        $("#piece-4").css({ top: 0, left: 100 });
        $("#piece-5").css({ top: 100, left: 200 });
        $("#piece-6").css({ top: 0, left: 200 });
      } else if (shuffle === 3) {
        $("#piece-1").css({ top: 0, left: 200 });
        $("#piece-2").css({ top: 0, left: 0 });
        $("#piece-3").css({ top: 100, left: 100 });
        $("#piece-4").css({ top: 100, left: 200 });
        $("#piece-5").css({ top: 0, left: 100 });
        $("#piece-6").css({ top: 100, left: 0 });
      } else if (shuffle === 4) {
        $("#piece-1").css({ top: 0, left: 200 });
        $("#piece-2").css({ top: 100, left: 200 });
        $("#piece-3").css({ top: 0, left: 100 });
        $("#piece-4").css({ top: 100, left: 100 });
        $("#piece-5").css({ top: 0, left: 0 });
        $("#piece-6").css({ top: 100, left: 0 });
      }
    }

    //  play the game

    $(".pieces").click(function () {
      if (tileClicked === false) {
        //  if no tile is clicked

        //  set variables
        firstTileClicked = $(this).attr("id");
        topPosFir = parseInt($(this).css("top"));
        leftPosFir = parseInt($(this).css("left"));

        //  highlight tile
        $(this).addClass("glow");
        tileClicked = true;
      } else {
        //  if you've clicked a tile

        //  set variables
        secondTileClicked = $(this).attr("id");
        topPosSec = parseInt($(this).css("top"));
        leftPosSec = parseInt($(this).css("left"));

        //  animations
        $("#" + firstTileClicked).css({ top: topPosSec, left: leftPosSec });
        $("#" + secondTileClicked).css({ top: topPosFir, left: leftPosFir });

        //  remove the glow and reset the first tile
        $(".pieces").removeClass("glow");
        tileClicked = false;

        //  test for the win

        setTimeout(function () {
          if (
            $("#piece-1").css("left") === "0px" &&
            $("#piece-1").css("top") === "0px" &&
            $("#piece-2").css("left") === "100px" &&
            $("#piece-2").css("top") === "0px" &&
            $("#piece-3").css("left") === "200px" &&
            $("#piece-3").css("top") === "0px" &&
            $("#piece-4").css("left") === "0px" &&
            $("#piece-4").css("top") === "100px" &&
            $("#piece-5").css("left") === "100px" &&
            $("#piece-5").css("top") === "100px" &&
            $("#piece-6").css("left") === "200px" &&
            $("#piece-6").css("top") === "100px"
          ) {
            $("p").text(
              "You have solved the puzzle in " +
                secs +
                " seconds using " +
                moves +
                " moves!!"
            );
            $("article").addClass("glow-2");
            moves = 0;
          }
        }, 1000);

        //  increment the move counter
        moves++;
      }
    }); //  end the click function
  }

  handleClose() {
    this.setState({ opendiag: !this.state.opendiag });
  }
  Reorder(list, startIndex, endIndex) {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  }
  onSwipe(box) {
    let tabs = list.slice();
    let item = box;
    //let id1 = 0;
    if (item !== undefined) {
      if (this.state.firstclick === false && this.state.secondclick === true) {
        //firstclick
        //var d = document.getElementById(item.id);
        this.state.ind1 = this.state.tableInit.findIndex(
          (t) => t.id === item.id
        );
        //  this.state.topPosFir=this.state.tableInit[this.state.ind1].top;
        //this.state.leftPosFir=this.state.tableInit[this.state.ind1].left;
        this.state.image1 = this.state.tableInit[this.state.ind1].src;

        this.setState({ firstclick: !this.state.firstclick, active: true });
        this.setState({ secondclick: !this.state.secondclick });
        if (
          this.state.tableInit[this.state.ind1].src ===
          this.state.tabImage[this.state.ind1].src
        ) {
          this.state.tableInit[this.state.ind1].mv = true;
        }
      } else {
        if (this.state.secondclick === false) {
          //secondclick
          //var d = document.getElementById(item.id);
          this.state.ind2 = this.state.tableInit.findIndex(
            (t) => t.id === item.id
          );
          //   this.state.topPosSec=this.state.tableInit[this.state.ind2].top;
          //   this.state.leftPosSec=this.state.tableInit[this.state.ind2].left;
          //   this.state.tableInit[this.state.ind1].top=this.state.topPosSec;
          //   this.state.tableInit[this.state.ind1].left=this.state.leftPosSec;
          //   this.state.tableInit[this.state.ind2].top=this.state.topPosFir;
          this.state.image2 = this.state.tableInit[this.state.ind2].src;
          this.setState({ active: false });
          this.state.tableInit[this.state.ind2].src = this.state.image1;
          this.state.tableInit[this.state.ind1].src = this.state.image2;
          if (
            this.state.tableInit[this.state.ind1].src ===
            this.state.tabImage[this.state.ind1].src
          ) {
            this.state.tableInit[this.state.ind1].mv = true;
          }
          if (
            this.state.tableInit[this.state.ind2].src ===
            this.state.tabImage[this.state.ind2].src
          ) {
            this.state.tableInit[this.state.ind2].mv = true;
          }
          let count = 0;
          let p = count + 1;
          for (let i = 0; i < 9; i++) {
            if (this.state.tableInit[i].mv === true) count++;
            this.state.count++;
          }
          if (count === 9) {
            setTimeout(() => this.setState({ opendiag: true }), 500);
          }
          //this.state.tableInit[this.state.ind2].left=this.state.leftPosFir;
          // this.state.tableInit.splice(this.state.ind1,0,this.state.tableInit[this.state.ind2]);
          //  this.state.tableInit.splice(this.state.ind2,0,this.state.tableInit[this.state.ind1]);

          this.setState({ firstclick: false });
          this.setState({ secondclick: !this.state.secondclick });
        }
      }

      this.state.tableInit = tabs;
    } else {
      return;
    }
    //switch between two values
  }

  render() {
    const list = this.state.tableInit;

    //  globals

    return (
      <>
        <Dialog
          className="dialog-box"
          open={this.state.opendiag}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle className="dialog-title" id="responsive-dialog-title">
            Félicitation &#128079;{" "}
          </DialogTitle>
          <DialogContent className="diag-content">
            <DialogContentText>
              félicitation! vous avez finis la partie du Puzzel avec succées.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleClose.bind(this)}
              color="#2664A3"
              autoFocus
            >
              Confirmer
            </Button>
          </DialogActions>
        </Dialog>

        <div className="container-puzzle">
          <article>
            <div className="pieces" id="piece-1"></div>
            <div className="pieces" id="piece-2"></div>
            <div className="pieces" id="piece-3"></div>
            <div className="pieces" id="piece-4"></div>
            <div className="pieces" id="piece-5"></div>
            <div className="pieces" id="piece-6"></div>
          </article>
        </div>
      </>
    );
  }
}

//C:\Users\hp\Downloads\puzzlegame\src
