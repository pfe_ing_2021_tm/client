/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Wednesday, October 30th 2019, 4:58:17 pm
 * Modified By: Amir Dorgham
 * -----
 */

import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";

class DatePanel extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.datePanelcontainer}>
        <span className={classes.calendarDay}>{this.props.calendarDay}</span>
        <div className={classes.separator} />
        <span className={classes.calendarMonth}>
          {this.props.calendarMonth}
        </span>
      </div>
    );
  }
}
export default withStyles(styles)(DatePanel);
