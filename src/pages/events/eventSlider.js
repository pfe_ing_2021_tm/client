/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Thursday, December 12th 2019, 4:28:56 pm
 * Modified By: Amir Dorgham
 * -----
 */

import React, { Component } from "react";
import Slider from "react-slick";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { history } from "../../history";
import { connect } from "react-redux";
import { compose } from "redux";
import { EventView } from "../../store/actions";

class EventsSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0
    };
  }

  // componentDidMount() {
  //   this.props.EventView(this.props.match.params.eventid)
  // }

  viewevent = x => {
    const data = {
      eventid: x,
      status: true
    };
    history.push(`/events/${x}`);
    this.props.EventView(data);
  };
  render() {
    const { classes } = this.props;
    const settings = {
      dots: false,
      overflow: "hidden",
      arrows: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      beforeChange: (prev, next) => {
        this.setState({ currentSlide: next });
      }
    };
    return (
      <div className={classes.sliderContainer}>
        <Slider {...settings} className={classes.event}>
          {this.props.data.map((item, i) => (
            <div
              className={
                i === this.state.currentSlide
                  ? classes.active
                  : classes.inactive
              }
            >
              {i === this.state.currentSlide ? (
                <div
                  className={classes.activeSlideContainer}
                  onClick={() => this.viewevent(item._id)}
                >
                  <img
                    id={`Events_${item.title}_Details`}
                    src={item.photo}
                    className={classes.eventImageActive}
                    alt=""
                  ></img>
                  <div className={classes.SlideTextArea}>
                    <span className={classes.slideTitle}>{item.title}</span>
                    <span className={classes.slideDescription}>
                      A partir de {item.date.hours}H
                    </span>
                  </div>
                </div>
              ) : (
                <div className={classes.inactiveSlideContainer}>
                  <img
                    src={item.photo}
                    className={classes.eventImageInactive}
                    alt="sorry"
                    onClick={() => this.viewevent(item._id)}
                  ></img>
                  <div className={classes.SlideTextArea}>
                    <span className={classes.slideTitleInactive}>
                      {item.title}
                    </span>
                    <span className={classes.slideDescriptionInactive}>
                      A partir de {item.date.hours}H
                    </span>
                  </div>
                </div>
              )}
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    EventView: data => dispatch(EventView(data))
  };
};

export default compose(
  withStyles(styles),
  connect(null, mapDispatchToProps)
)(EventsSlider);
