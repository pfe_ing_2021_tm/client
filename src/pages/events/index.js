/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Friday, December 13th 2019, 10:48:35 am
 * Modified By: Amir Dorgham
 * -----
 */

import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { compose } from "redux";
import Header from "./Header";
import EventsSlider from "./eventSlider";
import DatePanel from "./datePanel";
import frLocale from "date-fns/locale/fr";
import nextimg from "../../assets/img/next.png";
import previmg from "../../assets/img/prev.png";
import DateFnsUtils from "@date-io/date-fns";
import "date-fns";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import { getEvents } from "../../store/actions";
import { MuiPickersUtilsProvider, DatePicker } from "material-ui-pickers";
import CircularProgress from "@material-ui/core/CircularProgress";
import { history } from "../../history";
const token = localStorage.getItem("token");

class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      shouldShowFilterPanel: false,
      displayedEvents: [],
      moreDisplayedEvents: [],
      cities: [],
      categories: [],
      page: 1,
      city: "",
      token,
      from: moment(new Date()).format("YYYY-MM-DD"),
      categorie: "",
      shouldShowDate: false,
      maxPage: 0
    };
  }
  componentDidMount() {
    if (this.state.token === null) {
      return history.push("/");
    }
    let filterObject = {
      page: this.state.page,
      city: this.state.city,
      categorie: this.state.categorie,
      from: this.state.from
    };
    this.props.getEvents(filterObject);
  }
  componentWillReceiveProps(prevProps) {
    if (prevProps.events !== this.props.events) {
      this.setState({
        displayedEvents: prevProps.events.groupedEvents,
        cities: prevProps.events.cities,
        categories: prevProps.events.categories,
        loading: false,
        maxPage: prevProps.events.maxpage
      });
    }
  }
  toggleFilter = () => {
    this.setState({
      shouldShowFilterPanel: !this.state.shouldShowFilterPanel
    });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };
  handleDateChange = date => {
    this.setState({
      from: moment(date).format("YYYY-MM-DD"),
      shouldShowDate: true
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true, shouldShowFilterPanel: false, page: 1 });
    let filterObject = {
      page: this.state.page,
      city: this.state.city,
      categorie: this.state.categorie,
      from: this.state.from
    };
    this.props.getEvents(filterObject);
  };
  handleReset = event => {
    event.preventDefault();
    this.setState({
      shouldShowDate: false,
      city: "",
      categorie: "",
      from: moment(new Date()).format("YYYY-MM-DD")
    });
    let filterObject = {
      page: this.state.page,
      city: "",
      categorie: "",
      from: ""
    };
    //this.props.getEvents(filterObject);
  };
  handleNext = () => {
    if (this.state.page < this.state.maxPage) {
      this.setState({
        loading: true,
        page: this.state.page + 1
      });
      let filterObject = {
        page: this.state.page + 1,
        city: this.state.city,
        categorie: this.state.categorie,
        from: this.state.from
      };
      this.props.getEvents(filterObject);
    }
  };
  handlePrev = () => {
    if (this.state.page > 1) {
      this.setState({
        loading: true,
        page: this.state.page - 1
      });
      let filterObject = {
        page: this.state.page - 1,
        city: this.state.city,
        categorie: this.state.categorie,
        from: this.state.from
      };
      this.props.getEvents(filterObject);
    }
  };
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.container}>
        {this.state.loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh"
            }}
          >
            <CircularProgress className={classes.progress} color="secondary" />
          </div>
        ) : (
          <div className={classes.container}>
            <Header filterClick={this.toggleFilter} />

            {!this.state.shouldShowFilterPanel && (
              <div className={classes.eventsContainer}>
                {this.state.displayedEvents.length !== 0 &&
                  this.state.displayedEvents.map((value, index) => (
                    // <span style={{ color: "white" }}>{value.title}</span>
                    <div className={classes.eventRow}>
                      <div className={classes.dateIndexContainer}>
                        <DatePanel
                          calendarDay={value._id.day}
                          calendarMonth={value._id.month}
                        />
                      </div>
                      <EventsSlider data={value.events} />
                    </div>
                  ))}
                {this.state.displayedEvents.length !== 0 &&
                  this.state.maxPage !== 1 && (
                    <div className={classes.moreButtonContainer}>
                      <div className={classes.pageIconsContainer}>
                        <img
                          src={previmg}
                          className={
                            this.state.page !== 1
                              ? classes.pageIcons
                              : classes.pageIconsDisabled
                          }
                          alt=""
                          onClick={this.handlePrev}
                        />
                      </div>
                      <div>
                        <span className={classes.pageIndex}>
                          {this.state.page} / {this.state.maxPage}
                        </span>
                      </div>
                      <div className={classes.pageIconsContainer}>
                        <img
                          src={nextimg}
                          className={
                            this.state.page !== this.state.maxPage
                              ? classes.pageIcons
                              : classes.pageIconsDisabled
                          }
                          alt=""
                          onClick={this.handleNext}
                        />
                      </div>
                    </div>
                  )}
              </div>
            )}
            {this.state.shouldShowFilterPanel && (
              <form
                className={classes.formContainer}
                noValidate
                autoComplete="off"
              >
                <div className={classes.filterLabelContainer}>
                  <span className={classes.filterLabel}>Ville</span>
                </div>
                <TextField
                  FormHelperTextProps={{
                    className: classes.whiteColor
                  }}
                  InputLabelProps={{
                    className: classes.whiteColor
                  }}
                  InputProps={{
                    className:
                      this.state.city !== ""
                        ? classes.inputText
                        : classes.inputTextEmpty
                  }}
                  id="standard-select-ville"
                  select
                  label=""
                  // {this.state.city === "" ? "Ville" : ""}
                  className={classes.textField}
                  value={this.state.city}
                  onChange={this.handleChange("city")}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu
                    }
                  }}
                  // variant="outlined"
                  // helperText={this.state.city === "" ? "Ville" : ""}
                  // "Veuillez sélectionner votre ville"
                  // margin="normal"
                >
                  {this.state.cities.map(option => (
                    <MenuItem key={option} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
                <div className={classes.filterLabelContainer}>
                  <span className={classes.filterLabel}>Catégorie</span>
                </div>
                <TextField
                  FormHelperTextProps={{
                    className: classes.whiteColor
                  }}
                  InputLabelProps={{
                    className: classes.whiteColor
                  }}
                  InputProps={{
                    className:
                      this.state.categorie !== ""
                        ? classes.inputText
                        : classes.inputTextEmpty
                  }}
                  id="standard-select-category"
                  select
                  label=""
                  // {this.state.categorie === "" ? "Catégorie" : ""}
                  className={classes.textField}
                  value={this.state.categorie}
                  onChange={this.handleChange("categorie")}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu
                    }
                  }}
                  // variant="outlined"
                  // helperText={this.state.categorie === "" ? "Catégorie" : ""}
                  // "Veuillez sélectionner la catégorie"
                  // margin="normal"
                >
                  {this.state.categories.map(option => (
                    <MenuItem key={option} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
                <div className={classes.filterLabelContainer}>
                  <span className={classes.filterLabel}>Date</span>
                </div>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={frLocale}>
                  <DatePicker
                    id="dateId"
                    // variant="outlined"
                    // margin="normal"
                    label=""
                    // {this.state.from === "" ? "Date" : ""}
                    disablePast
                    format="yyyy-MM-dd"
                    className={classes.dateField}
                    InputLabelProps={{
                      className: classes.dateLabel
                    }}
                    InputProps={{
                      className: this.state.shouldShowDate
                        ? classes.inputText
                        : classes.inputDateEmpty
                    }}
                    value={this.state.from}
                    onChange={this.handleDateChange}
                  />
                </MuiPickersUtilsProvider>
                <div className={classes.submitButtonContainer}>
                  <button className={classes.button} onClick={this.handleReset}>
                    EFFACER
                  </button>
                  <button
                    className={classes.button}
                    onClick={this.handleSubmit}
                    id="Valider_Filter_Events"
                  >
                    VALIDER
                  </button>
                </div>
              </form>
            )}
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    events: state.events.listEvents
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getEvents: filterObject => dispatch(getEvents(filterObject))
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Events);
