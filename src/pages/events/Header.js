/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Friday, December 13th 2019, 10:50:41 am
 * Modified By: Amir Dorgham
 * -----
 */
import React, { Component } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import backimg from "../../assets/img/backprofile.png";
import { Link } from "react-router-dom";
import vyndimg from "../../assets/img/vynd.png";
import filterimg from "../../assets/img/filterimg.png";

class Header extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.header}>
        <div className={classes.headerTop}>
          <Link to="/">
            <div
              style={{
                display: "flex",
                alignItems: "center",
                paddingLeft: 40,
                marginTop: 15,
                marginBottom: 15
              }}
            >
              <img
                src={backimg}
                style={{ width: 35, marginRight: 15 }}
                alt=""
              />
            </div>
          </Link>
          <img
            id={`Show_Filter_Events`}
            src={filterimg}
            className={classes.filterimg}
            alt=""
            onClick={this.props.filterClick}
          />
        </div>
        {/* <div className={classes.headerBottom}>
          <span className={classes.headerBottomText1}>
            Prochains evenements
          </span>
          <div className={classes.headerBottomContainer}>
            <span className={classes.headerBottomText2}>Powered by </span>
            <img src={vyndimg} style={{ width: "9vw", marginLeft: 5 }} alt="" />
          </div>
          <div className={classes.headerBar}></div>
        </div> */}
      </div>
    );
  }
}
export default withStyles(styles)(Header);
