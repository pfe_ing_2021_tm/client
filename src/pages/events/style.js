/*
 * Created Date: Monday October 28th 2019
 * Author: Amir Dorgham
 * -----
 * Last Modified: Friday, December 13th 2019, 10:54:28 am
 * Modified By: Amir Dorgham
 * -----
 */

export const styles = (theme) => ({
  container: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  formContainer: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    width: "90%",
    marginTop: 30,
  },
  submitButtonContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    width: "85%",
    marginTop: 40,
    backgroundColor: "yeloow",
  },
  moreButtonContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    width: "100%",
    marginTop: 20,
    marginBottom: 30,
    // backgroundColor: "red"
  },
  filterLabelContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "75%",
    marginTop: 15,
    marginBottom: 5,
  },
  filterLabel: {
    color: "white",
    fontWeight: "bold",
    fontFamily: "NeulandGroteskCondensedRegular",
    letterSpacing: 1,
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 15,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 19,
    },
  },
  whiteColor: {
    color: "white",
    marginLeft: "6%",
    top: -25,
    position: "absolute",
    // backgroundColor: "green"
  },
  dateLabel: {
    color: "white",
    position: "absolute",
    top: -10,

    // backgroundColor: "green"
  },
  inputText: {
    fontFamily: "NeulandGroteskCondensedRegular",

    height: 50,
    color: "white",
    paddingLeft: "5%",
    backgroundColor: "#bd2132",
    borderRadius: 5,
    "&&&:before": {
      borderBottom: "none",
    },
    "&&:after": {
      borderBottom: "none",
    },
  },
  inputTextEmpty: {
    // paddingLeft: "5%",
    height: 50,
    color: "white",
    backgroundColor: "gray",
    borderRadius: 5,
    "&&&:before": {
      borderBottom: "none",
    },
    "&&:after": {
      borderBottom: "none",
    },
  },
  inputDateEmpty: {
    paddingLeft: "5%",
    height: 50,
    color: "white",
    backgroundColor: "gray",
    borderRadius: 5,
    "&&&:before": {
      borderBottom: "none",
    },
    "&&:after": {
      borderBottom: "none",
    },
  },
  moreButton: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    color: "white",
    fontSize: 15,
    border: "none",
    borderRadius: 3,
    padding: "1em 2em",
    backgroundColor: "white",
    fontWeight: "bold",
    fontFamily: "NeulandGroteskCondensedRegular",
    letterSpacing: 1,
    "&:hover": {
      cursor: "pointer",
    },
    "&:active": {
      backgroundColor: "white",
    },
  },
  button: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    color: "white",
    border: "none",
    borderRadius: 3,
    padding: "1em 2.5em",
    backgroundColor: "gray",
    fontFamily: "NeulandGroteskCondensedBold",
    letterSpacing: 1,
    cursor: "pointer",

    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 17,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      fontSize: 33,
      marginTop: 30,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 15,
    },
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    fontFamily: "NeulandGroteskCondensedRegular",
    width: "80%",
  },
  dateField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "80%",
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 250,
  },
  header: {
    width: "100%",
    height: "20vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  headerBottomContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  filterContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  headerTop: {
    width: "100%",
    height: "50%",
    display: "flex",
    // flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerBottom: {
    width: "100%",
    height: "50%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  headerBottomText1: {
    color: "white",
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  headerBottomText2: {
    color: "white",
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  headerBar: {
    width: "60%",
    height: 1,
    backgroundColor: "white",
    marginTop: 10,
    // marginBottom: 10
  },
  filterimg: {
    width: 35,
    paddingRight: 40,
    cursor: "pointer",
    // position: "absolute",
    // right: "calc(50vw - 165px)",
    // right: "30vw",
    // top: "3vh"
  },
  pageIcons: {
    width: 45,
  },
  pageIconsDisabled: {
    width: 45,
    filter: "grayscale(100%);",
  },
  pageIconsContainer: {
    width: 40,
    height: 40,

    backgroundColor: "white",
    borderRadius: "50%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  eventsContainer: {
    width: "100%",
    marginTop: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },

  event: {
    // marginBottom: 40,
    // width: "80%",
    marginLeft: "25%",
    height: 225,
  },
  active: {
    height: 230,
  },
  inactive: {
    height: 230,
  },
  eventImageActive: {
    boxShadow: "0px 0px 2px 1px  #FFFFFF",
    marginRight: "2%",
    width: "95%",
    height: 220,
    marginBottom: "2%",
    marginTop: 5,
    marginLeft: "2%",
    borderRadius: 15,
    transitionDuration: "0.3s",
  },
  eventImageInactive: {
    height: 220,
    width: "95%",
    borderRadius: 15,
    marginTop: 15,
    transitionDuration: "0.3s",
  },
  sliderContainer: {
    marginBottom: 30,
    marginTop: 10,
    paddingRight: 35,
    paddingTop: 30,
    height: 250,
    // backgroundColor: "rgba(255,255,255,1)"
  },
  slideTitle: {
    color: "#bd2132",
    width: "90%",
    marginLeft: 15,
    fontFamily: "NeulandGroteskCondensedBold",
    transitionDuration: "0.3s",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  slideDescription: {
    width: "90%",
    color: "white",
    fontFamily: "NeulandGroteskCondensedRegular",
    marginLeft: 15,
    transitionDuration: "0.3s",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  slideTitleInactive: {
    color: "white",
    fontSize: 11,
    fontFamily: "NeulandGroteskCondensedRegular",
    opacity: 0.5,
    marginTop: "13%",
    marginLeft: 25,
    transitionDuration: "0.3s",
  },
  slideDescriptionInactive: {
    color: "white",
    fontSize: 8,
    fontFamily: "NeulandGroteskCondensedRegular",
    opacity: 0.5,
    marginLeft: 25,
    transitionDuration: "0.3s",
  },
  pageIndex: {
    color: "white",
    fontSize: 16,
    fontFamily: "NeulandGroteskCondensedRegular",
  },
  activeSlideContainer: {
    // height: 250,
    // width: 250
    // backgroundColor: "red"
    // marginTop: 15,
    // marginLeft: 15
    // -moz-box-shadow: 13px 13px 0px 0px #FFFFFF;
    // -webkit-box-shadow: 13px 13px 0px 0px #FFFFFF;
  },
  SlideTextArea: {
    backgroundImage:
      "linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0.5),rgba(0,0,0,0.8),rgba(0,0,0,0.9), rgba(0,0,0,0.9), rgba(0,0,0,0.9), rgba(0,0,0,1));",
    position: "absolute",
    height: 60,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "flex-start",
    bottom: 0,
    // marginLeft: 5,
    // borderBottomRightRadius: 15,
    // borderBottomLeftRadius: 15,
    // backgroundColor: "red",
    paddingBottom: 5,
    width: 600,
  },
  shadowGradient: {
    backgroundImage:
      "linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0.7), rgba(0,0,0,0.8), rgba(0,0,0,0.9), rgba(255,255,255,1));",
    height: 50,
    width: 140,
    position: "absolute",
    bottom: 0,
  },
  dateIndexContainer: {
    position: "absolute",
    marginTop: 5,
    marginLeft: 15,
  },
  eventRow: {},
  datePanelcontainer: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    backgroundColor: "white",

    borderRadius: 10,
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      height: 60,
      width: 50,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      height: 100,
      width: 100,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      height: 65,
      width: 70,
    },
  },
  calendarDay: {
    textAlign: "center",
    color: "#bd2132",
    fontWeight: "bold",
    fontFamily: "NeulandGroteskCondensedRegular",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  calendarMonth: {
    marginTop: -5,
    textAlign: "center",
    fontWeight: "bold",
    fontFamily: "NeulandGroteskCondensedRegular",
    color: "black",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
  separator: {
    height: 1,
    width: "70%",
    backgroundColor: "black",
    marginBottom: 10,
    marginTop: 0,
  },
});
