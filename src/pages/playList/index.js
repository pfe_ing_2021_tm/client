/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Mon Apr 29 2019
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
import playlistImg from "../../assets/img/playlist.png";
import playlistImgDis from "../../assets/img/playlistdis.png";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import IcnPlay from "../../assets/img/playaudio.png";
import Icnoff from "../../assets/img/off.png";
import List from "./list";

const fakeData = [
  { id: 1, status: true },
  { id: 2, status: false },
  { id: 3, status: false },
  { id: 4, status: false }
];
class PlayList extends Component {
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };
  handleClickClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <div style={{ marginLeft: -24, marginRight: -24 }}>
        {fakeData.map(item => (
          <div
            style={{
              height: 150,
              margin: "1px 0",
              overflow: "hidden",
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-end"
            }}
            onClick={this.handleClickOpen}
          >
            <div className={classes.playlistDescripCont}>
              <div>
                <Typography component="p" className={classes.playListTitle}>
                  NEW YORK
                </Typography>
                <Typography component="p" className={classes.videoAuthor}>
                  Sed eu tortor in sevulput...
                </Typography>
              </div>
              {item.status ? (
                <img
                  src={IcnPlay}
                  alt="play"
                  className={classes.iconPlayAudio}
                />
              ) : (
                <img src={Icnoff} alt="play" className={classes.iconOff} />
              )}
            </div>
            {item.status ? (
              <img
                src={playlistImg}
                alt=""
                style={{
                  width: "100%",
                  position: "absolute",
                  height: 150,
                  overflow: "hidden"
                }}
              />
            ) : (
              <img
                src={playlistImgDis}
                alt=""
                style={{
                  width: "100%",
                  position: "absolute",
                  height: 150,
                  overflow: "hidden"
                }}
              />
            )}
          </div>
        ))}
        <List openList={this.state.open} onClose={this.handleClickClose} />
      </div>
    );
  }
}

PlayList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PlayList);
