/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Mon Apr 29 2019
 */
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import { styles } from './style';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Fakealbum from '../../assets/img/album.jpg';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class List extends React.Component {
  state = {
    open: false
  };

  handleClose = () => {
    this.props.onClose();
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          fullScreen
          open={this.props.openList}
          onClose={this.handleClose}
          TransitionComponent={Transition}
        >
          <div
            style={{
              background: '#000',
              height: '100%'
            }}
          >
            <div
              style={{
                height: 100,
                background: '#ED2A1C',
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
            >
              <div className={classes.videoProgressBoxDesc}>
                <img src={Fakealbum} alt="" className={classes.progressImg} />
                <div style={{ marginLeft: 20 }}>
                  <Typography component="p" className={classes.playVideoTitle}>
                    Hymn For The Weekend
                  </Typography>
                  <Typography component="p" className={classes.playVideoAuthor}>
                    Coldplay
                  </Typography>
                </div>
              </div>
              <IconButton
                color="inherit"
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon className={classes.videoCloseIcn} />
              </IconButton>
            </div>
            {/*List*/}
          </div>
        </Dialog>
      </div>
    );
  }
}

List.propTypes = {
  classes: PropTypes.object.isRequired,
  openList: PropTypes.bool.isRequired,
  onClose: PropTypes.func
};

export default withStyles(styles)(List);
