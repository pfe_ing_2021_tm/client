/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Mon Apr 22 2019
 */
import backgroundImg from '../../assets/img/background.jpg';

export const styles = theme => ({
  background: {
    background: `url(${backgroundImg})`,
    width: '100%',
    position: 'relative',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed'
  },
  sliderContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    alignContent: 'center'
  },
  sliderPoster: {
    width: '80%',
    marginBottom: -15
  },
  videoDescripCont: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '70%',
    alignItems: 'center',
    position: 'absolute'
  },

  videoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: '500',
    textTransform: 'uppercase'
  },
  videoAuthor: {
    color: theme.palette.secondary.main
  },
  playVideoAuthor: {
    color: '#999',
    fontSize: 13
  },
  iconPlay: {
    width: 35,
    color: theme.palette.secondary.main
  },
  btnNext: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 25,
    color: theme.palette.secondary.main,
    justifyContent: 'flex-end',
    width: '90%'
  },
  titleNext: {
    color: theme.palette.secondary.main
  },
  iconNext: {
    fontSize: 20,
    color: '#C70013',
    marginLeft: 10
  },
  bannerContainer: {
    width: '100%',
    justifyContent: 'center',
    display: 'flex',
    marginTop: 25
  },
  bannerItem: {
    display: 'flex',
    flexDirection: 'column'
  },
  bannerIndicator: {
    width: 10,
    margin: '2px 5px'
  },
  bannerCarousel: {
    display: 'flex',
    width: '80%'
  },
  playlistDescripCont: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '90%',
    alignItems: 'center',
    position: 'absolute',
    paddingBottom: 15,
    zIndex: 99
  },
  playListTitle: {
    color: theme.palette.secondary.main,
    fontSize: 30,
    fontWeight: '500',
    textTransform: 'uppercase'
  },
  playVideoTitle: {
    color: theme.palette.secondary.main,
    fontWeight: '500',
    textTransform: 'uppercase'
  },
  iconPlayAudio: {
    width: 30,
    color: theme.palette.secondary.main
  },
  iconOff: {
    width: 20,
    color: theme.palette.secondary.main
  },
  drivWhi: {
    position: 'relative',
    top: -2,
    fontSize: 10,
    color: theme.palette.secondary.main
  },
  galerieSmallItem: {
    height: 100,
    marginBottom: 8,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  progressBarCont: {
    background: 'rgba(000, 000, 000, 0.8)',
    height: 35,
    padding: '0 0px 40px 0px',
    position: 'absolute',
    bottom: 0,
    width: '100%'
  },
  progressLine: {
    width: '100%',
    height: 3,
    position: 'relative',
    top: -17
  },
  videoProgreesTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  videoProgressBoxDesc: {
    display: 'flex',
    alignItems: 'flex-start'
  },
  progressImg: {
    width: 50,
    position: 'relative',
    top: -8,
    left: 10
  },
  progressIcn: {
    color: theme.palette.secondary.main,
    position: 'relative',
    top: -10
  },
  videoCloseCont: {
    position: 'absolute',
    top: 10,
    background: '#000',
    borderRadius: 0,
    right: 0,
    padding: '5px 7px'
  },
  videoCloseIcn: {
    color: theme.palette.secondary.main,
    border: '1px solid #fff',
    borderRadius: 50,
    fontSize: 16
  }
});
