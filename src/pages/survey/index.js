/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-10-08 18:33:56
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-11 09:37:52
 */
import React, { Component } from "react";
import { Link } from "react-router-dom";

import backprofile from "../../assets/img/backprofile.png";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { getUserSurvey } from "../../store/actions";
import SurveyPopup from "./survey";
//import { red } from "@material-ui/core/colors";
import { history } from "../../history";
class Survey extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userSurvey: [],
    };
  }

  componentDidMount() {
    if (this.state.token === null) {
      return history.push("/");
    }
    this.props.getUserSurvey();
  }

  async componentWillReceiveProps(prevProps) {
    if (prevProps.userSurvey !== this.props.userSurvey) {
      let userSurvey = prevProps.userSurvey.filter(
        (item) => item._id === this.props.match.params.surveyid
      );
      this.setState({
        userSurvey: userSurvey,
      });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            paddingLeft: 40,
            marginTop: 25,
            width: "35%",
          }}
        >
          <Link
            to="/"
            style={{
              display: "contents",
            }}
          >
            <img
              src={backprofile}
              style={{ width: 35, marginRight: 15 }}
              alt=""
            />
            <p style={{ color: "#fff", fontSize: 18 }}>&nbsp;</p>
          </Link>
        </div>

        {!this.props.isloading && this.state.userSurvey.length === 0 && (
          <p
            className={classes.questionListe}
            style={{ fontSize: "30px", color: "#fff", margin: "25px" }}
          >
            Vous avez déjà répondu sur ce questionnaire
          </p>
        )}

        {this.state.userSurvey.length !== 0 &&
          this.state.userSurvey.map((item, index) => (
            <SurveyPopup
              key={index}
              data={item}
              index={index}
              count1={this.state.userSurvey.length}
              count={this.state.userSurvey.length}
            />
          ))}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isloading: state.ui.isLoading,
    userSurvey: state.userSurvey.userSurvey,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUserSurvey: () => dispatch(getUserSurvey()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Survey);
