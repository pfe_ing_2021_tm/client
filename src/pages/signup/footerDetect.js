/**
 * /*
 *
 * @format
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-06-12 15:07:28
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-12-04 12:11:46
 */

import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogContent from "@material-ui/core/DialogContent";
import { useSpring, animated } from "react-spring";
const FooterDetect = (props) => {
  const { classes } = props;
  const [opens, setOpens] = React.useState(false);

  const handleClose = () => {
    setOpens(false);
  };

  const handleOpen = () => {
    props.close();
    setOpens(true);
  };
  const [state, toggle] = useState(props.annimation);
  const { x } = useSpring({
    from: { x: 0 },
    x: props.annimation ? 1 : 0,
    config: { duration: 1000 },
  });

  return (
    <>
      <div>
        <animated.div
          style={{
            opacity: x.interpolate({ range: [0, 1], output: [0.8, 1] }),
            transform: x
              .interpolate({
                range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                output: [1, 0.97, 0.9, 1.1, 0.9, 1.1, 1.03, 1],
              })
              .interpolate((x) => `scale(${x})`),
          }}
        >
          <p className={classes.lienDetect} onClick={handleOpen}>
            pourquoi demandons-nous cela ?
          </p>
        </animated.div>
      </div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={opens}
        //open={true}
        classes={{ paperWidthSm: classes.dialog }}
      >
        <div>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            classes={{ root: classes.btnClose }}
            onClick={handleClose}
          >
            <CloseIcon onClick={handleClose} style={{ color: "#fff" }} />
          </IconButton>
        </div>

        <DialogContent dividers>
          <center>
            <p className={classes.titleCardP} style={{ marginTop: 20 }}>
              POURQUOI DOIS-JE AUTORISER L'ACCÈS À MA CAMÉRA ?
            </p>
            <p className={classes.etapsP}>
              Pour fonctionner correctement, l'analyse d'objets a besoin que la
              caméra de votre appareil détecte les objets autour de vous.
            </p>
            <p className={classes.titleCardP} style={{ marginTop: 20 }}>
              POURQUOI VÉRIFIONS-NOUS VOTRE ÂGE ?
            </p>
            <p className={classes.etapsP}>
              Nous sommes membre du groupe de sociétés Philip Morris
              International et le site Web ainsi que le contenu que vous êtes
              sur le point d'utiliser sont uniquement destinés à être utilisés
              et consultés par des fumeurs adultes confirmés. En conséquence,
              vous devez vous soumettre au processus de vérification de votre
              âge. Pour ce faire, vous devez prendre une photo de vous. Ce
              processus est automatisé et son seul objectif est de vérifier que
              vous êtes adulte. Si vous vous opposez à ce que nous procédions à
              cette vérification, il vous suffit de ne pas envoyer votre photo
              et de fermer votre navigateur. Veuillez noter que la disponibilité
              et le déroulement précis du processus de vérification de l'âge
              peuvent varier en fonction de votre pays d'origine (Tunisie) et de
              la législation. Nous traiterons vos données à caractère personnel
              conformément à notre Déclaration de confidentialité. Nous
              utilisons également des cookies conformément à notre Déclaration
              en matière de cookies disponible sur le site Web.
            </p>
            <p className={classes.titleCardP} style={{ marginTop: 20 }}>
              VÉRIFICATION FACIALE DE L'ÂGE
            </p>
            <p className={classes.etapsP}>
              Pour procéder à ce type de vérification, vous devez prendre une
              photo de vous.
              <br />
              Veuillez tenir compte des conditions suivantes :<br />
              Seulement un visage et une paire d'yeux (attention aux posters
              derrière vous)
              <br />
              Enlevez vos lunettes
              <br />
              Ne vous tenez pas devant une fenêtre
              <br />
              La taille et la qualité de la photo doivent être adéquates (pas
              trop sombre ni trop lumineuse, suffisamment nette et d'au moins 1
              000 x 1 000 pixels)
              <br />
              Ce que vous ne devez pas faire :<br />
              - demander à une tierce personne de se soumettre au processus de
              vérification de l'âge en votre nom ;<br />
              - utiliser les photos d'une autre personne ;<br />- utiliser les
              photos appartenant à quelqu'un d'autre ;{" "}
            </p>

            <div
              style={{
                width: "80%",
                height: 1,
                backgroundColor: "#ced4da",
                opacity: 0.5,
              }}
            />

            <div
              className={classes.btnDetect}
              onClick={handleClose}
              style={{
                width: 80,
                marginTop: 20,
                marginRight: 0,
                padding: 10,
              }}
            >
              j'ai compris
            </div>
          </center>
        </DialogContent>
      </Dialog>
      <div>
        <p
          className={classes.etaps}
          style={{
            fontSize: 15,
            letterSpacing: 1,
            padding: 10,
            paddingBottom: 50,
          }}
        >
          PRIERE DE NOTER QUE CETTE EXPERIENCE CONTIENT DES INFORMATIONS D'UN
          PRODUIT CONTENANT DU TABAC QUI EST RÉSERVÉ AUX FUMEURS QUI RÉSIDENTS
          EN TUNISIE ET ONT 18 ANS OU PLUS.
        </p>
      </div>
    </>
  );
};
export default withStyles(styles)(FooterDetect);
