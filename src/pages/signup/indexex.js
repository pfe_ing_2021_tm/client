/**
 * /*
 *
 * @format
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-06-12 09:15:45
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-10-27 13:12:01
 */

import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";
import clockAcs from "../../assets/img/clockAcs.png";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogContent from "@material-ui/core/DialogContent";
import light from "../../assets/img/light.png";
import account from "../../assets/img/account.png";
import shades from "../../assets/img/shades.png";
import check from "../../assets/img/checkACS.png";
import excla from "../../assets/img/exla.png";
import exclamation from "../../assets/img/exclamation-mark.png";
import FooterDetect from "./footerDetect";
import Webcam from "react-webcam";
import "./style.css";
import PopupWarrning from "./popupWarrning";
import Popper from "@material-ui/core/Popper";
import { ville } from "./data";
import Auto from "./auto";
import {
  registerToSF,
  registerToSFReset,
  registerToSFResetSucc,
  acsDetect,
  acsDetectReset,
} from "../../store/actions";
import moment from "moment";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { history } from "../../history";
import { useSpring, animated } from "react-spring";
function Alert(props) {
  return <MuiAlert elevation={6} variant='filled' {...props} />;
}

const Detect = (props) => {
  //
  const dispatch = useDispatch()
  const isLoading = useSelector((state) => state.ui.isLoading)
  const registrationFailACS = useSelector(
    (state) => state.registrationFailACS.registrationFailACS
  )
  const registerToSfSuccess = useSelector(
    (state) => state.registerToSf.registerToSf
  )
  const status = useSelector((state) => state.acsStatus.acsStatus)

  const [loading, setLoading] = React.useState(false)
  const [acsError, setAcsError] = React.useState(false)
  const [acsSuccess, setAcsSuccess] = React.useState(false)
  const [openUserExist, setOpenUserExist] = React.useState(false)

  useEffect(() => {
    //console.log("this status" , status)
    if (
      status === "error"
      //registrationFailACS && registrationFailACS.length > 0
    ) {
      handleClick()
      setOpenUserExist(false)
      //console.log("status error " , status)

      //   let test = registrationFailACS.split(":")
      //   if ( test[0] == "Validation error, Details" && test[2] == " 'username' " && test[1] == " 400003 Validation failed username already exists, Field name"
      //     ){
      //     setOpenUserExist(true)
      //   }
      //   else{
      //   setAcsError(registrationFailACS);
      //   }
      //   setAcsSuccess(null);
    } else if (
      status === "success"
      //registerToSfSuccess && registerToSfSuccess.length > 0
    ) {
      //console.log("status success " , status)
      handleOpenUserExist()
      // setAcsError(null);
      // handleClick();
      // setAcsSuccess(registerToSfSuccess);
      // setTimeout(() => {
      //   history.push("/login");
      // }, 1000);
    } else {
      setOpenUserExist(false)
    }
  }, [registrationFailACS, registerToSfSuccess])

  const afterCapture = () => {
    if (!failDetect) {
      setEtape(etape + 1)
    } else {
      setEtape(2)
      setImgSrc(null)
    }

    dispatch(acsDetectReset())
  }

  //
  const [open, setOpen] = React.useState(false)
  const [openCond, setOpenCond] = React.useState(false)
  const [index, setIndex] = React.useState(0)
  const [tentative, settentative] = React.useState(3)
  const [etape, setEtape] = React.useState(4) // default value etape 1 = 1
  const [failDetect, setFailDetect] = React.useState(false)
  const [emailValidation, setemailValidation] = React.useState(false)
  const [mobileValidation, setMobileValidation] = React.useState(false)
  const [openSnack, setOpenSnack] = React.useState(false)

  const [first_btn_click, setFirst_btn_click] = React.useState(false)
  const [annimationState, setAnnimationState] = React.useState(false)
  const [openCheckNumber, setOpenCheckNumber] = React.useState(false)

  // detection action ACS
  const detectionACS = useSelector((state) => state.detectionACS.detectionACS)
  const detectionFailACS = useSelector(
    (state) => state.detectionFailACS.detectionFailACS
  )

  useEffect(() => {
    if (
      Object.keys(props.match.params).length === 0 &&
      props.match.params.constructor === Object
    ) {
      if (detectionACS && detectionACS.length > 0 && detectionACS !== "") {
        setFailDetect(false)
        setEtape(3)
      } else if (
        detectionFailACS &&
        detectionFailACS.length > 0 &&
        detectionFailACS === "Image Not Recognized"
      ) {
        setIndex(2)
        setEtape(2)
        setImgSrc(null)
        dispatch(acsDetectReset())
        handleClickOpen(2)
      } else if (
        detectionFailACS &&
        detectionFailACS.length > 0 &&
        detectionFailACS !== ""
      ) {
        setFailDetect(true)
        setEtape(3)
        settentative(tentative - 1)
      }
    } else {
      fetch(
        `${process.env.REACT_APP_DOMAIN}/api/v1/signup/checkauthorization`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            authorisation: props.match.params.id.trim(),
          }),
        }
      )
        .catch((err) => {
          history.push("/signup")
        })
        .then((parsedRes) => {
          if (parsedRes.status === 200) {
            parsedRes.json().then((data) => {
              if (data && data.status === "success" && data.message === "ok") {
                setEtape(4)
              } else {
                history.push("/signup")
              }
            })
          } else {
            history.push("/signup")
          }
        })
    }
  }, [detectionACS, detectionFailACS])

  //

  const handleCloseCheckNumber = () => {
    setOpenCheckNumber(false)
  }

  const handleCloseUserExist = async () => {
    setOpenUserExist(false)
    //console.log("inside close" ,openUserExist )
  }

  const handleOpenUserExist = () => {
    setOpenUserExist(true)
  }

  const handleCloseRegistreUser = async () => {
    dispatch(registerToSFReset())
    dispatch(registerToSFResetSucc())
    //console.log("closed ")
    await handleCloseUserExist()
    setOpenUserExist(false)
    //console.log( openUserExist )
    //window.location.href = 'https://discovered.chifco.com/';
    history.push("/login")
  }

  const handleOpenCheckNumber = () => {
    setOpenCheckNumber(true)
  }

  const handleClick = () => {
    setOpenSnack(true)
  }
  const handleCloseSnack = (event, reason) => {
    if (reason === "clickaway") {
      return
    }
    dispatch(registerToSFReset())
    dispatch(registerToSFResetSucc())
    setOpenSnack(false)
    setOpenUserExist(false)
  }

  useEffect(() => {
    if (etape === 2) {
      setOpenCond(true)
    }
  }, [etape])
  const handleClickOpen = (v) => {
    setIndex(v)
    setOpen(true)
  }
  const handleCloseCond = () => {
    setOpenCond(false)
  }
  const handleClose = () => {
    setOpen(false)
  }

  useEffect(() => {
    setLoading(isLoading)
    if (isLoading) {
    } else {
      handleCloseSnack()
    }
  }, [isLoading])
  useEffect(() => {
    setLoading(false)
  }, [])

  const warrning = [
    {
      title: "DÉSOLÉ",
      desc: "Le contenu de ce site est strictement réservé aux adultes fumeurs de plus de 18 ans.",
    },
    {
      title: "L'IDENTIFICATION A ECHOUEE",
      desc: "Tu sembles toujours naussi jeune! Pour t'enregistrer avec nous redirige toi au plus vite vers nos hotesses Marlboro dans les points de vente le plus proches",
    },
    {
      title: "DÉSOLÉ",
      desc: "image non autorisée réessayez",
    },
  ]
  const closefn = () => {
    setOpen(false)
  }
  const webcamRef = React.useRef(null)
  const [imgSrc, setImgSrc] = React.useState(null)
  const [flashEffect, setFlashEffect] = React.useState(false)

  const { classes } = props
  const capture = React.useCallback(() => {
    setFlashEffect(true)
    const imageSrc = webcamRef.current.getScreenshot()

    setImgSrc(imageSrc)
    let data = {
      img: imageSrc,
    }
    dispatch(acsDetect(data))
  }, [webcamRef, setImgSrc])

  useEffect(() => {
    setFlashEffect(false)
  }, [flashEffect])
  const stopCapture = () => {
    setImgSrc(null)
    setFlashEffect(false)
    dispatch(acsDetectReset())
  }

  const success = () => {
    setEtape(3)
  }
  const echec = () => {
    setEtape(3)
    setFailDetect(true)
  }
  useEffect(() => {}, [])
  const [values, setValues] = React.useState({
    name: "",
    prenom: "",
    mobile: "",
    email: "",
    date: "",
  })

  const handleChange = (prop) => (event) => {
    if (prop === "email") {
      setemailValidation(false)
    }
    if (prop === "mobile") {
      setMobileValidation(false)
    }
    setValues({ ...values, [prop]: event.target.value })
  }
  const PopperMy = function (props) {
    return (
      <Popper {...props} className={classes.popper} placement="bottom-end" />
    )
  }
  const [valuest, setValuest] = useState({
    villeValue: "",
    genderValue: "",
    marcPref: "",
    marcAlter: "",
    operTel: "",
  })

  const returnValue = (v, x) => {
    if (x === "Ville") {
      setValuest({ ...valuest, villeValue: v })
    } else if (x === "Gender") {
      if (v === "Homme" || v === "Femme") {
        setValuest({ ...valuest, genderValue: v === "Homme" ? "M" : "F" })
      } else {
        setValuest({ ...valuest, genderValue: "" })
      }
    } else if (x === "La marque que tu prefere fumer") {
      setValuest({ ...valuest, marcPref: v })
    } else if (x === "La marque alternative") {
      setValuest({ ...valuest, marcAlter: v })
    } else if (x === "Operateur telephonique") {
      setValuest({ ...valuest, operTel: v })
    }
  }

  const gender = [{ name: "Homme" }, { name: "Femme" }]
  const marquePrefere = [
    { name: "Marlboro Red" },
    { name: "Marlboro Gold" },
    { name: "Marlboro Touch" },
    { name: "Merit Classic Filter " },
    { name: "Merit Recessed Blue" },
  ]
  const marqueAlternative = [
    { name: "Marlboro Red" },
    { name: "Marlboro Gold" },
    { name: "Marlboro Touch" },
    { name: "Merit Classic Filter " },
    { name: "Merit Recessed Blue" },
  ]
  const operateur = [
    { name: "Tunisie Telecom" },
    { name: "Ooredoo" },
    { name: "Orange" },
  ]

  const handleSubmit = (e) => {
    let data = {
      Body: {
        sObject: {
          "@type": "sf:Contact",
          Country__c: "TN",
          Email: values.email,
          FirstName: values.name,
          LastName: values.prenom,
          MobilePhone: `+216${values.mobile}`,
          DateOfBirth: values.date,
          Gender: valuest.genderValue,
          City: valuest.villeValue,
          District: valuest.villeValue,
          Telecom: valuest.operTel,
          PrimaryBrand: valuest.marcPref,
          AlternativeBrand: valuest.marcAlter,
          DateOfReg: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
          VerificationSource: "ACS",
          SelfRegistration: "True",
          PreferredLanguage: "EN",
          "Retailer ID": "420",
          "Device ID": "240",
          "Age Check": "True",
        },
      },
    }
    if (!emailValidation && !mobileValidation) {
      dispatch(registerToSF(data))
    }

    e.preventDefault()
  }
  const close = () => {
    setFirst_btn_click(true)
  }

  const returnReinitialiserPassword = () => {
    window.location.href = "https://cloud.pmiclicks.com/login"
  }

  const annimation = () => {
    setAnnimationState(!annimationState)
  }
  const returnLogin = () => {
    history.push("/login")
  }

  const verifyPhoneNumber = () => {
    if (values.mobile.toString().length !== 8) {
      setMobileValidation(true)
    } else {
      setMobileValidation(false)
      fetch(`${process.env.REACT_APP_DOMAIN}/api/v1/customer/verifyPhoneNum`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phone: values.mobile.toString().trim(),
        }),
      })
        .catch((err) => {})
        .then((res) => res.json())
        .then((parsedRes) => {
          if (parsedRes.status === "success") {
            handleOpenCheckNumber()
          } else {
          }
        })
    }
  }

  return (
    <>
      <div className={" stars "}>
        <Dialog
          onClose={handleCloseCheckNumber}
          aria-labelledby="customized-dialog-title"
          open={openCheckNumber}
          classes={{ paperWidthSm: classes.PopUpDialog }}
        >
          <DialogContent dividers>
            <center>
              <img src={exclamation} style={{ marginTop: -8 }} />
              <p className={classes.PopUpTitleCardP} style={{ marginTop: 20 }}>
                CE NUMÉRO EXISTE DÉJÀ
              </p>

              <div
                className={classes.btnDetect}
                onClick={returnLogin}
                style={{
                  width: 170,
                  marginTop: 20,
                  marginRight: 0,
                  padding: 10,
                }}
              >
                S'IDENTIFIER
              </div>

              <div
                className={classes.btnDetect}
                onClick={returnReinitialiserPassword}
                style={{
                  width: 170,
                  marginTop: 20,
                  marginRight: 0,
                  padding: 10,
                }}
              >
                REINITIALISER MON MOT DE PASSE
              </div>

              <div
                className={classes.btnDetect}
                onClick={handleCloseCheckNumber}
                style={{
                  width: 170,
                  marginTop: 20,
                  marginRight: 0,
                  padding: 10,
                }}
              >
                CONTINUER L'INSCRIPTION
              </div>
            </center>
          </DialogContent>
        </Dialog>

        <Dialog
          onClose={handleCloseRegistreUser}
          aria-labelledby="customized-dialog-title"
          open={openUserExist}
          classes={{ paperWidthSm: classes.PopUpDialog }}
        >
          <div>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              classes={{ root: classes.btnClose }}
              onClick={handleCloseRegistreUser}
            >
              <CloseIcon
                onClick={handleCloseRegistreUser}
                style={{ color: "#000000" }}
              />
            </IconButton>
          </div>
          <DialogContent dividers>
            <center>
              <p className={classes.PopUpTitleCardP} style={{ marginTop: 20 }}>
                Votre contact est bien enregistré.
              </p>
              <p className={classes.PopUpTitleCardP}>
                Un Sms vous sera envoyé pour créer votre mot de passe.
              </p>
            </center>
          </DialogContent>
        </Dialog>

        <center className={"twinkling"}>
          {etape === 1 && (
            <>
              <div>
                <p className={classes.etaps}>ÉTAPE 2</p>
                <p className={classes.titleDetect}>
                  VÉRIFIONS D'ABORD VOTRE ÂGE
                </p>
              </div>
              <div className={classes.card}>
                <img src={clockAcs} style={{ width: 70 }} />
                <p className={classes.textCard}>
                  Tout d'abord, veuillez confirmer que
                </p>
                <p className={classes.etape2textCard}>
                  vous êtes bien un fumeur adulte
                </p>
                <p className={classes.etape2textCard}>de plus de 23 ans</p>
              </div>
              <div className={classes.btnContainer}>
                <div
                  onClick={() =>
                    first_btn_click ? setEtape(etape + 1) : annimation()
                  }
                  className={classes.btnDetect}
                  style={{
                    marginBottom: 20,
                    filter: !first_btn_click ? "grayscale(1)" : "grayscale(0)",
                  }}
                >
                  ✔ OUI JE SUIS UN FUMEUR ET J'AI PLUS DE 23 ANS
                </div>
                <div
                  className={classes.labelInput}
                  onClick={() => handleClickOpen(0)}
                  style={{ fontSize: 15 }}
                >
                  NON JE NE SUIS PAS FUMEUR ET J'AI MOINS DE 23 ANS
                </div>
              </div>
            </>
          )}
          {etape === 2 && (
            <>
              <div>
                <p className={classes.etaps}>ÉTAPE 3</p>
              </div>

              <div className={classes.photo}>
                {imgSrc ? (
                  <img src={imgSrc} className={classes.picTaken} />
                ) : (
                  <Webcam
                    audio={false}
                    ref={webcamRef}
                    screenshotFormat="image/jpeg"
                    style={{
                      width: 280,
                      height: 303,
                      marginTop: -10,
                      position: "relative",
                      right: 0,
                      transform: "scaleX(-1)",
                    }}
                  />
                )}

                <div
                  className={!flashEffect ? "elementToFadeInAndOut" : ""}
                  style={{
                    backgroundColor: "white",
                    width: 300,
                    height: 300,
                    position: "absolute",
                    top: "1px",
                  }}
                />
              </div>
              <p className={classes.titleDetect}>
                C'EST LE MOMENT DE VOUS PRENDRE EN PHOTO !
              </p>

              <p className={classes.subTitleDetect}>
                Vérifiez votre âge en vous prenant en photo
              </p>
              {imgSrc ? (
                <div style={{ marginTop: -60 }}>
                  <p
                    onClick={stopCapture}
                    style={{
                      fontSize: 30,
                      color: "#fff",
                      zIndex: 9,
                      margin: "auto",
                      position: "relative",
                      top: 80,
                    }}
                  >
                    X
                  </p>
                  <div
                    className={[classes.press].join(" ")}
                    onClick={() => stopCapture}
                    style={{ overflow: "hidden", backgroundColor: "#6f6f6f" }}
                  >
                    <div className="loading"></div>
                  </div>
                  <p className={classes.subpress}>ANNULER</p>
                </div>
              ) : (
                <>
                  <div className={classes.press} onClick={capture}></div>
                  <p className={classes.subpress}> APPUYEZ </p>
                </>
              )}
            </>
          )}
          {etape === 3 && (
            <>
              <div>
                <p className={classes.etaps}>ÉTAPE 4</p>
              </div>
              <div className={classes.photo}>
                <img
                  src={imgSrc}
                  className={classes.picTaken}
                  alt="img avatar"
                  style={{ filter: failDetect ? "brightness(0.5)" : "" }}
                />
                {!failDetect ? (
                  <img
                    src={check}
                    alt="check"
                    style={{
                      width: 80,
                      position: "absolute",
                      top: 65,
                      margin: "auto",
                    }}
                  />
                ) : (
                  <img
                    src={excla}
                    alt="check"
                    style={{
                      width: 80,
                      position: "absolute",
                      top: 65,
                      margin: "auto",
                    }}
                  />
                )}
              </div>
              {failDetect ? (
                <p
                  className={classes.titleDetect}
                  style={{ marginTop: 0, lineHeight: 0.9 }}
                >
                  WOW,VOUS SEMBLEZ TROP <br /> JEUNE !
                </p>
              ) : (
                <p
                  className={classes.titleDetect}
                  style={{ marginTop: 0, lineHeight: 0.9 }}
                >
                  VÉRIFICATION <br /> RÉUSSIE !
                </p>
              )}
              {!failDetect ? (
                <p
                  className={classes.subTitleDetect}
                  style={{ marginTop: -20, marginBottom: 30 }}
                >
                  Vérifiez votre âge en vous prenant en photo
                </p>
              ) : (
                <p
                  className={classes.subTitleDetect}
                  style={{ marginTop: -20, marginBottom: 30 }}
                >
                  Veuillez réessayer en prenant une photo avec une <br />
                  meilleure luminosité
                </p>
              )}
              {tentative !== 0 ? (
                <div
                  onClick={() => afterCapture()}
                  className={classes.btnDetect}
                  style={{ marginBottom: 20, width: !failDetect ? 100 : 250 }}
                >
                  {!failDetect
                    ? "SUIVANT"
                    : `${tentative} TENTATIVE(S) RESTANTE(S)-RÉESSAYEZ!`}
                </div>
              ) : (
                <>
                  <div
                    // onClick={() => afterCapture()}
                    className={classes.btnDetect}
                    style={{
                      marginBottom: 20,
                      width: !failDetect ? 100 : 250,
                      filter: "grayscale(1)",
                    }}
                  >
                    Veuillez réessayer ultérieurement
                  </div>
                </>
              )}
            </>
          )}
          {etape === 4 && (
            <>
              <div>
                <p className={classes.etaps}>ÉTAPE 5</p>
              </div>
              <p className={classes.titleDetect}>
                DERNIÈRE ÉTAPE <br /> POUR ADHÉRER
              </p>
              <div>
                <form
                  autocomplete="off"
                  onSubmit={handleSubmit}
                  style={{ display: "flex", flexDirection: "column" }}
                >
                  <TextField
                    autocomplete="false"
                    required
                    id="Nom"
                    variant="filled"
                    placeholder="Nom* "
                    className={classes.textField}
                    value={values.name}
                    onChange={handleChange("name")}
                    margin="normal"
                    InputProps={{
                      classes: {
                        underline: classes.input,
                        input: classes.textInputf,
                      },
                      style: {
                        color: "white",
                        borderBottom: "1px solid",
                      },
                    }}
                    InputLabelProps={{
                      classes: {
                        root: classes.input,
                        focused: classes.input,
                      },
                    }}
                  />
                  <TextField
                    autocomplete="false"
                    required
                    id="Prenom"
                    variant="filled"
                    placeholder="Prenom*"
                    className={classes.textField}
                    value={values.prenom}
                    onChange={handleChange("prenom")}
                    margin="normal"
                    InputProps={{
                      classes: {
                        underline: classes.input,
                        input: classes.textInputf,
                      },
                      style: {
                        color: "white",
                        borderBottom: "1px solid",
                      },
                    }}
                    InputLabelProps={{
                      classes: {
                        root: classes.input,
                        focused: classes.input,
                      },
                    }}
                  />
                  <TextField
                    autocomplete="false"
                    required
                    id="mobile"
                    placeholder="Numéro de téléphone*"
                    className={classes.textField}
                    variant="filled"
                    margin="normal"
                    value={values.mobile}
                    onInput={(e) => {
                      if (!e.target.value) {
                        e.target.value = ""
                      } else {
                        if (parseInt(e.target.value)) {
                          e.target.value = Math.max(0, parseInt(e.target.value))
                            .toString()
                            .slice(0, 8)
                        } else {
                          e.target.value = ""
                        }
                      }
                    }}
                    onBlur={() => {
                      verifyPhoneNumber()
                    }}
                    onChange={handleChange("mobile")}
                    InputProps={{
                      className: "digitsOnly",
                      classes: {
                        underline: classes.input,
                        input: mobileValidation
                          ? classes.validation
                          : classes.textInputf,
                      },
                      style: {
                        color: "white",
                        borderBottom: "1px solid",
                      },
                    }}
                    InputLabelProps={{
                      classes: {
                        root: classes.input,
                        focused: classes.input,
                      },
                    }}
                  />
                  {mobileValidation && (
                    <div style={{ position: "relative" }}>
                      <p
                        className={classes.validation}
                        style={{ position: "absolute", top: -20, right: 35 }}
                      >
                        le numéro de téléphone doit être composé de 8 chiffres !
                      </p>
                    </div>
                  )}

                  <TextField
                    autocomplete="false"
                    required
                    id="email"
                    variant="filled"
                    placeholder="Email*"
                    onBlur={() => {
                      !/.+@.+\.[A-Za-z]+$/.test(values.email)
                        ? setemailValidation(true)
                        : setemailValidation(false)
                    }}
                    className={classes.textField}
                    value={values.email}
                    onChange={handleChange("email")}
                    margin="normal"
                    InputProps={{
                      classes: {
                        underline: classes.input,
                        input: emailValidation
                          ? classes.validation
                          : classes.textInputf,
                      },
                      style: {
                        color: emailValidation ? "white" : "red",
                        borderBottom: "1px solid",
                      },
                    }}
                    InputLabelProps={{
                      classes: {
                        root: classes.input,
                        focused: classes.input,
                      },
                    }}
                  />
                  {emailValidation && (
                    <div style={{ position: "relative" }}>
                      <p
                        className={classes.validation}
                        style={{ position: "absolute", top: -20, right: 35 }}
                      >
                        email invalide !
                      </p>
                    </div>
                  )}

                  <TextField
                    autocomplete="false"
                    required
                    variant="filled"
                    id="date"
                    type="date"
                    label={values.date !== "" ? "" : "Date de naissance*"}
                    onClick={() => {
                      setValues({ ...values, date: " ." })
                    }}
                    onFocus={() => {
                      setValues({ ...values, date: " ." })
                    }}
                    onChange={handleChange("date")}
                    className={classes.textField}
                    InputProps={{
                      classes: {
                        underline: classes.input,
                        input:
                          values.date !== ""
                            ? classes.textInputf
                            : classes.textInputfBlack,
                      },
                      style: {
                        color: "white",
                        borderBottom: "1px solid",
                      },
                    }}
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.inputDate,
                        focused: classes.input,
                      },
                    }}
                  />

                  <div style={{ position: "relative" }}>
                    <Auto
                      id={"Sexe*"}
                      data={gender}
                      returnValue={returnValue.bind(this)}
                    />
                    <Auto
                      id={"Opérateur téléphonique*"}
                      data={operateur}
                      returnValue={returnValue.bind(this)}
                    />
                    <Auto
                      id={"Ville*"}
                      data={ville}
                      returnValue={returnValue.bind(this)}
                    />
                    <Auto
                      id={"La marque fumée*"}
                      data={marquePrefere}
                      returnValue={returnValue.bind(this)}
                    />
                    <Auto
                      id={"La marque alternative*"}
                      data={marqueAlternative}
                      returnValue={returnValue.bind(this)}
                    />
                  </div>
                  <center>
                    {loading ? (
                      <CircularProgress
                        style={{ margin: "20px 0px" }}
                        disableShrink
                        color="secondary"
                      />
                    ) : (
                      <button
                        //onClick={() => setEtape(etape + 1)}
                        className={classes.btnDetect}
                        style={{ margin: "20px 0px", width: 150 }}
                        type="submit"
                      >
                        S'ENREGISTRER
                      </button>
                    )}
                  </center>
                </form>
              </div>
            </>
          )}
          <div>
            <FooterDetect close={close} annimation={annimationState} />
            <div>
              {open && (
                <PopupWarrning
                  data={warrning[index]}
                  open={open}
                  close={closefn}
                />
              )}
            </div>
          </div>
        </center>
        <p
          style={{
            zIndex: 1300,
            zIndex: "1300",
            position: "fixed",
            bottom: "0",
          }}
          className={["footer"].join(" ")}
        >
          avis important : fumer nuit à la santé
        </p>
        <div>
          <Dialog
            onClose={handleCloseCond}
            aria-labelledby="customized-dialog-title"
            open={openCond}
            //open={true}
            classes={{ paperWidthSm: classes.dialog }}
          >
            <div>
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                classes={{ root: classes.btnClose }}
                onClick={handleCloseCond}
              >
                <CloseIcon
                  onClick={handleCloseCond}
                  style={{ color: "#fff" }}
                />
              </IconButton>
            </div>

            <DialogContent dividers>
              <center>
                <p className={classes.titleCard} style={{ marginTop: 20 }}>
                  MERCI DE SUIVRE LES
                  <br /> INSTRUCTIONS SUIVANTES
                </p>

                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-around",
                    alignItems: "center",
                    marginBottom: 20,
                  }}
                >
                  <div className={classes.rec}>
                    <img src={light} />
                    <p className={classes.textCard}>
                      BONNE <br /> LUMIÉRE
                    </p>
                  </div>
                  <div className={classes.rec}>
                    <img src={account} />
                    <p className={classes.textCard}>
                      VOTRE VISAGE <br /> SEULEMENT
                    </p>
                  </div>
                  <div className={classes.rec}>
                    <img src={shades} />
                    <p className={classes.textCard} style={{ marginTop: 26 }}>
                      PAS DE <br /> LUNETTES
                    </p>
                  </div>
                </div>
                <p className={classes.textCard}>
                  CETTE PHOTO NE SERA EN ANCUN CAS ENREGISTRÉE
                </p>
                <div
                  style={{
                    width: "80%",
                    height: 1,
                    backgroundColor: "#ced4da",
                    opacity: 0.5,
                  }}
                />

                <div
                  className={classes.btnDetect}
                  onClick={handleCloseCond}
                  style={{
                    width: 80,
                    marginTop: 20,
                    marginRight: 0,
                    padding: 10,
                  }}
                >
                  j'ai compris
                </div>
              </center>
            </DialogContent>
          </Dialog>
          <Snackbar
            open={openSnack}
            autoHideDuration={6000}
            onClose={handleCloseSnack}
          >
            <Alert
              onClose={handleCloseSnack}
              severity={
                "error"
                // registrationFailACS ? "error" : acsSuccess ? "success" : ""
              }
            >
              {" "}
              erreur, veuillez réessayer
              {/* {acsError || acsSuccess} */}
            </Alert>
          </Snackbar>
        </div>
      </div>
    </>
  )
};

export default withStyles(styles)(Detect);
