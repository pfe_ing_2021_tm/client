import React from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";

const ErrroMessage = ({ error, classes }) => {
  if (error) {
    switch (error.type) {
      case "required":
        return <p className={classes.err}> champ obligatoire</p>;
      case "minLength":
        return (
          <p className={classes.err}>
            Your last name need minmium 2 charcaters
          </p>
        );
      case "pattern":
        return <p className={classes.err}>Enter a valid email address</p>;
      case "number":
        return (
          <p className={classes.err}>
            Numéro de Téléphone doit être composé de 8 chiffres
          </p>
        );

      case "validate":
        return (
          <p className={classes.err}>
            Numéro de Téléphone doit être composé de 8 chiffres
          </p>
        );
      case "maxLength":
        return <p className={classes.err}>max length</p>;
      default:
        return null;
    }
  }

  return null;
};
export default withStyles(styles)(ErrroMessage);
