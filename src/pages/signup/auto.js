import React, { useState, useEffect } from "react";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
const Auto = (props) => {
  const { classes } = props;
  const [display, setDisplay] = useState(false);
  const [options, setOptions] = useState([]);
  const [search, setSearch] = useState("");
  const wrapperRef = React.useRef(null);

  useEffect(() => {
    window.addEventListener("mousedown", handleClickOutside);
    return () => {
      window.removeEventListener("mousedown", handleClickOutside);
    };
  });

  const handleClickOutside = (event) => {
    const { current: wrap } = wrapperRef;
    if (wrap && !wrap.contains(event.target)) {
      setDisplay(false);
    }
  };

  const updatePokeDex = (poke) => {
    setSearch(poke);
    setDisplay(false);
  };

  useEffect(() => {
    props.returnValue(search, props.id);
  }, [search]);

  return (
    <div ref={wrapperRef} className="flex-container flex-column pos-rel">
      <TextField
        // onClick={() => setDisplay(!display)}
        onFocus={() => setDisplay(!display)}
        autoComplete="off"
        id={props.id}
        variant="filled"
        type="search"
        placeholder={props.id}
        className={classes.textFieldAuto}
        value={search}
        onChange={(event) => setSearch(event.target.value)}
        margin="normal"
        required
        InputProps={{
          classes: {
            underline: classes.input,
            input: classes.textInputf,
          },
          style: {
            color: "white",
            borderBottom: "1px solid",
            width: "100%",
          },
        }}
        InputLabelProps={{
          classes: {
            root: classes.input,
            focused: classes.input,
          },
        }}
      />

      {display && (
        <div className={classes.autoContainer}>
          {props.data
            .filter(
              ({ name }) =>
                name.toLowerCase().indexOf(search.toLowerCase()) > -1
            )
            .map((value, i) => {
              return (
                <div
                  onClick={() => updatePokeDex(value.name)}
                  className={classes.option}
                  key={i}
                  tabIndex="0"
                >
                  <span>{value.name}</span>
                </div>
              )
            })}
        </div>
      )}
    </div>
  )
};
export default withStyles(styles)(Auto);
