/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-06-12 15:07:28
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-12 16:37:08
 */

import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { styles } from "./style";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import excla from "../../assets/img/icone-permission.png";
const PopupWarrning = (props) => {
  const [opens, setOpens] = React.useState(props.open);

  const handleClose = () => {
    setOpens(false);
    props.close();
  };
  const { classes } = props;
  return (
    <>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={opens}
        classes={{ paperWidthSm: classes.dialog }}
      >
        <div>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            classes={{ root: classes.btnClose }}
            onClick={handleClose}
          >
            <CloseIcon onClick={handleClose} style={{ color: "#fff" }} />
          </IconButton>
        </div>

        <DialogContent dividers>
          <center>
            <img src={excla} style={{ marginTop: 25 }} />
            <p className={classes.titleCard}>{props.title} </p>
            <p className={classes.textCard} style={{ width: "75%" }}>
              {props.desc}
            </p>
            <div
              style={{
                width: "80%",
                height: 1,
                backgroundColor: "#ced4da",
                opacity: 0.5,
              }}
            />

            <div
              className={classes.btnDetect}
              onClick={handleClose}
              style={{
                width: 80,
                marginTop: 20,
                marginRight: 0,
                padding: 10,
              }}
            >
              j'ai compris
            </div>
          </center>
        </DialogContent>
      </Dialog>
    </>
  )
};
export default withStyles(styles)(PopupWarrning);
