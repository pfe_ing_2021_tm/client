/*
 * Author: <> with ❤ tarek.mdemegh
 * -----
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 * -----
 * Last Modified: Thursday, 1st July 2021 10:01:12 am
 * Modified By: tarek.mdemegh
 * -----
 * Copyright , Chifco - 2021
 */
import React, { useState, useEffect } from "react"
import { TextField } from "@material-ui/core"
import Auto from "./auto"
import moment from "moment"
import CircularProgress from "@material-ui/core/CircularProgress"
import { ville } from "./data"
import { useDispatch, useSelector } from "react-redux"
import { useTheme, withStyles } from "@material-ui/styles"
import { styles } from "./style"
import { register, resetRegistredino } from "../../store/actions"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import { history } from "../../history"
import PopupWarrning from "./popupWarrning"
const Signup = (props) => {
  const { classes } = props
  const [emailValidation, setemailValidation] = React.useState(false)
  const [mobileValidation, setMobileValidation] = React.useState(false)
  const [passwordValidation, setPasswordValidation] = React.useState(false)
  const [open, setOpen] = React.useState(false)

  const [loading, setLoading] = React.useState(false)
  const [values, setValues] = React.useState({
    name: "",
    prenom: "",
    mobile: "",
    email: "",
    date: "",
    password: "",
  })
  const [valuest, setValuest] = useState({
    villeValue: "",
    genderValue: "",
    marcPref: "",
    marcAlter: "",
    operTel: "",
  })
  const theme = useTheme()
  const dispatch = useDispatch()
  const gender = [{ name: "Homme" }, { name: "Femme" }]
  const marquePrefere = [
    { name: "Marlboro Red" },
    { name: "Marlboro Gold" },
    { name: "Marlboro Touch" },
    { name: "Merit Classic Filter " },
    { name: "Merit Recessed Blue" },
  ]
  const marqueAlternative = [
    { name: "Marlboro Red" },
    { name: "Marlboro Gold" },
    { name: "Marlboro Touch" },
    { name: "Merit Classic Filter " },
    { name: "Merit Recessed Blue" },
  ]
  const operateur = [
    { name: "Tunisie Telecom" },
    { name: "Ooredoo" },
    { name: "Orange" },
  ]
  useEffect(() => {
    dispatch(resetRegistredino())
  }, [])
  useEffect(() => {
    if (values.password.toString().length === 0) setPasswordValidation(false)
  }, [values.password])
  const handleChange = (prop) => (event) => {
    setemailValidation(false)
    setMobileValidation(false)
    setPasswordValidation(false)
    if (prop === "email") {
      setemailValidation(false)
    }
    if (prop === "mobile") {
      setMobileValidation(false)
    }
    setValues({ ...values, [prop]: event.target.value })
    setPasswordValidation(false)
  }
  const registerReducer = useSelector(
    (state) => state.registerReducer.registerReducer
  )

  console.log("registerReducer", registerReducer)
  const closefn = () => {
    setOpen(false)
    history.push("/")
  }

  useEffect(() => {
    if (registerReducer && registerReducer.status === false) {
      setOpen(true)
    } else if (registerReducer && registerReducer.status) {
      setOpen(true)
    } else {
      dispatch(resetRegistredino())
      setOpen(false)
    }
  }, [registerReducer])
  const handleSubmit = (e) => {
    let data = {
      Email: values.email,
      FirstName: values.name,
      LastName: values.prenom,
      MobilePhone: `${values.mobile}`,
      DateOfBirth: values.date,
      Gender: valuest.genderValue,
      City: valuest.villeValue,
      password: values.password,
    }
    if (!emailValidation && !mobileValidation && !passwordValidation) {
      dispatch(register(data))
    }

    e.preventDefault()
  }

  const returnValue = (v, x) => {
    if (x === "Ville*") {
      setValuest({ ...valuest, villeValue: v })
    } else if (x === "Sexe*") {
      if (v === "Homme" || v === "Femme") {
        setValuest({ ...valuest, genderValue: v === "Homme" ? "M" : "F" })
      } else {
        setValuest({ ...valuest, genderValue: "" })
      }
    }
  }
  const verifyPhoneNumber = () => {
    if (values.mobile.toString().length !== 8) {
      setMobileValidation(true)
    } else {
      setMobileValidation(false)
    }
    if (values.mobile.toString().length === 0) {
      setMobileValidation(false)
    }
  }
  return (
    <>
      {open && (
        <PopupWarrning
          title={registerReducer && registerReducer.title}
          desc={registerReducer && registerReducer.message}
          open={open}
          close={closefn}
        />
      )}
      <div>
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          classes={{ root: classes.btnClose }}
          onClick={() => {
            history.push("/login")
          }}
        >
          <CloseIcon
            onClick={() => {
              history.push("/login")
            }}
            style={{ color: "#fff" }}
          />
        </IconButton>
      </div>

      <p
        className={classes.titleDetect}
        style={{
          color: theme.palette.secondary.main,
        }}
      >
        inscrivez-vous
      </p>
      <div>
        <form
          onSubmit={handleSubmit}
          style={{ display: "flex", flexDirection: "column" }}
          autocomplete="off"
        >
          <TextField
            autocomplete="false"
            required
            id="Nom"
            variant="filled"
            placeholder="Nom* "
            className={classes.textField}
            value={values.name}
            onChange={handleChange("name")}
            margin="normal"
            InputProps={{
              classes: {
                underline: classes.input,
                input: classes.textInputf,
              },
              style: {
                color: "white",
                borderBottom: "1px solid",
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.input,
                focused: classes.input,
              },
            }}
          />
          <TextField
            autocomplete="false"
            required
            id="Prenom"
            variant="filled"
            placeholder="Prenom*"
            className={classes.textField}
            value={values.prenom}
            onChange={handleChange("prenom")}
            margin="normal"
            InputProps={{
              classes: {
                underline: classes.input,
                input: classes.textInputf,
              },
              style: {
                color: "white",
                borderBottom: "1px solid",
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.input,
                focused: classes.input,
              },
            }}
          />
          <TextField
            autocomplete="false"
            required
            id="mobile"
            placeholder="Numéro de téléphone*"
            className={classes.textField}
            variant="filled"
            margin="normal"
            value={values.mobile}
            onInput={(e) => {
              if (!e.target.value) {
                e.target.value = ""
              } else {
                if (parseInt(e.target.value)) {
                  e.target.value = Math.max(0, parseInt(e.target.value))
                    .toString()
                    .slice(0, 8)
                } else {
                  e.target.value = ""
                }
              }
            }}
            onBlur={() => {
              verifyPhoneNumber()
            }}
            onChange={handleChange("mobile")}
            InputProps={{
              className: "digitsOnly",
              classes: {
                underline: classes.input,
                input: mobileValidation
                  ? classes.validation
                  : classes.textInputf,
              },
              style: {
                color: "white",
                borderBottom: "1px solid",
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.input,
                focused: classes.input,
              },
            }}
          />
          {mobileValidation && (
            <div style={{ position: "relative" }}>
              <p
                className={classes.validation}
                style={{
                  position: "absolute",
                  top: -20,
                  right: 35,
                  fontSize: 14,
                }}
              >
                le numéro de téléphone doit être composé de 8 chiffres !
              </p>
            </div>
          )}

          <TextField
            autocomplete="nope"
            required
            id="email"
            variant="filled"
            placeholder="Email*"
            onBlur={() => {
              !/.+@.+\.[A-Za-z]+$/.test(values.email)
                ? setemailValidation(true)
                : setemailValidation(false)
            }}
            className={classes.textField}
            value={values.email}
            onChange={handleChange("email")}
            margin="normal"
            InputProps={{
              classes: {
                underline: classes.input,
                input: emailValidation
                  ? classes.validation
                  : classes.textInputf,
              },
              style: {
                color: emailValidation ? "white" : "red",
                borderBottom: "1px solid",
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.input,
                focused: classes.input,
              },
            }}
          />
          {emailValidation && (
            <div style={{ position: "relative" }}>
              <p
                className={classes.validation}
                style={{ position: "absolute", top: -20, right: 35 }}
              >
                email invalide !
              </p>
            </div>
          )}

          <TextField
            autocomplete="off"
            required
            id="password"
            variant="filled"
            placeholder="password*"
            type="password"
            onBlur={() => {
              !/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(values.password)
                ? setPasswordValidation(true)
                : setPasswordValidation(false)
            }}
            className={classes.textField}
            value={values.password}
            onChange={handleChange("password")}
            margin="normal"
            InputProps={{
              classes: {
                underline: classes.input,
                input: emailValidation
                  ? classes.validation
                  : classes.textInputf,
              },
              style: {
                color: emailValidation ? "white" : "red",
                borderBottom: "1px solid",
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.input,
                focused: classes.input,
              },
            }}
          />
          {passwordValidation && (
            <div style={{ position: "relative" }}>
              <p
                className={classes.validation}
                style={{
                  position: "absolute",
                  top: -20,
                  right: 35,
                  fontSize: 14,
                  width: "85%",
                }}
              >
                password invalide : Huit caractères au minimum, au moins une
                lettre et un chiffre: !
              </p>
            </div>
          )}
          <TextField
            autocomplete="false"
            required
            variant="filled"
            id="date"
            type="date"
            label={values.date !== "" ? "" : "Date de naissance*"}
            onClick={() => {
              setValues({ ...values, date: " ." })
            }}
            onFocus={() => {
              setValues({ ...values, date: " ." })
            }}
            onChange={handleChange("date")}
            className={classes.textField}
            InputProps={{
              classes: {
                underline: classes.input,
                input:
                  values.date !== ""
                    ? classes.textInputf
                    : classes.textInputfBlack,
              },
              style: {
                color: "white",
                borderBottom: "1px solid",
              },
            }}
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.inputDate,
                focused: classes.input,
              },
            }}
          />

          <div style={{ position: "relative" }} className={classes.textField}>
            <Auto
              id={"Sexe*"}
              data={gender}
              returnValue={returnValue.bind(this)}
            />

            <Auto
              id={"Ville*"}
              data={ville}
              returnValue={returnValue.bind(this)}
            />
          </div>
          <center>
            {loading ? (
              <CircularProgress
                style={{ margin: "20px 0px" }}
                disableShrink
                color="secondary"
              />
            ) : (
              <button
                //onClick={() => setEtape(etape + 1)}
                className={classes.btnDetect}
                style={{ margin: "20px 0px", width: 150 }}
                type="submit"
              >
                S'ENREGISTRER
              </button>
            )}
          </center>
        </form>
      </div>
    </>
  )
}
export default withStyles(styles)(Signup)
