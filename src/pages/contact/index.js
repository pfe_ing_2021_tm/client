/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-03-02 18:45:08
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-06-19 14:12:58
 */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputBase from "@material-ui/core/InputBase";
import logoDiscovred from "../../assets/img/logoDiscovred.png";
import { compose } from "redux";
import classNames from "classnames";
import { connect } from "react-redux";
import {
  tryLogin,
  userData,
  tryRegisterNickname,
  sendContact,
} from "../../store/actions";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";
import CloseIcon from "@material-ui/icons/Close";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import WarningIcon from "@material-ui/icons/Warning";
import { history } from "../../history";
import { styles } from "./style";
import bgContact from "../../assets/img/bgContact.png";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import backprofile from "../../assets/img/backprofile.png";
import InputAdornment from "@material-ui/core/InputAdornment";
import "../home/styleSheet.css";
import FormHelperText from "@material-ui/core/FormHelperText";

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent);

class Login extends React.Component {
  constructor(props) {
    super(props);
    const token = localStorage.getItem("token");
    this.state = {
      token,
      username: "",
      mobile: "",
      sujet: "",
      textarea: "",
      savepass: false,
      age: false,
      use: false,
      nextStep: true,
      open: false,
      error: false,
      errorvalue: "",
      nickname: "",
      placeholder: "",
      usernameValid: false,
      mobileValid: false,
      sujetValid: false,
      textareaValid: false,
      mobileErrMsg: "",
    };
  }
  checkValidation = () => {
    if (this.state.username === "") {
      this.setState({ usernameValid: true });
    }
    if (this.state.mobile === "") {
      this.setState({
        mobileValid: true,
        mobileErrMsg: "Numéro téléphone champ obligatoire !",
      });
    } else if (this.state.mobile.length !== 8) {
      this.setState({
        mobileValid: true,
        mobileErrMsg:
          " le numéro de téléphone doit être composé de 8 chiffres !",
      });
    }
    if (this.state.sujet === "") {
      this.setState({ sujetValid: true });
    }
    if (this.state.textarea === "") {
      this.setState({ textareaValid: true });
    }
  };
  componentDidMount() {
    if (this.state.token !== null) {
      //return history.push("home");
    }
    this.setState({ error: false });
  }

  componentDidUpdate(prevProps, nextProps) {
    //console.log("prevProps : ", prevProps);
    //console.log("nextProps : ", nextProps);
    if (
      prevProps.status !== this.props.status &&
      prevProps.status === "error"
    ) {
      this.handleClick("vous n'avez pas de pseudo");
    }
    if (prevProps.status !== this.props.status && prevProps.status === null) {
      this.handleConnection("");
    }

    if (
      prevProps.status !== this.props.status &&
      this.props.status === "errorLogin"
    ) {
      this.handleClick("Impossible de se connecter");
    }

    if (prevProps.contact !== this.props.contact) {
      //console.log("this.props.contact : ", this.props.contact)
      if (this.props.contact.status === "success") {
        return history.push("/");
      }
    }

    if (
      prevProps.nickname !== this.props.nickname &&
      prevProps.nickname !== null &&
      this.props.status !== "error" &&
      this.props.status !== "errorLogin" &&
      this.props.status !== "Le Pseudo existe déjà"
    ) {
      let name = "Guest-" + prevProps.nickname;
      this.setState({ nextStep: false, placeholder: name });
    }
    if (
      prevProps.status !== this.props.status &&
      this.props.status === "Le Pseudo existe déjà"
    ) {
      this.handleClick("Le Pseudo existe déjà");
    }
  }

  handleChange = (name) => (event) => {
    //console.log("this.state.mobile.length", this.state.mobile.length);
    if (name === "username") {
      this.setState({ usernameValid: false });
    }
    if (name === "mobile") {
      this.setState({ mobileValid: false });
    }

    if (name === "sujet") {
      this.setState({ sujetValid: false });
    }
    if (name === "textarea") {
      this.setState({ textareaValid: false });
    }
    this.setState({
      [name]: event.target.value,
    });
  };
  //   handleChangeName =name => event=>{
  //     (typeof(this.state.username)==="number" && this.state.username.length<=8  )?
  //  this.setState({
  //       [name]:event.target.value
  //     }):this.setState({ [name]:''})

  //   }

  handleSavePass = (event) => {
    this.setState({ savepass: event.target.checked });
  };
  handleAgeCheck = (event) => {
    this.setState({ age: event.target.checked });
  };
  handleUseCheck = (event) => {
    this.setState({ use: event.target.checked });
  };
  /** This is a connection function. */
  handleConnection = () => {
    const data = {
      username: this.state.username,
      mobile: this.state.mobile,
      sujet: this.state.sujet,
      textarea: this.state.textarea,
    };
    this.props.sendContact(data);
  };

  handleLastStep = () => {
    //to do register nickname
    const data = {
      login: this.state.username,
      pseudo:
        this.state.nickname === ""
          ? this.state.placeholder
          : this.state.nickname,
    };
    this.props.tryRegisterNickname(data);
  };

  handleClick = (value) => {
    this.setState({ errorvalue: value, open: true });
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.props.userData(null);
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { nextStep } = this.state;
    //console.log("this.state.mobile.length", this.state.mobile.length);
    return (
      <div
        style={{
          background: `url(${bgContact})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          height: "100%",
        }}
        className={classes.container}
      >
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
        >
          <MySnackbarContentWrapper
            onClose={this.handleClose}
            variant="error"
            className={classes.margin}
            message={this.state.errorvalue}
          />
        </Snackbar>
        <div>
          <Link to="/">
            <div
              style={{
                display: "flex",
                alignItems: "center",
                paddingLeft: "7%",
                marginTop: 25,
                marginBottom: 15,
              }}
            >
              <img
                src={backprofile}
                style={{ width: 35, marginRight: 15 }}
                alt=""
              />
            </div>
          </Link>
          <div className={classes.Titletext}>
            <p>N'hésitez pas à nous poser vos questions</p>
          </div>
        </div>
        {nextStep ? (
          <div style={{ width: "100%" }}>
            <div className={classes.forms}>
              <div className={classes.section}>
                {/* <InputBase
                  id="username"
                  placeholder="Nom d'utilisateur"
                  className={classes.textField}
                  value={this.state.username}
                  onChange={this.handleChange("username")}
                  classes={{
                    root: classes.bootstrapRoot,
                    input: classes.bootstrapInput
                  }}
                /> */}

                <TextField
                  error={this.state.usernameValid ? true : false}
                  helperText={
                    this.state.usernameValid
                      ? "Nom & Prenom champ obligatoire !"
                      : null
                  }
                  id="Nom & Prenom"
                  label="Nom & Prenom"
                  placeholder="Nom & Prenom"
                  className={classes.textField}
                  value={this.state.username}
                  onChange={this.handleChange("username")}
                  margin="normal"
                  InputProps={{
                    classes: {
                      underline: classes.input,
                      input: classes.textInputf,
                    },
                    style: {
                      color: "white",
                      borderBottom: "1px solid",
                    },
                  }}
                  InputLabelProps={{
                    classes: {
                      root: classes.input,
                      focused: classes.input,
                    },
                  }}
                />

                <TextField
                  error={this.state.mobileValid ? true : false}
                  helperText={
                    this.state.mobileValid ? this.state.mobileErrMsg : null
                  }
                  id="mobile"
                  label="Numéro téléphone"
                  placeholder="Numéro téléphone"
                  className={classes.textField}
                  margin="normal"
                  value={this.state.mobile}
                  onInput={(e) => {
                    if (!e.target.value) {
                      e.target.value = "";
                    } else {
                      if (parseInt(e.target.value)) {
                        e.target.value = Math.max(0, parseInt(e.target.value))
                          .toString()
                          .slice(0, 8);
                      } else {
                        e.target.value = "";
                        //console.log("mobile");
                        this.setState({ mobileValid: false });
                      }
                    }
                  }}
                  onChange={this.handleChange("mobile")}
                  InputProps={{
                    className: "digitsOnly",
                    classes: {
                      underline: classes.input,
                      input: classes.textInputf,
                    },
                    style: {
                      color: "white",
                      borderBottom: "1px solid",
                    },
                  }}
                  InputLabelProps={{
                    classes: {
                      root: classes.input,
                      focused: classes.input,
                    },
                  }}
                />
                <TextField
                  error={this.state.sujetValid ? true : false}
                  helperText={
                    this.state.sujetValid ? "Sujet champ obligatoire !" : null
                  }
                  id="subject"
                  label="Sujet"
                  placeholder="Sujet"
                  className={classes.textField}
                  value={this.state.sujet}
                  onChange={this.handleChange("sujet")}
                  margin="normal"
                  InputProps={{
                    classes: {
                      underline: classes.input,
                      input: classes.textInputf,
                    },
                    style: {
                      color: "white",
                      borderBottom: "1px solid",
                    },
                  }}
                  InputLabelProps={{
                    classes: {
                      root: classes.input,
                      focused: classes.input,
                    },
                  }}
                />
                <TextField
                  error={this.state.textareaValid ? true : false}
                  helperText={
                    this.state.textareaValid
                      ? "Message champ obligatoire !"
                      : null
                  }
                  id="textarea"
                  label="Message"
                  placeholder="Message"
                  className={classes.textField}
                  margin="normal"
                  multiline
                  value={this.state.textarea}
                  onChange={this.handleChange("textarea")}
                  InputProps={{
                    classes: {
                      underline: classes.input,
                      input: classes.textInputf,
                    },

                    style: {
                      color: "white",
                      borderBottom: "1px solid",
                    },
                  }}
                  InputLabelProps={{
                    classes: {
                      root: classes.input,
                      focused: classes.input,
                    },
                  }}
                />
              </div>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                marginRight: "10%",
                marginTop: "16%",
                marginBottom: "10%",
              }}
            >
              <div
                onClick={() =>
                  this.state.username !== "" &&
                  this.state.mobile !== "" &&
                  this.state.mobile.length === 8 &&
                  this.state.sujet !== "" &&
                  this.state.textarea !== ""
                    ? this.handleConnection()
                    : this.checkValidation()
                }
                className={classes.btnpopup}
              >
                Envoyer
              </div>
            </div>
            <div className={classes.f}>
              <div className={classes.bottomDarkContainer}></div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  position: "fixed",
                  bottom: 0,
                  width: "100%",
                  zIndex: 1300,
                }}
              >
                <p className={["footer"].join(" ")}>
                  avis important : fumer nuit à la santé
                </p>
              </div>
            </div>
            <div className={classes.bottomContainer}></div>
          </div>
        ) : (
          <div className={classes.section}>
            <p
              style={{
                color: "#fff",
                fontFamily: "NeulandGroteskCondensedRegular",
                textAlign: "center",
                fontSize: 36,
              }}
            >
              Félicitations !
              <br /> Vous avez gagné <br />
              <span
                style={{
                  color: "#ED2A1C",
                  marginTop: 10,
                  // fontFamily: "NeulandGroteskCondensedRegular",
                  // textAlign: "center",
                  // fontSize: 36
                }}
              >
                700 STARS
              </span>
            </p>
            <p
              style={{
                color: "#fff",
                fontFamily: "NeulandGroteskCondensedRegular",
                textAlign: "center",
                fontSize: 36,
              }}
            >
              Merci de choisir
              <br /> votre nom d'utilisateur
            </p>
            <InputBase
              id="outlined-name"
              placeholder="Nom d'utilisateur"
              className={classes.textField}
              placeholder={this.state.placeholder}
              value={this.state.nickname}
              onChange={this.handleChange("nickname")}
              classes={{
                root: classes.bootstrapRoot,
                input: classes.bootstrapInput,
              }}
            />

            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={this.handleLastStep}
            >
              Valider
            </Button>
            <p className={classes.footerNextStep}>
              avis important : fumer nuit à la santé
            </p>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.ui.isLoading,
    status: state.status.status,
    userInfo: state.User.userInfo,
    token: state.token.token,
    nickname: state.nickname.nickname,
    contact: state.contact.contact,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    tryLogin: (data) => dispatch(tryLogin(data)),
    sendContact: (data) => dispatch(sendContact(data)),
    userData: () => dispatch(userData(null, null, null, null)),
    tryRegisterNickname: (data) => dispatch(tryRegisterNickname(data)),
  };
};
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Login);
