/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Tue Apr 23 2019
 */

import backgroundImg from './assets/img/background.jpg';
import bgLogin from './assets/img/bg-login.jpg';

export const background = {
  background: `url(${backgroundImg})`,
  width: '100%',
  position: 'relative',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat'
};

export const backgroundLogin = {
  background: `url(${bgLogin})`,
  width: '100%',
  position: 'relative',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat'
};
