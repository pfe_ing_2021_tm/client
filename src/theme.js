/*
 * @Author: Aymen ZAOUALI
 * @Date: 2019-07-30 10:28:31
 * @Last Modified by: Aymen ZAOUALI
 * @Last Modified time: 2019-10-16 11:11:29
 */
import { createMuiTheme } from "@material-ui/core/styles"

export const theme = createMuiTheme({
  palette: {
    primary: { main: localStorage.getItem("primaryColor") },
    secondary: { main: "#ffffff" },
  },
  typography: {
    useNextVariants: true,
  },
})
