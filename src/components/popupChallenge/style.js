/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-06-18T13:19:05+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-19T11:37:40+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

export const styles = (theme) => ({
  root: {
    width: "200%",
    height: "150%",
  },
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
  appBar: {
    position: "relative",
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: 15,
    marginRight: 10,
    color: theme.palette.secondary.main,
    padding: "0 0px 0 10px",
  },
  drawerPaper: {
    [theme.breakpoints.between("lg", "xl")]: {
      width: 400,
    },
    [theme.breakpoints.between("xs", "sm")]: { width: "100%" },
    background: "#000",
  },

  icnlogout: {
    color: theme.palette.secondary.main,
    position: "absolute",
    right: 15,
    bottom: 15,
    textAlign: "right",
  },
  menutext: {
    color: "#888",
    margin: 2,
    textTransform: "uppercase",
    fontFamily: "NeulandGroteskCondensedRegular",
    fontSize: 20,
    lineHeight: 1,
  },
  icnButton: {
    marginLeft: 0,
    marginRight: 0,
    color: theme.palette.secondary.main,
  },
  headerContainer: {
    justifyContent: "space-between",
    height: 100,
    flexDirection: "row",
    display: "flex",
    alignItems: "center",
  },
  itemList: {
    padding: "0px 15px",
  },
  itemText: {
    color: theme.palette.secondary.main,
    margin: 0,
    lineHeight: 1.3,
    fontFamily: "NeulandGroteskCondensedBold",
    textTransform: "uppercase",
    fontSize: 30,
  },
  itemTextTableTitle: {
    color: theme.palette.secondary.main,
    margin: 0,
    lineHeight: 1.3,
    fontFamily: "NeulandGroteskCondensedBold",
    textTransform: "uppercase",
    fontSize: 25,
  },
  itemTextTable: {
    color: theme.palette.secondary.main,
    margin: 0,
    lineHeight: 1.3,
    fontSize: 20,
  },
  itemIcn: {
    display: "flex",
    justifyContent: " space-around",
    alignItems: "center",
    alignContent: "center",
    borderTop: "1px dashed #fff",
    borderBottom: "1px dashed #fff",
    padding: "14px 0",
    margin: 15,
  },
  link: {
    color: theme.palette.secondary.main,
    textDecoration: "inherit",
    cursor: "pointer",
  },
  footer: {
    [theme.breakpoints.between("lg", "xl")]: {
      position: "fixed", // fixed problem 100% width
      bottom: 0,
      height: 25,
      background: "rgb(255, 255, 255)",
      textAlign: "center",
      width: 400,

      margin: 0,
      textTransform: "uppercase",
      fontFamily: "NeulandGroteskCondensedBold",
      fontSize: 15,
      // fontWeight: "700",
      lineHeight: "24px",
    },
    [theme.breakpoints.between("xs", "sm")]: {
      position: "fixed", // fixed problem 100% width
      bottom: 0,
      height: 25,
      background: "rgb(255, 255, 255)",
      textAlign: "center",
      width: "100%",
      margin: 0,
      textTransform: "uppercase",
      fontFamily: "NeulandGroteskCondensedBold",
      fontSize: 15,
      // fontWeight: "700",
      lineHeight: "24px",

      // margin: 0,
      maxHeight: "100%",
      // maxWidth: 407
    },
  },

  footerText: {
    padding: "0 15px",
  },
  footerBorder: {
    borderTop: "1px solid #888",
    width: 35,
    marginLeft: 18,
    marginBottom: 10,
  },
  footerlist: {
    margin: 30,
    marginLeft: 15,
    marginBottom: 30,
  },
  IconReds: {
    display: "flex",
    flexDirection: "column",
    marginTop: " 3%",
    margin: "10px",
    alignItems: "center",
  },
  btnpopup: {
    background: "red 0% 0% no-repeat padding-box",
    borderRadius: "12px",
    width: "150px",
    height: "48px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    font: "Black 25px/30px Roboto",
    fontFamily: "Roboto",
    letterSpacing: 0,
    fontWeight: 900,
    color: theme.palette.secondary.main,
    letterSpacing: "0px",
    fontSize: "100%",

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      left: "22%",
    },
  },
  label: {
    //backgroundColor: "red",
    marginTop: "14px !important",
    marginBottom: "36px !important",
  },
  containerOper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      marginLeft: "80px !important",
      marginRight: "80px !important",
    },
  },
  formControl: {
    margin: theme.spacing(3),
  },
  titleDialog: {
    textAlign: "center ",
    font: "Black 25px/30px Roboto",
    fontFamily: "Roboto",
    letterSpacing: 0,
    fontWeight: 900,
    color: "black",
    letterSpacing: "0px",
    /* margin: 27px; */
    marginTop: "34px",
    marginLeft: "40px",
    marginRight: "40px",
    fontSize: "140%",
  },
  btnjouer: {
    background: "#FFBE04 0% 0% no-repeat padding-box",
    borderRadius: "12px",
    opacity: 1,
    width: "100px",
    height: "48px",
    position: "absolute",
    bottom: -21,
    justifyContent: "center",
    alignItems: "center",
    font: "Black 25px/30px Roboto",
    fontFamily: "Roboto",
    letterSpacing: 0,
    fontWeight: 900,
    color: "black",
    letterSpacing: "0px",
    fontSize: "140%",

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      left: "22%",
    },
  },
  table: {
    minWidth: 220,
  },
  tabsIndicator: {
    backgroundColor: theme.palette.secondary.main,
  },
  tabRoot: {
    textTransform: "initial",
    minWidth: 72,
    // fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "NeulandGroteskCondensedBold",
    marginTop: 30,
    color: "#999",
    /*"&:hover": {
      color: theme.palette.secondary.main,
      opacity: 1
    },
    "&$tabSelected": {
      color: theme.palette.secondary.main,
      fontWeight: theme.typography.fontWeightMedium,
      fontFamily: "NeulandGroteskCondensedBold"
    },
    "&:focus": {
      color: theme.palette.secondary.main
    },*/
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)

      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)

      fontSize: 25,
    },
  },
})
