import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./style";
import { compose } from "redux";
import { connect } from "react-redux"; 

import IconReds from "../../assets/img/reds.png";
import CONFIRMERPOPUP from "../../assets/img/CONFIRMER-POPUP.png";
import PopupResd from "../../assets/img/PopupResd.png";
import Fade from "@material-ui/core/Fade";
import { getGifts } from "../../store/actions";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Button from "@material-ui/core/Button";
function Transition(props) {
    return <Slide direction="down" {...props} mountOnEnter unmountOnExit />;
}

class Challenge extends Component {
    state = {
        open: false,
        reds: "5",
        image: ""
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
        localStorage.setItem("clicked", true);
    };
    componentDidMount() {
        //this.props.getGifts();
    }

    componentWillReceiveProps(prevProps) {
        if (prevProps.challenge !== this.props.challenge) {
            if (localStorage.getItem("clicked") !== true) {
                if (prevProps.challenge.data.length > 0) {
                    this.setState({ open: true, image: process.env.REACT_APP_DOMAIN + prevProps.challenge.data[0].image });
                }
            }
        }
    }
    goToGame() {
        this.setState({ open: false });
        localStorage.setItem("clicked", true);
        window.location.assign(`${this.props.challenge.data[0].jeu}/index.html`);
    }
    render() {
        const { classes } = this.props;
        return (
            <div>
                {/*<img
                    id="Menu_Reds"
                    src={IconReds}
                    alt="IconReds"
                    style={{ width: 25 }}
                    onClick={this.handleOpen}
                />*/}
                <Dialog
                    //modal={false}
                    disableBackdropClick
                    aria-labelledby="simple-dialog-title"
                    open={this.state.open}
                    onClose={this.handleCloseChallenge}
                    //TransitionComponent={Transition}
                    PaperProps={{
                        style: {
                            display: "flex",
                            background: "#fff",
                            justifyContent: "center",
                            overflowY: "visible"
                        }
                    }}
                >
                    <div
                        style={{
                            width: "250px",
                            height: "220px",
                            backgroundSize: "contain",
                            backgroundImage: this.state.image ? "url(" + this.state.image + ")" : ""
                        }}
                    >
                        <IconButton
                            style={{
                                justifyContent: "flex-start",
                                color: "black",
                                paddingBottom: "0px",
                                marginBottom: "0px",
                            }}
                            fontSize="large"
                            onClick={this.handleClose}
                            aria-label="Close"
                        >
                            <CloseIcon
                                style={{
                                    fontSize: 20,
                                    marginLeft: 5,
                                    marginTop: 5,
                                    padding: "0px"
                                }}
                            />
                        </IconButton>
                        {/*<div
                            className={classes.titleDialog}
                            style={{
                                color: "red",
                                borderColor: "#fff",
                                transition: this.state.err ? "color 1s" : "color 1s",
                                transform: this.state.err ? "scale(1.1,1.1)" : null,
                                transition: this.state.err ? "transform 0.5s ease" : null,
                                height: !this.state.existvouchers ? "5vh" : null
                            }}
                        >
                            Nouveau Challenge
                        </div>*/}
                        {/*<img className={classes.image} style={{ height: "200px", width: "300px" }} src={this.state.image} alt="" />*/}
                        <div
                            style={{
                                marginTop: "120px",
                                display: "flex",
                                justifyContent: "center",
                                overflowY: "visible"
                            }}
                        >
                            <FormControl
                                component="fieldset"
                                className={classes.formControl}
                            >
                                <FormLabel component="legend"></FormLabel>
                                <div
                                    className={classes.containerOper}
                                //style={{ width: "34px" }}
                                >
                                    <div>

                                    </div>
                                    <div
                                        style={{
                                            display: "flex",
                                            //   display:
                                            //     this.state.vouchers.length > 1 &&
                                            //     typeof this.state.vouchers !== "string" &&
                                            //     this.state.existvouchers
                                            //       ? "flex"
                                            //       : "",
                                            flexDirection: "column",
                                            //   height:
                                            //     typeof this.state.vouchers !== "string" &&
                                            //     this.state.existvouchers
                                            //       ? "300px"
                                            //       : "100px",
                                            justifyContent: "space-around",
                                            height: 60
                                        }}
                                    >
                                        <div>
                                            <Button
                                                className={classes.btnpopup}
                                                onClick={() => this.goToGame()}
                                            >Jouer</Button>
                                        </div>
                                    </div>
                                </div>
                            </FormControl>
                        </div>
                    </div>
                </Dialog>
            </div>
        );
    }
}

Challenge.propTypes = {
    classes: PropTypes.object.isRequired
};
const mapStateToProps = state => {
    return {
        challenge: state.challenge.challenge
    };
};
export default compose(
    withStyles(styles),
    connect(mapStateToProps)
)(Challenge);
