import React, { Component } from "react";
import { useSelector } from "react-redux";
import loadingGif from "../../assets/img/loading.gif";

  

const Loading = () => {
   const settings = useSelector((state) => state.settings.settings)
 return (
   <div
     style={{
       background: "#fff",
       display: "flex",
       justifyContent: "center",
       alignItems: "center",
       height: "100vh",
     }}
   >
     <img src={loadingGif} width="150px" alt="" />
   </div>
 )
}

export default Loading
