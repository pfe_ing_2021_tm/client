/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-06-18T13:15:33+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-06-19T11:39:03+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import MenuIcon from "@material-ui/icons/Menu";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import { styles } from "./style";
import { history } from "../../history";
import REDSIconWheel from "../../assets/img/wheel/REDSIconWheel.png";
import Redxp from "../../assets/img/redxp.svg";
import FormLabel from "@material-ui/core/FormLabel";
import { compose } from "redux";
import { connect } from "react-redux";
import {
  scrollTo,
  scrollToAndOpenSection,
  getGifts,
  getFeatures,
  getChallenge,
} from "../../store/actions";
import { Link } from "react-router-dom";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
//import TableContainer from '@material-ui/core/TableContainer';
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import BrandsSelection from "../../pages/brands";

function Transition(props) {
  return <Slide direction="right" {...props} />;
}

class DrawerView extends React.Component {
  state = {
    open: false,
    openChallenge: false,
    openClassement: false,
    reds: "",
    challenge: [],
    classement: [],
    classementTop10: [],
    brands: [],
    value: 0,
  };

  createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false, openClassement: false });
    this.props.scrollTo("");
  };
  handleCloseChallenge = () => {
    this.setState({ openChallenge: false });
  };
  handlepdf = (ch) => {
    if (ch == "cond") {
      let path = `https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/t%26c.pdf`;
      return (window.location.href = path);
    } else if (ch == "conf") {
      let path = `https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/Consumer+privacy+notice+FRv2.pdf`;
      return (window.location.href = path);
    } else if (ch === "sante") {
      let path = `https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/health-effects-of-smoking_fr.pdf`;
      return (window.location.href = path);
    } else if (ch === "challenge") {
      let path = `https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/Challenge.pdf`;
      return (window.location.href = path);
    }
  };

  handleLogout = async () => {
    await localStorage.removeItem("login");
    await localStorage.removeItem("refreshtoken");
    await localStorage.removeItem("fcmtoken");
    await localStorage.removeItem("_id");
    await localStorage.removeItem("uid");
    await localStorage.removeItem("token");
    //await localStorage.clear();
    await history.push("/");
  };
  componentDidMount() {
    this.props.getGifts();
    this.props.getFeatures();
    this.props.getChallenge();
  }
  componentWillReceiveProps(prevProps) {
    if (prevProps.wheelgift !== this.props.wheelgift) {
      if (prevProps.wheelgift !== null) {
        this.setState({
          reds: prevProps.wheelgift.score,
        });
      }
    }
    if (prevProps.brands !== this.props.brands) {
      if (this.props.brands !== null && this.props.brands.length > 0) {
        this.setState({
          brands: this.props.brands,
        });
      }else if(prevProps.brands !== null){
        this.setState({brands: prevProps.brands});
      }
    }
    if (prevProps.features !== null && prevProps.features !== undefined) {
      var recette = prevProps.features.filter(
        (item) => item.type === "recette"
      );
      this.setState({
        recette: recette.length > 0 ? true : false,
      });
    }
    if (prevProps.challenge !== null && prevProps.challenge !== undefined) {
      if (prevProps.challenge.status === "success") {
        this.setState({
          challenge:
            prevProps.challenge.data.length > 0
              ? prevProps.challenge.data[0]
              : [],
        });
        if (prevProps.challenge.data.length > 0) {
          if (
            prevProps.classement !== null &&
            prevProps.classement !== undefined &&
            prevProps.classement !== this.props.classement
          ) {
            let classement =
              prevProps.classement.data.length > 0
                ? prevProps.classement.data
                : [];
            let classementTop10 =
              prevProps.classement.dataTop10.length > 0
                ? prevProps.classement.dataTop10
                : [];
            let rang = prevProps.classement.rang;
            this.setState({
              classement: classement,
              classementTop10: classementTop10,
              rang: rang,
            });
          }
        }
      }
    }
  }
  getPopup() {
    this.setState({ openChallenge: true });
  }
  goToGame() {
    window.location.assign(`${this.state.challenge.jeu}/index.html`);
  }
  goToClassement() {
    this.setState({ openChallenge: false, openClassement: true, open: false });
  }
  handleChange = (event, value) => {
    this.setState({ value });
  };
  render() {
    const { classes } = this.props;
    const { value } = this.state;
    const rows = [
      this.createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
      this.createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
      this.createData("Eclair", 262, 16.0, 24, 6.0),
      this.createData("Cupcake", 305, 3.7, 67, 4.3),
      this.createData("Gingerbread", 356, 16.0, 49, 3.9),
    ];
    return (
      <div>
        <div className={classes.root}>
          <IconButton
            onClick={this.handleClickOpen}
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon style={{ fontSize: 37 }} id="Show_Menu_drawer" />
          </IconButton>

          <Dialog
            fullScreen
            open={this.state.open}
            onClose={this.handleClose}
            PaperProps={{
              classes: {
                root: classes.drawerPaper,
              },
            }}
            TransitionComponent={Transition}
          >
            <div>
              <IconButton
                style={{
                  justifyContent: "flex-start",
                  color: "#fff",
                }}
                fontSize="large"
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon
                  style={{
                    fontSize: 30,
                    marginLeft: 15,
                    marginTop: 40,
                    padding: "0 0 12px 0px",
                  }}
                />
              </IconButton>
              <List style={{ marginLeft: 15 }}>
                <div style={{ marginTop: 30 }}>
                  <ListItem button>
                    <Link to="/profile" style={{ textDecoration: "none" }}>
                      <p className={classes.itemText} id="Show_Profile">
                        PROFIL
                      </p>
                    </Link>
                  </ListItem>
                  <div
                    style={{
                      borderBottom: "1px solid #fff",
                      width: 30,
                      marginLeft: 15,
                    }}
                  />
                </div>

                <div style={{ marginTop: 10 }}>
                  {/* <ListItem button className={classes.itemList}>
                    <Link
                      to={
                        process.env.REACT_APP_EVENTS === "true"
                          ? "/events"
                          : "/home"
                      }
                      style={{
                        textDecoration: "none",
                        opacity:
                          process.env.REACT_APP_EVENTS === "true" ? "1" : "0.5",
                      }}
                    >
                      <div className={classes.itemText} id="Show_events">
                        Où Sortir ?
                      </div>
                    </Link>
                  </ListItem> */}
                  <ListItem button className={classes.itemList}>
                    <Link to={"/sendGifts"}>
                      <div className={classes.itemText} id="sendGifts">
                        Envoyer satrs
                      </div>
                    </Link>
                  </ListItem>
                  <ListItem button className={classes.itemList}>
                    <Link
                      to={
                        process.env.REACT_APP_SHOPMAP === "true"
                          ? "/shopmap"
                          : "/home"
                      }
                      style={{
                        textDecoration: "none",
                        opacity:
                          process.env.REACT_APP_SHOPMAP === "true"
                            ? "1"
                            : "0.5",
                      }}
                    >
                      <p className={classes.itemText} id="Show_events">
                        Magasin
                      </p>
                    </Link>
                  </ListItem>
                </div>

                {/* <div style={{ marginTop: 5 }}>
                  <ListItem button className={classes.itemList}>
                    <Link to={"/home"}
                          style={{
                            textDecoration: "none",
                            opacity: this.state.brands.length > 0  ? "1" : "0.5",
                          }}
                    >
                      <p
                        className={classes.itemText}
                        onClick={() => {
                          if(this.state.brands.length > 0 ) this.setState({ openBrandsSelection: true });
                        }}
                        id="mon_marlboro"
                      >
                        mon marlboro
                      </p>
                    </Link>
                  </ListItem>
                </div> */}
                <BrandsSelection
                  handleClose={() =>
                    this.setState({ openBrandsSelection: false, open: false })
                  }
                  open={this.state.openBrandsSelection}
                  notif={false}
                />

                <div style={{ marginTop: 5 }}>
                  <ListItem
                    button
                    className={classes.itemList}
                    style={{ padding: "0px 15px" }}
                  >
                    {typeof this.state.reds === "undefined" ||
                    this.state.reds < process.env.REACT_APP_REDS ? (
                      <div className={classes.itemText}>
                        <p
                          className={classes.link}
                          style={{ color: " rgb(139,139,139)" }}
                        >
                          la roue de la chance
                        </p>
                      </div>
                    ) : (
                      <div onClick={this.toRoue} className={classes.itemText}>
                        <p
                          className={classes.link}
                          onClick={() => history.push("/wheel")}
                        >
                          la roue de la chance
                        </p>
                      </div>
                    )}
                  </ListItem>
                </div>
                <div style={{ marginTop: 5 }}>
                  {/* <ListItem button className={classes.itemList}>
                <div
                  className={classes.itemText}
                  onClick={() => {
                    this.handleClose();
                  }}
                >
                  vidèos
                </div>
              </ListItem>
              <ListItem button className={classes.itemList}>
                <div
                  className={classes.itemText}
                  onClick={() => {
                    this.handleClose();
                    setTimeout(() => {
                      this.props.scrollTo("games");
                    }, 500);
                  }}
                >
                  jeux
                </div>
              </ListItem>

              <ListItem button className={classes.itemList}>
                <div
                  className={classes.itemText}
                  onClick={() => {
                    this.handleClose();
                    setTimeout(() => {
                      this.props.scrollTo("gallery");
                    }, 500);
                  }}
                >
                  galerie
                </div>
              </ListItem> */}
                </div>
              </List>
            </div>
            <div>
              <div className={classes.itemIcn}>
                <div className={classes.IconReds}>
                  <img src={REDSIconWheel} alt="" style={{ width: 40 }} />
                  <p
                    style={{
                      color: "white",
                      marginLeft: "-12%",
                      fontSize: 20,
                      marginTop: "10px",
                      fontFamily: "NeulandGroteskCondensedBold",
                    }}
                  >
                    {this.state.reds} STARS
                  </p>
                </div>
                {/* <Link to="/badge" style={{ textDecoration: "none" }}>
                  <img
                    id="Show_Badge_Miles"
                    src={Redxp}
                    alt=""
                    style={{ width: 101, marginTop: " -10%" }}
                  />
                </Link> */}
              </div>
            </div>
            <List className={classes.footerlist}>
              <div className={classes.footerBorder} />
              {/* <ListItem button className={classes.footerText}>
                <div
                  className={classes.menutext}
                  onClick={() => {
                    this.handlepdf("sante");
                  }}
                >
                  pmi & santè
                </div>
              </ListItem>
              <ListItem button className={classes.footerText}>
                <a href="https://cloud.pmiclicks.com/login">
                  <div className={classes.menutext}>centre d'aide pmi</div>
                </a>
              </ListItem>
              */}
              <ListItem button className={classes.footerText}>
                <div
                  className={classes.menutext}
                  onClick={() => {
                    this.handlepdf("cond")
                  }}
                >
                  conditions d'utilisation{" "}
                </div>
              </ListItem>
              <ListItem button className={classes.footerText}>
                <div
                  className={classes.menutext}
                  onClick={() => {
                    this.handlepdf("conf")
                  }}
                >
                  politique de confidentialité
                </div>
              </ListItem>
              {/* <ListItem
              button
              className={classes.footerText}
              onClick={this.handleLogout}
            >
              <div className={classes.menutext}>Déconnexion</div>
            </ListItem> */}
            </List>
            <List className={classes.footerlist}>
              <ListItem
                button
                className={classes.footerText}
                onClick={this.handleLogout}
              >
                <div className={classes.menutext} id="Déconnexion">
                  Déconnexion
                </div>
              </ListItem>
            </List>
          </Dialog>
          {/* <Dialog
            //modal={false}
            disableBackdropClick
            aria-labelledby="simple-dialog-title"
            open={this.state.openChallenge}
            onClose={this.handleCloseChallenge}
            //TransitionComponent={Transition}
            PaperProps={{
              style: {
                display: "flex",
                background: "rgb(31, 2, 2)",
                justifyContent: "center",
                overflowY: "visible",
              },
            }}
          >
            <IconButton
              style={{
                justifyContent: "flex-start",
                color: "#fff",
                paddingBottom: "0px",
                marginBottom: "0px",
              }}
              fontSize="large"
              onClick={this.handleCloseChallenge}
              aria-label="Close"
            >
              <CloseIcon
                style={{
                  fontSize: 20,
                  marginLeft: 5,
                  marginTop: 5,
                  padding: "0px",
                }}
              />
            </IconButton>

            <div>
              <div
                className={classes.titleDialog}
                style={{
                  color: this.state.err ? "red" : "#fff",
                  borderColor: "#fff",
                  transition: this.state.err ? "color 1s" : "color 1s",
                  transform: this.state.err ? "scale(1.1,1.1)" : null,
                  transition: this.state.err ? "transform 0.5s ease" : null,
                  height: !this.state.existvouchers ? "5vh" : null,
                }}
              >
                {this.state.challenge.challenge}
              </div>

              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  overflowY: "visible",
                }}
              >
                <FormControl
                  component="fieldset"
                  className={classes.formControl}
                >
                  <FormLabel component="legend"></FormLabel>
                  <div
                    className={classes.containerOper}
                  //style={{ width: "34px" }}
                  >
                    <div></div>
                    <div
                      style={{
                        display: "flex",
                        //   display:
                        //     this.state.vouchers.length > 1 &&
                        //     typeof this.state.vouchers !== "string" &&
                        //     this.state.existvouchers
                        //       ? "flex"
                        //       : "",
                        flexDirection: "column",
                        //   height:
                        //     typeof this.state.vouchers !== "string" &&
                        //     this.state.existvouchers
                        //       ? "300px"
                        //       : "100px",
                        justifyContent: "space-around",
                        height: 275,
                      }}
                    >
                      <div>
                        <Button
                          className={classes.btnpopup}
                          onClick={() => this.goToGame()}
                        >
                          Jouer
                        </Button>
                      </div>
                      <div>
                        <Button
                          className={classes.btnpopup}
                          onClick={() => this.goToClassement()}
                        >
                          Classement
                        </Button>
                      </div>
                    </div>
                  </div>
                </FormControl>
              </div>
            </div>
          </Dialog> */}
          <Dialog
            fullScreen
            open={this.state.openClassement}
            onClose={this.handleClose}
            PaperProps={{
              classes: {
                root: classes.drawerPaper,
              },
            }}
            TransitionComponent={Transition}
          >
            <div>
              <IconButton
                style={{
                  justifyContent: "flex-start",
                  color: "#fff",
                  paddingBottom: "0px",
                  marginBottom: "0px",
                }}
                fontSize="large"
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon
                  style={{
                    fontSize: 30,
                    marginLeft: 15,
                    marginTop: 20,
                    padding: "0 0 12px 0px",
                  }}
                />
              </IconButton>
              <Tabs
                value={value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                classes={{
                  root: classes.tabsRoot,
                  indicator: classes.tabsIndicator,
                }}
              >
                <Tab
                  label="Mon classement"
                  classes={{
                    root: classes.tabRoot,
                    selected: classes.tabSelected,
                  }}
                />
                <Tab
                  label="Top 10"
                  classes={{
                    root: classes.tabRoot,
                    selected: classes.tabSelected,
                  }}
                />
              </Tabs>
              {value === 0 && (
                <div style={{ marginTop: 30 }}>
                  <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell className={classes.itemTextTableTitle}>
                          Rang
                        </TableCell>
                        <TableCell className={classes.itemTextTableTitle}>
                          Joueurs
                        </TableCell>
                        <TableCell className={classes.itemTextTableTitle}>
                          Score
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {this.state.classement.length > 0 &&
                        this.state.classement.map((row, index) => (
                          <TableRow key={row.customer}>
                            <TableCell
                              className={classes.itemTextTable}
                              component="th"
                              style={{
                                color: row.selected ? "#ED2A1C" : "#fff",
                              }}
                              scope="row"
                            >
                              {this.state.rang > 5
                                ? index + this.state.rang - 5
                                : index + 1}
                            </TableCell>
                            <TableCell
                              className={classes.itemTextTable}
                              component="th"
                              style={{
                                color: row.selected ? "#ED2A1C" : "#fff",
                              }}
                              scope="row"
                            >
                              {row.customer}
                            </TableCell>
                            <TableCell
                              className={classes.itemTextTable}
                              style={{
                                color: row.selected ? "#ED2A1C" : "#fff",
                              }}
                            >
                              {row.score}
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </div>
              )}
              {value === 1 && (
                <div style={{ marginTop: 30 }}>
                  <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell className={classes.itemTextTableTitle}>
                          Rang
                        </TableCell>
                        <TableCell className={classes.itemTextTableTitle}>
                          Joueurs
                        </TableCell>
                        <TableCell className={classes.itemTextTableTitle}>
                          Score
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {this.state.classementTop10.length > 0 &&
                        this.state.classementTop10.map((row, index) => (
                          <TableRow key={row.customer}>
                            <TableCell
                              className={classes.itemTextTable}
                              style={{
                                color: row.selected ? "#ED2A1C" : "#fff",
                              }}
                              component="th"
                              scope="row"
                            >
                              {index + 1}
                            </TableCell>
                            <TableCell
                              className={classes.itemTextTable}
                              style={{
                                color: row.selected ? "#ED2A1C" : "#fff",
                              }}
                              component="th"
                              scope="row"
                            >
                              {row.customer}
                            </TableCell>
                            <TableCell
                              className={classes.itemTextTable}
                              style={{
                                color: row.selected ? "#ED2A1C" : "#fff",
                              }}
                            >
                              {row.score}
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </div>
              )}
            </div>
          </Dialog>
        </div>
      </div>
    )
  }
}

DrawerView.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
  return {
    brands:state.brandList.brandList,
    wheelgift: state.Gift.wheelgift,
    features: state.features.features,
    challenge: state.challenge.challenge,
    classement: state.classement.classement,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    scrollTo: (value) => dispatch(scrollTo(value)),
    scrollToAndOpenSection: (value) => dispatch(scrollToAndOpenSection(value)),
    getGifts: () => dispatch(getGifts()),
    getFeatures: () => dispatch(getFeatures()),
    getChallenge: () => dispatch(getChallenge()),
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DrawerView);
