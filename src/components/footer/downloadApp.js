import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import IcnDownload from "../../assets/img/download.png";
import LogoDiscovred from "../../assets/img/logoDiscovred.png";
import { styles } from "./style";

class DownloadApp extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.Dowcontainer} onClick={this.props.onClick}>
        <div
          style={{ display: "flex", alignItems: "center", padding: "0 10px" }}
        >
          <Typography component="p" className={classes.textAdd}>
            AJOUTEZ L’EXTENTION
          </Typography>
          <img src={LogoDiscovred} alt="" className={classes.logoDisco} />
        </div>

        <div
          style={{ display: "flex", alignItems: "center", padding: "0 10px" }}
        >
          <Typography component="p" className={classes.textDown}>
            INSTALLER
          </Typography>
          <img src={IcnDownload} alt="" className={classes.imgDow} />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(DownloadApp);
