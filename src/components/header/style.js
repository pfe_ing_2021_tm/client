/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Tue Apr 23 2019
 */
export const styles = theme => ({
  root: {
    width: "100%"
  },
  container: {
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundSize: "cover",
    position: "absolute"
  },
  dialogPaper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#96010F",
    backgroundSize: "cover",
    [theme.breakpoints.between("lg", "xl")]: {
      width: 400
    },
    [theme.breakpoints.between("sm", "md")]: { maxWidth: 600 },

    [theme.breakpoints.between("xs", "sm")]: {}
  },
  menuButton: {
    marginLeft: 15,
    marginRight: 15,
    color: "#fff"
  },
  icnButton: {
    marginLeft: 0,
    marginRight: 0,
    color: "#fff"
  },
  headerContainer: {
    justifyContent: "space-between",
    height: 100,
    flexDirection: "row",
    display: "flex",
    alignItems: "center"
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    alignItems: "center",
    background: "transparent"
  },
  paper: {
    // backgroundColor: theme.palette.background.paper,
    // justifyContent: "center",
    // alignItems: "center",
    // display: "flex",
    // flexDirection: "column",
    // padding: "25px 0px",
    // marginTop: "10%"
  },

  title: {
    fontFamily: "NeulandGroteskCondensedRegular",
    color: "rgb(139,139,139)",
    position: "relative",

    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      fontSize: 22,
      bottom: 103
    },

    [theme.breakpoints.between("sm", "md")]: {
      //ipod ( 600px => 950px)
      bottom: 140,
      fontSize: 30
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      bottom: 105,
      fontSize: 25
    }
  },
  redsText: {
    color: "red",
    fontFamily: "NeulandGroteskCondensedBold",
    position: "relative",
    [theme.breakpoints.between("xs", "sm")]: {
      fontSize: 22,
      bottom: 140
    },

    [theme.breakpoints.between("sm", "md")]: {
      bottom: 205,
      fontSize: 37
    },
    [theme.breakpoints.between("md", "xl")]: {
      bottom: 145,
      fontSize: 25
    }
  },

  image: {
    width: "50%",
    [theme.breakpoints.between("lg", "xl")]: {
      width: 200
    },
    [theme.breakpoints.between("xs", "sm")]: { width: "50%" }
  },
  btn: {
    position: "relative",
    [theme.breakpoints.between("xs", "sm")]: {
      //fontSize: 22,
      bottom: 150,
      width: 124
    },

    [theme.breakpoints.between("sm", "md")]: {
      bottom: 230,
      width: 160
      //  fontSize: 45
    },
    [theme.breakpoints.between("md", "xl")]: {
      bottom: 165,
      width: 115
      // fontSize: 25
    }
  }
});
