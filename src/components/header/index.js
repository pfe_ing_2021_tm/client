/**
 * @Author: <> with ❤ by Aymen ZAOUALI
 * @Date:   2019-04-29T09:33:02+01:00
 * @Last modified by:   <> with ❤ by Aymen ZAOUALI
 * @Last modified time: 2019-07-01T09:16:23+01:00
 * @License: The computer program contained herein contains proprietary information which is the property of Chifco.
 * @Copyright: Copyright (c) 2019 CHIFCO
 */

import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import { styles } from "./style";
import DrawerView from "../drawer";
import TransitionsPopper from "./popup";
import IconShop from "../../assets/img/shop_icon.png";
import { Link } from "react-router-dom";

import CitySelection from "../../pages/citySelection";
import Redxp from "../../assets/img/redxp.svg";

class Header extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.headerContainer}>
        <div style={{ display: "flex", alignItems: "center" }}>
          <DrawerView />
          {/* <IconButton
            className={classes.menuButton}
            style={{ marginLeft: 0, padding: 0 }}
            color="inherit"
            aria-label="Menu"
          >
            <Link to="/badge">
              <img
                src={Redxp}
                alt=""
                style={{ width: 60, padding: 10 }}
                id="Menu_Badge_Miles"
              />
            </Link>
          </IconButton> */}
        </div>

        <div style={{ marginRight: 15, display: "flex" }}>
          <IconButton
            className={classes.icnButton}
            color="inherit"
            aria-label="Menu"
          >
            <TransitionsPopper />
          </IconButton>
          <Link
            to={process.env.REACT_APP_SHOPMAP === "true" ? "/shopmap" : "/home"}
            style={{
              opacity: process.env.REACT_APP_SHOPMAP === "true" ? "1" : "0.5",
            }}
          >
            <IconButton
              onClick={this.handleClickOpen}
              className={classes.icnButton}
              color="inherit"
              aria-label="Menu"
            >
              <img
                src={IconShop}
                alt=""
                style={{ width: 29, paddingLeft: 3, paddingBottom: 2 }}
                id="Menu_shopmap"
              />
            </IconButton>
          </Link>
          <CitySelection />
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);
