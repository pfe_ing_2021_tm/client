import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./style";
import { compose } from "redux";
import { connect } from "react-redux"; 

import IconReds from "../../assets/img/reds.png";
import CONFIRMERPOPUP from "../../assets/img/CONFIRMER-POPUP.png";
import PopupResd from "../../assets/img/PopupResd.png";
import Fade from "@material-ui/core/Fade";
import { getGifts, getUserInfo } from "../../store/actions";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
function Transition(props) {
  return <Slide direction="down" {...props} mountOnEnter unmountOnExit />;
}

class TransitionsPopper extends Component {
  state = {
    open: false,
    reds: "",
  }

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }
  componentDidMount() {
    this.props.getGifts()
    this.props.getUserInfo()
  }
 
  componentDidUpdate(prevProps) {
 if (prevProps.wheelgift !== this.props.wheelgift) {
   if (prevProps.wheelgift !== null) {
     this.setState({
       // reds: prevProps.wheelgift.score
     })
   }
 }
 if (prevProps.userInfoData !== this.props.userInfoData) {
   if (this.props.userInfoData !== null) {
     this.setState({
       reds: this.props.userInfoData.reds,
     })
   }
 }
  }
  render() {
    const { classes } = this.props
    return (
      <div>
        <img
          id="Menu_Reds"
          src={IconReds}
          alt="IconReds"
          style={{ width: 26 }}
          onClick={this.handleOpen}
        />
        <Dialog
          fullScreen
          open={this.state.open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
          PaperProps={{
            classes: {
              root: classes.dialogPaper,
            },
            style: {
              backgroundColor: "transparent",
              boxShadow: "none",
              justifyContent: "center",
            },
          }}
        >
          <img className={classes.image} src={PopupResd} alt="" />
          <h2 className={classes.title}>VOUS AVEZ CUMULÉ</h2>
          <p className={classes.redsText}>{this.state.reds} &nbsp;STARS</p>
          <img
            src={CONFIRMERPOPUP}
            onClick={this.handleClose}
            alt="IconReds"
            className={classes.btn}
          />
        </Dialog>
      </div>
    )
  }
}

TransitionsPopper.propTypes = {
  classes: PropTypes.object.isRequired
};
const mapStateToProps = state => {
  return {
    wheelgift: state.Gift.wheelgift,
    userInfoData: state.userInfoData.userInfoData,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getGifts: () => dispatch(getGifts()),
    getUserInfo:()=>dispatch(getUserInfo())
  };
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(TransitionsPopper);
