/*
 * @Author: Tarek.Mdimagh ♥ ♥
 * @Date: 2020-09-21 14:50:02
 * @Last Modified by: Tarek.Mdimagh ♥ ♥ ♥
 * @Last Modified time: 2020-09-22 11:08:30
 */

import React, { Component, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { styles } from "./style";

import IconShop from "../../assets/img/shop_icon.png";
import { Link } from "react-router-dom";

import CitySelection from "../../pages/citySelection";
import Redxp from "../../assets/img/redxp.svg";
import { useLocation } from "react-router-dom";
const FooterComponent = (props) => {
  const { classes } = props;
  let location = useLocation();
  const [state, setstate] = useState("");
  //  const { classes } = props;

  useEffect(() => {
    setstate(location.pathname);
  }, []);

  return (
    <>
      {location && location.pathname && (
        <div
          style={{
            display:
              location.pathname === "/home" || location.pathname === "/"
                ? "flex"
                : "none",
            flexDirection: "column",
            justifyContent: "center",
            position: location.pathname === "/" ? "static" : "fixed",
            bottom: 0,
            width: "100%",
            zIndex: 1300,
          }}
        >
          <div className={classes.bottomDarkContainer}>
            <a href="https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/t%26c.pdf">
              <Typography component="h4" className={classes.bottomTypography}>
                TERME & CONDITIONS
              </Typography>
            </a>
            <a href="https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/Consumer+privacy+notice+FRv2.pdf">
              <Typography component="h4" className={classes.bottomTypography}>
                CONFIDENTIALITÉ
              </Typography>
            </a>
            {location.pathname === "/" && (
              <a href="https://discoveereprd.s3-eu-west-1.amazonaws.com/docs/health-effects-of-smoking_fr.pdf">
                <Typography component="h4" className={classes.bottomTypography}>
                  pmi & santè
                </Typography>
              </a>
            )}

            <Typography component="h4" className={classes.bottomTypography}>
              {/* <!-- OneTrust Cookies Settings button start --> */}

              <button
                id="ot-sdk-btn"
                class="ot-sdk-show-settings"
                style={{
                  background: "transparent",
                  border: "none",
                  padding: "0",
                  margin: "0",
                  color: "#fff",
                  fontSize: "10px",
                  fontFamily: "NeulandGroteskCondensedBold",
                  textTransform: "uppercase",
                }}
              >
                Paramètres des cookies
              </button>

              {/* <!-- OneTrust Cookies Settings button end --> */}
            </Typography>

            <Link to="/contact">
              <Typography component="h4" className={classes.bottomTypography}>
                CONTACT
              </Typography>
            </Link>
          </div>
          <p style={{ zIndex: 1300 }} className={["footer"].join(" ")}>
            avis important : fumer nuit à la santé
          </p>
        </div>
      )}
    </>
  );
};

export default withStyles(styles)(FooterComponent);
