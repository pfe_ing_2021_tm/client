export const styles = (theme) => ({
  bottomDarkContainer: {
    backgroundColor: "#111112",
    height: "10vh",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    paddingTop: 15,
    fontSize: 45,
    justifyContent: "space-around",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      justifyContent: "space-around",
      fontSize: 45,
    },
    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
      width: 386,
      margin: "0 0 0 0",
    },
  },
  bottomTypography: {
    marginRight: 5,
    marginLeft: 5,
    color: "white",
    // fontWeight: "heavy",
    fontFamily: "NeulandGroteskCondensedBold",
    fontSize: 10,
    textTransform: "uppercase",
    [theme.breakpoints.between("xs", "sm")]: {
      //Galaxi s3 jusqu'a ipod ( 0px => 600px)
      marginRight: 0,
      marginLeft: 0,
    },

    [theme.breakpoints.between("md", "xl")]: {
      //ipod jusqu'a xl screen ( 950px => 1920px)
    },
  },
});
